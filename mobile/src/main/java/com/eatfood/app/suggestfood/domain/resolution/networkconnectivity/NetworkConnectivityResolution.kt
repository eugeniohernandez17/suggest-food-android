package com.eatfood.app.suggestfood.domain.resolution.networkconnectivity

import com.eatfood.app.suggestfood.domain.presenter.GeneralPresenter

/**
 * Basic interface for the resolution of errors due to lack of connectivity to the network.
 *
 * Created by Usuario on 23/01/2017.
 */
interface NetworkConnectivityResolution {

    /**
     * Method to notify about the lack of connectivity to the network.
     *
     * @param requestCode the request code
     * @param presenter   the presenter
     * @see GeneralPresenter
     */
    fun onConnectivityUnavailable(requestCode: Int, presenter: GeneralPresenter<*>)

}
