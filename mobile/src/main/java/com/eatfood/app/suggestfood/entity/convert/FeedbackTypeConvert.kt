package com.eatfood.app.suggestfood.entity.convert

import com.eatfood.app.suggestfood.entity.Feedback
import io.objectbox.converter.PropertyConverter

/**
 * Created by Usuario on 24/03/2018.
 */
class FeedbackTypeConvert : PropertyConverter<Feedback.FeedbackType, String> {

    override fun convertToDatabaseValue(entityProperty: Feedback.FeedbackType?)
            = entityProperty?.name

    override fun convertToEntityProperty(databaseValue: String?): Feedback.FeedbackType?
            = databaseValue?.let { Feedback.FeedbackType.valueOf(databaseValue) }

}