package com.eatfood.app.suggestfood.features.capture_image.view

import android.graphics.Bitmap
import android.net.Uri
import android.support.v4.app.Fragment
import android.support.v7.app.AppCompatActivity

/**
 * Basic interface for display the options list to capture an image.
 *
 * Created by Usuario on 24/04/2018.
 */
interface CaptureImageView {

    /**
     * Callback for the result from requesting permissions.
     *
     * @see AppCompatActivity.onRequestPermissionsResult
     * @see Fragment.onRequestPermissionsResult
     */
    fun onPermissionsResult(requestCode: Int, permissions: Array<out String>, grantResults: IntArray)

    /**
     * Method for display the options list.
     * The [listener] is called if the image was successfully captured.
     *
     * @param listener the listener
     * @see OnCaptureResultListener
     */
    fun show(listener: OnCaptureResultListener)

    /**
     * This interface must be implemented by the class that called this view
     * to allow an interaction in this view to be communicated
     * when the operation is finished.
     */
    interface OnCaptureResultListener {

        /**
         * Method that is called when the user selects an image on the phone.
         *
         * @param imageUri the image's url in the file system of the phone
         * @param imageBitMap the image
         * @see Uri
         * @see Bitmap
         */
        fun onResult(imageUri: Uri, imageBitMap: Bitmap?): Unit?

    }

}