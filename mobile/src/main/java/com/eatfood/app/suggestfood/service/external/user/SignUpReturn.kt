package com.eatfood.app.suggestfood.service.external.user

import com.eatfood.app.suggestfood.entity.User
import com.eatfood.app.suggestfood.service.external.helper.ErrorHelper
import com.google.gson.annotations.SerializedName
import java.io.Serializable

/**
 * Created by Usuario on 06/09/2017.
 */

open class SignUpReturn private constructor(
        @field:SerializedName(value = "user", alternate = ["data"])
        val user: User
) {

    class Successful(user: User) : SignUpReturn(user), Serializable

    class Failure(user: User) : SignUpReturn(user), Serializable {

        private var errors: Map<String, List<String>>? = null

        override fun toString() = errors?.let {
            if (!it.isEmpty()) ErrorHelper.getError(it["full_messages"])
            else super.toString()
        } ?: super.toString()

    }
}
