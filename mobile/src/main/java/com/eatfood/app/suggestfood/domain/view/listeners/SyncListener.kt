package com.eatfood.app.suggestfood.domain.view.listeners

import android.accounts.Account

/**
 * Basic interface that includes the methods for data management through the sync process.
 *
 * Created by Usuario on 17/04/2018.
 */
interface SyncListener {

    /**
     * Method to add the synchronization process periodically to the user's account.
     *
     * @param account the user's account
     * @param syncFrequency the frequency of sync in minutes
     */
    fun addPeriodicSync(account: Account, syncFrequency: Long){}

    /**
     * Method to remove the synchronization process periodically to the user's account.
     *
     * @param account the user's account
     */
    fun removePeriodicSync(account: Account){}

}