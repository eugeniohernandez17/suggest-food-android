package com.eatfood.app.suggestfood.lib.image_loader

import android.widget.ImageView
import com.bumptech.glide.load.resource.drawable.GlideDrawable
import com.bumptech.glide.request.target.Target

/**
 * Basic interface for loading images in the view.
 *
 * Created by Usuario on 25/09/2017.
 */
interface ImageLoader {

    /**
     * Method for loading the uri param into the image view.
     *
     * @param imageView the image view
     * @param uri       the image uri
     * @see ImageView
     */
    fun load(imageView: ImageView, uri: String?): Target<GlideDrawable>?

}
