package com.eatfood.app.suggestfood.features.accounts.model

import com.eatfood.app.suggestfood.entity.AuthorizedUser

/**
 * Basic implementation of the [AccountsInteractor] interface for interactor in the Clean pattern.
 *
 * @property accountsRepository the accounts repository
 * @constructor Instantiates a new Accounts interactor.
 * @see AccountsRepository
 *
 * Created by Usuario on 04/04/2018.
 */
class AccountsInteractorImpl(private val accountsRepository: AccountsRepository) : AccountsInteractor {

    override fun find(email: String) = accountsRepository.find(email)

    override fun create(authorizedUser: AuthorizedUser?) = accountsRepository.create(authorizedUser)

}