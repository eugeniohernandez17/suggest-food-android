package com.eatfood.app.suggestfood.features.lost_password.model.callback

import com.eatfood.app.suggestfood.domain.event.MessageEvent
import com.eatfood.app.suggestfood.domain.model.callback.GeneralCallback
import com.eatfood.app.suggestfood.features.lost_password.LostPasswordEventType
import com.eatfood.app.suggestfood.lib.eventbus.EventBus
import com.eatfood.app.suggestfood.service.external.user.LostPasswordReturn
import com.eatfood.app.suggestfood.service.external.user.UserReturn
import com.google.gson.Gson

import okhttp3.Headers
import okhttp3.ResponseBody

/**
 * Callback for the different request events in the lost password process.
 *
 * @param eventBus the event bus
 * @param gson     the gson
 * @constructor Instantiates a new Lost password callback.
 * @see GeneralCallback
 * @see LostPasswordReturn.Successful
 * @see UserReturn.Failure
 * @see EventBus
 * @see Gson
 *
 * Created by Usuario on 17/09/2017.
 */
class LostPasswordCallback(eventBus: EventBus, gson: Gson) :
        GeneralCallback<LostPasswordReturn.Successful,
        UserReturn.Failure>(eventBus, gson, UserReturn.Failure::class.java) {

    override fun buildOnFailureMessage(): MessageEvent<*>
        = MessageEvent(LostPasswordEventType.ON_LOST_PASSWORD_ERROR)

    /*INTERFACES*/
    //1. GeneralCallback

    override fun onSuccessful(entity: LostPasswordReturn.Successful, headers: Headers) {
        val message = entity.message
        eventBus.post(MessageEvent(LostPasswordEventType.ON_LOST_PASSWORD_SUCCESS, message))
    }

    override fun onFailure(entity: UserReturn.Failure, httpCode: Int, responseBody: ResponseBody) {
        val messageEvent = buildOnFailureMessage()
        when (httpCode) {
            404 -> {
                onFailure(messageEvent, entity, httpCode, responseBody)
                eventBus.post(messageEvent)
            }
            else -> super.onFailure(entity, httpCode, responseBody)
        }
    }

    override fun onError(entity: UserReturn.Failure, throwable: Throwable)
        = eventBus.post(MessageEvent(LostPasswordEventType.ON_LOST_PASSWORD_ERROR, throwable))
}
