package com.eatfood.app.suggestfood.features.profile.edit.di

import com.eatfood.app.suggestfood.features.di.Feature
import com.eatfood.app.suggestfood.features.di.FeaturesComponent
import com.eatfood.app.suggestfood.features.profile.edit.view.ProfileActivity
import dagger.Component

/**
 * Injection component of the profile feature.
 *
 * @see ProfileModule
 * @see FeaturesComponent
 * @see ProfileActivity
 *
 * Created by Usuario on 28/09/2017.
 */
@Feature
@Component(modules = [(ProfileModule::class)], dependencies = [(FeaturesComponent::class)])
internal interface ProfileComponent {

    /**
     * Method for injecting dependencies.
     *
     * @param profileActivity the activity
     * @see ProfileActivity
     */
    fun inject(profileActivity: ProfileActivity)

}
