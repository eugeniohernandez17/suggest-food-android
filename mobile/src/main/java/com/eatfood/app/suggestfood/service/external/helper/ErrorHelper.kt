package com.eatfood.app.suggestfood.service.external.helper

/**
 * Class to analyze errors from the api services.
 *
 * Created by Usuario on 19/09/2017.
 */
object ErrorHelper {

    /**
     * Method to convert in a single line the list of errors.
     *
     * @param errors the errors list
     * @return the single line error
     */
    fun getError(errors: List<String>?): String {
        val builder = StringBuilder("")

        if (errors != null && !errors.isEmpty()) {
            errors.forEach {
                builder.append(it)
            }
        }

        return builder.toString()
    }
}
