package com.eatfood.app.suggestfood.domain.resolution

import android.content.Context
import android.support.v7.app.AlertDialog

/**
 * Basic abstract class for error resolution.
 * The class will present a [AlertDialog] popup with the actions that the user can perform.
 *
 * @property context the context
 * @constructor Instantiates a new General resolution.
 * @see AlertDialog
 *
 * Created by Usuario on 08/09/2017.
 */
abstract class GeneralResolution protected constructor(protected val context: Context) {

    /**
     * The alert dialog builder.
     *
     * @see AlertDialog.Builder
     * @see AlertDialog
     */
    protected val builder: AlertDialog.Builder = AlertDialog.Builder(context)

}
