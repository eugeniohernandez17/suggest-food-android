package com.eatfood.app.suggestfood.features.login.view

import com.eatfood.app.suggestfood.domain.view.fields.EmailValidationView
import com.eatfood.app.suggestfood.domain.view.fields.PasswordValidationView
import com.eatfood.app.suggestfood.domain.view.type.fields.FieldsView
import com.eatfood.app.suggestfood.entity.User

/**
 * Basic interface for the view in the MVP pattern.
 * It includes the methods to validate the fields of email and password.
 *
 * @see FieldsView
 * @see EmailValidationView
 * @see PasswordValidationView
 *
 * Created by Usuario on 07/01/2017.
 */
interface LoginView : FieldsView, EmailValidationView, PasswordValidationView {

    /**
     * Method to check if autologin should be done.
     *
     * @return true  - if the view have an extra user as a parameter for autologin
     *         false - if not have
     * @see [userParam]
     */
    val isAutoLogin: Boolean

    /**
     * Method to obtain the user object as a parameter for autologin.
     *
     * @return the user param
     * @see User
     */
    val userParam: User?

    /**
     * Method to navigate to the main screen.
     */
    fun navigateToMainScreen()

    /**
     * Method to load the user's contacts.
     */
    fun loadContacts()

    companion object {

        /**
         * The constant EXTRA_USER for identifying the user object between the view's parameters.
         */
        const val EXTRA_USER = "EXTRA_USER"
    }
}
