package com.eatfood.app.suggestfood.features.lost_password.di

import com.eatfood.app.suggestfood.features.di.Feature
import com.eatfood.app.suggestfood.features.di.FeaturesComponent
import com.eatfood.app.suggestfood.features.lost_password.view.LostPasswordActivity
import dagger.Component

/**
 * Injection component of the lost password feature.
 *
 * @see LostPasswordModule
 * @see FeaturesComponent
 * @see LostPasswordActivity
 *
 * Created by Usuario on 18/09/2017.
 */
@Feature
@Component(modules = [(LostPasswordModule::class)], dependencies = [(FeaturesComponent::class)])
internal interface LostPasswordComponent {

    /**
     * Method for injecting dependencies.
     *
     * @param lostPasswordActivity the activity
     * @see LostPasswordActivity
     */
    fun inject(lostPasswordActivity: LostPasswordActivity)

}
