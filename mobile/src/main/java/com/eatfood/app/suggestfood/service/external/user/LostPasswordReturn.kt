package com.eatfood.app.suggestfood.service.external.user

import com.eatfood.app.suggestfood.entity.User
import com.google.gson.annotations.SerializedName

import java.io.Serializable

/**
 * Created by Usuario on 17/09/2017.
 */

open class LostPasswordReturn private constructor(
        @field:SerializedName(value = "user", alternate = ["data"])
        val user: User
) {

    class Successful(user: User, val message: String) : LostPasswordReturn(user), Serializable
}
