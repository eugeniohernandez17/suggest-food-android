package com.eatfood.app.suggestfood.features.entity.user.model

import android.graphics.Bitmap
import android.net.Uri
import com.eatfood.app.suggestfood.entity.User

/**
 * Basic interface for the user repository in the Clean pattern.
 *
 * Created by Usuario on 24/09/2017.
 */
interface UserRepository {

    /**
     * Method for getting the user information using their id.
     *
     * @param user the user
     * @see User
     */
    fun getUser(user: User)

    /**
     * Method to update user information.
     *
     * @param user the user
     * @see User
     */
    fun updateUser(user: User)

    /**
     * Method to update the user's profile image.
     *
     * @param user the user
     * @param imageUri the image's url in the file system of the phone
     * @param imageBitMap the image
     * @see User
     * @see Uri
     * @see Bitmap
     */
    fun updateUser(user: User, imageUri: Uri, imageBitMap: Bitmap?)

}
