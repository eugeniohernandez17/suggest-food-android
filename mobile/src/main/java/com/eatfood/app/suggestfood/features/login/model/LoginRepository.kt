package com.eatfood.app.suggestfood.features.login.model

import com.eatfood.app.suggestfood.entity.User

/**
 * Basic interface for the login repository in the Clean pattern of the login feature.
 *
 * Created by Usuario on 07/01/2017.
 */
interface LoginRepository {

    /**
     * Method that performs the login process with calls to the services api.
     *
     * @param user the user
     * @see User
     */
    fun signIn(user: User)

}
