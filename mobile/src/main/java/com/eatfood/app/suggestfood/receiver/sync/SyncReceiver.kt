package com.eatfood.app.suggestfood.receiver.sync

import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import com.eatfood.app.suggestfood.domain.event.MessageEvent
import com.eatfood.app.suggestfood.features.intents.parameters.MessageEvent
import com.eatfood.app.suggestfood.lib.eventbus.EventBus
import com.eatfood.app.suggestfood.receiver.sync.di.DaggerSyncReceiverComponent
import com.eatfood.app.suggestfood.service.internal.sync.SyncService
import me.eugeniomarletti.extras.intent.IntentExtra
import javax.inject.Inject

/**
 * Base class for code that receives and handles broadcast intents sent by the sync process.
 *
 * @see BroadcastReceiver
 * @see SyncService
 *
 * Created by Usuario on 09/04/2018.
 */
class SyncReceiver : BroadcastReceiver() {

    @Inject
    lateinit var eventBus: EventBus

    companion object {
        /**
         * The constant ACTION for identifying the broadcast action your receiver subscribes to.
         */
        private const val ACTION = "com.eatfood.app.suggestfood.action.SYNC"

        /**
         * The constant EXTRA_MESSAGE for identifying the message sent by the sync process.
         */
        private const val EXTRA_MESSAGE = "EXTRA_MESSAGE"

        @JvmStatic
        fun sendBroadcast(context: Context, messageEvent: MessageEvent<*>){
            val intent = Intent(ACTION)
            with(IntentOptions){
                intent.message = messageEvent
            }
            context.sendBroadcast(intent)
        }
    }

    object IntentOptions {
        var Intent.message by IntentExtra.MessageEvent(EXTRA_MESSAGE)
    }

    override fun onReceive(context: Context, intent: Intent) {
        DaggerSyncReceiverComponent.builder().build().inject(this)
        with(IntentOptions){
            intent.message?.let { eventBus.post(it) }
        }
    }
}
