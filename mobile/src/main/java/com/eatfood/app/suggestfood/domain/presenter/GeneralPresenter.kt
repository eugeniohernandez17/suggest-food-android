package com.eatfood.app.suggestfood.domain.presenter

import android.app.Activity
import android.app.Fragment

import com.eatfood.app.suggestfood.domain.event.MessageEvent
import com.eatfood.app.suggestfood.domain.resolution.GeneralResolution
import com.eatfood.app.suggestfood.lib.eventbus.EventBus

/**
 * Basic interface for presenters in the MVP pattern.
 * Contains the basic events for the [Activity] and [Fragment] objects associated with it.
 * In addition, the method for receiving messages in the [EventBus] pattern is included.
 *
 * @param T event type for messages
 * @see Activity
 * @see Fragment
 * @see EventBus
 * @see MessageEvent
 *
 * Created by Usuario on 07/01/2017.
 */
interface GeneralPresenter<T : Any> {

    /**
     * On create event.
     */
    fun onCreate()

    /**
     * On destroy event.
     */
    fun onDestroy()

    /**
     * On message event.
     *
     * @param event the event
     * @see EventBus
     * @see T
     */
    fun onMessageEvent(event: MessageEvent<T>): Unit?

    /**
     * On resolution request.
     *
     * @param requestCode the request code for identifying the action callback
     * @see GeneralResolution
     */
    fun onResolution(requestCode: Int): Unit?

}
