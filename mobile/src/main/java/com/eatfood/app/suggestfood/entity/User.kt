package com.eatfood.app.suggestfood.entity

import io.objectbox.annotation.Entity
import io.objectbox.annotation.Id
import java.io.Serializable

/**
 * Business entity that represents a user of the application.
 *
 * @constructor Instantiates a new User.
 * @see Serializable
 * @see Credentials
 *
 * Created by Usuario on 13/01/2017.
 */

@Entity
data class User @JvmOverloads constructor(
        @Id(assignable = true)
        var id: Long = 0,

        override var email: String = "",
        override var password: String = "",

        var name: String? = null,
        var image: String? = null,
        var phone: String? = null,
        var notificationsUserId: String? = null
) : Serializable, Credentials {

    companion object {
        private const val serialVersionUID = 3835725563334924498L
    }

}
