package com.eatfood.app.suggestfood.config

import android.content.Context
import com.onesignal.OneSignal

/**
 * Class that contains the basic configuration of the [OneSignal] library.
 *
 * @see OneSignal
 *
 * Created by Usuario on 19/04/2018.
 */
object OneSignal {

    /**
     * Method that configures the OneSignal object with the basic configuration.
     */
    fun setup(context: Context){
        OneSignal.setLogLevel(OneSignal.LOG_LEVEL.DEBUG, OneSignal.LOG_LEVEL.NONE)
        OneSignal.startInit(context)
                .inFocusDisplaying(OneSignal.OSInFocusDisplayOption.Notification)
                .unsubscribeWhenNotificationsAreDisabled(true)
                .init()
    }

}