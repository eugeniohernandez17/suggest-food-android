package com.eatfood.app.suggestfood.features.feedback.di

import android.content.Context
import com.eatfood.app.suggestfood.content_provider.di.DatabaseModule
import com.eatfood.app.suggestfood.entity.Feedback
import com.eatfood.app.suggestfood.features.feedback.model.FeedbackInteractor
import com.eatfood.app.suggestfood.features.feedback.model.FeedbackInteractorImpl
import com.eatfood.app.suggestfood.features.feedback.model.FeedbackRepository
import com.eatfood.app.suggestfood.features.feedback.model.FeedbackRepositoryImpl
import com.eatfood.app.suggestfood.features.feedback.model.callback.FeedbackCallback
import com.eatfood.app.suggestfood.lib.eventbus.EventBus
import com.eatfood.app.suggestfood.service.external.feedback.FeedbacksService
import com.google.gson.Gson
import dagger.Module
import dagger.Provides
import io.objectbox.Box

/**
 * Injection dependency of the Feedbacks module.
 *
 * @property context the context
 * @constructor Instantiates a new Feedback module.
 * @see DatabaseModule
 * @see Module
 * @see Context
 *
 * Created by Usuario on 06/04/2018.
 */
@Module(includes = [(DatabaseModule::class)])
class FeedbackModule(val context: Context) {

    @Module
    companion object {

        /**
         * Provide feedback interactor.
         *
         * @param feedbackRepository the feedback repository
         * @return the feedback interactor
         * @see FeedbackRepository
         */
        @Provides
        @JvmStatic
        internal fun provideFeedbackInteractor(feedbackRepository: FeedbackRepository): FeedbackInteractor
                = FeedbackInteractorImpl(feedbackRepository)

        /**
         * Provide feedback repository.
         *
         * @param feedbackBox the feedback box
         * @param feedbacksService  the feedbacks service
         * @param feedbackCallback the feedback callback
         * @return the feedback repository
         * @see Box
         * @see FeedbacksService
         * @see FeedbackCallback
         */
        @Provides
        @JvmStatic
        internal fun provideFeedbackRepository(feedbackBox: Box<Feedback>,
                                               feedbacksService: FeedbacksService,
                                               feedbackCallback: FeedbackCallback): FeedbackRepository
                = FeedbackRepositoryImpl(feedbackBox, feedbacksService, feedbackCallback)

    }

    /**
     * Provide feedback callback feedback callback.
     *
     * @param eventBus the event bus pattern
     * @param gson     the gson object
     * @return the feedback callback
     * @see EventBus
     * @see Gson
     */
    @Provides
    internal fun provideFeedbackCallback(eventBus: EventBus, gson: Gson)
            = FeedbackCallback(eventBus, gson, context)

}