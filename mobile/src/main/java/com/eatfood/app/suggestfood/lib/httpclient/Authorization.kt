package com.eatfood.app.suggestfood.lib.httpclient

import okhttp3.Headers
import okhttp3.Request
import java.io.Serializable

/**
 * Class to identify the current session in the services api.
 * It contains the methods to update the request headers and
 * to update the session information at the time of the login.
 *
 * @param T the session type parameter
 *
 * Created by Usuario on 17/01/2017.
 */
interface Authorization<T : Serializable> {

    /**
     * Method for updating the headers of the requests.
     *
     * @param builder the request builder
     * @see Request.Builder
     */
    fun setAuthorizationHeader(builder: Request.Builder)

    /**
     * Method for updating the session information.
     *
     * @param headers the request headers
     * @param apply   the callback for update the unauthorized object
     * @see Headers
     */
    fun getAuthorizationHeader(headers: Headers, apply: (T) -> Unit)

}
