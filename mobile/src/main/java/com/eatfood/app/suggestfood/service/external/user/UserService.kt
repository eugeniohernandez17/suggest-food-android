package com.eatfood.app.suggestfood.service.external.user

import com.eatfood.app.suggestfood.entity.User
import retrofit2.Call
import retrofit2.http.*

/**
 * Api endpoints for user update.
 *
 * Created by Usuario on 13/01/2017.
 */
interface UserService {

    /**
     * End point to start session in the services api.
     *
     * @param user the user with valid credentials
     * @return the invocation to send the request
     * @see User
     * @see SignInReturn.Successful
     * @see Call
     */
    @POST("/api/v1/auth/sign_in.json")
    fun signIn(@Body user: User): Call<SignInReturn.Successful>

    /**
     * End point to registered a new user in the services api.
     *
     * @param user the new user
     * @return the invocation to send the request
     * @see User
     * @see SignUpReturn.Successful
     * @see Call
     */
    @POST("/api/v1/auth.json")
    fun signUp(@Body user: User): Call<SignUpReturn.Successful>

    /**
     * End point to send instruccions by email for recovery password.
     *
     * @param user the user information
     * @return the invocation to send the request
     * @see User
     * @see LostPasswordReturn.Successful
     * @see Call
     */
    @POST("/api/v1/auth/password.json")
    fun sendInstruccionsForRecoveryPassword(@Body user: User): Call<LostPasswordReturn.Successful>

    /**
     * End point for getting the user information.
     *
     * @param id the user id in the DB.
     * @return the invocation to send the request
     * @see User
     * @see UserReturn.Failure
     * @see Call
     */
    @GET("/api/v1/users/{user_id}.json")
    fun getUser(@Path("user_id") id: Long): Call<User>

    /**
     * End point for updating the user information.
     *
     * @param id   the user id in the DB.
     * @param user the user nformation
     * @return the invocation to send the request
     * @see UserReturn.Successful
     * @see UserReturn.Failure
     * @see Call
     */
    @PUT("/api/v1/users/{user_id}.json")
    fun updateUser(@Path("user_id") id: Long, @Body user: User): Call<UserReturn.Successful>

}
