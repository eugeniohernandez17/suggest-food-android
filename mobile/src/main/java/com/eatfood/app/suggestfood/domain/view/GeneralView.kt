package com.eatfood.app.suggestfood.domain.view

import android.support.annotation.StringRes
import com.eatfood.app.suggestfood.domain.presenter.GeneralPresenter
import com.eatfood.app.suggestfood.domain.resolution.GeneralResolution
import com.eatfood.app.suggestfood.domain.view.helper.ErrorHelper

/**
 * Basic interface for views in the MVP pattern.
 *
 * @see GeneralActivity
 *
 * Created by Usuario on 12/01/2017.
 */
interface GeneralView {

    /**
     * Method to check if the internet connection is enabled.
     *
     * @return true - if the internet connection is enabled
     */
    val isNetworkConnectivityAvailable: Boolean

    /**
     * Method to hide the phone keypad.
     */
    fun hideKeyboard()

    /**
     * Method to show the general messages of the application.
     *
     * @param idRes the id of the string resource
     */
    fun displayMessage(@StringRes idRes: Int)

    /**
     * Method to show the general messages of the application.
     *
     * @param message the message text
     */
    fun displayMessage(message: String)

    /**
     * Method to show the general errors of the application.
     *
     * @param error the error text
     * @see ErrorHelper
     */
    fun displayError(error: String)

    /**
     * Method to show the general errors of the application.
     *
     * @param error the error object
     * @param code the request code for resolution
     * @see GeneralResolution
     * @see Throwable
     */
    fun displayError(error: Throwable?, code: Int)

    /**
     * Callback to show non-connectivity error.
     *
     * @param code the request code for resolution
     * @param presenter the callback presenter
     * @see GeneralPresenter
     */
    fun onNetworkConnectivityDisabled(code: Int, presenter: GeneralPresenter<*>)
}
