package com.eatfood.app.suggestfood.service.internal.sync

/**
 * Enum object that represents the sync actions.
 *
 * Created by Usuario on 04/04/2018.
 */
enum class SyncType {
    ALL,
    FEEDBACKS
}