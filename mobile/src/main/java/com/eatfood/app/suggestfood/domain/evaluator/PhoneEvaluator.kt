package com.eatfood.app.suggestfood.domain.evaluator

import android.content.Context
import android.support.annotation.StringRes

import com.eatfood.app.suggestfood.R
import com.eatfood.app.suggestfood.domain.visitor.Evaluate

/**
 * Phone number evaluator based on the [MinMaxLengthEvaluator] class.
 * It contains the methods to know in detail if it is a valid phone number.
 *
 * @param context  the context
 * @param evaluate the evaluate object
 * @constructor Instantiates a new Phone evaluator.
 * @see MinMaxLengthEvaluator
 * @see Evaluate
 * @see Context
 *
 * Created by Usuario on 26/09/2017.
 */
internal class PhoneEvaluator(context: Context, evaluate: Evaluate)
    : MinMaxLengthEvaluator(context, evaluate, 6, 13) {

    @get:StringRes
    override val errorMinLength = R.string.general_error_invalid_phone_min

    @get:StringRes
    override val errorMaxLength = R.string.general_error_invalid_phone_max

    override fun validate(value: String?): Boolean {
        var result = super.validate(value)

        if (result && !this.evaluate.validPhoneNumber(value!!)) {
            this.errorId = R.string.general_error_invalid_phone
            result = false
        }

        return result
    }
}
