package com.eatfood.app.suggestfood.features.feedbacks.view.lists

import android.content.Context
import android.support.v7.widget.RecyclerView
import android.view.View
import android.widget.ImageView
import android.widget.TextView
import com.eatfood.app.suggestfood.R
import com.eatfood.app.suggestfood.domain.view.adapter.EntityRecyclerViewAdapter
import com.eatfood.app.suggestfood.entity.Feedback
import kotterknife.bindView
import java.util.*

/**
 * [EntityRecyclerViewAdapter] that can display a [Feedback] and makes a call to the
 * specified [FeedbackFragment.OnListFragmentInteractionListener].
 *
 * @param context  the context
 * @param items    the items
 * @param listener the listener
 * @constructor Instantiates a new Complaints recycler view adapter.
 * @see EntityRecyclerViewAdapter
 * @see Feedback
 * @see FeedbackFragment.OnListFragmentInteractionListener
 */
internal class FeedbackRecyclerViewAdapter private constructor(
        private val context: Context,
        items: MutableList<Feedback>,
        listener: FeedbackFragment.OnListFragmentInteractionListener
) : EntityRecyclerViewAdapter<Feedback, FeedbackRecyclerViewAdapter.ViewHolder, FeedbackFragment.OnListFragmentInteractionListener>(items, listener) {

    companion object {

        /**
         * Use this factory method to create a new instance of
         * this adapter using the provided parameters.
         *
         * @param context  the context
         * @param listener the listener
         * @return a new instance of adapter FeedbackRecyclerViewAdapter.
         */
        fun newInstance(context: Context, listener: FeedbackFragment.OnListFragmentInteractionListener)
                = FeedbackRecyclerViewAdapter(context, ArrayList(), listener)
    }

    override val layout = R.layout.fragment_feedback

    override fun getViewHolder(view: View) = ViewHolder(view)

    override fun onBindViewHolder(entity: Feedback, holder: ViewHolder, position: Int) {
        holder.mItem = entity
        holder.setId(context, entity.id)
        holder.txtName.text = entity.name
        holder.txtDescription.text = entity.description

        holder.imgShow.setOnClickListener {
            // Notify the active callbacks interface (the activity, if the
            // fragment is attached to one) that an item has been selected.
            holder.mItem?.let { mListener.onSelect(it) }
        }
    }

    /**
     * Basic implementation of the View Holder pattern using
     * the [RecyclerView.ViewHolder] class base.
     *
     * @see RecyclerView.ViewHolder
     */
    internal inner class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {

        private val txtId: TextView by bindView(R.id.txt_id)
        val txtName: TextView by bindView(R.id.txt_name)
        val txtDescription: TextView by bindView(R.id.txt_description)
        val imgShow: ImageView by bindView(R.id.img_show)

        /**
         * The Feedback item.
         */
        var mItem: Feedback? = null

        /**
         * Method to set the id of the feedback in the interface.
         */
        fun setId(context: Context, id: Long){
            txtId.text = context.getString(R.string.activity_feedbacks_format_id, id.toString())
        }
    }
}
