package com.eatfood.app.suggestfood.features.profile.show.di

import com.eatfood.app.suggestfood.domain.di.DomainComponent
import com.eatfood.app.suggestfood.features.di.Feature
import com.eatfood.app.suggestfood.features.profile.show.view.ShowProfileActivity
import dagger.Component

/**
 * Injection component of the profile feature.
 *
 * @see ProfileModule
 * @see DomainComponent
 * @see ShowProfileActivity
 *
 * Created by Usuario on 28/09/2017.
 */
@Feature
@Component(modules = [(ProfileModule::class)], dependencies = [(DomainComponent::class)])
interface ProfileComponent {

    /**
     * Method for injecting dependencies.
     *
     * @param showProfileActivity the activity
     * @see ShowProfileActivity
     */
    fun inject(showProfileActivity: ShowProfileActivity)

}
