package com.eatfood.app.suggestfood.domain.view.fields

/**
 * Methods to validate the password field in the UI.
 *
 * Created by Usuario on 06/09/2017.
 */
interface PasswordValidationView {

    /**
     * The password entered.
     */
    var password: String?

    /**
     * Method to show some error in the password field.
     *
     * @param error the error
     */
    fun showErrorInPassword(error: String?)
}
