package com.eatfood.app.suggestfood.features.feedbacks

import com.eatfood.app.suggestfood.lib.eventbus.EventBus

/**
 * General events used in the EventBus pattern to identify api responses in the feedbacks process.
 *
 * @see EventBus
 *
 * Created by Usuario on 10/10/2017.
 */
enum class FeedbacksEventType {

    /**
     * Event generated when the list of feedbacks has been retrieved correctly.
     */
    ON_LOAD_FEEDBACKS_SUCCESS,

    /**
     * Event generated when there is an error in the feedbacks recovery process.
     */
    ON_LOAD_FEEDBACKS_ERROR,

    /**
     * Event generated when the new feedback has been saved correctly.
     */
    ON_SAVE_FEEDBACKS_SUCCESS,

    /**
     * Event generated when there is an error in the feedback save process.
     */
    ON_SAVE_FEEDBACK_ERROR

}
