package com.eatfood.app.suggestfood.domain.view.fields

/**
 * Methods to validate the confirmation password field in the UI.
 *
 * Created by Usuario on 06/09/2017.
 */
interface ConfirmationPasswordValidationView {

    /**
     * The confirmation password entered.
     */
    var confirmationPassword: String?

    /**
     * Method to show some error in the confirmation password field.
     *
     * @param error the error
     */
    fun showErrorInConfirmationPassword(error: String?)
}
