package com.eatfood.app.suggestfood.features.settings.view

import android.annotation.TargetApi
import android.content.Context
import android.content.res.Configuration
import android.os.Build
import android.os.Bundle
import android.preference.PreferenceActivity
import android.preference.PreferenceFragment
import android.support.v4.app.NavUtils
import android.view.MenuItem
import com.eatfood.app.suggestfood.R
import com.eatfood.app.suggestfood.features.settings.view.fragments.DataSyncPreferenceFragment
import com.eatfood.app.suggestfood.features.settings.view.fragments.GeneralPreferenceFragment
import com.eatfood.app.suggestfood.features.settings.view.fragments.NotificationPreferenceFragment

/**
 * A [PreferenceActivity] that presents a set of application settings. On
 * handset devices, settings are presented as a single list. On tablets,
 * settings are split by category, with category headers shown to the left of
 * the list of settings.
 *
 * See [Android Design: Settings](http://developer.android.com/design/patterns/settings.html)
 * for design guidelines and the [Settings API Guide](http://developer.android.com/guide/topics/ui/settings.html)
 * for more information on developing a Settings UI.
 */
internal class SettingsActivity : AppCompatPreferenceActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setupActionBar()
    }

    override fun onMenuItemSelected(featureId: Int, item: MenuItem): Boolean {
        var result = super.onMenuItemSelected(featureId, item)
        when (item.itemId) {
            android.R.id.home -> {
                if (!result) {
                    NavUtils.navigateUpFromSameTask(this)
                }
                result = true
            }
        }
        return result
    }

    /**
     * {@inheritDoc}
     */
    override fun onIsMultiPane() = isXLargeTablet(this)

    /**
     * {@inheritDoc}
     */
    @TargetApi(Build.VERSION_CODES.HONEYCOMB)
    override fun onBuildHeaders(target: List<PreferenceActivity.Header>)
        = loadHeadersFromResource(R.xml.pref_headers, target)

    /**
     * This method stops fragment injection in malicious applications.
     * Make sure to deny any unknown fragments here.
     */
    override fun isValidFragment(fragmentName: String)
        = (PreferenceFragment::class.java.name == fragmentName
            || GeneralPreferenceFragment::class.java.name == fragmentName
            || NotificationPreferenceFragment::class.java.name == fragmentName
            || DataSyncPreferenceFragment::class.java.name == fragmentName)

    /*Own Methods*/

    /**
     * Set up the [android.app.ActionBar], if the API is available.
     */
    private fun setupActionBar() = supportActionBar?.setDisplayHomeAsUpEnabled(true)

    /**
     * Helper method to determine if the device has an extra-large screen. For
     * example, 10" tablets are extra-large.
     */
    private fun isXLargeTablet(context: Context): Boolean
        = context.resources.configuration.screenLayout and
            Configuration.SCREENLAYOUT_SIZE_MASK >= Configuration.SCREENLAYOUT_SIZE_XLARGE
}
