package com.eatfood.app.suggestfood.features.signup.presenter

import com.eatfood.app.suggestfood.domain.evaluator.EmailEvaluator
import com.eatfood.app.suggestfood.domain.evaluator.PasswordEvaluator
import com.eatfood.app.suggestfood.domain.evaluator.PasswordsMatchEvaluator
import com.eatfood.app.suggestfood.domain.event.MessageEvent
import com.eatfood.app.suggestfood.domain.presenter.FieldsPresenter
import com.eatfood.app.suggestfood.entity.User
import com.eatfood.app.suggestfood.features.signup.SignupEventType
import com.eatfood.app.suggestfood.features.signup.model.SignupInteractor
import com.eatfood.app.suggestfood.features.signup.view.SignupView
import com.eatfood.app.suggestfood.lib.eventbus.EventBus
import org.greenrobot.eventbus.Subscribe

/**
 * Basic implementation of the [SignupPresenter] interface
 * for the presenter in the MVP pattern.
 *
 * @param view                     the view
 * @param eventBus                 the event bus object
 * @property emailEvaluator           the email evaluator
 * @property passwordEvaluator        the password evaluator
 * @property passwordsMatchEvaluator  the passwords match evaluator
 * @property signupInteractor         the signup interactor
 * @constructor Instantiates a new Signup presenter.
 * @see SignupPresenter
 * @see SignupView
 * @see EventBus
 * @see EmailEvaluator
 * @see PasswordEvaluator
 * @see PasswordsMatchEvaluator
 * @see SignupInteractor
 *
 * Created by Usuario on 06/09/2017.
 */
internal class SignupPresenterImpl(
        view: SignupView,
        eventBus: EventBus,
        private val emailEvaluator: EmailEvaluator,
        private val passwordEvaluator: PasswordEvaluator,
        private val passwordsMatchEvaluator: PasswordsMatchEvaluator,
        private val signupInteractor: SignupInteractor
) : FieldsPresenter<SignupView, SignupEventType>(view, eventBus), SignupPresenter {

    override fun onError(event: MessageEvent<SignupEventType>, requestCode: Int) {
        super.onError(event, requestCode)
        view?.let {
            it.password = null
            it.confirmationPassword = null
        }
    }

    /*INTERFACES*/
    //1. SignupPresenter

    override fun validateSignUp() = view?.let {
        val email = it.email
        val password = it.password ?: ""
        val confirmationPassword = it.confirmationPassword ?: ""
        var valid = true

        it.hideKeyboard()

        if (!emailEvaluator.validate(email)) {
            valid = false
            it.showErrorInEmail(emailEvaluator.error)
        } else {
            it.showErrorInEmail(null)
        }

        if (!passwordEvaluator.validate(password)) {
            it.showErrorInPassword(passwordEvaluator.error)
        } else if (!passwordsMatchEvaluator.validate(arrayOf(password, confirmationPassword))) {
            it.showErrorInPassword(null)
            it.showErrorInConfirmationPassword(passwordsMatchEvaluator.error)
        } else {
            it.showErrorInPassword(null)
            it.showErrorInConfirmationPassword(null)

            if (valid) {
                doAction(SignupPresenter.SIGNUP_REQUEST_RESOLVED) {
                    signupInteractor.doSignUp(email, password)
                }
            }
        }
    }

    @Subscribe
    override fun onMessageEvent(event: MessageEvent<SignupEventType>) = when (event.type) {
        SignupEventType.ON_SIGN_UP_SUCCESS -> onSignUpSuccess(event.getData())
        SignupEventType.ON_SIGN_UP_ERROR -> onError(event, SignupPresenter.SIGNUP_REQUEST_RESOLVED)
    }

    override fun onResolution(requestCode: Int) = when (requestCode) {
        SignupPresenter.SIGNUP_REQUEST_RESOLVED -> validateSignUp()
        else -> {
        }
    }

    /*Own Methods*/

    private fun onSignUpSuccess(user: User) = view?.let {
        it.hideProgress()
        it.navigateToLoginScreen(user)
    }
}
