package com.eatfood.app.suggestfood.features.feedbacks.model.callback

import com.eatfood.app.suggestfood.domain.event.MessageEvent
import com.eatfood.app.suggestfood.domain.model.callback.GeneralCallback
import com.eatfood.app.suggestfood.features.feedbacks.FeedbacksEventType
import com.eatfood.app.suggestfood.lib.eventbus.EventBus
import com.eatfood.app.suggestfood.service.external.feedback.FeedbacksReturn
import com.google.gson.Gson
import okhttp3.Headers

/**
 * Callback for the different request events in the feedbacks loading process.
 *
 * @param eventBus   the event bus pattern
 * @param gson       the gson object
 * @constructor Instantiates a new Feedbacks callback.
 * @see GeneralCallback
 * @see FeedbacksReturn
 * @see EventBus
 * @see Gson
 *
 * Created by Usuario on 13/10/2017.
 */
class FeedbacksCallback(eventBus: EventBus, gson: Gson)
    : GeneralCallback<FeedbacksReturn, FeedbacksReturn>(eventBus, gson, FeedbacksReturn::class.java) {

    override fun onSuccessful(entity: FeedbacksReturn, headers: Headers)
        = eventBus.post(MessageEvent(FeedbacksEventType.ON_LOAD_FEEDBACKS_SUCCESS, entity))

    override fun onError(entity: FeedbacksReturn, throwable: Throwable)
        = eventBus.post(MessageEvent(FeedbacksEventType.ON_LOAD_FEEDBACKS_ERROR, throwable))

    override fun buildOnFailureMessage(): MessageEvent<*>
        = MessageEvent(FeedbacksEventType.ON_LOAD_FEEDBACKS_ERROR)

}