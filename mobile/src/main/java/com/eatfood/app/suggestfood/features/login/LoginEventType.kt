package com.eatfood.app.suggestfood.features.login

import com.eatfood.app.suggestfood.lib.eventbus.EventBus

/**
 * General events used in the EventBus pattern to identify api responses in the login process.
 *
 * @see EventBus
 *
 * Created by Usuario on 31/08/2017.
 */
enum class LoginEventType {

    /**
     * Event when the session was successfully created.
     */
    ON_SIGN_IN_SUCCESS,

    /**
     * Event generated when there is an error in the login process.
     */
    ON_SIGN_IN_ERROR

}
