package com.eatfood.app.suggestfood.features.session.di

import com.eatfood.app.suggestfood.content_provider.di.DatabaseModule
import com.eatfood.app.suggestfood.entity.AuthorizedUser
import com.eatfood.app.suggestfood.entity.User
import com.eatfood.app.suggestfood.features.session.model.SessionInteractor
import com.eatfood.app.suggestfood.features.session.model.SessionInteractorImpl
import com.eatfood.app.suggestfood.features.session.model.SessionRepository
import com.eatfood.app.suggestfood.features.session.model.SessionRepositoryImpl
import dagger.Module
import dagger.Provides
import io.objectbox.Box

/**
 * Injection dependency of the Session module.
 *
 * @see DatabaseModule
 * @see Module
 *
 * Created by Usuario on 07/01/2017.
 */
@Module(includes = [(DatabaseModule::class)])
object SessionModule {

    /**
     * Provide session interactor.
     *
     * @param sessionRepository the session repository
     * @return the session interactor
     * @see SessionRepository
     */
    @Provides
    @JvmStatic
    fun provideSessionInteractor(sessionRepository: SessionRepository): SessionInteractor
            = SessionInteractorImpl(sessionRepository)

    /**
     * Provide session repository.
     *
     * @param userBox the user box
     * @param authorizedUserBox the authorized user box
     * @return the session repository
     * @see Box
     */
    @Provides
    @JvmStatic
    fun provideSessionRepository(userBox: Box<User>,
                                 authorizedUserBox: Box<AuthorizedUser>): SessionRepository
            = SessionRepositoryImpl(userBox, authorizedUserBox)

}