package com.eatfood.app.suggestfood.features.login.di

import com.eatfood.app.suggestfood.domain.evaluator.EmailEvaluator
import com.eatfood.app.suggestfood.domain.evaluator.PasswordEvaluator
import com.eatfood.app.suggestfood.domain.model.callback.Callback
import com.eatfood.app.suggestfood.features.accounts.di.AccountsModule
import com.eatfood.app.suggestfood.features.accounts.model.AccountsInteractor
import com.eatfood.app.suggestfood.features.login.model.LoginInteractor
import com.eatfood.app.suggestfood.features.login.model.LoginInteractorImpl
import com.eatfood.app.suggestfood.features.login.model.LoginRepository
import com.eatfood.app.suggestfood.features.login.model.LoginRepositoryImpl
import com.eatfood.app.suggestfood.features.login.model.callback.LoginCallback
import com.eatfood.app.suggestfood.features.login.presenter.LoginPresenter
import com.eatfood.app.suggestfood.features.login.presenter.LoginPresenterImpl
import com.eatfood.app.suggestfood.features.login.view.LoginView
import com.eatfood.app.suggestfood.features.notifications.di.NotificationsModule
import com.eatfood.app.suggestfood.features.notifications.model.NotificationsInteractor
import com.eatfood.app.suggestfood.features.session.model.SessionInteractor
import com.eatfood.app.suggestfood.lib.eventbus.EventBus
import com.eatfood.app.suggestfood.lib.httpclient.Authorization
import com.eatfood.app.suggestfood.service.external.user.SignInReturn
import com.eatfood.app.suggestfood.service.external.user.UserService
import com.google.gson.Gson
import dagger.Module
import dagger.Provides
import javax.inject.Named

/**
 * Injection dependency of the Login module.
 *
 * @property view the view
 * @property authorization the authorization method for the requests
 * @constructor Instantiates a new Login module.
 * @see LoginComponent
 * @see AccountsModule
 * @see NotificationsModule
 * @see Module
 * @see LoginView
 * @see Authorization
 *
 * Created by Usuario on 12/01/2017.
 */
@Module(includes = [(AccountsModule::class), (NotificationsModule::class)])
class LoginModule(private val view: LoginView, private val authorization: Authorization<*>) {

    /**
     * Provide login presenter.
     *
     * @param eventBus                the event bus pattern
     * @param emailEvaluator          the email evaluator
     * @param passwordEvaluator       the password evaluator
     * @param loginInteractor         the login interactor
     * @param sessionInteractor       the session interactor
     * @param accountsInteractor      the accounts interactor
     * @param notificationsInteractor the notifications interactor
     * @return the login presenter
     * @see EventBus
     * @see EmailEvaluator
     * @see PasswordEvaluator
     * @see LoginInteractor
     * @see SessionInteractor
     * @see AccountsInteractor
     * @see NotificationsInteractor
     */
    @Provides
    internal fun provideLoginPresenter(eventBus: EventBus,
                                       emailEvaluator: EmailEvaluator,
                                       passwordEvaluator: PasswordEvaluator,
                                       loginInteractor: LoginInteractor,
                                       sessionInteractor: SessionInteractor,
                                       accountsInteractor: AccountsInteractor,
                                       notificationsInteractor: NotificationsInteractor): LoginPresenter
        = LoginPresenterImpl(view, eventBus, emailEvaluator, passwordEvaluator,
            loginInteractor, sessionInteractor, accountsInteractor, notificationsInteractor)

    /**
     * Provide login interactor.
     *
     * @param loginRepository the login repository
     * @return the login interactor
     * @see LoginRepository
     */
    @Provides
    fun provideLoginInteractor(loginRepository: LoginRepository): LoginInteractor
        = LoginInteractorImpl(loginRepository)

    /**
     * Provide login repository.
     *
     * @param userService  the user service
     * @param userCallback the user callback
     * @return the login repository
     * @see UserService
     * @see Callback
     */
    @Provides
    fun provideLoginRepository(userService: UserService,
                               @Named("LoginCallback")
                               userCallback: Callback<SignInReturn.Successful, SignInReturn.Failure>): LoginRepository
        = LoginRepositoryImpl(userService, userCallback)

    /**
     * Provide login callback.
     *
     * @param eventBus      the event bus pattern
     * @param gson          the gson object
     * @return the callback
     * @see EventBus
     * @see Gson
     */
    @Provides
    @Named("LoginCallback")
    internal fun provideLoginCallback(eventBus: EventBus,
                                      gson: Gson) : Callback<SignInReturn.Successful, SignInReturn.Failure>
        = LoginCallback(eventBus, gson, authorization)
}
