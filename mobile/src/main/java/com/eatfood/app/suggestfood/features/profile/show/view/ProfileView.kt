package com.eatfood.app.suggestfood.features.profile.show.view

import com.eatfood.app.suggestfood.domain.view.type.progress.ProgressView
import com.eatfood.app.suggestfood.entity.User

/**
 * Basic interface for the view in the MVP pattern.
 *
 * @see ProgressView
 * @see com.eatfood.app.suggestfood.features.profile.general.ProfileView
 *
 * Created by Usuario on 18/10/2017.
 */
interface ProfileView : ProgressView, com.eatfood.app.suggestfood.features.profile.general.ProfileView {

    /**
     * Method to navigate to the user dashboard screen.
     *
     * @param user the user information
     * @see User
     */
    fun navigateToDashboardScreen(user: User)

    /**
     * Method to navigate to the edit profile screen.
     *
     * @param user the user information
     * @see User
     */
    fun navigateToEditProfileScreen(user: User)

}
