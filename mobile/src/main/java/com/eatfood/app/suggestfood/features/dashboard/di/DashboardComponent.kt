package com.eatfood.app.suggestfood.features.dashboard.di

import com.eatfood.app.suggestfood.features.dashboard.view.MainActivity
import com.eatfood.app.suggestfood.features.di.Feature
import com.eatfood.app.suggestfood.features.di.FeaturesComponent
import dagger.Component

/**
 * Injection component of the dashboard feature.
 *
 * @see DashboardModule
 * @see FeaturesComponent
 * @see MainActivity
 *
 * Created by Usuario on 07/01/2017.
 */
@Feature
@Component(modules = [(DashboardModule::class)], dependencies = [(FeaturesComponent::class)])
interface DashboardComponent {

    /**
     * Method for injecting dependencies.
     *
     * @param mainActivity the activity
     * @see MainActivity
     */
    fun inject(mainActivity: MainActivity)

}
