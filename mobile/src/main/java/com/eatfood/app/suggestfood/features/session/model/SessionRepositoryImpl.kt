package com.eatfood.app.suggestfood.features.session.model

import com.eatfood.app.suggestfood.entity.AuthorizedUser
import com.eatfood.app.suggestfood.entity.User
import io.objectbox.Box

/**
 * Basic implementation of the [SessionRepository] for the repository in the Clean pattern.
 *
 * @property userBox the user box
 * @property authorizedUserBox the authorized user box
 * @constructor Instantiates a new Session repository.
 * @see SessionRepository
 * @see Box
 *
 * Created by Usuario on 04/09/2017.
 */
class SessionRepositoryImpl(
        private val userBox: Box<User>,
        private val authorizedUserBox: Box<AuthorizedUser>
) : SessionRepository {

    override val isValidSession: Boolean
        get() {
            val authorizedUser = session
            return authorizedUser != null && authorizedUser.isValid
        }

    override var session: AuthorizedUser?
        get() = authorizedUserBox.query().build().findFirst()
        set(authorizedUser) {
            if (authorizedUser != null) {
                authorizedUserBox.put(authorizedUser)
                userBox.put(authorizedUser.user.target)
            }
        }

    override fun updateUserInfo(user: User) {
        session?.user?.target = user
    }

    override fun invalidateSession() {
        session?.let {
            authorizedUserBox.remove(it)
        }
    }

}
