package com.eatfood.app.suggestfood.domain.view

import android.os.Bundle
import android.support.annotation.LayoutRes
import butterknife.ButterKnife
import com.eatfood.app.suggestfood.domain.presenter.GeneralPresenter
import com.eatfood.app.suggestfood.domain.resolution.http.HttpResolution
import com.eatfood.app.suggestfood.domain.resolution.networkconnectivity.NetworkConnectivityResolution
import javax.inject.Inject

/**
 * Basic class for application activity objects
 * that implement the [GeneralView] interface of the MVP pattern.
 *
 * @param P the presenter type
 * @see AbsctractActivity
 *
 * @see GeneralView
 *
 * Created by Usuario on 20/01/2017.
 */
abstract class GeneralActivity<P : GeneralPresenter<*>> : AbsctractActivity(), GeneralView {

    /**
     * The Presenter object.
     */
    @Inject
    lateinit var presenter: P

    /**
     * The Network connectivity resolution.
     */
    @Inject
    lateinit var networkConnectivityResolution: NetworkConnectivityResolution

    /**
     * The Http resolution.
     */
    @Inject
    lateinit var httpResolution: HttpResolution

    override val isNetworkConnectivityAvailable: Boolean
        get() = super.isNetworkConnectivityAvailable

    override fun onDestroy() {
        presenter.onDestroy()
        super.onDestroy()
    }

    /**INTERFACE */
    //1. GeneralView

    override fun hideKeyboard() = super.hideKeyboard()

    override fun displayMessage(idRes: Int) = displayMessage(getString(idRes))

    override fun displayError(error: String) = displayMessage(error)

    override fun displayError(error: Throwable?, code: Int)
        = httpResolution.onException(error, code, presenter)

    override fun onNetworkConnectivityDisabled(code: Int, presenter: GeneralPresenter<*>)
        = networkConnectivityResolution.onConnectivityUnavailable(code, presenter)

    /*ABSTRACT MEHTODS*/

    /**
     * Method that implements the dependency injection configuration.
     */
    protected abstract fun setupInjection()

    /*Own Methods*/

    protected open fun onCreate(@LayoutRes layoutResID: Int, savedInstanceState: Bundle?) {
        setContentView(layoutResID)
        ButterKnife.bind(this)
        config()
        presenter.onCreate()
    }

    /**
     * General view configuration.
     */
    protected open fun config() = setupInjection()
}
