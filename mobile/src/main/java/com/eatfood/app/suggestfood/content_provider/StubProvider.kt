package com.eatfood.app.suggestfood.content_provider

import android.content.ContentProvider
import android.content.ContentValues
import android.net.Uri

/**
 * Define an implementation of [ContentProvider] that stubs out all methods.
 *
 * @see ContentProvider
 *
 * Created by Usuario on 27/03/2018.
 */
class StubProvider : ContentProvider() {
    override fun insert(p0: Uri?, p1: ContentValues?) = null

    override fun query(p0: Uri?, p1: Array<out String>?, p2: String?, p3: Array<out String>?, p4: String?) = null

    override fun onCreate() = true

    override fun update(p0: Uri?, p1: ContentValues?, p2: String?, p3: Array<out String>?) = 0

    override fun delete(p0: Uri?, p1: String?, p2: Array<out String>?) = 0

    override fun getType(p0: Uri?) = null
}