package com.eatfood.app.suggestfood.features.signup.model.callback

import com.eatfood.app.suggestfood.domain.event.MessageEvent
import com.eatfood.app.suggestfood.domain.model.callback.GeneralCallback
import com.eatfood.app.suggestfood.features.signup.SignupEventType
import com.eatfood.app.suggestfood.lib.eventbus.EventBus
import com.eatfood.app.suggestfood.service.external.user.SignUpReturn
import com.google.gson.Gson

import okhttp3.Headers

/**
 * Callback for the different request events in the sign up process.
 *
 * @param eventBus the event bus pattern
 * @param gson     the gson object
 * @constructor Instantiates a new Signup callback.
 * @see GeneralCallback
 * @see SignUpReturn.Successful
 * @see SignUpReturn.Failure
 * @see EventBus
 * @see Gson
 *
 * Created by Usuario on 06/09/2017.
 */
class SignupCallback(eventBus: EventBus, gson: Gson)
    : GeneralCallback<SignUpReturn.Successful, SignUpReturn.Failure>(eventBus, gson, SignUpReturn.Failure::class.java) {

    override fun buildOnFailureMessage(): MessageEvent<*>
        = MessageEvent(SignupEventType.ON_SIGN_UP_ERROR)

    /*INTERFACES*/
    //1. GeneralCallback

    override fun onSuccessful(entity: SignUpReturn.Successful, headers: Headers)
        = eventBus.post(MessageEvent(SignupEventType.ON_SIGN_UP_SUCCESS, entity.user))

    override fun onError(entity: SignUpReturn.Failure, throwable: Throwable)
        = eventBus.post(MessageEvent(SignupEventType.ON_SIGN_UP_ERROR, throwable))
}
