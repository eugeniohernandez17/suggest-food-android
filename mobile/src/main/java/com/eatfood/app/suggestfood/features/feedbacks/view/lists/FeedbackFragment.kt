package com.eatfood.app.suggestfood.features.feedbacks.view.lists

import android.content.Context
import android.support.annotation.DrawableRes
import android.support.annotation.StringRes
import android.support.v7.widget.RecyclerView
import android.view.View
import butterknife.OnClick
import com.eatfood.app.suggestfood.R
import com.eatfood.app.suggestfood.domain.view.fragment.PaginableListFragment
import com.eatfood.app.suggestfood.domain.view.helper.IconHelper
import com.eatfood.app.suggestfood.entity.Feedback
import kotlinx.android.synthetic.main.content_no_feedbacks.*

/**
 * A fragment representing a list of Items.
 *
 * Activities containing this fragment MUST implement
 * the [FeedbackFragment.OnListFragmentInteractionListener] interface.
 *
 * Mandatory empty constructor for the fragment manager to instantiate the
 * fragment (e.g. upon screen orientation changes).
 */
internal class FeedbackFragment : PaginableListFragment<Feedback, FeedbackFragment.OnListFragmentInteractionListener>() {

    private lateinit var iconHelper: IconHelper

    override val layout = R.layout.fragment_feedback_list

    override fun onAttach(context: Context?) {
        super.onAttach(context)
        if (context is OnListFragmentInteractionListener) {
            mListener = context
        } else {
            throw RuntimeException(context?.toString() + " must implement OnListFragmentInteractionListener")
        }
    }

    override fun onCreateView(view: View) {
        super.onCreateView(view)
        context?.let { iconHelper = IconHelper(it) }
    }

    override fun configRecyclerView(recyclerView: RecyclerView) = mListener?.let {
        recyclerViewAdapter = FeedbackRecyclerViewAdapter.newInstance(context!!, it)
        recyclerView.adapter = recyclerViewAdapter
    }

    /*EVENTS*/

    /**
     * Method to add a new feedback.
     */
    @OnClick(R.id.fab_add_feedback)
    fun onAddFeedback() {
        mListener?.onAddFeedback()
    }

    /*Own Methods*/

    /**
     * Method to indicate that there are no feedbacks to load.
     *
     * @param title the message text id
     * @param icon  the icon id
     */
    fun displayNoFeedbacks(@StringRes title: Int, @DrawableRes icon: Int){
        txtTitle.text = getString(title)
        imgIcon.setImageDrawable(iconHelper.getDrawable(icon))
        txtTitle.visibility = View.VISIBLE
        imgIcon.visibility = View.VISIBLE
    }

    /**
     * Method inverse to [displayNoFeedbacks].
     */
    fun hideNoFeedbacks() = try {
        txtTitle.visibility = View.INVISIBLE
        imgIcon.visibility = View.INVISIBLE
    } catch (e: IllegalStateException){
        e.printStackTrace()
    }

    /**
     * @see PaginableListFragment.OnListFragmentInteractionListener
     */
    interface OnListFragmentInteractionListener
        : PaginableListFragment.OnListFragmentInteractionListener<Feedback> {

        /**
         * Method to add a new feedback.
         */
        fun onAddFeedback(): Unit?

    }
}
