package com.eatfood.app.suggestfood.features.dashboard.di

import com.eatfood.app.suggestfood.domain.model.callback.GeneralCallback
import com.eatfood.app.suggestfood.features.accounts.di.AccountsModule
import com.eatfood.app.suggestfood.features.accounts.model.AccountsInteractor
import com.eatfood.app.suggestfood.features.dashboard.model.callback.UserCallback
import com.eatfood.app.suggestfood.features.dashboard.presenter.DashboardPresenter
import com.eatfood.app.suggestfood.features.dashboard.presenter.DashboardPresenterImpl
import com.eatfood.app.suggestfood.features.dashboard.view.DashboardView
import com.eatfood.app.suggestfood.features.entity.di.EntityModule
import com.eatfood.app.suggestfood.features.entity.user.model.UserInteractor
import com.eatfood.app.suggestfood.features.session.model.SessionInteractor
import com.eatfood.app.suggestfood.features.settings.di.SettingsModule
import com.eatfood.app.suggestfood.features.settings.model.SettingsInteractor
import com.eatfood.app.suggestfood.lib.eventbus.EventBus
import com.eatfood.app.suggestfood.service.external.user.UserReturn
import com.google.gson.Gson
import dagger.Module
import dagger.Provides
import java.io.Serializable
import javax.inject.Named

/**
 * Injection dependency of the Dashboard module.
 *
 * @property dashboardView the dashboard view
 * @constructor Instantiates a new Dashboard module.
 * @see DashboardComponent
 * @see EntityModule
 * @see SettingsModule
 * @see AccountsModule
 * @see Module
 * @see DashboardView
 *
 * Created by Usuario on 07/01/2017.
 */
@Module(includes = [(EntityModule::class), (SettingsModule::class), (AccountsModule::class)])
class DashboardModule(private val dashboardView: DashboardView) {

    /**
     * Provide dashboard presenter.
     *
     * @param eventBus           the event bus pattern
     * @param sessionInteractor  the session interactor
     * @param userInteractor     the user interactor
     * @param settingsInteractor the settings interactor
     * @param accountsInteractor the accounts interactor
     * @return the dashboard presenter
     * @see EventBus
     * @see SessionInteractor
     * @see UserInteractor
     * @see SettingsInteractor
     * @see AccountsInteractor
     */
    @Provides
    internal fun provideDashboardPresenter(eventBus: EventBus,
                                           sessionInteractor: SessionInteractor,
                                           userInteractor: UserInteractor,
                                           settingsInteractor: SettingsInteractor,
                                           accountsInteractor: AccountsInteractor): DashboardPresenter
        = DashboardPresenterImpl(dashboardView, eventBus, sessionInteractor, userInteractor,
            settingsInteractor, accountsInteractor)

    /**
     * Provide the user callback for the Dashboard module.
     *
     * @param eventBus the event bus pattern
     * @param gson     the gson object
     * @return the user callback
     */
    @Provides
    @Named("User_Callback")
    @Suppress("unchecked_cast")
    internal fun provideUserCallback(eventBus: EventBus, gson: Gson)
        = UserCallback(eventBus, gson) as GeneralCallback<Serializable, UserReturn.Failure>
}
