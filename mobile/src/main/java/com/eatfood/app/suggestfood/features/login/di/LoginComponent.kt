package com.eatfood.app.suggestfood.features.login.di

import com.eatfood.app.suggestfood.features.di.Feature
import com.eatfood.app.suggestfood.features.di.FeaturesComponent
import com.eatfood.app.suggestfood.features.login.view.LoginActivity
import dagger.Component

/**
 * Injection component of the login feature.
 *
 * @see LoginModule
 * @see FeaturesComponent
 * @see LoginActivity
 *
 * Created by Usuario on 12/01/2017.
 */
@Feature
@Component(modules = [(LoginModule::class)], dependencies = [(FeaturesComponent::class)])
internal interface LoginComponent {

    /**
     * Method for injecting dependencies.
     *
     * @param loginActivity the activity
     * @see LoginActivity
     */
    fun inject(loginActivity: LoginActivity)

}
