package com.eatfood.app.suggestfood.features.profile.edit

import com.eatfood.app.suggestfood.lib.eventbus.EventBus

/**
 * General events used in the EventBus pattern to identify api responses
 * in the profile updating process.
 *
 * @see EventBus
 *
 * Created by Usuario on 25/09/2017.
 */
enum class ProfileEventType {

    /**
     * Event when the user profile was successfully updated.
     */
    ON_UPDATE_PROFILE_SUCCESS,

    /**
     * Event generated when there is an error in the profile updating process.
     */
    ON_UPDATE_PROFILE_ERROR

}
