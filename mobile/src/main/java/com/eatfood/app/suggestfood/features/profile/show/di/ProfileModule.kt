package com.eatfood.app.suggestfood.features.profile.show.di

import com.eatfood.app.suggestfood.features.profile.general.ProfilePresenterHelper
import com.eatfood.app.suggestfood.features.profile.show.presenter.ProfilePresenter
import com.eatfood.app.suggestfood.features.profile.show.presenter.ProfilePresenterImpl
import com.eatfood.app.suggestfood.features.profile.show.view.ProfileView
import com.eatfood.app.suggestfood.lib.eventbus.EventBus
import dagger.Module
import dagger.Provides

/**
 * Injection dependency of the Profile module.
 *
 * @property view the view
 * @constructor Instantiates a new Profile module.
 * @see ProfileComponent
 * @see Module
 * @see ProfileView
 *
 * Created by Usuario on 28/09/2017.
 */
@Module
class ProfileModule(private val view: ProfileView) {

    /**
     * Provide profile presenter profile presenter.
     *
     * @param eventBus          the event bus pattern
     * @return the profile presenter
     * @see EventBus
     * @see ProfilePresenterHelper
     */
    @Provides
    internal fun provideProfilePresenter(eventBus: EventBus,
                                         profileHelper: ProfilePresenterHelper): ProfilePresenter
        = ProfilePresenterImpl(view, eventBus, profileHelper)

    /**
     * Provide profile presenter helper.
     *
     * @return the profile presenter helper
     */
    @Provides
    internal fun provideProfilePresenterHelper() = ProfilePresenterHelper(view)

}
