package com.eatfood.app.suggestfood.lib.gson

import com.google.gson.annotations.Expose

/**
 * Class to exclude fields in deserialization
 *
 * @see ExclusionStrategy
 *
 * Created by Usuario on 17/10/2017.
 */
internal object DeserializationExclusionStrategy : ExclusionStrategy() {

    override fun shouldSkipField(expose: Expose): Boolean = !expose.deserialize

}
