package com.eatfood.app.suggestfood.domain.view.helper

import android.graphics.Typeface
import android.widget.TextView
import com.eatfood.app.suggestfood.config.Font

/**
 * Helper for the custom fonts.
 *
 * @see Font
 *
 * Created by Usuario on 07/09/2017.
 */
object FontHelper {

    /**
     * Set custom font to a textview.
     *
     * @param T  the textview type
     * @param view the view
     * @param font the custom font
     * @see TextView
     * @see Font
     */
    fun <T : TextView> setFont(view: T, font: Font) {
        val typeface = Typeface.createFromAsset(view.context.assets, "fonts/$font")
        view.typeface = typeface
    }

}
