package com.eatfood.app.suggestfood.features.accounts.di

import android.accounts.AccountManager
import com.eatfood.app.suggestfood.features.accounts.model.AccountsInteractor
import com.eatfood.app.suggestfood.features.accounts.model.AccountsInteractorImpl
import com.eatfood.app.suggestfood.features.accounts.model.AccountsRepository
import com.eatfood.app.suggestfood.features.accounts.model.AccountsRepositoryImpl
import dagger.Module
import dagger.Provides

/**
 * Injection dependency of the Accounts module.
 *
 * @see Module
 *
 * Created by Usuario on 04/04/2018.
 */
@Module
object AccountsModule {

    /**
     * Provide accounts interactor.
     *
     * @param accountsRepository the accounts repository
     * @return the accounts interactor
     * @see AccountsRepository
     */
    @Provides
    @JvmStatic
    fun provideAccountsInteractor(accountsRepository: AccountsRepository): AccountsInteractor
            = AccountsInteractorImpl(accountsRepository)

    /**
     * Provide accounts repository.
     *
     * @param accountManager the account manager
     * @return the accounts repository
     * @see AccountManager
     */
    @Provides
    @JvmStatic
    fun provideAccountsRepository(accountManager: AccountManager): AccountsRepository
            = AccountsRepositoryImpl(accountManager)

}