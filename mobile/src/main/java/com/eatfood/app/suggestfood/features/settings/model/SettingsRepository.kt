package com.eatfood.app.suggestfood.features.settings.model

/**
 * Basic interface for the session repository in the Clean pattern.
 *
 * Created by Usuario on 14/04/2018.
 */
interface SettingsRepository {

    /**
     * Method to obtain the frequency of sync in minutes.
     *
     * @return the frequency of sync in minutes
     */
    val syncFrequency: Long

    /**
     * Method to delete the current user settings.
     */
    fun clearSettings()

}