package com.eatfood.app.suggestfood.features.notifications.di

import com.eatfood.app.suggestfood.features.notifications.model.NotificationsInteractor
import com.eatfood.app.suggestfood.features.notifications.model.NotificationsInteractorImpl
import com.eatfood.app.suggestfood.features.notifications.model.NotificationsRepository
import com.eatfood.app.suggestfood.features.notifications.model.NotificationsRepositoryImpl
import dagger.Module
import dagger.Provides

/**
 * Injection dependency of the notifications module.
 *
 * @see Module
 *
 * Created by Usuario on 19/04/2018.
 */
@Module
object NotificationsModule {

    /**
     * Provide notifications interactor.
     *
     * @param notificationsRepository the notifications repository
     * @return the notifications interactor
     * @see NotificationsRepository
     */
    @Provides
    @JvmStatic
    fun provideNotificationsInteractor(notificationsRepository: NotificationsRepository): NotificationsInteractor
            = NotificationsInteractorImpl(notificationsRepository)

    /**
     * Provide notifications repository.
     *
     * @return the notifications repository
     * @see NotificationsRepository
     */
    @Provides
    @JvmStatic
    fun provideNotificationsRepository(): NotificationsRepository
            = NotificationsRepositoryImpl()

}