package com.eatfood.app.suggestfood.features.authorization.di

import com.eatfood.app.suggestfood.features.di.Feature
import com.eatfood.app.suggestfood.lib.httpclient.Authorization
import dagger.Component

/**
 * Injection component of the authorization feature.
 *
 * @see AuthorizationModule
 *
 * Created by Usuario on 22/04/2018.
 */
@Feature
@Component(modules = [(AuthorizationModule::class)])
interface AuthorizationComponent {

    /**
     * Provide the object for the authorization of the api request.
     *
     * @see Authorization
     */
    fun provideAuthorization(): Authorization<*>

}