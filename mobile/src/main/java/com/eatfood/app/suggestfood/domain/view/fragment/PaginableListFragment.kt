package com.eatfood.app.suggestfood.domain.view.fragment

import android.app.Activity
import android.support.v4.app.Fragment
import android.view.View
import com.eatfood.app.suggestfood.domain.view.fragment.PaginableListFragment.OnListFragmentInteractionListener
import com.srx.widget.PullCallback
import com.srx.widget.PullToLoadView
import java.io.Serializable

/**
 * A fragment representing a list of Items of [E] type, paged through [pullToLoadView].
 * Contains the [OnListFragmentInteractionListener] interface to interact with the context.
 *
 * @param E the entity type parameter
 * @param L the listener type parameter
 * @see EntityListFragment
 * @see PullToLoadView
 *
 * Created by Usuario on 09/10/2017.
 */
internal abstract class PaginableListFragment<E : Serializable,
                                              L : PaginableListFragment.OnListFragmentInteractionListener<E>>
    : EntityListFragment<E, L>() {

    private lateinit var pullToLoadView: PullToLoadView

    override fun onCreateView(view: View) {
        val  list = view.findViewById<View>(android.R.id.list)
        if (list is PullToLoadView) {
            pullToLoadView = list
            pullToLoadView.isLoadMoreEnabled(true)
            pullToLoadView.setPullCallback(mListener)
            configRecyclerView(view, pullToLoadView.recyclerView)
            mListener?.initLoad()
        }
    }

    /*Own Methods*/

    /**
     * Function to start the automatic progress in the view.
     */
    fun initLoad() = pullToLoadView.initLoad()

    /**
     * Function to finish the automatic progress in the view.
     */
    fun setComplete() = pullToLoadView.setComplete()

    /**
     * This interface must be implemented by activities that contain this
     * [Fragment] to allow an interaction in this fragment to be communicated
     * to the [Activity] and potentially other fragments contained in that
     * activity.
     *
     * @param E the entity type parameter
     * @see EntityListFragment.OnListFragmentInteractionListener
     * @see PullCallback
     */
    internal interface OnListFragmentInteractionListener<E : Serializable>
        : EntityListFragment.OnListFragmentInteractionListener<E>, PullCallback {

        /**
         * Method that starts the loading of the paginable list.
         */
        fun initLoad()
    }
}
