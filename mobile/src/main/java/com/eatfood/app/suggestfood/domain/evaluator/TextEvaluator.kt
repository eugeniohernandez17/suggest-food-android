package com.eatfood.app.suggestfood.domain.evaluator

import android.content.Context

import com.eatfood.app.suggestfood.domain.visitor.Evaluate

/**
 * Character evaluator based on the [FieldEvaluator] interface.
 *
 * @param context  the context
 * @param evaluate the evaluate object
 * @constructor Instantiates a new Text evaluator.
 * @see FieldEvaluator
 * @see Evaluate
 * @see Context
 *
 * Created by Usuario on 30/08/2017.
 */
internal open class TextEvaluator(context: Context, evaluate: Evaluate)
    : FieldEvaluator<String>(context, evaluate) {

    override fun validate(value: String?) = !this.evaluate.isEmpty(value)
}
