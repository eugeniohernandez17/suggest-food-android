package com.eatfood.app.suggestfood.domain.visitor

import com.eatfood.app.suggestfood.domain.Patterns
import com.eatfood.app.suggestfood.domain.evaluate.FieldEvaluate

/**
 * Basic interface of the patron Visitor.
 * Contains statements to evaluate the different fields of the models or the interfaces.
 *
 * @see FieldEvaluate
 *
 * Created by Usuario on 12/01/2017.
 */
interface Evaluate {

    /**
     * Evaluates whether the text string is empty.
     *
     * @param value the text
     * @return true - if the text is blank or empty
     */
    fun isEmpty(value: String?): Boolean

    /**
     * Method that indicates if the text string is a valid email.
     *
     * @param email the email
     * @return true - if a valid email
     * @see Patterns.EMAIL_ADDRESS
     */
    fun validEmail(email: String): Boolean

    /**
     * Method that evaluates if the text string does not exceed
     * the limit indicated as parameter.
     *
     * @param text  the text
     * @param limit the limit
     * @return true - if the text does not exceed the indicated limit
     */
    fun validMaxLength(text: String, limit: Int): Boolean

    /**
     * Method that evaluates if the text string matches
     * the minimum size indicated as a parameter.
     *
     * @param text  the text
     * @param limit the limit
     * @return true - if the text meets the minimum size indicated
     */
    fun validMinLength(text: String, limit: Int): Boolean

    /**
     * Method that evaluates if the text strings matches.
     *
     * @param text1      the first text
     * @param text2      the second text
     * @param ignoreCase true - lowercase and capital letters will not be taken into account
     *                   false - the texts must be exactly the same
     * @return true - if the texts are equals
     */
    fun match(text1: String, text2: String, ignoreCase: Boolean): Boolean

    /**
     * Method that indicates if the text string is a valid phone number.
     *
     * @param phone the phone number
     * @return true - if a valid phone number
     */
    fun validPhoneNumber(phone: String): Boolean
}
