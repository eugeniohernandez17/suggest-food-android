package com.eatfood.app.suggestfood.domain.resolution.http

import android.content.Context
import android.content.DialogInterface

import com.eatfood.app.suggestfood.R
import com.eatfood.app.suggestfood.domain.error.HttpException
import com.eatfood.app.suggestfood.domain.presenter.GeneralPresenter
import com.eatfood.app.suggestfood.domain.resolution.GeneralResolution
import com.eatfood.app.suggestfood.domain.view.helper.IconHelper

import java.net.ConnectException
import java.net.SocketTimeoutException
import java.net.UnknownHostException

import javax.net.ssl.SSLPeerUnverifiedException

/**
 * Basic implementation of the [HttpResolution] interface.
 * It presents an [DialogInterface] with the actions that the user can perform.
 *
 * @param context the context
 * @property iconHelper the icon helper
 * @constructor Instantiates a new Http resolution.
 * @see HttpResolution
 * @see GeneralResolution
 * @see DialogInterface
 * @see Context
 * @see IconHelper
 *
 * Created by Usuario on 12/09/2017.
 */
class HttpResolutionImpl(context: Context) : GeneralResolution(context), HttpResolution {

    private var iconHelper: IconHelper = IconHelper(context)

    override fun onException(error: Throwable?, code: Int, presenter: GeneralPresenter<*>) {
        when(error){
            is UnknownHostException, is SSLPeerUnverifiedException, is ConnectException -> {
                onHostNotFound()
            }
            is SocketTimeoutException -> onRequestTimeOut(code, presenter)
            is HttpException -> when (error.code) {
                500 -> onInternalServerError(code, presenter)
                503 -> onServiceUnavailable()
                404 -> onNotFound()
                else -> { }
            }
        }
    }

    /*Own Methods*/

    private fun basicConfig() {
        builder.setIcon(iconHelper.getDrawable(R.drawable.ic_error_outline_black_24dp))
        builder.setTitle(R.string.alert)
        builder.setNeutralButton(android.R.string.ok, null)
    }

    private fun onHostNotFound() {
        basicConfig()
        builder.setMessage(context.getString(R.string.network_error_host_not_found))
        builder.show()
    }

    private fun onRequestTimeOut(code: Int, presenter: GeneralPresenter<*>) {
        builder.setIcon(iconHelper.getDrawable(R.drawable.ic_history_black_24dp))
        builder.setTitle(R.string.time_out)
        builder.setMessage(context.getString(R.string.network_error_time_out))
        builder.setNegativeButton(R.string.cancel, null)
        builder.setPositiveButton(R.string.retry) { _, _ -> presenter.onResolution(code) }
        builder.show()
    }

    private fun onInternalServerError(code: Int, presenter: GeneralPresenter<*>) {
        builder.setIcon(iconHelper.getDrawable(R.drawable.ic_error_outline_black_24dp))
        builder.setTitle(R.string.warning)
        builder.setMessage(context.getString(R.string.http_error_internal_server))
        builder.setNegativeButton(R.string.cancel, null)
        builder.setPositiveButton(R.string.retry) { _, _ -> presenter.onResolution(code) }
        builder.show()
    }

    private fun onServiceUnavailable() {
        basicConfig()
        builder.setIcon(iconHelper.getDrawable(R.drawable.ic_report_black_24dp))
        builder.setMessage(context.getString(R.string.http_error_server_in_overload_or_maintenance))
        builder.show()
    }

    private fun onNotFound() {
        basicConfig()
        builder.setMessage(context.getString(R.string.http_error_not_found))
        builder.show()
    }

}
