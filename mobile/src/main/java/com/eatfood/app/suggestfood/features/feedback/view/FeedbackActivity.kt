package com.eatfood.app.suggestfood.features.feedback.view

import android.app.Activity
import com.eatfood.app.suggestfood.BuildConfig
import com.eatfood.app.suggestfood.R
import com.eatfood.app.suggestfood.entity.Feedback
import com.eatfood.app.suggestfood.features.feedback.view.FeedbackActivity.OnInteractionListener
import org.rm3l.maoni.Maoni
import org.rm3l.maoni.common.contract.Listener

/**
 * A feedback screen that offers to add or show an only feedback.
 *
 * @property activity the activity that calls the interface
 * @property listener the callback listener
 * @see Activity
 * @see OnInteractionListener
 * @see FeedbackView
 * @see Listener
 *
 * Created by Usuario on 16/03/2018.
 */
class FeedbackActivity(
        private val activity: Activity,
        private val listener: OnInteractionListener
) : FeedbackView, Listener {

    private var feedbackAction: FeedbackAction = FeedbackAction.SHOW

    lateinit var type: Feedback.FeedbackType

    //1. FeedbackView

    override fun displayNewFeedbackScreen(type: Feedback.FeedbackType) {
        feedbackAction = FeedbackAction.ADD
        this.type = type
        displayView()
    }

    //2. Listener

    override fun onSendButtonClicked(feedback: org.rm3l.maoni.common.model.Feedback): Boolean {
        val result = Feedback(name = "New", description = feedback.userComment.toString(), type = type)
        listener.onResult(feedbackAction, result)
        return true
    }

    override fun onDismiss() = Unit

    /*Own Methods*/

    private fun displayView(){
        Maoni.Builder(activity, "${BuildConfig.APPLICATION_ID}.fileprovider")
                .withTheme(R.style.AppTheme_Basic)
                .withHeader(R.color.colorPrimary)
                .withWindowTitle(activity.getString(R.string.activity_feedback_title))
                .withWindowSubTitle(null)
                .withListener(this)
                .disableCapturingFeature()
                .disableLogsCapturingFeature()
                .hideKeyboardOnStart()
                .build().start(activity)
    }

    /**
     * Actions to apply on feedback.
     */
    enum class FeedbackAction {
        ADD, SHOW
    }

    /**
     * The callback listener.
     */
    interface OnInteractionListener {

        /**
         * Response method after the action has been applied.
         *
         * @param feedbackAction action applied to feedback
         * @param feedback the feedback
         * @see [onSendButtonClicked]
         * @see FeedbackAction
         * @see Feedback
         */
        fun onResult(feedbackAction: FeedbackAction, feedback: Feedback)

    }
}