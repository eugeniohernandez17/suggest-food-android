package com.eatfood.app.suggestfood.service.external.feedback

import com.eatfood.app.suggestfood.entity.Feedback
import com.eatfood.app.suggestfood.service.external.helper.ErrorHelper
import java.io.Serializable

/**
 * Created by Usuario on 24/03/2018.
 */
open class FeedbackReturn private constructor(val feedback: Feedback, var remote: Boolean){

    class Successful(feedback: Feedback, remote: Boolean) : FeedbackReturn(feedback, remote), Serializable

    class Failure(feedback: Feedback, remote: Boolean) : FeedbackReturn(feedback, remote), Serializable {

        var errors: List<String>? = null

        override fun toString(): String = ErrorHelper.getError(errors)

    }
}