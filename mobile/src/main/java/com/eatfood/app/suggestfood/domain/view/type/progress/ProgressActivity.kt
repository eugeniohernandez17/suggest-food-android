package com.eatfood.app.suggestfood.domain.view.type.progress

import com.eatfood.app.suggestfood.domain.presenter.GeneralPresenter
import com.eatfood.app.suggestfood.domain.view.GeneralActivity
import com.eatfood.app.suggestfood.domain.view.helper.LoadProgressHelper

import javax.inject.Inject

/**
 * Activity with support for the show the progress of a task in background.
 *
 * @param P the presenter type
 * @see GeneralActivity
 * @see ProgressView
 *
 * Created by Usuario on 22/09/2017.
 */
abstract class ProgressActivity<P : GeneralPresenter<*>> : GeneralActivity<P>(), ProgressView {

    @Inject
    lateinit var loadProgressHelper: LoadProgressHelper

    override fun config() {
        super.config()
        configLoadProgress()
    }

    /*INTERFACES*/
    //1. ProgressView

    override fun showProgress() = loadProgressHelper.show()

    override fun hideProgress() = loadProgressHelper.hide()

    /*ABSTRACT METHODS*/

    /**
     * Config load progress.
     */
    protected abstract fun configLoadProgress()
}
