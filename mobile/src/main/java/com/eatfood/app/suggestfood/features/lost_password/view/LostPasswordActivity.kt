package com.eatfood.app.suggestfood.features.lost_password.view

import android.os.Bundle
import android.view.inputmethod.EditorInfo
import butterknife.OnClick
import butterknife.OnEditorAction
import com.eatfood.app.suggestfood.R
import com.eatfood.app.suggestfood.config.Font
import com.eatfood.app.suggestfood.content_provider.di.DatabaseModule
import com.eatfood.app.suggestfood.domain.di.DaggerDomainComponent
import com.eatfood.app.suggestfood.domain.di.DomainModule
import com.eatfood.app.suggestfood.domain.resolution.di.ResolutionModule
import com.eatfood.app.suggestfood.domain.view.helper.ErrorHelper
import com.eatfood.app.suggestfood.domain.view.helper.FontHelper
import com.eatfood.app.suggestfood.domain.view.helper.IconHelper
import com.eatfood.app.suggestfood.domain.view.helper.MessageHelper
import com.eatfood.app.suggestfood.domain.view.type.fields.FormActivity
import com.eatfood.app.suggestfood.features.authorization.di.DaggerAuthorizationComponent
import com.eatfood.app.suggestfood.features.di.DaggerFeaturesComponent
import com.eatfood.app.suggestfood.features.lost_password.di.DaggerLostPasswordComponent
import com.eatfood.app.suggestfood.features.lost_password.di.LostPasswordModule
import com.eatfood.app.suggestfood.features.lost_password.presenter.LostPasswordPresenter
import com.eatfood.app.suggestfood.lib.di.LibsModule
import kotlinx.android.synthetic.main.activity_lost_password.*
import kotlinx.android.synthetic.main.content_header_app.*

/**
 * A screen for recover the account password by using the user email.
 *
 * @see LostPasswordView
 * @see FormActivity
 * @see LostPasswordPresenter
 *
 * Created by Usuario on 18/09/2017.
 */
internal class LostPasswordActivity : FormActivity<LostPasswordPresenter>(), LostPasswordView {

    /*INTERFACES*/
    //1. LostPasswordView

    override val email: String
        get() = txtEmail.text.toString()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        super.onCreate(R.layout.activity_lost_password, savedInstanceState)
    }

    override fun config() {
        super.config()
        configureFonts()
        configureIcons()
    }

    override fun setupInjection() {
        val databaseModule = DatabaseModule(this)
        val authorization = DaggerAuthorizationComponent.builder()
                .databaseModule(databaseModule)
                .build().provideAuthorization()
        val domainComponent = DaggerDomainComponent.builder()
                .libsModule(LibsModule(this, authorization))
                .resolutionModule(ResolutionModule(this))
                .domainModule(DomainModule(this))
                .build()
        val featuresComponent = DaggerFeaturesComponent.builder()
                .domainComponent(domainComponent)
                .databaseModule(databaseModule)
                .build()
        DaggerLostPasswordComponent.builder()
                .featuresComponent(featuresComponent)
                .lostPasswordModule(LostPasswordModule(this))
                .build().inject(this)
    }

    override fun displayMessage(message: String) = MessageHelper.display(lostPasswordForm, message)

    override fun setInputs(enabled: Boolean) = super.setInputs(enabled, txtEmail, btnLostPassword)

    override fun configLoadProgress()
        = loadProgressHelper.config(null, getString(R.string.activity_lost_password_dialog_message))

    /*EVENTS*/

    @OnEditorAction(R.id.txtEmail)
    fun onEditorEmail(id: Int): Boolean {
        if (id == EditorInfo.IME_ACTION_GO || id == EditorInfo.IME_NULL) {
            presenter.validLostPassword()
            return true
        }
        return false
    }

    @OnClick(R.id.btnLostPassword)
    fun onClickLostPassword() {
        presenter.validLostPassword()
    }

    @OnClick(R.id.btnSignIn)
    fun onClickSignIn() = finish()

    override fun showErrorInEmail(error: String?) = ErrorHelper.display(txtEmail, error)

    /*Own Methods*/

    private fun configureFonts() = FontHelper.setFont(txtTitle, Font.TRAJAN_BOLD)

    private fun configureIcons() {
        iconHelper.setIcon(IconHelper.DrawablePosition.LEFT, R.drawable.ic_mail_outline_black_24dp, txtEmail)
        iconHelper.setIcon(IconHelper.DrawablePosition.LEFT, R.drawable.ic_perm_identity_red_24dp, btnSignIn)
    }
}
