package com.eatfood.app.suggestfood.features.signup.model

import com.eatfood.app.suggestfood.entity.User

/**
 * Basic interface for the signup repository in the Clean pattern of the sign up feature.
 *
 * Created by Usuario on 06/09/2017.
 */
interface SignupRepository {

    /**
     * Method that performs the sign up process with calls to the services api.
     *
     * @param user the user
     * @see User
     */
    fun signup(user: User)

}
