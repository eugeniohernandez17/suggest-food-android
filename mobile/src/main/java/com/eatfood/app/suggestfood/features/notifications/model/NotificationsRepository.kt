package com.eatfood.app.suggestfood.features.notifications.model

import com.eatfood.app.suggestfood.entity.AuthorizedUser

/**
 * Basic interface for the notifications repository in the Clean pattern of the notifications feature.
 *
 * Created by Usuario on 19/04/2018.
 */
interface NotificationsRepository {

    /**
     * Method to set the [authorizedUser] to the notifications server.
     *
     * @see AuthorizedUser
     */
    var authorizedUser: AuthorizedUser

}