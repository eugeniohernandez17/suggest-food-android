package com.eatfood.app.suggestfood.domain.evaluator

import android.content.Context
import android.support.annotation.StringRes

import com.eatfood.app.suggestfood.R
import com.eatfood.app.suggestfood.domain.visitor.Evaluate
import com.eatfood.app.suggestfood.domain.visitor.Evaluator

/**
 * Generic class for field evaluation.
 * The [T] parameter indicates to which type the field to be evaluated belongs.
 *
 * Example:
 * ```
 * FieldEvaluator<String> evaluator = ...
 * evaluator.validate("Text"); //true
 * ```
 *
 * @param T the type field parameter
 * @property context  the context
 * @property evaluate the evaluate object
 * @constructor Instantiates a new Field evaluator.
 * @see Evaluator
 * @see Evaluate
 * @see Context
 *
 * Created by Usuario on 12/01/2017.
 */
internal abstract class FieldEvaluator<T>(
        protected val context: Context,
        protected val evaluate: Evaluate
) : Evaluator<T> {

    /**
     * The error id of the resource string
     */
    @get:StringRes
    var errorId = R.string.general_error_field_required

    override val error: String
        get() = context.getString(errorId)
}
