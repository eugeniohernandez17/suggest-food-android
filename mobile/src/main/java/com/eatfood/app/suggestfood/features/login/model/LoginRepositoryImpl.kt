package com.eatfood.app.suggestfood.features.login.model

import com.eatfood.app.suggestfood.domain.model.callback.Callback
import com.eatfood.app.suggestfood.entity.User
import com.eatfood.app.suggestfood.service.external.user.SignInReturn
import com.eatfood.app.suggestfood.service.external.user.UserService
import com.onesignal.OneSignal

import retrofit2.Call
import retrofit2.Response

/**
 * Basic implementation of the [LoginRepository] for the repository in the Clean pattern.
 *
 * @property userService  the user service
 * @property userCallback the user callback
 * @constructor Instantiates a new Login repository.
 * @see LoginRepository
 * @see UserService
 * @see Callback
 * @see SignInReturn
 *
 * Created by Usuario on 12/01/2017.
 */
class LoginRepositoryImpl(
        private val userService: UserService,
        private val userCallback: Callback<SignInReturn.Successful, SignInReturn.Failure>
) : LoginRepository {

    override fun signIn(user: User) {
        OneSignal.idsAvailable {
            userId, _ ->
            user.notificationsUserId = userId
            userService.signIn(user).enqueue(object : retrofit2.Callback<SignInReturn.Successful> {

                override fun onResponse(call: Call<SignInReturn.Successful>,
                                        response: Response<SignInReturn.Successful>) =
                        if (response.isSuccessful) {
                            userCallback.onSuccessful(response.body()!!, response.headers())
                        } else {
                            userCallback.onFailure(SignInReturn.Failure(user),
                                    response.code(), response.errorBody()!!)
                        }

                override fun onFailure(call: Call<SignInReturn.Successful>, throwable: Throwable) {
                    userCallback.onError(SignInReturn.Failure(user), throwable)
                }
            })
        }
    }
}
