package com.eatfood.app.suggestfood.domain.view.fragment

import android.app.Activity
import android.support.v4.app.Fragment
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.view.View
import com.eatfood.app.suggestfood.domain.view.adapter.EntityRecyclerViewAdapter
import com.eatfood.app.suggestfood.domain.view.fragment.EntityListFragment.OnListFragmentInteractionListener
import java.io.Serializable

/**
 * A fragment representing a list of Items of [E] type.
 * Contains the [OnListFragmentInteractionListener] interface to interact with the context.
 * It also has the [EntityRecyclerViewAdapter] object to display each of the items in the list,
 * it must be instantiated in the subclasses.
 *
 * @param E the entity type parameter
 * @param L the listener type parameter
 * @see Fragment
 * @see RecyclerView
 *
 * Created by Usuario on 08/10/2017.
 */
internal abstract class EntityListFragment<E : Serializable,
                                           L : EntityListFragment.OnListFragmentInteractionListener<E>>
    : GeneralFragment() {

    /**
     * The listener.
     *
     * @see OnListFragmentInteractionListener
     */
    protected var mListener: L? = null

    /**
     * The Recycler view adapter.
     *
     * @see RecyclerView
     */
    protected lateinit var recyclerViewAdapter: EntityRecyclerViewAdapter<E, out RecyclerView.ViewHolder, L>

    override fun onDetach() {
        super.onDetach()
        mListener = null
    }

    /**
     * Event generated when creating the view and that will allow to configure the object
     * that will contain the list of items.
     *
     * @param view the list of items
     * @see configRecyclerView
     */
    override fun onCreateView(view: View) {
        if (view is RecyclerView) {
            configRecyclerView(view, view)
        }
    }

    /*ABSTRACT METHODS*/

    /**
     * Method to configure the eecycler view object.
     * It can add animations and listeners.
     *
     * @param recyclerView the recycler view
     * @see RecyclerView
     */
    protected abstract fun configRecyclerView(recyclerView: RecyclerView): Unit?

    /*Own Methods*/

    /**
     * Method to load the list of items in the adapter.
     *
     * @param items the item list
     * @see E
     * @see EntityRecyclerViewAdapter.addAll
     */
    fun loadData(items: List<E>) = recyclerViewAdapter.addAll(items)

    /**
     * Method to delete the list of items in the adapter.
     *
     * @see EntityRecyclerViewAdapter.clear
     */
    fun clearData() = recyclerViewAdapter.clear()

    /**
     * Method to configure the object that will contain the list of items.
     *
     * @param view         the view
     * @param recyclerView the recycler view
     * @see View
     * @see RecyclerView
     */
    protected fun configRecyclerView(view: View, recyclerView: RecyclerView) {
        recyclerView.layoutManager = LinearLayoutManager(view.context)
        configRecyclerView(recyclerView)
    }

    /**
     * This interface must be implemented by activities that contain this
     * [Fragment] to allow an interaction in this fragment to be communicated
     * to the [Activity] and potentially other fragments contained in that
     * activity.
     *
     * @param E the entity type parameter
     * @see EntityRecyclerViewAdapter.OnListInteractionListener
     */
    internal interface OnListFragmentInteractionListener<E : Serializable>
        : EntityRecyclerViewAdapter.OnListInteractionListener<E>
}
