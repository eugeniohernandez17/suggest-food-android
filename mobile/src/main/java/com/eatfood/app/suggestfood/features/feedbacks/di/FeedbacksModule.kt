package com.eatfood.app.suggestfood.features.feedbacks.di

import com.eatfood.app.suggestfood.features.accounts.di.AccountsModule
import com.eatfood.app.suggestfood.features.accounts.model.AccountsInteractor
import com.eatfood.app.suggestfood.features.feedback.di.FeedbackModule
import com.eatfood.app.suggestfood.features.feedback.model.FeedbackInteractor
import com.eatfood.app.suggestfood.features.feedbacks.model.FeedbacksInteractor
import com.eatfood.app.suggestfood.features.feedbacks.model.FeedbacksInteractorImpl
import com.eatfood.app.suggestfood.features.feedbacks.model.FeedbacksRepository
import com.eatfood.app.suggestfood.features.feedbacks.model.FeedbacksRepositoryImpl
import com.eatfood.app.suggestfood.features.feedbacks.model.callback.FeedbacksCallback
import com.eatfood.app.suggestfood.features.feedbacks.presenter.FeedbacksPresenter
import com.eatfood.app.suggestfood.features.feedbacks.presenter.FeedbacksPresenterImpl
import com.eatfood.app.suggestfood.features.feedbacks.view.FeedbacksView
import com.eatfood.app.suggestfood.features.session.model.SessionInteractor
import com.eatfood.app.suggestfood.lib.eventbus.EventBus
import com.eatfood.app.suggestfood.service.external.feedback.FeedbacksService
import com.google.gson.Gson
import dagger.Module
import dagger.Provides

/**
 * Injection dependency of the Login module.
 *
 * @property view the view
 * @constructor Instantiates a new Feedbacks module.
 * @see FeedbacksComponent
 * @see AccountsModule
 * @see FeedbackModule
 * @see Module
 * @see FeedbacksView
 *
 * Created by Usuario on 08/10/2017.
 */
@Module(includes = [(AccountsModule::class), (FeedbackModule::class)])
class FeedbacksModule(private val view: FeedbacksView) {

    /**
     * Provide feedbacks presenter.
     *
     * @param eventBus            the event bus pattern
     * @param feedbacksInteractor the feedbacks interactor
     * @param feedbackInteractor  the feedback interactor
     * @param sessionInteractor   the session interactor
     * @param accountsInteractor  the accounts interactor
     * @return the feedbacks presenter
     * @see EventBus
     * @see FeedbacksInteractor
     * @see FeedbackInteractor
     * @see SessionInteractor
     * @see AccountsInteractor
     */
    @Provides
    internal fun provideFeedbacksPresenter(eventBus: EventBus,
                                           feedbacksInteractor: FeedbacksInteractor,
                                           feedbackInteractor: FeedbackInteractor,
                                           sessionInteractor: SessionInteractor,
                                           accountsInteractor: AccountsInteractor): FeedbacksPresenter
        = FeedbacksPresenterImpl(view, eventBus, feedbacksInteractor, feedbackInteractor,
            sessionInteractor, accountsInteractor)

    /**
     * Provide feedbacks interactor.
     *
     * @param feedbacksRepository the feedbacks repository
     * @return the feedbacks interactor
     * @see FeedbacksRepository
     */
    @Provides
    internal fun provideFeedbacksInteractor(feedbacksRepository: FeedbacksRepository): FeedbacksInteractor
        = FeedbacksInteractorImpl(feedbacksRepository)

    /**
     * Provide feedbacks repository.
     *
     * @param feedbacksService  the feedbacks service
     * @param feedbacksCallback the feedbacks callback
     * @return the feedbacks repository
     * @see FeedbacksService
     * @see FeedbacksCallback
     */
    @Provides
    internal fun provideFeedbacksRepository(feedbacksService: FeedbacksService,
                                            feedbacksCallback: FeedbacksCallback): FeedbacksRepository
        = FeedbacksRepositoryImpl(feedbacksService, feedbacksCallback)

    /**
     * Provide feedbacks callback feedbacks callback.
     *
     * @param eventBus the event bus pattern
     * @param gson     the gson object
     * @return the feedbacks callback
     * @see EventBus
     * @see Gson
     */
    @Provides
    internal fun provideFeedbacksCallback(eventBus: EventBus, gson: Gson)
        = FeedbacksCallback(eventBus, gson)

}
