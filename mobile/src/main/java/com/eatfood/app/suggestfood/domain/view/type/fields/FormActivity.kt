package com.eatfood.app.suggestfood.domain.view.type.fields

import android.os.Bundle
import android.support.annotation.LayoutRes

import com.eatfood.app.suggestfood.domain.presenter.GeneralPresenter
import com.eatfood.app.suggestfood.domain.view.helper.IconHelper
import com.eatfood.app.suggestfood.domain.view.type.progress.ProgressActivity

/**
 * Form activity with support for UI fields.
 *
 * @param P the presenter type
 * @see ProgressActivity
 * @see FieldsView
 *
 * Created by Usuario on 07/09/2017.
 */
abstract class FormActivity<P : GeneralPresenter<*>> : ProgressActivity<P>(), FieldsView {

    protected val iconHelper : IconHelper
        get() =  IconHelper(this)

    override fun onCreate(@LayoutRes layoutResID: Int, savedInstanceState: Bundle?) {
        hideActionBar()
        super.onCreate(layoutResID, savedInstanceState)
    }

    /*INTERFACES*/
    //1. FieldsView

    override fun disableInputs() = setInputs(false)

    override fun enableInputs() = setInputs(true)

    /*ABSTRACT METHODS*/

    /**
     * Method that allows you to enable or disable the interface fields.
     *
     * @param enabled true - to enable
     *                false - to disable
     */
    protected abstract fun setInputs(enabled: Boolean)
}
