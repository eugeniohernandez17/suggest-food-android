package com.eatfood.app.suggestfood.domain.view.helper

import android.support.design.widget.TextInputLayout
import android.widget.TextView

/**
 * Class for the visualization of the errors in the interfaces.
 *
 * Created by Usuario on 30/08/2017.
 */
object ErrorHelper {

    /**
     * Method to show the errors of a text field.
     *
     * @param field the text field
     * @param error the error text
     * @see TextView.setError
     * @see TextInputLayout.setError
     */
    fun display(field: TextView, error: String?) {
        val parent = field.parentForAccessibility
        when(parent){
            is TextInputLayout -> {
                parent.isErrorEnabled = error != null
                parent.error = error
            }
            else -> field.error = error
        }
    }

}
