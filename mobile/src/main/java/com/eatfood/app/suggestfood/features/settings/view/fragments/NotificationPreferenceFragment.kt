package com.eatfood.app.suggestfood.features.settings.view.fragments

import android.annotation.TargetApi
import android.os.Build
import android.os.Bundle
import com.eatfood.app.suggestfood.R
import com.eatfood.app.suggestfood.content_provider.di.DatabaseModule
import com.eatfood.app.suggestfood.domain.di.DaggerDomainComponent
import com.eatfood.app.suggestfood.domain.di.DomainModule
import com.eatfood.app.suggestfood.domain.resolution.di.ResolutionModule
import com.eatfood.app.suggestfood.features.authorization.di.DaggerAuthorizationComponent
import com.eatfood.app.suggestfood.features.settings.di.DaggerSettingsComponent
import com.eatfood.app.suggestfood.features.settings.di.SettingsModule
import com.eatfood.app.suggestfood.lib.di.LibsModule

/**
 * This fragment shows notification preferences only. It is used when the
 * activity is showing a two-pane settings UI.
 *
 * @see SettingsPreferenceFragment
 *
 * Created by Usuario on 30/09/2017.
 */
@TargetApi(Build.VERSION_CODES.HONEYCOMB)
internal class NotificationPreferenceFragment : SettingsPreferenceFragment() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        onCreated(R.xml.pref_notification)

        // Bind the summaries of EditText/List/Dialog/Ringtone preferences
        // to their values. When their values change, their summaries are
        // updated to reflect the new value, per the Android Design
        // guidelines.
        bindPreferenceSummaryToValue(findPreference("recipes_new_message_ringtone"))
    }

    override fun setupInjection() {
        val databaseModule = DatabaseModule(activity)
        val authorization = DaggerAuthorizationComponent.builder()
                .databaseModule(databaseModule)
                .build().provideAuthorization()
        val domainComponent = DaggerDomainComponent.builder()
                .libsModule(LibsModule(activity, authorization))
                .resolutionModule(ResolutionModule(activity))
                .domainModule(DomainModule(activity))
                .build()
        DaggerSettingsComponent.builder()
                .domainComponent(domainComponent)
                .databaseModule(databaseModule)
                .settingsModule(SettingsModule(this,defaultPrefs))
                .build().inject(this)
    }

}
