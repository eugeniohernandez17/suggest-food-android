package com.eatfood.app.suggestfood.features.feedback.model

import com.eatfood.app.suggestfood.entity.Feedback

/**
 * Basic implementation of the [FeedbackInteractor] interface for interactor in the Clean pattern.
 *
 * @param feedbackRepository the feedback repository
 * @constructor Instantiates a new Feedback interactor.
 * @see FeedbackInteractor
 * @see FeedbackRepository
 *
 * Created by Usuario on 22/03/2018.
 */
class FeedbackInteractorImpl(private val feedbackRepository: FeedbackRepository) : FeedbackInteractor {

    override val totalPendingFeedback = feedbackRepository.totalPendingFeedback

    override val lastPendingFeedback = feedbackRepository.lastPendingFeedback

    override fun addPendingFeedback(feedback: Feedback) = feedbackRepository.addPendingFeedback(feedback)

    override fun removePendingFeedback(feedback: Feedback) = feedbackRepository.removePendingFeedback(feedback)

    override fun addFeedback(feedback: Feedback, remote: Boolean) = feedbackRepository.addFeedback(feedback, remote)

}