package com.eatfood.app.suggestfood.lib.objectbox

import android.content.Context
import com.eatfood.app.suggestfood.entity.MyObjectBox
import io.objectbox.BoxStore
import io.objectbox.DebugFlags

/**
 * Created by Usuario on 04/09/2017.
 */
object ObjectboxBuilder {

    /**
     * Method to construct the object connection to the local database..
     *
     * @param context the context
     * @param nameDb  the name of the DB
     * @return the dao session
     * @see Context
     */
    fun build(context: Context, nameDb: String): BoxStore
        =  MyObjectBox.builder().name(nameDb)
            .debugFlags(DebugFlags.LOG_QUERIES or DebugFlags.LOG_QUERY_PARAMETERS or DebugFlags.LOG_TRANSACTIONS_READ or DebugFlags.LOG_TRANSACTIONS_WRITE)
            .androidContext(context).build()

}
