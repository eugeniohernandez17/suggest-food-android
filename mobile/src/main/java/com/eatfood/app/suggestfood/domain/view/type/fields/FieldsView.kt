package com.eatfood.app.suggestfood.domain.view.type.fields

import com.eatfood.app.suggestfood.domain.view.type.progress.ProgressView

/**
 * Basic interface for views in the MVP pattern they require validate fields in the UI.
 *
 * @see ProgressView
 *
 * Created by Usuario on 06/09/2017.
 */
interface FieldsView : ProgressView {

    /**
     * Blocks the fields of the UI so that they can not be modified.
     */
    fun disableInputs()

    /**
     * Enables UI fields so that they can be modified.
     */
    fun enableInputs()

}
