package com.eatfood.app.suggestfood.features.profile.show.view

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.support.design.widget.CollapsingToolbarLayout
import android.support.v4.widget.NestedScrollView
import android.support.v7.widget.Toolbar
import android.view.MenuItem
import android.widget.ImageView
import android.widget.TextView
import butterknife.OnClick
import com.eatfood.app.suggestfood.R
import com.eatfood.app.suggestfood.content_provider.di.DatabaseModule
import com.eatfood.app.suggestfood.domain.di.DaggerDomainComponent
import com.eatfood.app.suggestfood.domain.di.DomainModule
import com.eatfood.app.suggestfood.domain.resolution.di.ResolutionModule
import com.eatfood.app.suggestfood.domain.view.helper.MessageHelper
import com.eatfood.app.suggestfood.domain.view.type.progress.ProgressActivity
import com.eatfood.app.suggestfood.entity.User
import com.eatfood.app.suggestfood.features.authorization.di.DaggerAuthorizationComponent
import com.eatfood.app.suggestfood.features.dashboard.view.MainActivity
import com.eatfood.app.suggestfood.features.intents.parameters.User
import com.eatfood.app.suggestfood.features.profile.edit.view.ProfileActivity
import com.eatfood.app.suggestfood.features.profile.general.ProfileView.Companion.EXTRA_USER
import com.eatfood.app.suggestfood.features.profile.show.di.DaggerProfileComponent
import com.eatfood.app.suggestfood.features.profile.show.di.ProfileModule
import com.eatfood.app.suggestfood.features.profile.show.presenter.ProfilePresenter
import com.eatfood.app.suggestfood.lib.di.LibsModule
import com.eatfood.app.suggestfood.lib.image_loader.ImageLoader
import kotterknife.bindView
import me.eugeniomarletti.extras.intent.IntentExtra
import org.jetbrains.anko.intentFor
import org.jetbrains.anko.startActivityForResult
import javax.inject.Inject

/**
 * A profile screen that offers to show the user information.
 * Implements the interface [ProfileView]
 *
 * @see ProfileView
 * @see ProgressActivity
 * @see ProfilePresenter
 *
 * Created by Usuario on 18/10/2017.
 */
class ShowProfileActivity : ProgressActivity<ProfilePresenter>(), ProfileView {

    companion object {
        const val REQUEST_EDITPROFILE = 0

        @JvmStatic
        fun profileScreenIntent(context: Context, user: User)
                = context.intentFor<ShowProfileActivity>(EXTRA_USER to user)
    }

    object IntentOptions {
        var Intent.user by IntentExtra.User(EXTRA_USER)
    }

    // UI references.

    private val toolbarLayout: CollapsingToolbarLayout by bindView(R.id.toolbar_layout)
    private val toolbar: Toolbar by bindView(R.id.toolbar)
    private val containerProfile: NestedScrollView by bindView(R.id.container_profile)
    private val imgUser: ImageView by bindView(R.id.img_user)
    private val txtPhone: TextView by bindView(R.id.txt_phone)
    private val txtEmail: TextView by bindView(R.id.txt_email)

    @Inject
    lateinit var imageLoader: ImageLoader

    /**INTERFACES */
    //1. ProfileView

    /**
     * The user object for identifying the user object between the view's parameters.
     *
     * @see IntentOptions
     */
    override var userParam: User?
        get() = with(IntentOptions){ intent.user }
        set(value) = with(IntentOptions){ intent.user = value }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        super.onCreate(R.layout.activity_show_profile, savedInstanceState)
    }

    override fun onOptionsItemSelected(item: MenuItem) = when (item.itemId) {
        android.R.id.home -> {
            presenter.onViewDashboard()
            true
        }
        else -> super.onOptionsItemSelected(item)
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent) =
            when (requestCode) {
                REQUEST_EDITPROFILE -> with(IntentOptions) {
                    presenter.onProfileEditingFinish(data.user!!)
                }
                else -> super.onActivityResult(requestCode, resultCode, data)
            }

    override fun onBackPressed() {
        presenter.onViewDashboard()
        super.onBackPressed()
    }

    override fun config() {
        super.config()
        setupActionBar()
    }

    override fun configLoadProgress()
        = loadProgressHelper.config(null, getString(R.string.activity_profile_updating_profile))

    override fun setupInjection() {
        val authorization = DaggerAuthorizationComponent.builder()
                .databaseModule(DatabaseModule(this))
                .build().provideAuthorization()
        val domainComponent = DaggerDomainComponent.builder()
                .libsModule(LibsModule(this, authorization))
                .resolutionModule(ResolutionModule(this))
                .domainModule(DomainModule(this))
                .build()
        DaggerProfileComponent.builder()
                .domainComponent(domainComponent)
                .profileModule(ProfileModule(this))
                .build().inject(this)
    }

    /*EVENTS*/

    @OnClick(R.id.fab)
    fun onViewEditProfile() {
        presenter.onViewEditProfile()
    }

    override fun loadProfile(user: User) {
        toolbarLayout.title = user.name
        txtPhone.text = user.phone
        txtEmail.text = user.email
        imageLoader.load(imgUser, user.image)
    }

    override fun displayMessage(message: String) = MessageHelper.display(containerProfile, message)

    override fun navigateToDashboardScreen(user: User) {
        setResult(RESULT_OK, MainActivity.mainScreenIntent(this, user))
        finish()
    }

    override fun navigateToEditProfileScreen(user: User)
        = startActivityForResult<ProfileActivity>(REQUEST_EDITPROFILE, EXTRA_USER to user)

    /*Own Methods*/

    override fun setupActionBar() {
        setSupportActionBar(toolbar)
        super.setupActionBar()
    }
}
