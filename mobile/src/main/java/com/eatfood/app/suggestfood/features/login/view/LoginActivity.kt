package com.eatfood.app.suggestfood.features.login.view

import android.Manifest
import android.app.LoaderManager
import android.content.Context
import android.content.Intent
import android.content.Loader
import android.database.Cursor
import android.os.Bundle
import android.view.inputmethod.EditorInfo
import butterknife.OnClick
import butterknife.OnEditorAction
import com.eatfood.app.suggestfood.R
import com.eatfood.app.suggestfood.config.Font
import com.eatfood.app.suggestfood.content_provider.di.DatabaseModule
import com.eatfood.app.suggestfood.domain.di.DaggerDomainComponent
import com.eatfood.app.suggestfood.domain.di.DomainModule
import com.eatfood.app.suggestfood.domain.resolution.di.ResolutionModule
import com.eatfood.app.suggestfood.domain.view.adapter.SimpleListAdapter
import com.eatfood.app.suggestfood.domain.view.cursor.loader.ContactsCursorLoader
import com.eatfood.app.suggestfood.domain.view.helper.ErrorHelper
import com.eatfood.app.suggestfood.domain.view.helper.FontHelper
import com.eatfood.app.suggestfood.domain.view.helper.IconHelper
import com.eatfood.app.suggestfood.domain.view.helper.MessageHelper
import com.eatfood.app.suggestfood.domain.view.type.fields.FormActivity
import com.eatfood.app.suggestfood.entity.User
import com.eatfood.app.suggestfood.features.authorization.di.DaggerAuthorizationComponent
import com.eatfood.app.suggestfood.features.dashboard.view.MainActivity
import com.eatfood.app.suggestfood.features.di.DaggerFeaturesComponent
import com.eatfood.app.suggestfood.features.intents.parameters.User
import com.eatfood.app.suggestfood.features.login.di.DaggerLoginComponent
import com.eatfood.app.suggestfood.features.login.di.LoginModule
import com.eatfood.app.suggestfood.features.login.presenter.LoginPresenter
import com.eatfood.app.suggestfood.features.lost_password.view.LostPasswordActivity
import com.eatfood.app.suggestfood.features.signup.view.SignupActivity
import com.eatfood.app.suggestfood.lib.di.LibsModule
import kotlinx.android.synthetic.main.activity_login.*
import kotlinx.android.synthetic.main.content_header_app.*
import me.eugeniomarletti.extras.intent.IntentExtra
import org.jetbrains.anko.clearTop
import org.jetbrains.anko.intentFor
import org.jetbrains.anko.newTask
import org.jetbrains.anko.startActivity
import permissions.dispatcher.NeedsPermission
import permissions.dispatcher.RuntimePermissions

/**
 * A login screen that offers login via email/password. Implements the interface [LoginView].
 *
 * @see LoginView
 * @see FormActivity
 * @see LoginPresenter
 *
 * Created by Usuario on 12/01/2017.
 */
@RuntimePermissions
class LoginActivity : FormActivity<LoginPresenter>(), LoginView,
        LoaderManager.LoaderCallbacks<Cursor> {

    companion object {
        @JvmStatic
        fun loginScreenIntent(context:Context) = context.intentFor<LoginActivity>().newTask().clearTop()

        @JvmStatic
        fun loginScreenIntent(context: Context, user: User)
                = loginScreenIntent(context).putExtra(LoginView.EXTRA_USER, user)
    }

    object IntentOptions {
        var Intent.user by IntentExtra.User(LoginView.EXTRA_USER)
    }

    /**
     * The user object for identifying the user object between the view's parameters.
     *
     * @see IntentOptions
     * @see [isAutoLogin]
     */
    override var userParam: User?
        get() = with(IntentOptions){ intent.user }
        set(value) = with(IntentOptions){ intent.user = value }

    /*INTERFACES*/
    //1.LoginView

    override val email: String
        get() = txtEmail.text.toString()

    override var password: String?
        get() = txtPassword.text.toString()
        set(password) = txtPassword.setText(password)

    override val isAutoLogin: Boolean
        get() = userParam != null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        super.onCreate(R.layout.activity_login, savedInstanceState)
    }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<String>, grantResults: IntArray) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        LoginActivityPermissionsDispatcher.onRequestPermissionsResult(this, requestCode, grantResults)
    }

    override fun config() {
        super.config()
        configureFonts()
        configureIcons()
    }

    override fun setupInjection() {
        val databaseModule = DatabaseModule(this)
        val authorization = DaggerAuthorizationComponent.builder()
                .databaseModule(databaseModule)
                .build().provideAuthorization()
        val domainComponent = DaggerDomainComponent.builder()
                .libsModule(LibsModule(this, authorization))
                .resolutionModule(ResolutionModule(this))
                .domainModule(DomainModule(this))
                .build()
        val featuresComponent = DaggerFeaturesComponent.builder()
                .domainComponent(domainComponent)
                .databaseModule(databaseModule)
                .build()
        DaggerLoginComponent.builder()
                .featuresComponent(featuresComponent)
                .loginModule(LoginModule(this, authorization))
                .build().inject(this)
    }

    override fun displayMessage(message: String) = MessageHelper.display(loginForm, message)

    override fun setInputs(enabled: Boolean)
        = super.setInputs(enabled, txtEmail, txtPassword, btnSignIn, btnSignUp)

    override fun configLoadProgress()
        = loadProgressHelper.config(null, getString(R.string.activity_login_dialog_message))

    /*EVENTS*/

    @OnEditorAction(R.id.txtPassword)
    fun onEditorPassword(id: Int): Boolean {
        if (id == EditorInfo.IME_ACTION_GO || id == EditorInfo.IME_NULL) {
            presenter.validateLogin()
            return true
        }
        return false
    }

    @OnClick(R.id.btnSignIn)
    fun onClickSignIn() {
        presenter.validateLogin()
    }

    @OnClick(R.id.btnSignUp)
    fun onClickSignUp() = startActivity<SignupActivity>()

    @OnClick(R.id.btnLostPassword)
    fun onClickLostPassword() = startActivity<LostPasswordActivity>()

    override fun showErrorInEmail(error: String?) = ErrorHelper.display(txtEmail, error)

    override fun showErrorInPassword(error: String?) = ErrorHelper.display(txtPassword, error)

    override fun navigateToMainScreen() = redirect(MainActivity.mainScreenIntent(this), true)

    override fun loadContacts() = LoginActivityPermissionsDispatcher.initLoadContactsWithCheck(this)

    //2. LoaderManager.LoaderCallbacks<Cursor>

    override fun onCreateLoader(i: Int, bundle: Bundle): Loader<Cursor> {
        return ContactsCursorLoader(this)
    }

    override fun onLoadFinished(loader: Loader<Cursor>, cursor: Cursor) {
        if (loader is ContactsCursorLoader) {
            val emails = loader.getEmails(cursor)
            txtEmail.setAdapter(SimpleListAdapter(this, emails))
        }
    }

    override fun onLoaderReset(loader: Loader<Cursor>) {}

    /*Own Methods*/

    private fun configureFonts() = FontHelper.setFont(txtTitle, Font.TRAJAN_BOLD)

    private fun configureIcons() {
        iconHelper.setIcon(IconHelper.DrawablePosition.LEFT, R.drawable.ic_mail_outline_black_24dp, txtEmail)
        iconHelper.setIcon(IconHelper.DrawablePosition.LEFT, R.drawable.ic_lock_outline_black_24dp, txtPassword)
    }

    /**
     * Method to init the load of the contacts in the view.
     * If the permissions were not granted, the load will not start.
     */
    @NeedsPermission(Manifest.permission.GET_ACCOUNTS, Manifest.permission.READ_CONTACTS)
    fun initLoadContacts() {
        loaderManager.initLoader(0, Bundle(), this)
    }
}
