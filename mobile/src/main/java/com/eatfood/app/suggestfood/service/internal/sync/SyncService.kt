package com.eatfood.app.suggestfood.service.internal.sync

import android.content.Intent
import com.eatfood.app.suggestfood.service.internal.AbstractService
import com.eatfood.app.suggestfood.service.internal.sync.adapter.SyncAdapter

/**
 * Define a Service that returns an IBinder for the sync adapter class,
 * allowing the sync adapter framework to call onPerformSync().
 *
 * @see AbstractService
 *
 * Created by Usuario on 03/04/2018.
 */
class SyncService : AbstractService() {

    companion object {
        /**
         * Storage for an instance of the sync adapter
         *
         * @see SyncAdapter
         */
        @JvmStatic
        private var syncAdapter: SyncAdapter? = null

        /**
         * Object to use as a thread-safe lock
         */
        @JvmStatic
        private val sSyncAdapterLock: Any = Any()
    }

    /**
     * Instantiate the sync adapter object.
     * Create the sync adapter as a singleton.
     * Set the sync adapter as syncable.
     * Disallow parallel syncs.
     */
    override fun onCreate() {
        synchronized(sSyncAdapterLock){
            syncAdapter = syncAdapter?:SyncAdapter(applicationContext)
        }
    }

    /**
     * Return an object that allows the system to invoke the sync adapter.
     * Get the object that allows external processes to call onPerformSync().
     * The object is created in the base class code when the SyncAdapter
     * constructors call super()
     */
    override fun onBind(intent:Intent) = syncAdapter?.syncAdapterBinder

}
