package com.eatfood.app.suggestfood.features.di

import javax.inject.Scope

/**
 * Identifies a type that the injector is an app.
 *
 * @see Scope
 *
 * Created by Usuario on 22/04/2018.
 */
@Scope
@kotlin.annotation.Retention
annotation class Application
