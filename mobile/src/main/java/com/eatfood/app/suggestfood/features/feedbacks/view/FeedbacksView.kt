package com.eatfood.app.suggestfood.features.feedbacks.view

import android.accounts.Account
import com.eatfood.app.suggestfood.domain.view.type.progress.ProgressView
import com.eatfood.app.suggestfood.entity.Feedback

/**
 * Basic interface for the view in the MVP pattern.
 *
 * @see ProgressView
 *
 * Created by Usuario on 08/10/2017.
 */
interface FeedbacksView : ProgressView {

    /**
     * Method to display the list of complaints.
     */
    fun displayComplaintsScreen()

    /**
     * Method to display the list of suggestions.
     */
    fun displaySuggestionsScreen()

    /**
     * Method to display the list of feedbacks.
     */
    fun displayFeedbacksScreen()

    /**
     * Method to display the screen for creating a new feedback.
     *
     * @param type the feedback type to register
     * @see Feedback.FeedbackType
     */
    fun displayNewFeedbackScreen(type: Feedback.FeedbackType)

    /**
     * Function to initiate the automatic progress of the load of feedbacks in the view.
     */
    fun initLoadFeedbacks()

    /**
     * Method to display the message of pending feedbacks to send.
     *
     * @param total the total number of the feedbacks pending
     */
    fun displayPendingFeedbackMessage(total: Int)

    /**
     * Method to display the message of pending feedbacks to send during synchronization.
     *
     * @param total the total number of the feedbacks pending
     */
    fun displaySyncPendingFeedbackMessage(total: Int)

    /**
     * Method to load the list of feedbacks in the view.
     *
     * @param feedbacks the list of feedbacks
     * @see List
     * @see Feedback
     */
    fun loadFeedbacks(feedbacks: List<Feedback>): Unit?

    /**
     * Method to delete the list of feedbacks in the view.
     */
    fun clearFeedbacks()

    /**
     * Method to indicate that there are no feedbacks to load.
     *
     * @param feedbackType the current feedback type
     */
    fun displayNoFeedbacksError(feedbackType: Feedback.FeedbackType)

    /**
     * Method inverse to [displayNoFeedbacksError].
     */
    fun hideNoFeedbacksError()

    /**
     * Method to synchronize [account] feedbacks.
     *
     * @param account the associated account
     * @see Account
     */
    fun syncPendingFeedbacks(account: Account)
}
