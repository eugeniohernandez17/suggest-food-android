package com.eatfood.app.suggestfood.features.feedbacks.model

import com.eatfood.app.suggestfood.entity.Feedback
import com.eatfood.app.suggestfood.entity.User

/**
 * Basic interface for the feedbacks repository in the Clean pattern of the feedbacks feature.
 *
 * Created by Usuario on 10/10/2017.
 */
interface FeedbacksRepository {

    /**
     * Method that loads the feedbacks of the user
     * based on the [Feedback.FeedbackType] passed as a parameter.
     *
     * @param feedbackType the feedback type
     * @param page         the page
     * @param user         the user
     * @see Feedback.FeedbackType
     * @see User
     */
    fun loadFeedbacks(feedbackType: Feedback.FeedbackType, page: Int, user: User)

}
