package com.eatfood.app.suggestfood.domain.evaluator

import android.content.Context

import com.eatfood.app.suggestfood.R
import com.eatfood.app.suggestfood.domain.visitor.Evaluate

/**
 * Email evaluator based on the [TextEvaluator] class.
 * It contains the methods to know in detail if it is a valid email.
 *
 * @param context  the context
 * @param evaluate the evaluate object
 * @constructor Instantiates a new Email evaluator.
 * @see TextEvaluator
 * @see Context
 * @see Evaluate
 *
 * Created by Usuario on 30/08/2017.
 */
internal class EmailEvaluator(context: Context, evaluate: Evaluate) : TextEvaluator(context, evaluate) {

    override fun validate(value: String?): Boolean {
        var result = super.validate(value)
        if (result && !this.evaluate.validEmail(value!!)) {
            this.errorId = R.string.general_error_invalid_email
            result = false
        }
        return result
    }
}
