package com.eatfood.app.suggestfood.service.internal.sync.di

import com.eatfood.app.suggestfood.features.di.Feature
import com.eatfood.app.suggestfood.features.di.FeaturesComponent
import com.eatfood.app.suggestfood.service.internal.sync.adapter.SyncAdapter
import dagger.Component

/**
 * Injection component of the sync feedbacks feature.
 *
 * @see SyncModule
 * @see SyncAdapter
 *
 * Created by Usuario on 03/04/2018.
 */
@Feature
@Component(modules = [(SyncModule::class)], dependencies = [(FeaturesComponent::class)])
internal interface SyncComponent {

    /**
     * Method for injecting dependencies.
     *
     * @param syncAdapter the sync adapter
     * @see SyncAdapter
     */
    fun inject(syncAdapter: SyncAdapter)

}
