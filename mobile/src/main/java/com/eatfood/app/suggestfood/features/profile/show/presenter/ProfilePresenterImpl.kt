package com.eatfood.app.suggestfood.features.profile.show.presenter

import com.eatfood.app.suggestfood.domain.event.MessageEvent
import com.eatfood.app.suggestfood.domain.presenter.ProgressPresenter
import com.eatfood.app.suggestfood.entity.User
import com.eatfood.app.suggestfood.features.profile.general.ProfilePresenterHelper
import com.eatfood.app.suggestfood.features.profile.show.view.ProfileView
import com.eatfood.app.suggestfood.lib.eventbus.EventBus

import org.greenrobot.eventbus.Subscribe

/**
 * Basic implementation of the [ProfilePresenter] interface for the presenter in the MVP pattern.
 *
 * @param view     the view
 * @param eventBus the event bus object
 * @property profileHelper the profile helper
 * @constructor Instantiates a new Profile presenter.
 * @see ProfilePresenter
 * @see ProfileView
 * @see EventBus
 * @see ProfilePresenterHelper
 *
 * Created by Usuario on 25/09/2017.
 */
class ProfilePresenterImpl(
        view: ProfileView,
        eventBus: EventBus,
        private val profileHelper: ProfilePresenterHelper
) : ProgressPresenter<ProfileView, Any>(view, eventBus), ProfilePresenter {

    override fun onCreate() {
        super.onCreate()
        profileHelper.loadProfile()
    }

    @Subscribe
    override fun onMessageEvent(event: MessageEvent<Any>) = Unit

    override fun onResolution(requestCode: Int) = Unit

    override fun onViewDashboard() = profileHelper.userParam?.let {
        view?.navigateToDashboardScreen(it)
    }

    override fun onViewEditProfile() = profileHelper.userParam?.let {
        view?.navigateToEditProfileScreen(it)
    }

    override fun onProfileEditingFinish(user: User) {
        view?.userParam = user
        profileHelper.loadProfile(user)
    }
}
