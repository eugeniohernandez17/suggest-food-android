package com.eatfood.app.suggestfood.content_provider.di

import android.content.Context
import com.eatfood.app.suggestfood.config.Application
import com.eatfood.app.suggestfood.entity.AuthorizedUser
import com.eatfood.app.suggestfood.entity.Feedback
import com.eatfood.app.suggestfood.entity.User
import dagger.Module
import dagger.Provides
import io.objectbox.Box
import io.objectbox.BoxStore

/**
 * Injection of dependencies of the application database.
 *
 * @property context the context
 * @constructor Instantiates a new Database module.
 * @see Module
 * @see Context
 *
 * Created by Usuario on 23/04/2018.
 */
@Module
class DatabaseModule(private val context: Context) {

    /**
     * Provide the authorized user box object.
     * It's necessary for the CRUD actions in the [AuthorizedUser] objects.
     *
     * @param boxStore the box store object
     * @return the authorized user box object
     * @see BoxStore
     * @see AuthorizedUser
     */
    @Provides
    fun provideAuthorizedUserBox(boxStore: BoxStore): Box<AuthorizedUser> = boxStore.boxFor(AuthorizedUser::class.java)

    /**
     * Provide the user box object.
     * It's necessary for the CRUD actions in the [User] objects.
     *
     * @param boxStore the box store object
     * @return the user box object
     * @see BoxStore
     * @see User
     */
    @Provides
    fun provideUserBox(boxStore: BoxStore): Box<User> = boxStore.boxFor(User::class.java)

    /**
     * Provide the feedback box object.
     * It's necessary for the CRUD actions in the [Feedback] objects.
     *
     * @param boxStore the box store object
     * @return the feedback box object
     * @see BoxStore
     * @see Feedback
     */
    @Provides
    fun provideFeedbackBox(boxStore: BoxStore): Box<Feedback> = boxStore.boxFor(Feedback::class.java)

    /**
     * Provide the box store object.
     * The connection to the local database.
     *
     * @return the box store object
     * @see BoxStore
     */
    @Provides
    fun provideBoxStore(): BoxStore = Application.getBoxStore(context)

}