package com.eatfood.app.suggestfood.domain.evaluate

import com.eatfood.app.suggestfood.domain.Patterns
import com.eatfood.app.suggestfood.domain.visitor.Evaluate
import com.google.i18n.phonenumbers.NumberParseException
import com.google.i18n.phonenumbers.PhoneNumberUtil
import java.util.*

/**
 * Class that implements the [Evaluate] interface.
 * Contains the acceptance criteria to validate the fields of the models or the interfaces.
 *
 * @property phoneNumberUtil the phone number util
 * @property locale          the locale
 * @constructor Instantiates a new [Evaluate] object.
 * @see Evaluate
 * @see PhoneNumberUtil
 * @see Locale
 *
 * Created by Usuario on 30/08/2017.
 */
class FieldEvaluate(
        private val phoneNumberUtil: PhoneNumberUtil,
        private val locale: Locale
) : Evaluate {

    override fun isEmpty(value: String?): Boolean = value?.isEmpty() ?: true

    override fun validEmail(email: String): Boolean = Patterns.EMAIL_ADDRESS.matcher(email).matches()

    override fun validMaxLength(text: String, limit: Int): Boolean = text.length <= limit

    override fun validMinLength(text: String, limit: Int): Boolean = text.length >= limit

    override fun match(text1: String, text2: String, ignoreCase: Boolean): Boolean =
            if (ignoreCase) text1.equals(text2, ignoreCase = true) else text1 == text2

    override fun validPhoneNumber(phone: String): Boolean
        = try {
            val country = locale.country
            val phoneNumber = phoneNumberUtil.parse(phone, country)
            phoneNumberUtil.isValidNumber(phoneNumber)
        } catch (e: NumberParseException) {
            e.printStackTrace()
            false
        }

}
