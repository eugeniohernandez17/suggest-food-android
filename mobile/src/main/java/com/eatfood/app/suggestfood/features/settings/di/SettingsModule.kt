package com.eatfood.app.suggestfood.features.settings.di

import com.eatfood.app.suggestfood.features.accounts.di.AccountsModule
import com.eatfood.app.suggestfood.features.accounts.model.AccountsInteractor
import com.eatfood.app.suggestfood.features.session.model.SessionInteractor
import com.eatfood.app.suggestfood.features.settings.model.SettingsInteractor
import com.eatfood.app.suggestfood.features.settings.model.SettingsInteractorImpl
import com.eatfood.app.suggestfood.features.settings.model.SettingsRepository
import com.eatfood.app.suggestfood.features.settings.model.SettingsRepositoryImpl
import com.eatfood.app.suggestfood.features.settings.presenter.fragments.PreferencePresenter
import com.eatfood.app.suggestfood.features.settings.presenter.fragments.PreferencePresenterImpl
import com.eatfood.app.suggestfood.features.settings.view.fragments.PreferenceView
import com.eatfood.app.suggestfood.prefs.schemas.DefaultPrefs
import dagger.Module
import dagger.Provides

/**
 * Injection dependency of the Settings module.
 *
 * @property preferenceView the preference view
 * @property defaultPrefs   the default preferences object
 * @constructor Instantiates a new Settings module.
 * @see SettingsComponent
 * @see AccountsModule
 * @see Module
 * @see PreferenceView
 * @see DefaultPrefs
 *
 * Created by Usuario on 30/09/2017.
 */
@Module(includes = [(AccountsModule::class)])
class SettingsModule(
        private val preferenceView: PreferenceView? = null,
        private val defaultPrefs: DefaultPrefs
) {

    /**
     * Provide preference presenter.
     *
     * @param settingsInteractor the settings interactor
     * @param sessionInteractor  the session interactor
     * @param accountsInteractor the accounts interactor
     * @return the preference presenter
     * @see SettingsInteractor
     * @see SessionInteractor
     * @see AccountsInteractor
     */
    @Provides
    fun providePreferencePresenter(settingsInteractor: SettingsInteractor,
                                 sessionInteractor: SessionInteractor,
                                 accountsInteractor: AccountsInteractor): PreferencePresenter {
        return PreferencePresenterImpl(preferenceView, settingsInteractor, sessionInteractor, accountsInteractor)
    }

    /**
     * Provide settings interactor.
     *
     * @param settingsRepository the settings repository
     * @return the settings interactor
     * @see SettingsRepository
     */
    @Provides
    fun provideSettingsInteractor(settingsRepository: SettingsRepository): SettingsInteractor
            = SettingsInteractorImpl(settingsRepository)

    /**
     * Provide settings repository
     *
     * @return the settings repository
     */
    @Provides
    fun provideSettingsRepository(): SettingsRepository
            = SettingsRepositoryImpl(defaultPrefs)

}
