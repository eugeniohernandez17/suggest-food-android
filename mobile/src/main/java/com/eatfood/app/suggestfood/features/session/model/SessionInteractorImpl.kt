package com.eatfood.app.suggestfood.features.session.model

import com.eatfood.app.suggestfood.entity.AuthorizedUser
import com.eatfood.app.suggestfood.entity.User

/**
 * Basic implementation of the [SessionInteractor] interface for interactor in the Clean pattern.
 *
 * @property sessionRepository the session repository
 * @constructor Instantiates a new Session interactor.
 * @see SessionInteractor
 * @see SessionRepository
 *
 * Created by Usuario on 04/09/2017.
 */
class SessionInteractorImpl(private val sessionRepository: SessionRepository) : SessionInteractor {

    override val isValidSession: Boolean
        get() = sessionRepository.isValidSession

    override var session: AuthorizedUser?
        get() = sessionRepository.session
        set(authorizedUser) {
            sessionRepository.session = authorizedUser
        }

    override fun updateUserInfo(user: User) = sessionRepository.updateUserInfo(user)

    override fun invalidateSession() = sessionRepository.invalidateSession()

}
