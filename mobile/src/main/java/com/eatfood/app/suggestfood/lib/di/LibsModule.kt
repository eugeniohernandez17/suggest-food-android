package com.eatfood.app.suggestfood.lib.di

import android.content.Context
import com.eatfood.app.suggestfood.BuildConfig
import com.eatfood.app.suggestfood.lib.eventbus.EventBus
import com.eatfood.app.suggestfood.lib.eventbus.GreenRobotEventBus
import com.eatfood.app.suggestfood.lib.gson.GsonBuilder
import com.eatfood.app.suggestfood.lib.httpclient.Authorization
import com.eatfood.app.suggestfood.lib.httpclient.HttpClient
import com.eatfood.app.suggestfood.lib.image_loader.GlideImageLoader
import com.eatfood.app.suggestfood.lib.image_loader.ImageLoader
import com.eatfood.app.suggestfood.lib.retrofit.Retrofit
import com.eatfood.app.suggestfood.lib.retrofit.RetrofitImpl
import com.google.firebase.storage.FirebaseStorage
import com.google.gson.Gson
import com.google.i18n.phonenumbers.PhoneNumberUtil
import dagger.Module
import dagger.Provides
import okhttp3.OkHttpClient
import javax.inject.Singleton

/**
 * Injection dependency of the Libraries module.
 *
 * @property context the context
 * @property authorization the authorization method for the requests
 * @constructor Instantiates a new Libs module.
 * @see Module
 * @see Context
 * @see Authorization
 *
 * Created by Usuario on 05/07/2016.
 */
@Module
class LibsModule(private val context: Context, private val authorization: Authorization<*>) {

    /**
     * Provide image loader.
     *
     * @return the image loader
     * @see Context
     */
    @Provides
    @Singleton
    internal fun provideImageLoader(): ImageLoader = GlideImageLoader(context)

    /**
     * Provide the http client.
     *
     * @return the http client object
     */
    @Provides
    fun provideOkHttpClient() = HttpClient.build(authorization)

    @Module
    companion object {

        /**
         * Provide event bus pattern.
         *
         * @return the event bus
         */
        @Provides
        @Singleton
        @JvmStatic
        fun provideEventBus(): EventBus = GreenRobotEventBus()

        /**
         * Provide retrofit object.
         *
         * @param gson         the gson
         * @param okHttpClient the ok http client
         * @return the retrofit object
         * @see Gson
         * @see OkHttpClient
         */
        @Provides
        @Singleton
        @JvmStatic
        fun provideRetrofit(gson: Gson, okHttpClient: OkHttpClient): Retrofit
                = RetrofitImpl(BuildConfig.SERVER, gson, okHttpClient)

        /**
         * Provide the gson object.
         *
         * @return the gson object
         * @see GsonBuilder.build
         */
        @Provides
        @Singleton
        @JvmStatic
        fun provideGson() = GsonBuilder.build()

        /**
         * Provide phone number util.
         *
         * @return the phone number util
         */
        @Provides
        @Singleton
        @JvmStatic
        fun providePhoneNumberUtil() = PhoneNumberUtil.getInstance()

        /**
         * Provide the Firebase Storage instance.
         *
         * @return the Firebase Storage instance
         */
        @Provides
        @Singleton
        @JvmStatic
        fun provideFirebaseStorage() = FirebaseStorage.getInstance()
    }
}
