package com.eatfood.app.suggestfood.lib.image_loader

import android.content.Context
import android.widget.ImageView
import com.bumptech.glide.Glide

/**
 * Basic implementation of the [ImageLoader] interface.
 *
 * @param context the context
 * @constructor Instantiates a new [ImageLoader] object in the [Context]
 * @see ImageLoader
 *
 * Created by Usuario on 25/09/2017.
 */
class GlideImageLoader(context: Context) : ImageLoader {

    private val glideRequestManager = Glide.with(context)

    override fun load(imageView: ImageView, uri: String?) = uri?.let{
        glideRequestManager.load(uri).into(imageView)
    }

}
