package com.eatfood.app.suggestfood.domain.view.fragment

import android.os.Bundle
import android.support.annotation.LayoutRes
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import butterknife.ButterKnife

/**
 * A simple [Fragment] subclass.
 *
 * @see Fragment
 *
 * Created by Usuario on 11/10/2017.
 */
internal abstract class GeneralFragment : Fragment() {

    @get:LayoutRes
    protected abstract val layout: Int

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        val view = inflater.inflate(layout, container, false)
        ButterKnife.bind(this, view)
        onCreateView(view)
        return view
    }

    /*ABSTRACT METHODS*/

    /**
     * Event generated when creating the view.
     *
     * @param view the list of items
     * @see View
     */
    protected abstract fun onCreateView(view: View)

}
