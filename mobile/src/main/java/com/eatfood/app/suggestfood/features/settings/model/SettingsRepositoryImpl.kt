package com.eatfood.app.suggestfood.features.settings.model

import com.eatfood.app.suggestfood.prefs.schemas.DefaultPrefs

/**
 * Basic implementation of the [SettingsRepository] for the repository in the Clean pattern.
 *
 * @property defaultPrefs
 * @constructor Instantiates a new Settings repository.
 * @see SettingsRepository
 * @see DefaultPrefs
 *
 * Created by Usuario on 14/04/2018.
 */
class SettingsRepositoryImpl(private val defaultPrefs: DefaultPrefs) : SettingsRepository {

    override val syncFrequency: Long
        get() = defaultPrefs.syncFrequency.toLong()

    override fun clearSettings() {
        /*TODO:Implementation not completed.*/
        defaultPrefs.removeRecipesNewNessage()
        defaultPrefs.removeRecipesNewMessageRingtone()
        defaultPrefs.removeRecipesNewMessageVibrate()
        defaultPrefs.removeSyncFrequency()
    }

}
