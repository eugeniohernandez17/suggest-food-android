package com.eatfood.app.suggestfood.entity

import com.eatfood.app.suggestfood.entity.Feedback.FeedbackType
import com.eatfood.app.suggestfood.entity.convert.FeedbackTypeConvert
import com.google.gson.annotations.SerializedName
import io.objectbox.annotation.Convert
import io.objectbox.annotation.Entity
import io.objectbox.annotation.Id
import io.objectbox.relation.ToOne
import java.io.Serializable

/**
 * Commercial entity that represents a comment of a user according
 * to the [FeedbackType] identified.
 *
 * @constructor Instantiates a new Feedback.
 * @see FeedbackType
 * @see Serializable
 *
 * Created by Usuario on 09/10/2017.
 */

@Entity
data class Feedback @JvmOverloads constructor(
        @Id
        var id: Long = 0,
        var name: String? = null,

        @SerializedName(value = "email_address")
        var email: String? = null,
        var topic: String? = null,
        var description: String? = null,

        @Convert(converter = FeedbackTypeConvert::class, dbType = String::class)
        @SerializedName(value = "category")
        var type: FeedbackType? = null
): Serializable {

    companion object {
        private const val serialVersionUID = 9114923551292026814L
    }

    /**
     * The enum Feedback type.
     *
     * @see type
     */
    enum class FeedbackType {
        COMPLAINT, SUGGESTION, FEEDBACK
    }

    lateinit var user: ToOne<User>
    lateinit var feedbackAssociate: ToOne<Feedback>

    /*Own Methods*/

    /**
     * Is same type.
     *
     * @param feedbackType the feedback type to compare
     * @return true - if it is of the same type
     * @see FeedbackType
     */
    fun isSameType(feedbackType: FeedbackType) = this.type == feedbackType

}
