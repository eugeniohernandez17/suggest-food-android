package com.eatfood.app.suggestfood.domain.view.cursor.loader

import android.content.Context
import android.content.CursorLoader
import android.database.Cursor
import android.net.Uri
import android.provider.ContactsContract
import java.util.*

/**
 * Basic phone charger contacts.
 *
 * @param context the context
 * @constructor Instantiates a new Contacts cursor.
 * @see CursorLoader
 * @see Context
 *
 * Created by Usuario on 05/09/2017.
 */
class ContactsCursorLoader(context: Context): CursorLoader(
        context,

        // Retrieve data rows for the device user's 'profile' contact.
        URI_FOR_CONTACTS, ProfileQuery.PROJECTION,

        // Select only email addresses.
        ContactsContract.Contacts.Data.MIMETYPE + " = ?", SELECTION_ARGS_FOR_CONTACTS,

        // Show primary email addresses first. Note that there won't be
        // a primary email address if the user hasn't specified one.
        ContactsContract.Contacts.Data.IS_PRIMARY + " DESC"
) {

    /*STATIC METHODS*/

    companion object {
        private val URI_FOR_CONTACTS = Uri.withAppendedPath(ContactsContract.Profile.CONTENT_URI,
                ContactsContract.Contacts.Data.CONTENT_DIRECTORY)

        private val SELECTION_ARGS_FOR_CONTACTS
                = arrayOf(ContactsContract.CommonDataKinds.Email.CONTENT_ITEM_TYPE)
    }

    private interface ProfileQuery {
        companion object {
            /**
             * The Projection.
             */
            val PROJECTION = arrayOf(ContactsContract.CommonDataKinds.Email.ADDRESS,
                    ContactsContract.CommonDataKinds.Email.IS_PRIMARY)

            /**
             * The constant ADDRESS.
             */
            const val ADDRESS = 0
        }
    }

    /*Own Methods*/

    /**
     * Method to get contacts emails through the Cursor object.
     *
     * @param cursor the cursor object
     * @return the contacts emails
     * @see Cursor
     */
    fun getEmails(cursor: Cursor): List<String> {
        val emails = ArrayList<String>()
        cursor.moveToFirst()
        while (!cursor.isAfterLast) {
            emails.add(cursor.getString(ProfileQuery.ADDRESS))
            cursor.moveToNext()
        }
        return emails
    }
}
