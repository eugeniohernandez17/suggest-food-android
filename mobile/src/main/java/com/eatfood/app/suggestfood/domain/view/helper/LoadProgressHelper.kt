package com.eatfood.app.suggestfood.domain.view.helper

import android.app.ProgressDialog
import android.content.Context

import com.eatfood.app.suggestfood.R

/**
 * Helper for the load progress.
 *
 * @param context the context
 * @constructor Instantiates a new Load progress helper.
 * @see ProgressDialog
 * @see Context
 *
 * Created by Usuario on 07/09/2017.
 */
class LoadProgressHelper(context: Context) {

    private val progressDialog: ProgressDialog

    init {
        progressDialog = build(context)
    }

    /**
     * Configuration of the progress dialog.
     *
     * @param title   the title
     * @param message the message
     */
    fun config(title: String?, message: String) {
        progressDialog.setTitle(title)
        progressDialog.setMessage(message)
    }

    /**
     * Show the progress dialog.
     */
    fun show() = progressDialog.show()

    /**
     * Hide the progress dialog.
     */
    fun hide() = progressDialog.dismiss()

    /*Own Methods*/

    private fun build(context: Context): ProgressDialog {
        val progressDialog = ProgressDialog(context, R.style.AppTheme_Dark_Dialog)
        progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER)
        progressDialog.setCancelable(false)
        progressDialog.isIndeterminate = true
        return progressDialog
    }
}
