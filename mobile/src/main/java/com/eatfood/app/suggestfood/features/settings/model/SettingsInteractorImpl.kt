package com.eatfood.app.suggestfood.features.settings.model

/**
 * Basic implementation of the [SettingsInteractor] interface for interactor in the Clean pattern.
 *
 * @property settingsRepository the settings repository
 * @constructor Instantiates a new Settings interactor.
 * @see SettingsInteractor
 * @see SettingsRepository
 *
 * Created by Usuario on 14/04/2018.
 */
class SettingsInteractorImpl(private val settingsRepository: SettingsRepository) : SettingsInteractor {

    override val syncFrequency: Long
        get() = settingsRepository.syncFrequency

    override fun clearSettings() = settingsRepository.clearSettings()

}