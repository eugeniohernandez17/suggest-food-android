package com.eatfood.app.suggestfood.features.accounts.model

import android.accounts.Account
import android.accounts.AccountManager
import com.eatfood.app.suggestfood.domain.model.UserAuthorization
import com.eatfood.app.suggestfood.entity.AuthorizedUser

/**
 * Basic implementation of the [AccountsRepository] for the repository in the Clean pattern.
 *
 * @property accountManager the account manager
 * @constructor Instantiates a new Accounts Repository.
 * @see AccountsRepository
 * @see AccountManager
 *
 * Created by Usuario on 04/04/2018.
 */
class AccountsRepositoryImpl(private val accountManager: AccountManager) : AccountsRepository {

    override fun find(email: String): Account? {
        val accounts = accountManager.accounts.filter {
            it.name == email
        }
        return if (!accounts.isEmpty()) accounts[0] else null
    }

    override fun create(authorizedUser: AuthorizedUser?) = authorizedUser?.let {
        val authtokenType = UserAuthorization.ACCOUNT_TYPE
        val account = Account(it.email, authtokenType)
        if (accountManager.addAccountExplicitly(account, it.password, null)) {
            accountManager.setAuthToken(account, authtokenType, it.accessToken)
            account
        } else {
            null
        }
    }

}