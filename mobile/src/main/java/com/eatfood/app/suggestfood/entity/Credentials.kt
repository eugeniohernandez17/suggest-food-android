package com.eatfood.app.suggestfood.entity

/**
 * Basic interface that contains the credentials of a user of the app.
 *
 * @see User
 * @see AuthorizedUser
 *
 * Created by Usuario on 23/04/2018.
 */
interface Credentials {
    var email: String
    var password: String
}