package com.eatfood.app.suggestfood.features.signup.model

import com.eatfood.app.suggestfood.entity.User

/**
 * Basic implementation of the [SignupInteractor] interface for interactor in the Clean pattern.
 *
 * @property signupRepository the signup repository
 * @constructor Instantiates a new Signup interactor.
 * @see SignupInteractor
 * @see SignupRepository
 *
 * Created by Usuario on 06/09/2017.
 */
class SignupInteractorImpl(private val signupRepository: SignupRepository) : SignupInteractor {

    override fun doSignUp(email: String, password: String)
        = signupRepository.signup(User(email = email, password = password))
}
