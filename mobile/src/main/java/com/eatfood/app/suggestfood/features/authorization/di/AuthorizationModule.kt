package com.eatfood.app.suggestfood.features.authorization.di

import com.eatfood.app.suggestfood.domain.di.DomainModule
import com.eatfood.app.suggestfood.domain.model.UserAuthorization
import com.eatfood.app.suggestfood.features.session.di.SessionModule
import com.eatfood.app.suggestfood.features.session.model.SessionRepository
import com.eatfood.app.suggestfood.lib.httpclient.Authorization
import dagger.Module
import dagger.Provides

/**
 * Injection dependency of the Accounts module.
 *
 * @see AuthorizationComponent
 * @see DomainModule
 * @see SessionModule
 * @see Module
 *
 * Created by Usuario on 22/04/2018.
 */
@Module(includes = [(DomainModule::class), (SessionModule::class)])
object AuthorizationModule {

    /**
     * Provide the object for the authorization of the api request.
     * It is required to obtain the current session to update the configuration parameters.
     *
     * @param sessionRepository the session repository
     * @return the authorization object
     * @see Authorization
     * @see UserAuthorization
     * @see SessionRepository
     */
    @Provides
    @JvmStatic
    internal fun provideAuthorization(sessionRepository: SessionRepository): Authorization<*>
            = UserAuthorization(sessionRepository)

}