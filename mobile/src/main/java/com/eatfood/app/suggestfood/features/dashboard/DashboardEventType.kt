package com.eatfood.app.suggestfood.features.dashboard

/**
 * General events used in the EventBus pattern to identify api responses in the dashboard.
 *
 * Created by Usuario on 24/09/2017.
 */
enum class DashboardEventType {

    /**
     * Event when the user information is successfully obtained.
     */
    ON_GET_USER_INFO_SUCCESS,

    /**
     * Event generated when there is an error obtained the user information.
     * @see .ON_GET_USER_INFO_SUCCESS
     */
    ON_GET_USER_INFO_ERROR

}
