package com.eatfood.app.suggestfood.prefs.schemas

import com.rejasupotaro.android.kvs.annotations.Key
import com.rejasupotaro.android.kvs.annotations.Table

/**
 * Scheme of the application settings.
 *
 * @see DefaultPrefs
 *
 * Created by Usuario on 02/10/2017.
 */
@Table(name = "com.eatfood.app.suggestfood_preferences")
class DefaultPrefsSchema {

    //Recipes

    @Key(name = "recipes_new_message")
    internal val recipesNewNessage = true

    @Key(name = "recipes_new_message_ringtone")
    internal var recipesNewMessageRingtone: String? = null

    @Key(name = "recipes_new_message_vibrate")
    internal val recipesNewMessageVibrate = true

    //Data and Sync

    @Key(name = "sync_frequency")
    internal val syncFrequency: String = "180"

}
