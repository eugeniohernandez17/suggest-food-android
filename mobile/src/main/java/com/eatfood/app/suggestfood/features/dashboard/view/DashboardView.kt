package com.eatfood.app.suggestfood.features.dashboard.view

import com.eatfood.app.suggestfood.domain.view.listeners.SyncListener
import com.eatfood.app.suggestfood.domain.view.type.progress.ProgressView
import com.eatfood.app.suggestfood.entity.User

/**
 * Basic interface for the view in the MVP pattern.
 * It includes the methods for data management through the sync process.
 *
 * @see ProgressView
 * @see SyncListener
 *
 * Created by Usuario on 07/01/2017.
 */
interface DashboardView : ProgressView, SyncListener {

    /**
     * Method for loading the user information.
     *
     * @param user the user
     * @see User
     */
    fun loadUser(user: User)

    /**
     * Method to navigate to the profile screen.
     *
     * @param user the user information
     */
    fun navigateToProfileScreen(user: User)

    /**
     * Method to navigate to the login screen.
     */
    fun navigateToLoginScreen()

    /**
     * Method to navigate to the Settings screen.
     */
    fun navigateToSettingsScreen()

    /**
     * Method to navigate to the feedbacks screen.
     */
    fun navigateToFeedbacksScreen()
}
