package com.eatfood.app.suggestfood.service.internal.sync.di

import com.eatfood.app.suggestfood.features.feedback.di.FeedbackModule
import com.eatfood.app.suggestfood.features.feedback.model.FeedbackInteractor
import com.eatfood.app.suggestfood.features.session.model.SessionInteractor
import com.eatfood.app.suggestfood.service.internal.sync.presenter.SyncPresenter
import com.eatfood.app.suggestfood.service.internal.sync.presenter.SyncPresenterImpl
import dagger.Module
import dagger.Provides

/**
 * Injection dependency of the Sync Feedbacks module.
 *
 * @see SyncComponent
 * @see FeedbackModule
 * @see Module
 *
 * Created by Usuario on 03/04/2018.
 */
@Module(includes = [(FeedbackModule::class)])
class SyncModule {

    /**
     * Provide sync presenter.
     *
     * @param sessionInteractor the session interactor
     * @param feedbackInteractor the feedback interactor
     * @return the sync presenter
     * @see SessionInteractor
     * @see FeedbackInteractor
     */
    @Provides
    fun provideSyncPresenter(sessionInteractor: SessionInteractor,
                             feedbackInteractor: FeedbackInteractor): SyncPresenter
        = SyncPresenterImpl(sessionInteractor, feedbackInteractor)

}