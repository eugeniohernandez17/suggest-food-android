package com.eatfood.app.suggestfood.features.feedbacks.presenter

import com.eatfood.app.suggestfood.R
import com.eatfood.app.suggestfood.domain.event.MessageEvent
import com.eatfood.app.suggestfood.domain.presenter.ProgressPresenter
import com.eatfood.app.suggestfood.entity.Feedback
import com.eatfood.app.suggestfood.features.accounts.model.AccountsInteractor
import com.eatfood.app.suggestfood.features.feedback.model.FeedbackInteractor
import com.eatfood.app.suggestfood.features.feedbacks.FeedbacksEventType
import com.eatfood.app.suggestfood.features.feedbacks.model.FeedbacksInteractor
import com.eatfood.app.suggestfood.features.feedbacks.view.FeedbacksView
import com.eatfood.app.suggestfood.features.session.model.SessionInteractor
import com.eatfood.app.suggestfood.lib.eventbus.EventBus
import com.eatfood.app.suggestfood.service.external.feedback.FeedbackReturn
import com.eatfood.app.suggestfood.service.external.feedback.FeedbacksReturn
import org.greenrobot.eventbus.Subscribe

/**
 * Basic implementation of the [FeedbacksPresenter] interface for the presenter in the MVP pattern.
 *
 * @param feedbacksView       the feedbacks view
 * @param eventBus            the event bus pattern
 * @property feedbacksInteractor the feedbacks interactor
 * @property feedbackInteractor the feedback interacto
 * @property sessionInteractor   the session interactor
 * @property accountsInteractor the accounts interactor
 * @constructor Instantiates a new Feedbacks presenter.
 * @see FeedbacksPresenter
 * @see FeedbacksView
 * @see EventBus
 * @see FeedbacksInteractor
 * @see FeedbackInteractor
 * @see SessionInteractor
 * @see AccountsInteractor
 *
 * Created by Usuario on 08/10/2017.
 */
class FeedbacksPresenterImpl(
        feedbacksView: FeedbacksView,
        eventBus: EventBus,
        private val feedbacksInteractor: FeedbacksInteractor,
        private val feedbackInteractor: FeedbackInteractor,
        private val sessionInteractor: SessionInteractor,
        private val accountsInteractor: AccountsInteractor
) : ProgressPresenter<FeedbacksView, FeedbacksEventType>(feedbacksView, eventBus), FeedbacksPresenter {

    private var hasLoadedAllItems = false

    private var isLoading = true

    private var nextPage = 0

    private var feedbackType: Feedback.FeedbackType = Feedback.FeedbackType.COMPLAINT

    override fun onCreate() {
        super.onCreate()
        onViewComplaintsScreen()
        checkPendingFeedbacks()
    }

    override fun doAction(requestCode: Int, callback: () -> Unit) = view?.let {
        if (it.isNetworkConnectivityAvailable) {
            super.doAction(requestCode, callback)
        } else {
            emptyFeedbacks()
            isLoading = false
            it.hideProgress()
            it.onNetworkConnectivityDisabled(requestCode, this)
        }
    }

    override fun onError(event: MessageEvent<FeedbacksEventType>, requestCode: Int) {
        super.onError(event, requestCode)
        isLoading = false
    }

    /*INTERFACES*/
    //1. FeedbacksPresenter

    @Subscribe(sticky = true)
    override fun onMessageEvent(event: MessageEvent<FeedbacksEventType>) = when (event.type) {
        FeedbacksEventType.ON_LOAD_FEEDBACKS_SUCCESS -> {
            onLoadFeedbacksSuccess(event.getData())
        }
        FeedbacksEventType.ON_LOAD_FEEDBACKS_ERROR -> {
            onError(event, FeedbacksPresenter.LOAD_FEEDBACKS_REQUEST_RESOLVED)
        }
        FeedbacksEventType.ON_SAVE_FEEDBACKS_SUCCESS -> addFeedback(event.getData())
        FeedbacksEventType.ON_SAVE_FEEDBACK_ERROR -> {
            onErrorFeedback(event, FeedbacksPresenter.ADD_FEEDBACK_REQUEST_RESOLVED)
        }
    }

    override fun onResolution(requestCode: Int) = when (requestCode) {
        FeedbacksPresenter.LOAD_FEEDBACKS_REQUEST_RESOLVED -> view?.initLoadFeedbacks()
        FeedbacksPresenter.ADD_FEEDBACK_REQUEST_RESOLVED -> {
            val feedback = feedbackInteractor.lastPendingFeedback
            feedback?.let { onAddFeedback(feedback) }
        }
        else -> {
        }
    }

    override fun onViewComplaintsScreen() = view?.let {
        resetPagination()
        this.feedbackType = Feedback.FeedbackType.COMPLAINT
        it.displayComplaintsScreen()
    }

    override fun onViewSuggestionsScreen() = view?.let {
        resetPagination()
        this.feedbackType = Feedback.FeedbackType.SUGGESTION
        it.displaySuggestionsScreen()
    }

    override fun onViewFeedbacksScreen() = view?.let {
        resetPagination()
        this.feedbackType = Feedback.FeedbackType.FEEDBACK
        it.displayFeedbacksScreen()
    }

    override fun onViewNewFeedbackScreen() = view?.displayNewFeedbackScreen(feedbackType)

    override fun initLoadFeedbacks() {
        doAction(FeedbacksPresenter.LOAD_FEEDBACKS_REQUEST_RESOLVED) {
            view?.initLoadFeedbacks()
        }
    }

    override fun onAddFeedback(feedback: Feedback) = view?.let {
        sessionInteractor.session?.let { feedback.user = it.user }
        feedbackInteractor.addPendingFeedback(feedback)
        if (!isLoading()) {
            doAction(FeedbacksPresenter.ADD_FEEDBACK_REQUEST_RESOLVED, false) {
                feedbackInteractor.addFeedback(feedback)
                it.displayMessage(R.string.activity_feedback_saving_feedback)
            }
        } else {
            it.displayPendingFeedbackMessage(feedbackInteractor.totalPendingFeedback)
        }
    }

    //2. PullCallback

    override fun onLoadMore() {
        if (!isLoading())
            loadData(nextPage)
    }

    override fun onRefresh() {
        view?.let {
            it.clearFeedbacks()
            it.hideNoFeedbacksError()
            if (!isLoading())
                loadData(1)
        }
    }

    override fun isLoading() = isLoading

    override fun hasLoadedAllItems() = hasLoadedAllItems

    /*Own Methods*/

    private fun doAction(requestCode: Int, showError: Boolean, callback: () -> Unit) = view?.let{
        if (showError){
            doAction(requestCode, callback)
        } else {
            super.doAction(requestCode, callback)
        }
    }

    private fun checkPendingFeedbacks() = view?.let{
        val totalPendingFeedback = feedbackInteractor.totalPendingFeedback
        if (totalPendingFeedback > 0){
            val session = sessionInteractor.session
            val account = session?.let { accountsInteractor.find(it.email) }
            if (account != null) {
                it.displaySyncPendingFeedbackMessage(totalPendingFeedback)
                it.syncPendingFeedbacks(account)
            }
        }
    }

    private fun onErrorFeedback(event: MessageEvent<FeedbacksEventType>, requestCode: Int){
        val response = event.getData<FeedbackReturn.Failure>()
        if (this.feedbackType == response.feedback.type){
            onError(event, requestCode)
        }
    }

    private fun loadData(page: Int) {
        isLoading = true
        doAction(FeedbacksPresenter.LOAD_FEEDBACKS_REQUEST_RESOLVED) {
            val user = sessionInteractor.session?.user?.target
            user?.let { feedbacksInteractor.loadFeedbacks(feedbackType, page, it) }
        }
    }

    private fun resetPagination() {
        hasLoadedAllItems = false
        isLoading = false
        nextPage = 0
    }

    private fun onLoadFeedbacksSuccess(feedbacksReturn: FeedbacksReturn) = view?.let {
        isLoading = false

        val feedbacks = feedbacksReturn.feedbacks

        if (feedbacks != null && !feedbacks.isEmpty() || !feedbacksReturn.isFirstPage) {
            nextPage = feedbacksReturn.nextPage
            loadFeedbacks(feedbacksReturn.feedbackType, feedbacks ?: listOf())
        } else if (this.feedbackType == feedbacksReturn.feedbackType) {
            emptyFeedbacks()
        }

        it.hideProgress()
    }

    private fun loadFeedbacks(feedbackType: Feedback.FeedbackType?, feedbacks: List<Feedback>) {
        if (this.feedbackType == feedbackType && view != null) {
            view?.loadFeedbacks(feedbacks)
            hasLoadedAllItems = feedbacks.isEmpty()
        }
    }

    private fun addFeedback(feedback: Feedback) {
        feedbackInteractor.removePendingFeedback(feedback.feedbackAssociate.target)
        if (this.feedbackType == feedback.type) {
            view?.let {
                it.hideNoFeedbacksError()
                it.loadFeedbacks(listOf(feedback))
            }
        }
    }

    private fun emptyFeedbacks() {
        view?.displayNoFeedbacksError(feedbackType)
        hasLoadedAllItems = true
    }
}
