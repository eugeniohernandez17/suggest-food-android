package com.eatfood.app.suggestfood.receiver.sync.di

import com.eatfood.app.suggestfood.lib.di.LibsModule
import com.eatfood.app.suggestfood.receiver.sync.SyncReceiver
import dagger.Component
import javax.inject.Singleton

/**
 * Injection component of the Sync receiver.
 *
 * @see LibsModule
 * @see SyncReceiver
 *
 * Created by Usuario on 09/04/2018.
 */
@Singleton
@Component(modules = [(LibsModule::class)])
interface SyncReceiverComponent {

    /**
     * Method for injecting dependencies.
     *
     * @param syncReceiver the sync receiver
     * @see SyncReceiver
     */
    fun inject(syncReceiver: SyncReceiver)

}