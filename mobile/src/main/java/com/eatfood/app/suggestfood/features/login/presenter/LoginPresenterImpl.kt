package com.eatfood.app.suggestfood.features.login.presenter

import com.eatfood.app.suggestfood.domain.evaluator.EmailEvaluator
import com.eatfood.app.suggestfood.domain.evaluator.PasswordEvaluator
import com.eatfood.app.suggestfood.domain.event.MessageEvent
import com.eatfood.app.suggestfood.domain.presenter.FieldsPresenter
import com.eatfood.app.suggestfood.entity.AuthorizedUser
import com.eatfood.app.suggestfood.features.accounts.model.AccountsInteractor
import com.eatfood.app.suggestfood.features.login.LoginEventType
import com.eatfood.app.suggestfood.features.login.model.LoginInteractor
import com.eatfood.app.suggestfood.features.login.view.LoginView
import com.eatfood.app.suggestfood.features.notifications.model.NotificationsInteractor
import com.eatfood.app.suggestfood.features.session.model.SessionInteractor
import com.eatfood.app.suggestfood.lib.eventbus.EventBus
import org.greenrobot.eventbus.Subscribe
import org.greenrobot.eventbus.ThreadMode

/**
 * Basic implementation of the [LoginPresenter] interface
 * for the presenter in the MVP pattern.
 *
 * @param loginView                  the login view
 * @param eventBus                   the event bus pattern
 * @property emailEvaluator          the email evaluator
 * @property passwordEvaluator       the password evaluator
 * @property loginInteractor         the login interactor
 * @property sessionInteractor       the session interactor
 * @property accountsInteractor      the accounts interactor
 * @property notificationsInteractor the notifications interactor
 * @constructor Instantiates a new Login presenter.
 * @see LoginPresenter
 * @see EventBus
 * @see EmailEvaluator
 * @see PasswordEvaluator
 * @see LoginInteractor
 * @see SessionInteractor
 * @see AccountsInteractor
 * @see NotificationsInteractor
 *
 * Created by Usuario on 07/01/2017.
 */
internal class LoginPresenterImpl(
        loginView: LoginView,
        eventBus: EventBus,
        private val emailEvaluator: EmailEvaluator,
        private val passwordEvaluator: PasswordEvaluator,
        private val loginInteractor: LoginInteractor,
        private val sessionInteractor: SessionInteractor,
        private val accountsInteractor: AccountsInteractor,
        private val notificationsInteractor: NotificationsInteractor
) : FieldsPresenter<LoginView, LoginEventType>(loginView, eventBus), LoginPresenter {

    override fun onCreate() {
        super.onCreate()
        view?.let {
            it.loadContacts()
            if (it.isAutoLogin) {
                it.userParam?.let {
                    login(it.email, it.password)
                }
            }
        }
    }

    override fun onError(event: MessageEvent<LoginEventType>, requestCode: Int) {
        super.onError(event, requestCode)
        view?.password = null
    }

    /*INTERFACES*/
    //1. LoginPresenter

    override fun validateLogin() = view?.let {
        val email = it.email
        val password = it.password
        var valid = true

        it.hideKeyboard()

        if (!emailEvaluator.validate(email)) {
            valid = false
            it.showErrorInEmail(emailEvaluator.error)
        } else {
            it.showErrorInEmail(null)
        }

        if (!passwordEvaluator.validate(password)) {
            it.showErrorInPassword(passwordEvaluator.error)
        } else if (password != null) {
            it.showErrorInPassword(null)
            if (valid) login(email, password)
        }
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    override fun onMessageEvent(event: MessageEvent<LoginEventType>) = when (event.type) {
        LoginEventType.ON_SIGN_IN_SUCCESS -> onSignInSuccess(event.getData())
        LoginEventType.ON_SIGN_IN_ERROR -> onError(event, LoginPresenter.LOGIN_REQUEST_RESOLVED)
    }

    override fun onResolution(requestCode: Int) = when (requestCode) {
        LoginPresenter.LOGIN_REQUEST_RESOLVED -> validateLogin()
        else -> {}
    }

    /*Own Methods*/

    private fun login(email: String, password: String) =
        doAction(LoginPresenter.LOGIN_REQUEST_RESOLVED) {
            loginInteractor.doSignIn(email, password)
        }

    private fun onSignInSuccess(authorizedUser: AuthorizedUser) {
        sessionInteractor.session = authorizedUser
        accountsInteractor.create(authorizedUser)
        notificationsInteractor.authorizedUser = authorizedUser
        view?.let {
            it.hideProgress()
            it.navigateToMainScreen()
        }
    }
}
