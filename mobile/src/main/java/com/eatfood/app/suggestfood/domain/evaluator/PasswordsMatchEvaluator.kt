package com.eatfood.app.suggestfood.domain.evaluator

import android.content.Context

import com.eatfood.app.suggestfood.R
import com.eatfood.app.suggestfood.domain.visitor.Evaluate

/**
 * Password evaluator based on the [FieldEvaluator] class.
 * It contains the methods to know if the passwords match.
 *
 * @param context  the context
 * @param evaluate the evaluate object
 * @constructor Instantiates a new Field evaluator.
 * @see FieldEvaluator
 *
 * Created by Usuario on 05/09/2017.
 */
internal class PasswordsMatchEvaluator(context: Context, evaluate: Evaluate)
    : FieldEvaluator<Array<String>>(context, evaluate) {

    override fun validate(value: Array<String>?): Boolean {
        val result = value != null && value.size == 2 && evaluate.match(value[0], value[1], false)
        if (!result) {
            this.errorId = R.string.general_error_not_match_password
        }
        return result
    }

}
