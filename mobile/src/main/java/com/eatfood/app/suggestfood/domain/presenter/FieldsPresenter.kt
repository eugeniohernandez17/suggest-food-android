package com.eatfood.app.suggestfood.domain.presenter

import com.eatfood.app.suggestfood.domain.event.MessageEvent
import com.eatfood.app.suggestfood.domain.view.type.fields.FieldsView
import com.eatfood.app.suggestfood.lib.eventbus.EventBus

/**
 * Support of the presenter for the evaluation of fields in the UI.
 *
 * @constructor Instantiates a new General presenter.
 * @param view     the view
 * @param eventBus the event bus object
 * @see FieldsView
 * @see ProgressPresenter
 * @see EventBus
 *
 * Created by Usuario on 06/09/2017.
 */
abstract class FieldsPresenter<T : FieldsView, E : Any> protected constructor(view: T, eventBus: EventBus)
    : ProgressPresenter<T, E>(view, eventBus) {

    override fun doAction(requestCode: Int, callback: () -> Unit) = super.doAction(requestCode) {
        view!!.disableInputs()
        callback()
    }

    override fun onError(event: MessageEvent<E>, requestCode: Int)
            = super.onError(event, requestCode).also { view?.enableInputs() }

    /*Own Methods*/

    protected fun finishEvent() = view?.let {
        it.enableInputs()
        it.hideProgress()
    }
}
