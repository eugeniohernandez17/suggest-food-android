package com.eatfood.app.suggestfood.features.dashboard.presenter

import com.eatfood.app.suggestfood.domain.presenter.GeneralPresenter
import com.eatfood.app.suggestfood.entity.AuthorizedUser
import com.eatfood.app.suggestfood.entity.User
import com.eatfood.app.suggestfood.features.dashboard.DashboardEventType

/**
 * Basic interface for the presenter in the MVP pattern of the dashboard feature.
 *
 * @see GeneralPresenter
 *
 * Created by Usuario on 07/01/2017.
 */
interface DashboardPresenter : GeneralPresenter<DashboardEventType> {

    /**
     * Method to check if the current session of the user is valid.
     *
     * @see AuthorizedUser.isValid
     */
    fun checkCurrentSession(): Unit?

    /**
     * Method to close the user's current session.
     */
    fun logout()

    /**
     * Event generated by pressing the component to see the user profile.
     */
    fun onViewProfile()

    /**
     * Event generated by pressing the component to see the settings screen.
     */
    fun onViewSettings(): Unit?

    /**
     * Event generated by pressing the component to see the feedbacks screen.
     */
    fun onViewFeedbacks(): Unit?

    /**
     * Event generated when profile showing is finished.
     *
     * @param user the user information updated
     * @see User
     */
    fun onProfileShowingFinish(user: User)

    companion object {

        /**
         * The constant to identify the get user information action.
         *
         * @see GeneralPresenter.onResolution
         */
        const val GET_USER_INFO_REQUEST_CODE = 1
    }

}
