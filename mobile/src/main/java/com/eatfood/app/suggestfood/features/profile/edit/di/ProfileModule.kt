package com.eatfood.app.suggestfood.features.profile.edit.di

import com.eatfood.app.suggestfood.domain.evaluator.PhoneEvaluator
import com.eatfood.app.suggestfood.domain.evaluator.TextEvaluator
import com.eatfood.app.suggestfood.domain.model.callback.GeneralCallback
import com.eatfood.app.suggestfood.features.capture_image.di.CaptureImageModule
import com.eatfood.app.suggestfood.features.entity.di.EntityModule
import com.eatfood.app.suggestfood.features.entity.user.model.UserInteractor
import com.eatfood.app.suggestfood.features.profile.edit.model.callback.UserCallback
import com.eatfood.app.suggestfood.features.profile.edit.presenter.ProfilePresenter
import com.eatfood.app.suggestfood.features.profile.edit.presenter.ProfilePresenterImpl
import com.eatfood.app.suggestfood.features.profile.edit.view.ProfileView
import com.eatfood.app.suggestfood.features.profile.general.ProfilePresenterHelper
import com.eatfood.app.suggestfood.features.session.model.SessionInteractor
import com.eatfood.app.suggestfood.lib.eventbus.EventBus
import com.eatfood.app.suggestfood.service.external.user.UserReturn
import com.google.gson.Gson
import dagger.Module
import dagger.Provides
import java.io.Serializable
import javax.inject.Named

/**
 * Injection dependency of the Profile module.
 *
 * @property view the view
 * @constructor Instantiates a new Profile module.
 * @see ProfileComponent
 * @see EntityModule
 * @see CaptureImageModule
 * @see Module
 * @see ProfileView
 *
 * Created by Usuario on 28/09/2017.
 */
@Module(includes = [(EntityModule::class),(CaptureImageModule::class)])
class ProfileModule(private val view: ProfileView) {

    /**
     * Provide profile presenter profile presenter.
     *
     * @param eventBus          the event bus pattern
     * @param textEvaluator     the text evaluator
     * @param phoneEvaluator    the phone evaluator
     * @param sessionInteractor the session interactor
     * @param userInteractor    the user interactor
     * @return the profile presenter
     * @see EventBus
     * @see TextEvaluator
     * @see PhoneEvaluator
     * @see SessionInteractor
     * @see UserInteractor
     */
    @Provides
    internal fun provideProfilePresenter(eventBus: EventBus,
                                         profileHelper: ProfilePresenterHelper,
                                         textEvaluator: TextEvaluator,
                                         phoneEvaluator: PhoneEvaluator,
                                         sessionInteractor: SessionInteractor,
                                         userInteractor: UserInteractor): ProfilePresenter
        = ProfilePresenterImpl(view, eventBus, profileHelper,
            textEvaluator, phoneEvaluator, sessionInteractor, userInteractor)

    /**
     * Provide profile presenter helper.
     *
     * @return the profile presenter helper
     */
    @Provides
    internal fun provideProfilePresenterHelper() = ProfilePresenterHelper(view)

    /**
     * Provide the user callback for the Profile module.
     *
     * @param eventBus the event bus pattern
     * @param gson     the gson object
     * @return the user callback
     */
    @Provides
    @Named("User_Callback")
    @Suppress("unchecked_cast")
    internal fun provideUserCallback(eventBus: EventBus, gson: Gson)
        = UserCallback<UserReturn.Successful>(eventBus, gson) as GeneralCallback<Serializable, UserReturn.Failure>
}
