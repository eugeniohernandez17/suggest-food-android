package com.eatfood.app.suggestfood.features.signup.di

import com.eatfood.app.suggestfood.features.di.Feature
import com.eatfood.app.suggestfood.features.di.FeaturesComponent
import com.eatfood.app.suggestfood.features.signup.view.SignupActivity
import dagger.Component

/**
 * Injection component of the sign up feature.
 *
 * @see SignupModule
 * @see FeaturesComponent
 * @see SignupActivity
 *
 * Created by Usuario on 06/09/2017.
 */
@Feature
@Component(modules = [(SignupModule::class)], dependencies = [(FeaturesComponent::class)])
internal interface SignupComponent {

    /**
     * Method for injecting dependencies.
     *
     * @param signupActivity the activity
     * @see SignupActivity
     */
    fun inject(signupActivity: SignupActivity)

}
