package com.eatfood.app.suggestfood.features.settings.view.fragments

import android.preference.Preference
import android.preference.PreferenceFragment
import android.preference.PreferenceManager
import android.support.annotation.XmlRes
import android.view.MenuItem

import com.eatfood.app.suggestfood.features.settings.view.SettingsActivity
import com.eatfood.app.suggestfood.prefs.schemas.DefaultPrefs
import org.jetbrains.anko.startActivity

import javax.inject.Inject

/**
 * Basic implementation of support for configuration fragments.
 * Implements the interface [PreferenceView].
 * This class implements the [Preference.OnPreferenceChangeListener] interface
 * to make the actions when the preference's value changes.
 *
 * @see PreferenceFragment
 * @see PreferenceView
 * @see Preference.OnPreferenceChangeListener
 *
 * Created by Usuario on 30/09/2017.
 */
internal abstract class SettingsPreferenceFragment :
        PreferenceFragment(),
        PreferenceView,
        Preference.OnPreferenceChangeListener {

    @Inject
    lateinit var sBindPreferenceSummaryToValueListener: Preference.OnPreferenceChangeListener

    val defaultPrefs: DefaultPrefs
        get() = DefaultPrefs.get(activity)

    override fun onOptionsItemSelected(item: MenuItem) = when (item.itemId) {
        android.R.id.home -> {
            startActivity<SettingsActivity>()
            true
        }
        else -> super.onOptionsItemSelected(item)
    }

    /*INTERFACES*/
    //3. Preference.OnPreferenceChangeListener

    override fun onPreferenceChange(preference: Preference, value: Any): Boolean {
        return sBindPreferenceSummaryToValueListener.onPreferenceChange(preference, value)
    }

    /*ABSTRACT MEHTODS*/

    /**
     * Method that implements the dependency injection configuration.
     */
    protected abstract fun setupInjection()

    /*Own Methods*/

    protected fun onCreated(@XmlRes preferencesResId: Int) {
        addPreferencesFromResource(preferencesResId)
        setHasOptionsMenu(true)
        setupInjection()
    }

    /**
     * Binds a preference's summary to its value. More specifically, when the
     * preference's value is changed, its summary (line of text below the
     * preference title) is updated to reflect the value. The summary is also
     * immediately updated upon calling this method. The exact display format is
     * dependent on the type of preference.
     *
     * @see [sBindPreferenceSummaryToValueListener]
     */
    protected fun bindPreferenceSummaryToValue(preference: Preference) {
        // Set the listener to watch for value changes.
        preference.onPreferenceChangeListener = this

        // Trigger the listener immediately with the preference's
        // current value.
        this.onPreferenceChange(preference,
                PreferenceManager
                        .getDefaultSharedPreferences(preference.context)
                        .getString(preference.key, ""))
    }

}
