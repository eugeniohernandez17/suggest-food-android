package com.eatfood.app.suggestfood.service.internal.authentication

import android.app.Service
import android.content.Intent

/**
 * Service that provides methods for managing app accounts.
 *
 * @see Service
 *
 * Created by Usuario on 31/03/2018.
 */
class AuthenticatorService : Service() {

    private lateinit var accountAuthenticator: AccountAuthenticator

    override fun onCreate() {
        accountAuthenticator = AccountAuthenticator(this)
    }

    override fun onBind(intent: Intent) = accountAuthenticator.iBinder
}
