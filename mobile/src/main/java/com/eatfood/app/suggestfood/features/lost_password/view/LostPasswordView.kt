package com.eatfood.app.suggestfood.features.lost_password.view

import com.eatfood.app.suggestfood.domain.view.fields.EmailValidationView
import com.eatfood.app.suggestfood.domain.view.type.fields.FieldsView

/**
 * Basic interface for the view in the MVP pattern.
 * It includes the methods to validate the email field.
 *
 * @see FieldsView
 * @see EmailValidationView
 *
 * Created by Usuario on 14/09/2017.
 */
interface LostPasswordView : FieldsView, EmailValidationView
