package com.eatfood.app.suggestfood.features.di

import com.eatfood.app.suggestfood.domain.di.DomainComponent
import com.eatfood.app.suggestfood.features.session.di.SessionModule
import com.eatfood.app.suggestfood.features.session.model.SessionInteractor
import com.eatfood.app.suggestfood.service.external.ServiceModule
import com.eatfood.app.suggestfood.service.external.feedback.FeedbacksService
import com.eatfood.app.suggestfood.service.external.user.UserService
import dagger.Component

/**
 * Injection component of the all features in the app.
 *
 * @see SessionModule
 * @see ServiceModule
 * @see DomainComponent
 *
 * Created by Usuario on 22/04/2018.
 */
@Application
@Component(modules = [(SessionModule::class), (ServiceModule::class)], dependencies = [(DomainComponent::class)])
internal interface FeaturesComponent : DomainComponent {

    //Session

    fun provideSessionInteractor(): SessionInteractor

    //Service

    fun provideUserService(): UserService
    fun provideFeedbacksService(): FeedbacksService
}