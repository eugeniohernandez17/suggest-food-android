package com.eatfood.app.suggestfood.domain.model.callback

import com.eatfood.app.suggestfood.domain.error.HttpException
import com.eatfood.app.suggestfood.domain.event.MessageEvent
import com.eatfood.app.suggestfood.lib.eventbus.EventBus
import com.eatfood.app.suggestfood.lib.gson.GsonBuilder
import com.google.gson.Gson
import okhttp3.ResponseBody
import java.io.IOException
import java.io.Serializable

/**
 * Basic implementation of the [Callback] interface.
 *
 * @param <E> the type parameter for the [Callback] interface
 * @param <F> the type parameter for the [Callback] interface
 * @property eventBus   the event bus pattern
 * @property gson       the gson object
 * @property errorClass the error class
 * @constructor Instantiates a new General callback.
 * @see Callback
 * @see Serializable
 * @see EventBus
 * @see GsonBuilder
 *
 * Created by Usuario on 12/01/2017.
 */
abstract class GeneralCallback<E : Serializable, F : Serializable> protected constructor(
        protected val eventBus: EventBus,
        protected val gson: Gson,
        private val errorClass: Class<F>
) : Callback<E, F> {

    /*INTERFACES*/
    //1. Callback

    override fun onFailure(entity: F, httpCode: Int, responseBody: ResponseBody)
        = eventBus.post(buildOnFailureMessage(entity, httpCode, responseBody))

    /*ABSTRACT METHODS*/

    /**
     * Build on failure message event.
     *
     * @return the message event
     */
    protected abstract fun buildOnFailureMessage(): MessageEvent<*>

    /*Own Methods*/

    /**
     * On failure method by case.
     *
     * @param entity       the entity
     * @param httpCode     the http response code
     * @param responseBody the response body
     * @return the message event
     */
    protected fun buildOnFailureMessage(entity: F, httpCode: Int, responseBody: ResponseBody): MessageEvent<*> {
        val event = buildOnFailureMessage()
        when (httpCode) {
            503, 500, 404 -> event.error = HttpException(httpCode)
            else -> onFailure(event, entity, httpCode, responseBody)
        }
        return event
    }

    /**
     * On failure default method.
     *
     * @param event        the event
     * @param entity       the entity
     * @param httpCode     the http response code
     * @param responseBody the response body
     */
    protected fun onFailure(event: MessageEvent<*>, entity: F, httpCode: Int, responseBody: ResponseBody) =
            try {
                val errors = gson.fromJson(responseBody.string(), errorClass)
                event.message = errors.toString()
                event.setData(entity)
            } catch (e: IOException) {
                e.printStackTrace()
            }
}
