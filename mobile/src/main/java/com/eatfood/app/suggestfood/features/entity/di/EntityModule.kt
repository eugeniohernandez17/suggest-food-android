package com.eatfood.app.suggestfood.features.entity.di

import com.eatfood.app.suggestfood.domain.model.callback.GeneralCallback
import com.eatfood.app.suggestfood.entity.User
import com.eatfood.app.suggestfood.features.entity.user.model.UserInteractor
import com.eatfood.app.suggestfood.features.entity.user.model.UserInteractorImpl
import com.eatfood.app.suggestfood.features.entity.user.model.UserRepository
import com.eatfood.app.suggestfood.features.entity.user.model.UserRepositoryImpl
import com.eatfood.app.suggestfood.service.external.user.UserReturn
import com.eatfood.app.suggestfood.service.external.user.UserService
import com.google.firebase.storage.FirebaseStorage
import dagger.Module
import dagger.Provides
import java.io.Serializable
import javax.inject.Named

/**
 * Injection dependency of the Entity module.
 *
 * @see Module
 *
 * Created by Usuario on 22/09/2017.
 */
@Module
object EntityModule {

    /**
     * Provide user interactor.
     *
     * @param userRepository the user repository
     * @return the user interactor
     * @see UserRepository
     */
    @Provides
    @JvmStatic
    internal fun provideUserInteractor(userRepository: UserRepository): UserInteractor
        = UserInteractorImpl(userRepository)

    /**
     * Provide user repository.
     *
     * @param firebaseStorage the firebase storage instance
     * @param userService the user service
     * @return the user repository
     * @see FirebaseStorage
     * @see UserService
     * @see GeneralCallback
     * @see User
     * @see UserReturn
     */
    @Provides
    @JvmStatic
    internal fun provideUserRepository(firebaseStorage: FirebaseStorage,
                                       userService: UserService,
                                       @Named("User_Callback")
                                       userCallback: GeneralCallback<Serializable, UserReturn.Failure>): UserRepository
        = UserRepositoryImpl(firebaseStorage, userService, userCallback)
}
