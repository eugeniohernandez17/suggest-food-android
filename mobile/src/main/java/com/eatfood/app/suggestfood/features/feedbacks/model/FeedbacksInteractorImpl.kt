package com.eatfood.app.suggestfood.features.feedbacks.model

import com.eatfood.app.suggestfood.entity.Feedback
import com.eatfood.app.suggestfood.entity.User

/**
 * Basic implementation of the [FeedbacksInteractor] interface for interactor in the Clean pattern.
 *
 * @param feedbacksRepository the feedbacks repository
 * @constructor Instantiates a new Feedbacks interactor.
 * @see FeedbacksInteractor
 * @see FeedbacksRepository
 *
 * Created by Usuario on 10/10/2017.
 */
class FeedbacksInteractorImpl(private val feedbacksRepository: FeedbacksRepository)
    : FeedbacksInteractor {

    override fun loadFeedbacks(feedbackType: Feedback.FeedbackType, page: Int, user: User)
        = this.feedbacksRepository.loadFeedbacks(feedbackType, page, user)

}
