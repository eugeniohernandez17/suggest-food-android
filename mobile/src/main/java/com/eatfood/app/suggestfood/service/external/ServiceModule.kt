package com.eatfood.app.suggestfood.service.external

import com.eatfood.app.suggestfood.lib.retrofit.Retrofit
import com.eatfood.app.suggestfood.service.external.feedback.FeedbacksService
import com.eatfood.app.suggestfood.service.external.user.UserService
import dagger.Module
import dagger.Provides

/**
 * Injection dependency of the External Services module.
 *
 * @see Module
 *
 * Created by Usuario on 31/08/2017.
 */
@Module
object ServiceModule {

    /**
     * Provide user service.
     *
     * @param retrofit the retrofit object
     * @return the user service
     * @see Retrofit
     */
    @Provides
    @JvmStatic
    internal fun provideUserService(retrofit: Retrofit): UserService
        = retrofit.createService(UserService::class.java)

    /**
     * Provide feedbacks service.
     *
     * @param retrofit the retrofit object
     * @return the feedbacks service
     * @see Retrofit
     */
    @Provides
    @JvmStatic
    internal fun provideFeedbacksService(retrofit: Retrofit): FeedbacksService
        = retrofit.createService(FeedbacksService::class.java)

}
