package com.eatfood.app.suggestfood.service.internal.sync.presenter

import com.eatfood.app.suggestfood.features.feedback.model.FeedbackInteractor
import com.eatfood.app.suggestfood.features.session.model.SessionInteractor
import com.eatfood.app.suggestfood.service.internal.sync.SyncType

/**
 * Basic implementation of the [SyncPresenter] interface for the presenter in the MVP pattern.
 *
 * @property sessionInteractor the session interactor
 * @property feedbackInteractor the feedback interactor
 * @constructor Instantiates a new SyncFeedback presenter.
 * @see SessionInteractor
 * @see FeedbackInteractor
 *
 * Created by Usuario on 04/04/2018.
 */
class SyncPresenterImpl(
        val sessionInteractor: SessionInteractor,
        val feedbackInteractor: FeedbackInteractor
) : SyncPresenter {

    override fun sync(syncType: SyncType) = when(syncType){
        SyncType.FEEDBACKS -> syncFeedbacks()
        SyncType.ALL -> {
            syncFeedbacks()
        }
    }

    /*Own Methods*/

    private fun syncFeedbacks(){
        val total = feedbackInteractor.totalPendingFeedback
        val authorizedUser = sessionInteractor.session
        authorizedUser?.let {
            for (i in 0 until total) {
                val feedback = feedbackInteractor.lastPendingFeedback
                feedback?.let {
                    it.user.target = authorizedUser.user.target
                    feedbackInteractor.addFeedback(it, false)
                }
            }
        }
    }
}