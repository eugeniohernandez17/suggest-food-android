package com.eatfood.app.suggestfood.features.login.model.callback

import com.eatfood.app.suggestfood.domain.event.MessageEvent
import com.eatfood.app.suggestfood.domain.model.callback.GeneralCallback
import com.eatfood.app.suggestfood.entity.AuthorizedUser
import com.eatfood.app.suggestfood.features.login.LoginEventType
import com.eatfood.app.suggestfood.lib.eventbus.EventBus
import com.eatfood.app.suggestfood.lib.httpclient.Authorization
import com.eatfood.app.suggestfood.service.external.user.SignInReturn
import com.google.gson.Gson
import okhttp3.Headers

/**
 * Callback for the different request events in the login process.
 *
 * @constructor Instantiates a new Login callback.
 * @param gson          the gson object
 * @param authorization the authorization method for the requests
 * @see GeneralCallback
 * @see SignInReturn.Successful
 * @see SignInReturn.Failure
 * @see EventBus
 * @see Gson
 * @see Authorization
 * @see AuthorizedUser
 *
 * Created by Usuario on 01/09/2017.
 */
class LoginCallback(
        eventBus: EventBus,
        gson: Gson,
        private val authorization: Authorization<*>
) : GeneralCallback<SignInReturn.Successful, SignInReturn.Failure>(eventBus, gson, SignInReturn.Failure::class.java) {

    override fun buildOnFailureMessage(): MessageEvent<*>
            = MessageEvent(LoginEventType.ON_SIGN_IN_ERROR)

    /*INTERFACES*/
    //1. GeneralCallback

    override fun onSuccessful(entity: SignInReturn.Successful, headers: Headers) {
        val user = entity.user
        val authorizedUser = AuthorizedUser()
        authorizedUser.user.target = user
        authorization.getAuthorizationHeader(headers){
            if (it is AuthorizedUser) {
                authorizedUser.accessToken = it.accessToken
                authorizedUser.client = it.client
                authorizedUser.uid = it.uid
                authorizedUser.expiry = it.expiry
            }
        }
        eventBus.post(MessageEvent(LoginEventType.ON_SIGN_IN_SUCCESS, authorizedUser))
    }

    override fun onError(entity: SignInReturn.Failure, throwable: Throwable)
        = eventBus.post(MessageEvent(LoginEventType.ON_SIGN_IN_ERROR, throwable))
}
