package com.eatfood.app.suggestfood.features.feedbacks.view

import android.accounts.Account
import android.os.Bundle
import android.support.design.widget.BottomNavigationView
import android.view.MenuItem
import com.eatfood.app.suggestfood.R
import com.eatfood.app.suggestfood.content_provider.di.DatabaseModule
import com.eatfood.app.suggestfood.domain.di.DaggerDomainComponent
import com.eatfood.app.suggestfood.domain.di.DomainModule
import com.eatfood.app.suggestfood.domain.resolution.di.ResolutionModule
import com.eatfood.app.suggestfood.domain.view.GeneralActivity
import com.eatfood.app.suggestfood.domain.view.helper.MessageHelper
import com.eatfood.app.suggestfood.entity.Feedback
import com.eatfood.app.suggestfood.features.authorization.di.DaggerAuthorizationComponent
import com.eatfood.app.suggestfood.features.di.DaggerFeaturesComponent
import com.eatfood.app.suggestfood.features.feedback.di.FeedbackModule
import com.eatfood.app.suggestfood.features.feedback.view.FeedbackActivity
import com.eatfood.app.suggestfood.features.feedbacks.di.DaggerFeedbacksComponent
import com.eatfood.app.suggestfood.features.feedbacks.di.FeedbacksModule
import com.eatfood.app.suggestfood.features.feedbacks.presenter.FeedbacksPresenter
import com.eatfood.app.suggestfood.features.feedbacks.view.lists.FeedbackFragment
import com.eatfood.app.suggestfood.lib.di.LibsModule
import com.eatfood.app.suggestfood.service.internal.sync.Sync
import com.eatfood.app.suggestfood.service.internal.sync.SyncType
import kotlinx.android.synthetic.main.activity_feedbacks.*

/**
 * A window with navigation support for lists of types of feedbacks.
 * Implements the interface [FeedbacksView].
 *
 * @see FeedbacksView
 * @see GeneralActivity
 * @see FeedbacksPresenter
 *
 * Created by Usuario on 08/10/2017.
 */
internal class FeedbacksActivity : GeneralActivity<FeedbacksPresenter>(), FeedbacksView,
        BottomNavigationView.OnNavigationItemSelectedListener,
        FeedbackFragment.OnListFragmentInteractionListener,
        FeedbackActivity.OnInteractionListener {

    private lateinit var fragment: FeedbackFragment

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        super.onCreate(R.layout.activity_feedbacks, savedInstanceState)
    }

    override fun config() {
        super.config()
        setupNavigation()
    }

    override fun setupInjection() {
        val databaseModule = DatabaseModule(this)
        val authorization = DaggerAuthorizationComponent.builder()
                .databaseModule(databaseModule)
                .build().provideAuthorization()
        val domainComponent = DaggerDomainComponent.builder()
                .libsModule(LibsModule(this, authorization))
                .resolutionModule(ResolutionModule(this))
                .domainModule(DomainModule(this))
                .build()
        val featuresComponent = DaggerFeaturesComponent.builder()
                .domainComponent(domainComponent)
                .databaseModule(databaseModule)
                .build()
        DaggerFeedbacksComponent.builder()
                .featuresComponent(featuresComponent)
                .databaseModule(databaseModule)
                .feedbackModule(FeedbackModule(this))
                .feedbacksModule(FeedbacksModule(this))
                .build().inject(this)
    }

    /*INTERFACES*/
    //1. FeedbacksView

    override fun displayMessage(message: String) = MessageHelper.display(this, message)

    override fun displayComplaintsScreen() = showFeedbackFragment()

    override fun displaySuggestionsScreen() = showFeedbackFragment()

    override fun displayFeedbacksScreen() = showFeedbackFragment()

    override fun displayNewFeedbackScreen(type: Feedback.FeedbackType) {
        FeedbackActivity(this, this).displayNewFeedbackScreen(type)
    }

    override fun initLoadFeedbacks() = fragment.initLoad()

    override fun displayPendingFeedbackMessage(total: Int) {
        val numberOfFeedbacksPending = getQuantityString(R.plurals.numberOfFeedbacksPending, total)
        val message = getString(R.string.activity_feedback_pending_feedbacks_message, numberOfFeedbacksPending)
        displayMessage(message)
    }

    override fun displaySyncPendingFeedbackMessage(total: Int) {
        val numberOfFeedbacksPending = getQuantityString(R.plurals.numberOfFeedbacksPending, total)
        val message = getString(R.string.activity_feedback_pending_feedbacks_message_sync, numberOfFeedbacksPending)
        displayMessage(message)
    }

    override fun loadFeedbacks(feedbacks: List<Feedback>) = fragment.loadData(feedbacks)

    override fun clearFeedbacks() = fragment.clearData()

    override fun displayNoFeedbacksError(feedbackType: Feedback.FeedbackType) = when(feedbackType){
        Feedback.FeedbackType.COMPLAINT
            -> fragment.displayNoFeedbacks(R.string.activity_feedbacks_complaints_empty, R.drawable.ic_complaint_24dp)
        Feedback.FeedbackType.SUGGESTION
            -> fragment.displayNoFeedbacks(R.string.activity_feedbacks_suggestion_empty, R.drawable.ic_suggestion_24dp)
        Feedback.FeedbackType.FEEDBACK
            -> fragment.displayNoFeedbacks(R.string.activity_feedbacks_feedbacks_empty, R.drawable.ic_feedback_24dp)
    }

    override fun hideNoFeedbacksError() = fragment.hideNoFeedbacks()

    override fun syncPendingFeedbacks(account: Account) = Sync.request(this, account, SyncType.FEEDBACKS)

    //1.1 ProgressView

    /**
     * The function to show the progress is automatic and
     * is shown with the fragment attached to the view.
     *
     * @see FeedbackFragment.initLoad
     * @see [onLoadMore]
     */
    @Deprecated("", ReplaceWith("Unit"))
    override fun showProgress() = Unit

    override fun hideProgress() = fragment.setComplete()

    //2. BottomNavigationView.OnNavigationItemSelectedListener

    override fun onNavigationItemSelected(item: MenuItem) = when (item.itemId) {
        R.id.nav_complaints -> {
            presenter.onViewComplaintsScreen()
            true
        }
        R.id.nav_suggestions -> {
            presenter.onViewSuggestionsScreen()
            true
        }
        R.id.nav_feedbacks -> {
            presenter.onViewFeedbacksScreen()
            true
        }
        else -> false
    }

    //3. FeedbackFragment.OnListFragmentInteractionListener

    override fun initLoad() = presenter.initLoadFeedbacks()

    override fun onLoadMore() = presenter.onLoadMore()

    override fun onRefresh() = presenter.onRefresh()

    override fun isLoading() = presenter.isLoading

    override fun hasLoadedAllItems() = presenter.hasLoadedAllItems()

    override fun onSelect(item: Feedback) {
        TODO("Not implemented yet")
    }

    override fun onAddFeedback() = presenter.onViewNewFeedbackScreen()

    //4. FeedbackActivity.OnInteractionListener

    override fun onResult(feedbackAction: FeedbackActivity.FeedbackAction, feedback: Feedback) {
        when (feedbackAction) {
            FeedbackActivity.FeedbackAction.ADD -> presenter.onAddFeedback(feedback)
            FeedbackActivity.FeedbackAction.SHOW -> { }
        }
    }

    /*Own Methods*/

    private fun setupNavigation() = navigation.setOnNavigationItemSelectedListener(this)

    private fun showFeedbackFragment() {
        this.fragment = FeedbackFragment()
        showFragment(R.id.content, fragment)
    }

}
