package com.eatfood.app.suggestfood.features.lost_password.presenter

import com.eatfood.app.suggestfood.domain.presenter.GeneralPresenter
import com.eatfood.app.suggestfood.features.lost_password.LostPasswordEventType

/**
 * Basic interface for the presenter in the MVP pattern of the lost password feature.
 *
 * @see GeneralPresenter
 * @see LostPasswordEventType
 *
 * Created by Usuario on 14/09/2017.
 */
interface LostPasswordPresenter : GeneralPresenter<LostPasswordEventType> {

    /**
     * Method that will validate the user's email and generate the lost password process.
     */
    fun validLostPassword(): Unit?

    companion object {

        /**
         * The constant to identify the [validLostPassword] action.
         *
         * @see GeneralPresenter.onResolution
         */
        const val LOST_PASSWORD_REQUEST_RESOLVED = 1
    }

}
