package com.eatfood.app.suggestfood.features.signup.model

import com.eatfood.app.suggestfood.domain.model.callback.Callback
import com.eatfood.app.suggestfood.entity.User
import com.eatfood.app.suggestfood.service.external.user.SignUpReturn
import com.eatfood.app.suggestfood.service.external.user.UserService

import retrofit2.Call
import retrofit2.Response

/**
 * Basic implementation of the [SignupRepository] for the repository in the Clean pattern.
 *
 * @property userService  the user service
 * @property userCallback the user callback
 * @constructor Instantiates a new Signup repository.
 * @see SignupRepository
 * @see UserService
 * @see SignUpReturn
 * @see Callback
 *
 * Created by Usuario on 06/09/2017.
 */
class SignupRepositoryImpl(
        private val userService: UserService,
        private val userCallback: Callback<SignUpReturn.Successful, SignUpReturn.Failure>
) : SignupRepository {

    override fun signup(user: User) {
        userService.signUp(user).enqueue(object : retrofit2.Callback<SignUpReturn.Successful> {
            override fun onResponse(call: Call<SignUpReturn.Successful>,
                                    response: Response<SignUpReturn.Successful>) =
                    if (response.isSuccessful) {
                        val data = response.body()!!
                        val userResponse = data.user
                        userResponse.password = user.password
                        userCallback.onSuccessful(data, response.headers())
                    } else {
                        userCallback.onFailure(SignUpReturn.Failure(user), response.code(),
                                response.errorBody()!!)
                    }

            override fun onFailure(call: Call<SignUpReturn.Successful>, throwable: Throwable) {
                userCallback.onError(SignUpReturn.Failure(user), throwable)
            }
        })
    }
}
