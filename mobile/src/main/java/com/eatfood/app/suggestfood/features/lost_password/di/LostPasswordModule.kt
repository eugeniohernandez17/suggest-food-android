package com.eatfood.app.suggestfood.features.lost_password.di

import com.eatfood.app.suggestfood.domain.evaluator.EmailEvaluator
import com.eatfood.app.suggestfood.domain.model.callback.Callback
import com.eatfood.app.suggestfood.features.lost_password.model.LostPasswordInteractor
import com.eatfood.app.suggestfood.features.lost_password.model.LostPasswordInteractorImpl
import com.eatfood.app.suggestfood.features.lost_password.model.LostPasswordRepository
import com.eatfood.app.suggestfood.features.lost_password.model.LostPasswordRepositoryImpl
import com.eatfood.app.suggestfood.features.lost_password.model.callback.LostPasswordCallback
import com.eatfood.app.suggestfood.features.lost_password.presenter.LostPasswordPresenter
import com.eatfood.app.suggestfood.features.lost_password.presenter.LostPasswordPresenterImpl
import com.eatfood.app.suggestfood.features.lost_password.view.LostPasswordView
import com.eatfood.app.suggestfood.lib.eventbus.EventBus
import com.eatfood.app.suggestfood.service.external.user.LostPasswordReturn
import com.eatfood.app.suggestfood.service.external.user.UserReturn
import com.eatfood.app.suggestfood.service.external.user.UserService
import com.google.gson.Gson
import dagger.Module
import dagger.Provides
import javax.inject.Named

/**
 * Injection dependency of the Lost password module.
 *
 * @property view the view
 * @constructor Instantiates a new Lost password module.
 * @see LostPasswordComponent
 * @see Module
 * @see LostPasswordView
 *
 * Created by Usuario on 18/09/2017.
 */
@Module
class LostPasswordModule(private val view: LostPasswordView) {

    /**
     * Provide lost password presenter.
     *
     * @param eventBus               the event bus pattern
     * @param emailEvaluator         the email evaluator
     * @param lostPasswordInteractor the lost password interactor
     * @return the lost password presenter
     * @see EventBus
     * @see EmailEvaluator
     * @see LostPasswordInteractor
     */
    @Provides
    internal fun provideLostPasswordPresenter(eventBus: EventBus,
                                              emailEvaluator: EmailEvaluator,
                                              lostPasswordInteractor: LostPasswordInteractor): LostPasswordPresenter
        = LostPasswordPresenterImpl(view, eventBus, emailEvaluator, lostPasswordInteractor)

    /**
     * Provide lost password interactor.
     *
     * @param lostPasswordRepository the lost password repository
     * @return the lost password interactor
     * @see LostPasswordRepository
     */
    @Provides
    internal fun provideLostPasswordInteractor(lostPasswordRepository: LostPasswordRepository): LostPasswordInteractor
        = LostPasswordInteractorImpl(lostPasswordRepository)

    /**
     * Provide lost password repository.
     *
     * @param userService  the user service
     * @param userCallback the user callback
     * @return the lost password repository
     * @see UserService
     * @see Callback
     */
    @Provides
    internal fun provideLostPasswordRepository(userService: UserService,
                                               @Named("LostPasswordCallback")
                                               userCallback: Callback<LostPasswordReturn.Successful, UserReturn.Failure>): LostPasswordRepository
        = LostPasswordRepositoryImpl(userService, userCallback)

    /**
     * Provide lost password callback callback.
     *
     * @param eventBus the event bus pattern
     * @param gson     the gson object
     * @return the callback
     * @see EventBus
     * @see Gson
     */
    @Provides
    @Named("LostPasswordCallback")
    internal fun provideLostPasswordCallback(eventBus: EventBus,
                                             gson: Gson): Callback<LostPasswordReturn.Successful, UserReturn.Failure>
        = LostPasswordCallback(eventBus, gson)
}
