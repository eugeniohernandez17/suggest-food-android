package com.eatfood.app.suggestfood.lib.gson

import com.google.gson.FieldAttributes
import com.google.gson.annotations.Expose

/**
 * Class to exclude fields in serialization or deserialization.
 *
 * @see com.google.gson.ExclusionStrategy
 * @see SerializationExclusionStrategy
 * @see DeserializationExclusionStrategy
 *
 * Created by Usuario on 25/09/2017.
 */
internal abstract class ExclusionStrategy : com.google.gson.ExclusionStrategy {

    override fun shouldSkipField(fieldAttributes: FieldAttributes): Boolean {
        val expose = fieldAttributes.getAnnotation(Expose::class.java)
        return expose != null && shouldSkipField(expose)
    }

    override fun shouldSkipClass(clazz: Class<*>): Boolean = false

    /*ABSTRACT METHODS*/

    /**
     * Method that indicates whether the field should be ignored
     * in serialization or deserialization.
     *
     * @param expose the expose annotation of the field that is under test
     * @return true if the field should be ignored; otherwise false
     */
    internal abstract fun shouldSkipField(expose: Expose): Boolean
}
