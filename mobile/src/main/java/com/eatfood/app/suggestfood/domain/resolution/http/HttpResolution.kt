package com.eatfood.app.suggestfood.domain.resolution.http

import com.eatfood.app.suggestfood.domain.presenter.GeneralPresenter

/**
 * Basic interface for troubleshooting errors due to http request errors.
 *
 * Created by Usuario on 12/09/2017.
 */
interface HttpResolution {

    /**
     * Method to notify about errors in http request.
     *
     * @param error     the error object
     * @param code      the request code
     * @param presenter the presenter
     * @see GeneralPresenter
     */
    fun onException(error: Throwable?, code: Int, presenter: GeneralPresenter<*>)

}
