package com.eatfood.app.suggestfood.lib.retrofit

import com.google.gson.Gson

import okhttp3.OkHttpClient
import retrofit2.converter.gson.GsonConverterFactory

/**
 * Basic implementation of the external service builder.
 *
 * @param baseUrl      the base url of the api service
 * @param gson         the gson object
 * @param okHttpClient the http client
 * @constructor Instantiates a new [Retrofit].
 * @see Retrofit
 * @see Gson
 * @see OkHttpClient
 *
 * Created by Usuario on 13/01/2017.
 */
class RetrofitImpl(baseUrl: String, gson: Gson, okHttpClient: OkHttpClient) : Retrofit {

    private val retrofit = retrofit2.Retrofit.Builder()
            .baseUrl(baseUrl)
            .addConverterFactory(GsonConverterFactory.create(gson))
            .client(okHttpClient)
            .build()

    override fun <T> createService(classService: Class<T>): T = retrofit.create(classService)

}
