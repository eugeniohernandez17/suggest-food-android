package com.eatfood.app.suggestfood.features.login.model

import com.eatfood.app.suggestfood.entity.AuthorizedUser

/**
 * Basic interface for the interactor in the Clean pattern of the login feature.
 *
 * Created by Usuario on 07/01/2017.
 */
interface LoginInteractor {

    /**
     * Method that performs the login process.
     *
     * @param email    the user email
     * @param password the user password
     * @see AuthorizedUser
     */
    fun doSignIn(email: String, password: String)

}
