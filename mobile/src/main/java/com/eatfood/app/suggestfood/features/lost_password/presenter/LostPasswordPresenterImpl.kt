package com.eatfood.app.suggestfood.features.lost_password.presenter

import com.eatfood.app.suggestfood.domain.evaluator.EmailEvaluator
import com.eatfood.app.suggestfood.domain.event.MessageEvent
import com.eatfood.app.suggestfood.domain.presenter.FieldsPresenter
import com.eatfood.app.suggestfood.features.lost_password.LostPasswordEventType
import com.eatfood.app.suggestfood.features.lost_password.model.LostPasswordInteractor
import com.eatfood.app.suggestfood.features.lost_password.view.LostPasswordView
import com.eatfood.app.suggestfood.lib.eventbus.EventBus
import org.greenrobot.eventbus.Subscribe

/**
 * Basic implementation of the [LostPasswordPresenter] interface
 * for the presenter in the MVP pattern.
 *
 * @param view                   the view
 * @param eventBus               the event bus object
 * @property emailEvaluator         the email evaluator
 * @property lostPasswordInteractor the lost password interactor
 * @constructor Instantiates a new Lost Password presenter.
 * @see LostPasswordPresenter
 * @see LostPasswordView
 * @see EventBus
 * @see EmailEvaluator
 * @see LostPasswordInteractor
 *
 * Created by Usuario on 14/09/2017.
 */
internal class LostPasswordPresenterImpl(
        view: LostPasswordView,
        eventBus: EventBus,
        private val emailEvaluator: EmailEvaluator,
        private val lostPasswordInteractor: LostPasswordInteractor
) : FieldsPresenter<LostPasswordView, LostPasswordEventType>(view, eventBus), LostPasswordPresenter {

    override fun validLostPassword() = view?.let {
        val email = it.email

        it.hideKeyboard()

        if (!emailEvaluator.validate(email)) {
            it.showErrorInEmail(emailEvaluator.error)
        } else {
            it.showErrorInEmail(null)
            doAction(LostPasswordPresenter.LOST_PASSWORD_REQUEST_RESOLVED) {
                lostPasswordInteractor.doRecoverPassword(email)
            }
        }

    }

    @Subscribe
    override fun onMessageEvent(event: MessageEvent<LostPasswordEventType>) = when (event.type) {
        LostPasswordEventType.ON_LOST_PASSWORD_SUCCESS -> onLostPasswordSuccess(event.message)
        LostPasswordEventType.ON_LOST_PASSWORD_ERROR -> onLostPasswordError(event)
    }

    override fun onResolution(requestCode: Int) = when (requestCode) {
        LostPasswordPresenter.LOST_PASSWORD_REQUEST_RESOLVED -> validLostPassword()
        else -> {
        }
    }

    /*Own Methods*/

    private fun onLostPasswordSuccess(message: String?) = view?.let {
        finishEvent()
        it.displayMessage(message ?: "")
    }

    private fun onLostPasswordError(event: MessageEvent<LostPasswordEventType>) = view?.let {
        finishEvent()

        if (event.isSimpleMessage)
            it.displayError(event.message ?: "")
        else
            it.displayError(event.error, LostPasswordPresenter.LOST_PASSWORD_REQUEST_RESOLVED)
    }
}
