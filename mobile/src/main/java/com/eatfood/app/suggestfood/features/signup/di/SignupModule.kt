package com.eatfood.app.suggestfood.features.signup.di

import com.eatfood.app.suggestfood.domain.evaluator.EmailEvaluator
import com.eatfood.app.suggestfood.domain.evaluator.PasswordEvaluator
import com.eatfood.app.suggestfood.domain.evaluator.PasswordsMatchEvaluator
import com.eatfood.app.suggestfood.domain.model.callback.Callback
import com.eatfood.app.suggestfood.features.signup.model.SignupInteractor
import com.eatfood.app.suggestfood.features.signup.model.SignupInteractorImpl
import com.eatfood.app.suggestfood.features.signup.model.SignupRepository
import com.eatfood.app.suggestfood.features.signup.model.SignupRepositoryImpl
import com.eatfood.app.suggestfood.features.signup.model.callback.SignupCallback
import com.eatfood.app.suggestfood.features.signup.presenter.SignupPresenter
import com.eatfood.app.suggestfood.features.signup.presenter.SignupPresenterImpl
import com.eatfood.app.suggestfood.features.signup.view.SignupView
import com.eatfood.app.suggestfood.lib.eventbus.EventBus
import com.eatfood.app.suggestfood.service.external.user.SignUpReturn
import com.eatfood.app.suggestfood.service.external.user.UserService
import com.google.gson.Gson
import dagger.Module
import dagger.Provides
import javax.inject.Named

/**
 * Injection dependency of the Signup module.
 *
 * @property view the view
 * @constructor Instantiates a new Signup module.
 * @see SignupComponent
 * @see SignupView
 *
 * Created by Usuario on 06/09/2017.
 */
@Module
class SignupModule(private val view: SignupView) {

    /**
     * Provide signup presenter.
     *
     * @param eventBus                the event bus pattern
     * @param emailEvaluator          the email evaluator
     * @param passwordEvaluator       the password evaluator
     * @param passwordsMatchEvaluator the passwords match evaluator
     * @param signupInteractor        the signup interactor
     * @return the signup presenter
     * @see EventBus
     * @see EmailEvaluator
     * @see PasswordEvaluator
     * @see PasswordsMatchEvaluator
     * @see SignupInteractor
     */
    @Provides
    internal fun provideSignupPresenter(eventBus: EventBus,
                                        emailEvaluator: EmailEvaluator,
                                        passwordEvaluator: PasswordEvaluator,
                                        passwordsMatchEvaluator: PasswordsMatchEvaluator,
                                        signupInteractor: SignupInteractor): SignupPresenter
        = SignupPresenterImpl(view, eventBus, emailEvaluator, passwordEvaluator,
                passwordsMatchEvaluator, signupInteractor)

    /**
     * Provide signup interactor.
     *
     * @param signupRepository the signup repository
     * @return the signup interactor
     * @see SignupRepository
     */
    @Provides
    fun provideSignupInteractor(signupRepository: SignupRepository): SignupInteractor
        = SignupInteractorImpl(signupRepository)

    /**
     * Provide signup repository.
     *
     * @param userService  the user service
     * @param userCallback the user callback
     * @return the signup repository
     * @see UserService
     * @see Callback
     */
    @Provides
    fun provideSignupRepository(userService: UserService,
                                @Named("SignupCallback")
                                userCallback: Callback<SignUpReturn.Successful, SignUpReturn.Failure>): SignupRepository
        = SignupRepositoryImpl(userService, userCallback)

    /**
     * Provide signup callback callback.
     *
     * @param eventBus the event bus pattern
     * @param gson     the gson object
     * @return the callback
     * @see EventBus
     * @see Gson
     */
    @Provides
    @Named("SignupCallback")
    fun provideSignupCallback(eventBus: EventBus,
                              gson: Gson): Callback<SignUpReturn.Successful, SignUpReturn.Failure>
        = SignupCallback(eventBus, gson)

}
