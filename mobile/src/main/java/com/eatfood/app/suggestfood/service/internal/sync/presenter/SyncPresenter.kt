package com.eatfood.app.suggestfood.service.internal.sync.presenter

import com.eatfood.app.suggestfood.service.internal.sync.SyncType

/**
 * Basic interface for the presenter in the MVP pattern of the sync feature.
 *
 * Created by Usuario on 04/04/2018.
 */
interface SyncPresenter {

    /**
     * Method that executes the synchronization process determined by the action.
     *
     * @param syncType the type of action
     * @see SyncType
     */
    fun sync(syncType: SyncType)

}