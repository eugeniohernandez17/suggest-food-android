package com.eatfood.app.suggestfood.features.feedback.view

import com.eatfood.app.suggestfood.entity.Feedback

/**
 * Basic interface for the view in the MVP pattern.
 *
 * Created by Usuario on 16/03/2018.
 */
interface FeedbackView {

    /**
     * Method to display the screen for creating a new feedback.
     *
     * @param type the feedback type
     */
    fun displayNewFeedbackScreen(type: Feedback.FeedbackType)

}