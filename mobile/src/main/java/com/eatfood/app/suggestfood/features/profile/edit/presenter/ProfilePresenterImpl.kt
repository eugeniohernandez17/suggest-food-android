package com.eatfood.app.suggestfood.features.profile.edit.presenter

import android.graphics.Bitmap
import android.net.Uri
import com.eatfood.app.suggestfood.domain.evaluator.PhoneEvaluator
import com.eatfood.app.suggestfood.domain.evaluator.TextEvaluator
import com.eatfood.app.suggestfood.domain.event.MessageEvent
import com.eatfood.app.suggestfood.domain.presenter.FieldsPresenter
import com.eatfood.app.suggestfood.features.entity.user.model.UserInteractor
import com.eatfood.app.suggestfood.features.profile.edit.ProfileEventType
import com.eatfood.app.suggestfood.features.profile.edit.view.ProfileView
import com.eatfood.app.suggestfood.features.profile.general.ProfilePresenterHelper
import com.eatfood.app.suggestfood.features.session.model.SessionInteractor
import com.eatfood.app.suggestfood.lib.eventbus.EventBus
import com.eatfood.app.suggestfood.service.external.user.UserReturn
import org.greenrobot.eventbus.Subscribe

/**
 * Basic implementation of the [ProfilePresenter] interface for the presenter in the MVP pattern.
 *
 * @param view              the view
 * @param eventBus          the event bus object
 * @property profileHelper     the profile helper
 * @property textEvaluator     the text evaluator
 * @property phoneEvaluator    the phone evaluator
 * @property sessionInteractor the session interactor
 * @property userInteractor    the user interactor
 * @constructor Instantiates a new Profile presenter.
 * @see ProfilePresenter
 * @see ProfileView
 * @see EventBus
 * @see ProfilePresenterHelper
 * @see TextEvaluator
 * @see PhoneEvaluator
 * @see SessionInteractor
 * @see UserInteractor
 *
 * Created by Usuario on 25/09/2017.
 */
internal class ProfilePresenterImpl(
        view: ProfileView,
        eventBus: EventBus,
        private val profileHelper: ProfilePresenterHelper,
        private val textEvaluator: TextEvaluator,
        private val phoneEvaluator: PhoneEvaluator,
        private val sessionInteractor: SessionInteractor,
        private val userInteractor: UserInteractor
) : FieldsPresenter<ProfileView, ProfileEventType>(view, eventBus), ProfilePresenter {

    override fun onCreate() {
        super.onCreate()
        profileHelper.loadProfile()
    }

    override fun updateProfile() = view?.let {
        val name = it.name
        val phone = it.phone

        var valid = true

        if (!textEvaluator.validate(name)) {
            valid = false
            it.showErrorInName(textEvaluator.error)
        } else {
            it.showErrorInName(null)
        }

        if (!phoneEvaluator.validate(phone)) {
            it.showErrorInPhone(phoneEvaluator.error)
        } else {
            it.showErrorInPhone(null)
            if (valid) updateProfile(name, phone)
        }
    }

    override fun onViewProfile() = view?.let {
        val user = profileHelper.userParam

        if (user != null) {
            it.navigateToProfileScreen(user)
        }
    }

    override fun onEditProfileImage() = view?.displayOptionsToCaptureImage()

    override fun onEditProfileImage(imageUri: Uri, imageBitMap: Bitmap?) = view?.let {
        sessionInteractor.session?.let {  userInteractor.updateUser(it, imageUri, imageBitMap) }
        it.showProgress()
    }

    @Subscribe
    override fun onMessageEvent(event: MessageEvent<ProfileEventType>) = when (event.type) {
        ProfileEventType.ON_UPDATE_PROFILE_SUCCESS -> onUpdateProfileSuccess(event)
        ProfileEventType.ON_UPDATE_PROFILE_ERROR -> onError(event, ProfilePresenter.UPDATE_PROFILE_REQUEST_RESOLVED)
    }

    override fun onResolution(requestCode: Int) = when (requestCode) {
        ProfilePresenter.UPDATE_PROFILE_REQUEST_RESOLVED -> updateProfile()
        else -> {
        }
    }

    /*Own Methods*/

    private fun updateProfile(name: String, phone: String) {
        doAction(ProfilePresenter.UPDATE_PROFILE_REQUEST_RESOLVED) {
            sessionInteractor.session?.let {
                val user = it.user.target
                user.name = name
                user.phone = phone
                userInteractor.updateUser(it)
            }
        }
    }

    private fun onUpdateProfileSuccess(event: MessageEvent<ProfileEventType>) {
        val user = event.getData<UserReturn.Successful>().user
        sessionInteractor.updateUserInfo(user)
        view?.let {
            finishEvent()
            it.userParam = user
            it.displayMessage(event.message ?: "")
            it.loadProfile(user)
        }
    }
}
