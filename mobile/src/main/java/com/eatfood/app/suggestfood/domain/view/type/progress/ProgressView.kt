package com.eatfood.app.suggestfood.domain.view.type.progress

import com.eatfood.app.suggestfood.domain.view.GeneralView

/**
 * Basic interface for views in the MVP pattern they allow to show or hide the progress of a task.
 *
 * @see GeneralView
 *
 * Created by Usuario on 22/09/2017.
 */
interface ProgressView : GeneralView {

    /**
     * Displays the progress of the executed action in the background.
     */
    fun showProgress()

    /**
     * Hides the progress of the executed action in the background.
     * It is executed at the end of the associated action.
     */
    fun hideProgress()

}
