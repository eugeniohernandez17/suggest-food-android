package com.eatfood.app.suggestfood.config

import android.content.Context
import android.support.multidex.MultiDex
import com.crashlytics.android.Crashlytics
import com.eatfood.app.suggestfood.lib.objectbox.ObjectboxBuilder
import io.fabric.sdk.android.Fabric
import io.objectbox.BoxStore

/**
 * Base class for maintaining global application state.
 *
 * Created by Usuario on 08/01/2017.
 */
class Application : android.app.Application() {

    private lateinit var boxStore: BoxStore

    override fun onCreate() {
        super.onCreate()
        Fabric.with(this, Crashlytics())
        boxStore = ObjectboxBuilder.build(this, "suggest-food-db")
        OneSignal.setup(this)
    }

    override fun attachBaseContext(base: Context) {
        super.attachBaseContext(base)
        MultiDex.install(this)
    }

    companion object {

        /*STATIC METHODS*/

        /**
         * Method for obtaining the connection to the local database.
         *
         * @param context the application context
         * @return the connection to the local database
         * @see Context
         * @see BoxStore
         */
        fun getBoxStore(context: Context) = (context.applicationContext as Application).boxStore
    }
}
