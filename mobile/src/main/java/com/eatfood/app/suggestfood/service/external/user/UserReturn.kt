package com.eatfood.app.suggestfood.service.external.user

import com.eatfood.app.suggestfood.entity.User
import com.eatfood.app.suggestfood.service.external.helper.ErrorHelper

import java.io.Serializable

/**
 * Created by Usuario on 24/09/2017.
 */

open class UserReturn private constructor(val user: User) {

    open class Successful(user: User, var message: String?) : UserReturn(user), Serializable

    class Failure(user: User) : UserReturn(user), Serializable {

        var errors: List<String>? = null

        override fun toString(): String = ErrorHelper.getError(errors)
    }
}
