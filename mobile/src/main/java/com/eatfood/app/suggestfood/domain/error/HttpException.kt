package com.eatfood.app.suggestfood.domain.error

/**
 * Exception for an unexpected, non-2xx HTTP response.
 *
 * @property code the http code
 * @constructor Instantiates a new Http exception.
 * @see Exception
 *
 * Created by Usuario on 12/09/2017.
 */
class HttpException(val code: Int) : Exception() {

    override fun equals(other: Any?): Boolean = when(other){
        is HttpException -> code == other.code
        else -> false
    }

    override fun hashCode(): Int = code
}
