package com.eatfood.app.suggestfood.config

/**
 * Custom text fonts.
 *
 * @property nameFile the name of file in the assets folder.
 * @constructor Instantiates a new constant [Font].
 *
 * Created by Usuario on 17/02/2017.
 */
enum class Font constructor(private val nameFile: String) {

    /**
     * Trajan bold font.
     */
    TRAJAN_BOLD("TrajanBold.ttf");

    override fun toString(): String = nameFile

}
