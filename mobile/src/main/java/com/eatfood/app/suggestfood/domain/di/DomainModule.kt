package com.eatfood.app.suggestfood.domain.di

import android.accounts.AccountManager
import android.content.Context
import android.preference.Preference
import com.eatfood.app.suggestfood.domain.evaluate.FieldEvaluate
import com.eatfood.app.suggestfood.domain.evaluator.*
import com.eatfood.app.suggestfood.domain.resolution.di.ResolutionModule
import com.eatfood.app.suggestfood.domain.view.helper.LoadProgressHelper
import com.eatfood.app.suggestfood.domain.view.listeners.PreferenceChangeListener
import com.eatfood.app.suggestfood.domain.visitor.Evaluate
import com.google.i18n.phonenumbers.PhoneNumberUtil
import dagger.Module
import dagger.Provides
import java.util.*
import javax.inject.Singleton

/**
 * Injection dependency of the Domain module.
 *
 * @property context the context
 * @constructor Instantiates a new Domain module.
 * @see DomainComponent
 * @see Module
 * @see Context
 *
 * Created by Usuario on 30/08/2017.
 */
@Module(includes = [(ResolutionModule::class)])
class DomainModule(private val context: Context) {

    /**
     * Provide the account manager of the application.
     *
     * @return the Account Manager object
     * @see AccountManager
     */
    @Provides
    fun provideAccountManager(): AccountManager = AccountManager.get(context)

    /**
     * Provide the evaluator object to validate texts. Requires the evaluate object
     * to know the evaluation criteria.
     *
     * @param evaluate the evaluate object
     * @return the text evaluator
     * @see TextEvaluator
     * @see Evaluate
     */
    @Provides
    internal fun provideTextEvaluator(evaluate: Evaluate) = TextEvaluator(context, evaluate)

    /**
     * Provide the evaluator object to validate emails. Requires the evaluate object
     * to know the evaluation criteria.
     *
     * @param evaluate the evaluate object
     * @return the email evaluator
     * @see EmailEvaluator
     * @see Evaluate
     */
    @Provides
    internal fun provideEmailEvaluator(evaluate: Evaluate) = EmailEvaluator(context, evaluate)

    /**
     * Provide the evaluator object to validate passwords. Requires the evaluate object
     * to know the evaluation criteria.
     *
     * @param evaluate the evaluate object
     * @return the password evaluator
     * @see PasswordEvaluator
     * @see Evaluate
     */
    @Provides
    internal fun providePasswordEvaluator(evaluate: Evaluate) = PasswordEvaluator(context, evaluate)

    /**
     * Provide the evaluator object to validate passwords match. Requires the evaluate object
     * to know the evaluation criteria.
     *
     * @param evaluate the evaluate object
     * @return the passwords match evaluator
     * @see PasswordsMatchEvaluator
     * @see Evaluate
     */
    @Provides
    internal fun providePasswordsMatchEvaluator(evaluate: Evaluate)
            = PasswordsMatchEvaluator(context, evaluate)

    /**
     * Provide the evaluator object to validate phone numbers. Requires the evaluate object
     * to know the evaluation criteria.
     *
     * @param evaluate the evaluate object
     * @return the phone evaluator
     * @see PhoneEvaluator
     * @see Evaluate
     */
    @Provides
    internal fun providePhoneEvaluator(evaluate: Evaluate) = PhoneEvaluator(context, evaluate)

    /**
     * Provides the evaluation criteria to validate fields of the models or interfaces.
     *
     * @return the evaluate object
     * @see Evaluate
     * @see FieldEvaluate
     */
    @Provides
    @Singleton
    internal fun provideEvaluate(phoneNumberUtil: PhoneNumberUtil): Evaluate {
        /*TODO:Locale shoulbe change for the user locale.*/
        return FieldEvaluate(phoneNumberUtil, Locale.US)
    }

    /**
     * Provide load progress helper.
     *
     * @return the load progress helper
     * @see LoadProgressHelper
     */
    @Provides
    internal fun provideLoadProgressHelper() = LoadProgressHelper(context)

    /**
     * Provide preference change listener preference.
     *
     * @return the preference change listener
     * @see Preference.OnPreferenceChangeListener
     * @see PreferenceChangeListener
     */
    @Provides
    internal fun providePreferenceChangeListener(): Preference.OnPreferenceChangeListener
        = PreferenceChangeListener()

}
