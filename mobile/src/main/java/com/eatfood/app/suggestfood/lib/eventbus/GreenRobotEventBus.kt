package com.eatfood.app.suggestfood.lib.eventbus

/**
 * Basic implementation of the [EventBus] pattern.
 *
 * @see EventBus
 *
 * Created by Usuario on 11/06/2016.
 */
class GreenRobotEventBus : EventBus {

    private val eventBus = org.greenrobot.eventbus.EventBus.getDefault()

    override fun register(subscriber: Any) = eventBus.register(subscriber)

    override fun unregister(subscriber: Any) = eventBus.unregister(subscriber)

    override fun post(event: Any) = eventBus.post(event)
}
