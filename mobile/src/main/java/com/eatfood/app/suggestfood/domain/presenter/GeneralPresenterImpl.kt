package com.eatfood.app.suggestfood.domain.presenter

import com.eatfood.app.suggestfood.domain.view.GeneralView
import com.eatfood.app.suggestfood.lib.eventbus.EventBus

/**
 * Basic implementation of the [GeneralPresenter] interface for presenters in the MVP pattern.
 *
 * @param T the type parameter for the View object
 * @param E the type parameter for the event type
 * @property view     the view
 * @property eventBus the event bus object
 * @constructor Instantiates a new General presenter.
 * @see GeneralView
 * @see EventBus
 *
 * Created by Usuario on 12/01/2017.
 */
abstract class GeneralPresenterImpl<T : GeneralView, E : Any> protected constructor(
        protected var view: T?,
        private val eventBus: EventBus
) : GeneralPresenter<E> {

    override fun onCreate() = eventBus.register(this)

    override fun onDestroy() {
        eventBus.unregister(this)
        view = null
    }
}
