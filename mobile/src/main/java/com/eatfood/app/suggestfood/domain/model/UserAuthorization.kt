package com.eatfood.app.suggestfood.domain.model

import com.eatfood.app.suggestfood.BuildConfig
import com.eatfood.app.suggestfood.entity.AuthorizedUser
import com.eatfood.app.suggestfood.features.session.model.SessionRepository
import com.eatfood.app.suggestfood.lib.httpclient.Authorization

import okhttp3.Headers
import okhttp3.Request

/**
 * Class that provides the [Authorization] mechanism of the requests in the app.
 * It is required to obtain the current session to update the configuration parameters.
 *
 * @property sessionRepository the session repository
 * @constructor Instantiates a new User authorization.
 * @see Authorization
 * @see AuthorizedUser
 * @see SessionRepository
 *
 * Created by Usuario on 17/01/2017.
 */
class UserAuthorization(private val sessionRepository: SessionRepository) : Authorization<AuthorizedUser> {

    companion object {
        /**
         * Constant that indicates the type of account of the application.
         */
        const val ACCOUNT_TYPE = BuildConfig.APPLICATION_ID
    }

    /**
     * Configuration variables in the request.
     *
     * @property fieldName the name of the field in the header.
     * @constructor Instantiates a new constant [Header] field.
     */
    private enum class Header(val fieldName: String) {
        /**
         * Access token authentication.
         */
        ACCESS_TOKEN("Access-Token"),
        /**
         * Client token authentication.
         */
        CLIENT("Client"),
        /**
         * Uid token authentication.
         * In general, it will always be the client's email.
         */
        UID("Uid"),
        /**
         * Time in milliseconds for the session to expired.
         */
        EXPIRY("expiry")
    }

    override fun setAuthorizationHeader(builder: Request.Builder) {
        val user = sessionRepository.session

        if (user != null) {
            builder.addHeader(Header.ACCESS_TOKEN.fieldName, user.accessToken ?: "")
            builder.addHeader(Header.CLIENT.fieldName, user.client ?: "")
            builder.addHeader(Header.UID.fieldName, user.uid ?: "")
            builder.addHeader(Header.EXPIRY.fieldName, user.expiry?.toString() ?: "")
        }
    }

    override fun getAuthorizationHeader(headers: Headers, apply: (AuthorizedUser) -> Unit) {
        val unauthorizedObject = AuthorizedUser()
        unauthorizedObject.accessToken = headers.get(Header.ACCESS_TOKEN.fieldName)
        unauthorizedObject.client = headers.get(Header.CLIENT.fieldName)
        unauthorizedObject.uid = headers.get(Header.UID.fieldName)
        unauthorizedObject.expiry = java.lang.Long.parseLong(headers.get(Header.EXPIRY.fieldName))
        apply(unauthorizedObject)
    }
}
