package com.eatfood.app.suggestfood.features.feedback.model

import com.eatfood.app.suggestfood.entity.Feedback

/**
 * Basic interface for the interactor in the Clean pattern of the feedback feature.
 *
 * Created by Usuario on 22/03/2018.
 */
interface FeedbackInteractor {

    /**
     * Method that indicates the number of pending feedback not sent to the remote storage.
     */
    val totalPendingFeedback: Int

    /**
     * Method that returns the last pending feedback to send to the remote storage.
     * Internally it will be removed from the local storage.
     *
     * @see Feedback
     * @see [removePendingFeedback]
     */
    val lastPendingFeedback: Feedback?

    /**
     * Method that registers a new pending feedback in the local storage.
     * This feedback should send later to the remote storage.
     *
     * @param feedback the new feedback
     * @see [totalPendingFeedback]
     * @see Feedback
     */
    fun addPendingFeedback(feedback: Feedback)

    /**
     * Method that remove the feedback in the local storage.
     *
     * @param feedback the feedback
     * @see [totalPendingFeedback]
     * @see feedback
     */
    fun removePendingFeedback(feedback: Feedback)

    /**
     * Method that registers a new feedback in the remote storage.
     *
     * @param feedback the new feedback
     * @param remote   true - if the response should be async
     * @see Feedback
     */
    fun addFeedback(feedback: Feedback, remote: Boolean = true)

}