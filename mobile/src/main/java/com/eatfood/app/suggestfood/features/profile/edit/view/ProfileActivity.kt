package com.eatfood.app.suggestfood.features.profile.edit.view

import android.app.Activity
import android.content.Intent
import android.graphics.Bitmap
import android.net.Uri
import android.os.Bundle
import android.view.Menu
import android.view.MenuItem
import butterknife.OnClick
import com.eatfood.app.suggestfood.R
import com.eatfood.app.suggestfood.content_provider.di.DatabaseModule
import com.eatfood.app.suggestfood.domain.di.DaggerDomainComponent
import com.eatfood.app.suggestfood.domain.di.DomainModule
import com.eatfood.app.suggestfood.domain.resolution.di.ResolutionModule
import com.eatfood.app.suggestfood.domain.view.helper.ErrorHelper
import com.eatfood.app.suggestfood.domain.view.helper.MessageHelper
import com.eatfood.app.suggestfood.domain.view.type.fields.FormActivity
import com.eatfood.app.suggestfood.entity.User
import com.eatfood.app.suggestfood.features.authorization.di.DaggerAuthorizationComponent
import com.eatfood.app.suggestfood.features.capture_image.di.CaptureImageModule
import com.eatfood.app.suggestfood.features.capture_image.view.CaptureImageView
import com.eatfood.app.suggestfood.features.di.DaggerFeaturesComponent
import com.eatfood.app.suggestfood.features.intents.parameters.User
import com.eatfood.app.suggestfood.features.profile.edit.di.DaggerProfileComponent
import com.eatfood.app.suggestfood.features.profile.edit.di.ProfileModule
import com.eatfood.app.suggestfood.features.profile.edit.presenter.ProfilePresenter
import com.eatfood.app.suggestfood.features.profile.general.ProfileView.Companion.EXTRA_USER
import com.eatfood.app.suggestfood.features.profile.show.view.ShowProfileActivity
import com.eatfood.app.suggestfood.lib.di.LibsModule
import com.eatfood.app.suggestfood.lib.image_loader.ImageLoader
import kotlinx.android.synthetic.main.activity_profile.*
import kotlinx.android.synthetic.main.content_profile.*
import me.eugeniomarletti.extras.intent.IntentExtra
import javax.inject.Inject

/**
 * A profile edit screen that offers to edit the user information.
 * Implements the interface [ProfileView]
 *
 * @see ProfileView
 * @see FormActivity
 * @see ProfilePresenter
 * @see CaptureImageView.OnCaptureResultListener
 *
 * Created by Usuario on 29/09/2017.
 */
internal class ProfileActivity :
        FormActivity<ProfilePresenter>(),
        ProfileView,
        CaptureImageView.OnCaptureResultListener
{

    object IntentOptions {
        var Intent.user by IntentExtra.User(EXTRA_USER)
    }

    @Inject
    lateinit var imageLoader: ImageLoader

    @Inject
    lateinit var captureImageView: CaptureImageView

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<out String>, grantResults: IntArray) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        captureImageView.onPermissionsResult(requestCode, permissions, grantResults)
    }

    /*EVENTS*/

    /**
     * Method to edit the profile image.
     */
    @OnClick(R.id.imgUser)
    fun onEditProfileImage() {
        presenter.onEditProfileImage()
    }

    /**INTERFACES */
    //1. ProfileView

    /**
     * The user object for identifying the user object between the view's parameters.
     *
     * @see IntentOptions
     */
    override var userParam: User?
        get() = with(IntentOptions){ intent.user }
        set(value) = with(IntentOptions){ intent.user = value }

    override val name: String
        get() = txtName.text.toString()

    override val phone: String
        get() = txtPhone.text.toString()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        super.onCreate(R.layout.activity_profile, savedInstanceState)
    }

    override fun onCreateOptionsMenu(menu: Menu) = createOptionsMenu(menu, R.menu.menu_profile)

    override fun onOptionsItemSelected(item: MenuItem) = when (item.itemId) {
        android.R.id.home -> {
            presenter.onViewProfile()
            true
        }
        R.id.update_profile -> {
            presenter.updateProfile()
            true
        }
        else -> super.onOptionsItemSelected(item)
    }

    override fun onBackPressed() {
        presenter.onViewProfile()
        super.onBackPressed()
    }

    override fun config() {
        super.config()
        setupActionBar()
    }

    override fun configLoadProgress()
        = loadProgressHelper.config(null, getString(R.string.activity_profile_updating_profile))

    override fun setupInjection() {
        val databaseModule = DatabaseModule(this)
        val authorization = DaggerAuthorizationComponent.builder()
                .databaseModule(databaseModule)
                .build().provideAuthorization()
        val domainComponent = DaggerDomainComponent.builder()
                .libsModule(LibsModule(this, authorization))
                .resolutionModule(ResolutionModule(this))
                .domainModule(DomainModule(this))
                .build()
        val featuresComponent = DaggerFeaturesComponent.builder()
                .domainComponent(domainComponent)
                .databaseModule(databaseModule)
                .build()
        DaggerProfileComponent.builder()
                .featuresComponent(featuresComponent)
                .captureImageModule(CaptureImageModule(this))
                .profileModule(ProfileModule(this))
                .build().inject(this)
    }

    override fun loadProfile(user: User) {
        txtName.setText(user.name)
        txtPhone.setText(user.phone)
        imageLoader.load(imgUser, user.image)
    }

    override fun showErrorInName(error: String?) = ErrorHelper.display(txtName, error)

    override fun showErrorInPhone(error: String?) = ErrorHelper.display(txtPhone, error)

    override fun navigateToProfileScreen(user: User) {
        setResult(Activity.RESULT_OK, ShowProfileActivity.profileScreenIntent(this, user))
        finish()
        //NavUtils.navigateUpTo(this, resultIntent);
    }

    override fun displayOptionsToCaptureImage() = captureImageView.show(this)

    override fun displayMessage(message: String) = MessageHelper.display(containerProfile, message)

    override fun setInputs(enabled: Boolean) = super.setInputs(enabled, txtName, txtPhone)

    override fun onResult(imageUri: Uri, imageBitMap: Bitmap?) = presenter.onEditProfileImage(imageUri, imageBitMap)

    /*Own Methods*/

    override fun setupActionBar() {
        setSupportActionBar(toolbar)
        super.setupActionBar()
    }
}
