package com.eatfood.app.suggestfood.features.signup.view

import android.os.Bundle
import android.view.inputmethod.EditorInfo
import butterknife.OnClick
import butterknife.OnEditorAction
import com.eatfood.app.suggestfood.R
import com.eatfood.app.suggestfood.config.Font
import com.eatfood.app.suggestfood.content_provider.di.DatabaseModule
import com.eatfood.app.suggestfood.domain.di.DaggerDomainComponent
import com.eatfood.app.suggestfood.domain.di.DomainModule
import com.eatfood.app.suggestfood.domain.resolution.di.ResolutionModule
import com.eatfood.app.suggestfood.domain.view.helper.ErrorHelper
import com.eatfood.app.suggestfood.domain.view.helper.FontHelper
import com.eatfood.app.suggestfood.domain.view.helper.IconHelper
import com.eatfood.app.suggestfood.domain.view.helper.MessageHelper
import com.eatfood.app.suggestfood.domain.view.type.fields.FormActivity
import com.eatfood.app.suggestfood.entity.User
import com.eatfood.app.suggestfood.features.authorization.di.DaggerAuthorizationComponent
import com.eatfood.app.suggestfood.features.di.DaggerFeaturesComponent
import com.eatfood.app.suggestfood.features.login.view.LoginActivity
import com.eatfood.app.suggestfood.features.signup.di.DaggerSignupComponent
import com.eatfood.app.suggestfood.features.signup.di.SignupModule
import com.eatfood.app.suggestfood.features.signup.presenter.SignupPresenter
import com.eatfood.app.suggestfood.lib.di.LibsModule
import kotlinx.android.synthetic.main.activity_signup.*
import kotlinx.android.synthetic.main.content_header_app.*

/**
 * A sign-up screen that allows registering new users in the application.
 *
 * @see SignupView
 * @see FormActivity
 * @see SignupPresenter
 *
 * Created by Usuario on 06/09/2017.
 */
class SignupActivity : FormActivity<SignupPresenter>(), SignupView {

    /*INTERFACES*/
    //1.SignupView

    override val email: String
        get() = txtEmail.text.toString()

    override var password: String?
        get() = txtPassword.text.toString()
        set(password) = txtPassword.setText(password)

    override var confirmationPassword: String?
        get() = txtPasswordConfirmation.text.toString()
        set(confirmationPassword) = txtPasswordConfirmation.setText(confirmationPassword)

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        super.onCreate(R.layout.activity_signup, savedInstanceState)
    }

    override fun config() {
        super.config()
        configureFonts()
        configureIcons()
    }

    override fun setupInjection() {
        val databaseModule = DatabaseModule(this)
        val authorization = DaggerAuthorizationComponent.builder()
                .databaseModule(databaseModule)
                .build().provideAuthorization()
        val domainComponent = DaggerDomainComponent.builder()
                .libsModule(LibsModule(this, authorization))
                .resolutionModule(ResolutionModule(this))
                .domainModule(DomainModule(this))
                .build()
        val featuresComponent = DaggerFeaturesComponent.builder()
                .domainComponent(domainComponent)
                .databaseModule(databaseModule)
                .build()
        DaggerSignupComponent.builder()
                .featuresComponent(featuresComponent)
                .signupModule(SignupModule(this))
                .build().inject(this)
    }

    override fun displayMessage(message: String) = MessageHelper.display(signupForm, message)

    override fun setInputs(enabled: Boolean)
        = setInputs(enabled, txtEmail, txtPassword, txtPasswordConfirmation, btnSignUp)

    override fun configLoadProgress()
        = loadProgressHelper.config(null, getString(R.string.activity_sign_up_dialog_message))

    /*EVENTS*/

    @OnEditorAction(R.id.txtPasswordConfirmation)
    fun onEditorPasswordConfirmation(id: Int): Boolean {
        if (id == EditorInfo.IME_ACTION_GO || id == EditorInfo.IME_NULL) {
            presenter.validateSignUp()
            return true
        }
        return false
    }

    @OnClick(R.id.btnSignUp)
    fun onSignUp() {
        presenter.validateSignUp()
    }

    @OnClick(R.id.btnCancel)
    fun onCancel() = finish()

    override fun showErrorInEmail(error: String?) = ErrorHelper.display(txtEmail, error)

    override fun showErrorInPassword(error: String?) = ErrorHelper.display(txtPassword, error)

    override fun showErrorInConfirmationPassword(error: String?)
        = ErrorHelper.display(txtPasswordConfirmation, error)

    override fun navigateToLoginScreen(user: User)
        = redirect(LoginActivity.loginScreenIntent(this, user), true)

    /*Own Methods*/

    private fun configureFonts() = FontHelper.setFont(txtTitle, Font.TRAJAN_BOLD)

    private fun configureIcons() {
        iconHelper.setIcon(IconHelper.DrawablePosition.LEFT, R.drawable.ic_mail_outline_black_24dp, txtEmail)
        iconHelper.setIcon(IconHelper.DrawablePosition.LEFT, R.drawable.ic_lock_outline_black_24dp, txtPassword)
        iconHelper.setIcon(IconHelper.DrawablePosition.LEFT, R.drawable.ic_lock_black_24dp, txtPasswordConfirmation)
    }
}
