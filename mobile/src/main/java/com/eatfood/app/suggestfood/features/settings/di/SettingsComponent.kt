package com.eatfood.app.suggestfood.features.settings.di

import com.eatfood.app.suggestfood.domain.di.DomainComponent
import com.eatfood.app.suggestfood.features.di.Feature
import com.eatfood.app.suggestfood.features.session.di.SessionModule
import com.eatfood.app.suggestfood.features.settings.view.fragments.DataSyncPreferenceFragment
import com.eatfood.app.suggestfood.features.settings.view.fragments.GeneralPreferenceFragment
import com.eatfood.app.suggestfood.features.settings.view.fragments.NotificationPreferenceFragment
import com.eatfood.app.suggestfood.features.settings.view.fragments.SettingsPreferenceFragment
import dagger.Component

/**
 * Injection component of the settings feature.
 *
 * @see SettingsModule
 * @see SessionModule
 * @see DomainComponent
 * @see SettingsPreferenceFragment
 *
 * Created by Usuario on 30/09/2017.
 */
@Feature
@Component(modules = [(SettingsModule::class), (SessionModule::class)], dependencies = [(DomainComponent::class)])
internal interface SettingsComponent {

    /**
     * Method for injecting dependencies in the [GeneralPreferenceFragment] class.
     *
     * @param fragment the general preference fragment
     * @see GeneralPreferenceFragment
     */
    fun inject(fragment: GeneralPreferenceFragment)

    /**
     * Method for injecting dependencies in the [NotificationPreferenceFragment] class.
     *
     * @param fragment the notification preference fragment
     * @see NotificationPreferenceFragment
     */
    fun inject(fragment: NotificationPreferenceFragment)

    /**
     * Method for injecting dependencies in the [DataSyncPreferenceFragment] class.
     *
     * @param fragment the data and sync preference fragment
     * @see DataSyncPreferenceFragment
     */
    fun inject(fragment: DataSyncPreferenceFragment)

}
