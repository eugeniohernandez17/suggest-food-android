package com.eatfood.app.suggestfood.features.lost_password.model

import com.eatfood.app.suggestfood.entity.User

/**
 * Basic interface for the lost password repository in the Clean pattern
 * of the lost password feature.
 *
 * Created by Usuario on 18/09/2017.
 */
interface LostPasswordRepository {

    /**
     * Method that send the instructions for recovery password.
     *
     * @param user the user information
     * @see User
     */
    fun sendInstruccionsForRecoveryPassword(user: User)

}
