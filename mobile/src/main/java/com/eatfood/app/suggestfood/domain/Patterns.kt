package com.eatfood.app.suggestfood.domain

import java.util.regex.Pattern

/**
 * Regular expressions for texts.
 *
 * Created by Usuario on 30/08/2017.
 */

object Patterns {

    val EMAIL_ADDRESS = Pattern.compile("""
        |[a-zA-Z0-9+._%-+]{1,256}@[a-zA-Z0-9][a-zA-Z0-9-]{0,64}(.[a-zA-Z0-9][a-zA-Z0-9-]{0,25})+
    """.trimMargin())!!

}
