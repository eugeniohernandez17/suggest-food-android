package com.eatfood.app.suggestfood.features.login.model

import com.eatfood.app.suggestfood.entity.User

/**
 * Basic implementation of the [LoginInteractor] interface
 * for interactor in the Clean pattern.
 *
 * @property loginRepository the login repository
 * @constructor Instantiates a new Login interactor.
 * @see LoginInteractor
 * @see LoginRepository
 *
 * Created by Usuario on 07/01/2017.
 */
class LoginInteractorImpl(private val loginRepository: LoginRepository) : LoginInteractor {

    override fun doSignIn(email: String, password: String) {
        loginRepository.signIn(User(email = email, password = password))
    }
}
