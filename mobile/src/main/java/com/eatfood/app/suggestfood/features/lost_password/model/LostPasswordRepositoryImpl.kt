package com.eatfood.app.suggestfood.features.lost_password.model

import com.eatfood.app.suggestfood.domain.model.callback.Callback
import com.eatfood.app.suggestfood.entity.User
import com.eatfood.app.suggestfood.service.external.user.LostPasswordReturn
import com.eatfood.app.suggestfood.service.external.user.UserReturn
import com.eatfood.app.suggestfood.service.external.user.UserService

import retrofit2.Call
import retrofit2.Response

/**
 * Basic implementation of the [LostPasswordRepository] for the repository
 * in the Clean pattern.
 *
 * @property userService  the user service
 * @property userCallback the user callback
 * @constructor Instantiates a new Lost password repository.
 * @see LostPasswordRepository
 * @see UserService
 * @see Callback
 * @see LostPasswordReturn
 *
 * Created by Usuario on 18/09/2017.
 */
class LostPasswordRepositoryImpl(
        private val userService: UserService,
        private val userCallback: Callback<LostPasswordReturn.Successful, UserReturn.Failure>
) : LostPasswordRepository {

    override fun sendInstruccionsForRecoveryPassword(user: User) {
        userService.sendInstruccionsForRecoveryPassword(user).enqueue(object : retrofit2.Callback<LostPasswordReturn.Successful> {
            override fun onResponse(call: Call<LostPasswordReturn.Successful>,
                                    response: Response<LostPasswordReturn.Successful>) =
                    if (response.isSuccessful) {
                        userCallback.onSuccessful(response.body()!!, response.headers())
                    } else {
                        userCallback.onFailure(UserReturn.Failure(user),
                                response.code(), response.errorBody()!!)
                    }

            override fun onFailure(call: Call<LostPasswordReturn.Successful>, throwable: Throwable) {
                userCallback.onError(UserReturn.Failure(user), throwable)
            }
        })
    }
}
