package com.eatfood.app.suggestfood.features.capture_image.di

import android.support.v7.app.AppCompatActivity
import com.eatfood.app.suggestfood.features.capture_image.view.CaptureImage
import com.eatfood.app.suggestfood.features.capture_image.view.CaptureImageView
import dagger.Module
import dagger.Provides

/**
 * Injection dependency of the capture image module.
 *
 * @property activity the activity
 * @constructor Instantiates a new Capture Image module.
 * @see Module
 * @see AppCompatActivity
 *
 * Created by Usuario on 24/04/2018.
 */
@Module
class CaptureImageModule(private val activity: AppCompatActivity) {

    /**
     * Provide capture image view.
     *
     * @return the capture image view
     * @see CaptureImageView
     */
    @Provides
    fun provideCaptureImageView(): CaptureImageView = CaptureImage(activity)

}