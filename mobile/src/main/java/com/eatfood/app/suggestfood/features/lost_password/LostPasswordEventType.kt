package com.eatfood.app.suggestfood.features.lost_password

import com.eatfood.app.suggestfood.lib.eventbus.EventBus

/**
 * General events used in the EventBus pattern to identify api responses
 * in the lost password process.
 *
 * @see EventBus
 *
 * Created by Usuario on 14/09/2017.
 */
enum class LostPasswordEventType {

    /**
     * Event when the user password was successfully recovered.
     */
    ON_LOST_PASSWORD_SUCCESS,

    /**
     * Event generated when there is an error in the lost password process.
     */
    ON_LOST_PASSWORD_ERROR

}
