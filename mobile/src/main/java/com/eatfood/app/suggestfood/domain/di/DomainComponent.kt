package com.eatfood.app.suggestfood.domain.di

import android.accounts.AccountManager
import android.preference.Preference
import com.eatfood.app.suggestfood.domain.evaluator.*
import com.eatfood.app.suggestfood.domain.resolution.http.HttpResolution
import com.eatfood.app.suggestfood.domain.resolution.networkconnectivity.NetworkConnectivityResolution
import com.eatfood.app.suggestfood.domain.view.helper.LoadProgressHelper
import com.eatfood.app.suggestfood.lib.di.LibsModule
import com.eatfood.app.suggestfood.lib.eventbus.EventBus
import com.eatfood.app.suggestfood.lib.image_loader.ImageLoader
import com.eatfood.app.suggestfood.lib.retrofit.Retrofit
import com.google.firebase.storage.FirebaseStorage
import com.google.gson.Gson
import dagger.Component
import javax.inject.Singleton

/**
 * Injection of dependencies of the entire domain of the application.
 *
 * @see LibsModule
 * @see DomainModule
 *
 * Created by Usuario on 21/04/2018.
 */
@Singleton
@Component(modules = [(LibsModule::class), (DomainModule::class)])
internal interface DomainComponent {

    //Libs

    fun provideEventBus(): EventBus
    fun provideRetrofit(): Retrofit
    fun provideGson(): Gson
    fun provideImageLoader(): ImageLoader
    fun provideFirebaseStorage(): FirebaseStorage

    //Domain

    fun provideAccountManager(): AccountManager
    fun provideTextEvaluator(): TextEvaluator
    fun provideEmailEvaluator(): EmailEvaluator
    fun providePasswordEvaluator(): PasswordEvaluator
    fun providePasswordsMatchEvaluator(): PasswordsMatchEvaluator
    fun providePhoneEvaluator(): PhoneEvaluator
    fun provideLoadProgressHelper(): LoadProgressHelper
    fun providePreferenceChangeListener(): Preference.OnPreferenceChangeListener

    fun provideNetworkConnectivityResolution(): NetworkConnectivityResolution
    fun provideHttpResolution(): HttpResolution

}