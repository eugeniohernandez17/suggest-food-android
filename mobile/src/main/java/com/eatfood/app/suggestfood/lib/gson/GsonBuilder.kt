package com.eatfood.app.suggestfood.lib.gson

import com.google.gson.FieldNamingPolicy

/**
 * Created by Usuario on 31/08/2017.
 */
object GsonBuilder {

    /**
     * Gson object construction method.
     *
     * @return the gson object
     */
    fun build(): com.google.gson.Gson {
        return com.google.gson.GsonBuilder()
                .addSerializationExclusionStrategy(SerializationExclusionStrategy)
                .addDeserializationExclusionStrategy(DeserializationExclusionStrategy)
                .setFieldNamingPolicy(FieldNamingPolicy.LOWER_CASE_WITH_UNDERSCORES)
                .create()
    }
}
