package com.eatfood.app.suggestfood.domain.visitor

/**
 * Basic interface of the patron Visitor.
 * Contains statements to evaluate a specific field of the models or the interfaces.
 *
 * @param T the class type of the field value
 *
 * Created by Usuario on 12/01/2017.
 */
interface Evaluator<T> {

    /**
     * Method to obtain the error generated after evaluating the field.
     *
     * @see validate
     */
    val error: String

    /**
     * Method to evaluate the value of the field.
     *
     * @param value the value of the field
     * @return true - if the value is valid
     */
    fun validate(value: T?): Boolean
}
