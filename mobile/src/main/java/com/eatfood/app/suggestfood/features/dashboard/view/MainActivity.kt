package com.eatfood.app.suggestfood.features.dashboard.view

import android.accounts.Account
import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.support.design.widget.NavigationView
import android.support.design.widget.Snackbar
import android.support.v4.view.GravityCompat
import android.support.v7.app.ActionBarDrawerToggle
import android.view.Menu
import android.view.MenuItem
import android.view.View
import android.widget.TextView
import butterknife.Bind
import butterknife.ButterKnife
import butterknife.OnClick
import com.eatfood.app.suggestfood.R
import com.eatfood.app.suggestfood.content_provider.di.DatabaseModule
import com.eatfood.app.suggestfood.domain.di.DaggerDomainComponent
import com.eatfood.app.suggestfood.domain.di.DomainModule
import com.eatfood.app.suggestfood.domain.resolution.di.ResolutionModule
import com.eatfood.app.suggestfood.domain.view.helper.MessageHelper
import com.eatfood.app.suggestfood.domain.view.type.progress.ProgressActivity
import com.eatfood.app.suggestfood.entity.User
import com.eatfood.app.suggestfood.features.authorization.di.DaggerAuthorizationComponent
import com.eatfood.app.suggestfood.features.dashboard.di.DaggerDashboardComponent
import com.eatfood.app.suggestfood.features.dashboard.di.DashboardModule
import com.eatfood.app.suggestfood.features.dashboard.presenter.DashboardPresenter
import com.eatfood.app.suggestfood.features.di.DaggerFeaturesComponent
import com.eatfood.app.suggestfood.features.feedbacks.view.FeedbacksActivity
import com.eatfood.app.suggestfood.features.intents.parameters.User
import com.eatfood.app.suggestfood.features.login.view.LoginActivity
import com.eatfood.app.suggestfood.features.profile.general.ProfileView.Companion.EXTRA_USER
import com.eatfood.app.suggestfood.features.profile.show.view.ShowProfileActivity
import com.eatfood.app.suggestfood.features.settings.di.SettingsModule
import com.eatfood.app.suggestfood.features.settings.view.SettingsActivity
import com.eatfood.app.suggestfood.lib.di.LibsModule
import com.eatfood.app.suggestfood.lib.image_loader.ImageLoader
import com.eatfood.app.suggestfood.prefs.schemas.DefaultPrefs
import com.eatfood.app.suggestfood.service.internal.sync.Sync
import de.hdodenhof.circleimageview.CircleImageView
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.app_bar_main.*
import me.eugeniomarletti.extras.intent.IntentExtra
import org.jetbrains.anko.*
import javax.inject.Inject

/**
 * Main activity to display the user's dashboard. Implements the interface [DashboardView].
 * It also includes the functionality to display the drop-down menu and toolbar.
 *
 * @see DashboardView
 * @see ProgressActivity
 * @see DashboardPresenter
 *
 * Created by Usuario on 12/01/2017.
 */
class MainActivity : ProgressActivity<DashboardPresenter>(), DashboardView,
        NavigationView.OnNavigationItemSelectedListener {

    companion object {
        private val TAG = MainActivity::class.java.name
        private const val REQUEST_SHOW_PROFILE = 0

        @JvmStatic
        fun mainScreenIntent(context: Context)
                = context.intentFor<MainActivity>().newTask().clearTop()

        @JvmStatic
        fun mainScreenIntent(context: Context, user: User) = with(IntentOptions) {
            context.intentFor<MainActivity>().apply {
                this.user = user
            }
        }
    }

    object IntentOptions {
        var Intent.user by IntentExtra.User(EXTRA_USER)
    }

    private lateinit var defaultPrefs: DefaultPrefs

    @Inject
    lateinit var imageLoader: ImageLoader

    private lateinit var headerContainer: HeaderContainer

    internal class HeaderContainer(view: View, private val presenter: DashboardPresenter) {

        @Bind(R.id.txt_name_user)
        lateinit var txtNameUser: TextView

        @Bind(R.id.txt_email_user)
        lateinit var txtEmailUser: TextView

        @Bind(R.id.img_user)
        lateinit var imgUser: CircleImageView

        init {
            ButterKnife.bind(this, view)
        }

        @OnClick(R.id.img_user)
        fun onViewProfile() = presenter.onViewProfile()
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        super.onCreate(R.layout.activity_main, savedInstanceState)
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent) =
            when (requestCode) {
                REQUEST_SHOW_PROFILE -> with(IntentOptions) {
                    presenter.onProfileShowingFinish(data.user!!)
                }
                else -> super.onActivityResult(requestCode, resultCode, data)
            }

    override fun onBackPressed() = if (drawerLayout.isDrawerOpen(GravityCompat.START)) {
        drawerLayout.closeDrawer(GravityCompat.START)
    } else {
        super.onBackPressed()
    }

    override fun onCreateOptionsMenu(menu: Menu) = createOptionsMenu(menu, R.menu.main)

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.

        /*TODO:Main Menu options not implemented yet.*/
        when (item.itemId) {
            R.id.action_help -> { }
            R.id.action_settings -> presenter.onViewSettings()
            R.id.action_feecback -> { /*TODO: Redirect to Send Feedback screen.*/ }
            else -> return super.onOptionsItemSelected(item)
        }

        return true
    }

    override fun onNavigationItemSelected(item: MenuItem): Boolean {
        // Handle navigation view item clicks here.

        /*TODO:Main Menu options not implemented yet.*/
        when (item.itemId) {
            R.id.nav_my_recipe -> { }
            R.id.nav_my_menus -> { }
            R.id.nav_share -> { }
            R.id.nav_feedback -> presenter.onViewFeedbacks()
            R.id.nav_help -> { }
            R.id.nav_settings -> presenter.onViewSettings()
            R.id.nav_logout -> presenter.logout()
            else -> { }
        }

        drawerLayout.closeDrawer(GravityCompat.START)
        return true
    }

    override fun config() {
        this.defaultPrefs = DefaultPrefs.get(this)
        super.config()

        setSupportActionBar(toolbar)
        setupNavigation()
    }

    override fun configLoadProgress() {
        val text = getString(R.string.activity_dashboard_loading_user)
        val message = getString(R.string.loading_with_text, text)
        loadProgressHelper.config(null, message)
    }

    override fun setupInjection() {
        val databaseModule = DatabaseModule(this)
        val authorization = DaggerAuthorizationComponent.builder()
                .databaseModule(databaseModule)
                .build().provideAuthorization()
        val domainComponent = DaggerDomainComponent.builder()
                .libsModule(LibsModule(this, authorization))
                .resolutionModule(ResolutionModule(this))
                .domainModule(DomainModule(this))
                .build()
        val featuresComponent = DaggerFeaturesComponent.builder()
                .domainComponent(domainComponent)
                .databaseModule(databaseModule)
                .build()
        DaggerDashboardComponent.builder()
                .featuresComponent(featuresComponent)
                .dashboardModule(DashboardModule(this))
                .settingsModule(SettingsModule(defaultPrefs = defaultPrefs))
                .build().inject(this)
    }

    /*EVENTS*/

    /**
     * Method to add a new food suggestion.
     *
     * @param view the action button
     */
    @OnClick(R.id.fab)
    fun onAddSuggestion(view: View) {
        Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                .setAction("Action", null).show()
    }

    /*INTERFACES*/
    //1. DashboardView

    override fun displayMessage(message: String) = MessageHelper.display(containerMain, message)

    override fun loadUser(user: User) {
        headerContainer.txtNameUser.text = user.name
        headerContainer.txtEmailUser.text = user.email
        imageLoader.load(headerContainer.imgUser, user.image)
    }

    override fun addPeriodicSync(account: Account, syncFrequency: Long)
        = Sync.add(this, account, syncFrequency)

    override fun removePeriodicSync(account: Account)
        = Sync.remove(this, account)

    override fun navigateToProfileScreen(user: User)
        = startActivityForResult<ShowProfileActivity>(REQUEST_SHOW_PROFILE, EXTRA_USER to user)

    override fun navigateToLoginScreen() = redirect(LoginActivity.loginScreenIntent(this), true)

    override fun navigateToSettingsScreen() = startActivity<SettingsActivity>()

    override fun navigateToFeedbacksScreen() = startActivity<FeedbacksActivity>()

    /**Own Methods */

    private fun setupNavigation() {
        val toggle = ActionBarDrawerToggle(this, drawerLayout, toolbar,
                R.string.navigation_drawer_open, R.string.navigation_drawer_close)
        drawerLayout.addDrawerListener(toggle)
        toggle.syncState()
        navigationView.setNavigationItemSelectedListener(this)
        val header = navigationView.getHeaderView(0)
        headerContainer = HeaderContainer(header, presenter)
    }
}
