package com.eatfood.app.suggestfood.domain.evaluator

import android.content.Context
import android.support.annotation.StringRes

import com.eatfood.app.suggestfood.domain.visitor.Evaluate

/**
 * Text evaluator based on the [TextEvaluator] class
 * that determines the maximum and minimum number of characters allowed.
 *
 * @param context   the context
 * @param evaluate  the evaluate object
 * @property minLength the min length
 * @property maxLength the max length
 * @constructor Instantiates a new MinManLength evaluator.
 * @see TextEvaluator
 * @see Evaluate
 * @see Context
 *
 * Created by Usuario on 26/09/2017.
 */
internal abstract class MinMaxLengthEvaluator(
        context: Context,
        evaluate: Evaluate,
        private val minLength: Int,
        private val maxLength: Int
) : TextEvaluator(context, evaluate) {

    private var limit = 0

    override val error: String
        get() = if (limit > 0) context.getString(errorId, limit) else super.error

    @get:StringRes
    internal abstract val errorMinLength: Int

    @get:StringRes
    internal abstract val errorMaxLength: Int

    override fun validate(value: String?): Boolean {
        var result = super.validate(value)

        this.limit = 0

        if (result && !this.evaluate.validMinLength(value!!, minLength)) {
            this.errorId = errorMinLength
            this.limit = minLength
            result = false
        } else if (!this.evaluate.validMaxLength(value!!, maxLength)) {
            this.errorId = errorMaxLength
            this.limit = maxLength
            result = false
        }

        return result
    }
}
