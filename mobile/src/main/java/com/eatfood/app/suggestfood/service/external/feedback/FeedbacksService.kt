package com.eatfood.app.suggestfood.service.external.feedback

import com.eatfood.app.suggestfood.entity.Feedback

import retrofit2.Call
import retrofit2.http.*

/**
 * Api endpoints for feedbacks update.
 *
 * Created by Usuario on 16/10/2017.
 */
interface FeedbacksService {

    /**
     * End point for getting the feedback list of the user.
     *
     * @param userId the user id
     * @param type   the feedback type
     * @param page   the page to load
     * @return the invocation to send the request
     * @see FeedbacksReturn
     * @see Feedback.FeedbackType
     */
    @GET("/api/v1/users/{user_id}/feedbacks.json")
    fun getFeedbacks(@Path("user_id") userId: Long,
                     @Query("type") type: Feedback.FeedbackType,
                     @Query("page") page: Int): Call<FeedbacksReturn>

    /**
     * End point for register a new feedback.
     *
     * @param userId the user id
     * @param params the feedback information to send to the server
     * @return the invocation to send the request
     * @see Feedback
     */
    @POST("/api/v1/users/{{user_id}}/feedbacks.json")
    fun registerFeedback(@Path("user_id") userId: Long,
                         @Body params: Map<String, @JvmSuppressWildcards Feedback>): Call<Feedback>

}
