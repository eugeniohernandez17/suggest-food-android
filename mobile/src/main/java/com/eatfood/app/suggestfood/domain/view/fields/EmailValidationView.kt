package com.eatfood.app.suggestfood.domain.view.fields

/**
 * Methods to validate the email field in the UI.
 *
 * Created by Usuario on 06/09/2017.
 */
interface EmailValidationView {

    /**
     * The email entered.
     */
    val email: String

    /**
     * Method to show some error in the email field.
     *
     * @param error the error
     */
    fun showErrorInEmail(error: String?)

}
