package com.eatfood.app.suggestfood.features.intents.parameters

import com.eatfood.app.suggestfood.domain.event.MessageEvent
import com.eatfood.app.suggestfood.entity.User
import me.eugeniomarletti.extras.intent.IntentExtra
import me.eugeniomarletti.extras.intent.base.Serializable

/**
 * The view's parameters.
 *
 * Created by Usuario on 31/01/2018.
 */

fun IntentExtra.User(name: String) = Serializable<User>(name = name)

fun IntentExtra.MessageEvent(name: String) = Serializable<MessageEvent<*>>(name = name)