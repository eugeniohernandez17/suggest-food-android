package com.eatfood.app.suggestfood.features.entity.user.model

import android.graphics.Bitmap
import android.net.Uri
import com.eatfood.app.suggestfood.entity.AuthorizedUser

/**
 * Basic implementation of the [UserInteractor] interface for interactor in the Clean pattern.
 *
 * @param userRepository the user repository
 * @constructor Instantiates a new User interactor.
 * @see UserRepository
 *
 * Created by Usuario on 22/09/2017.
 */
class UserInteractorImpl(private val userRepository: UserRepository) : UserInteractor {

    override fun getUser(authorizedUser: AuthorizedUser)
        = this.userRepository.getUser(authorizedUser.user.target)

    override fun updateUser(authorizedUser: AuthorizedUser)
        = this.userRepository.updateUser(authorizedUser.user.target)

    override fun updateUser(authorizedUser: AuthorizedUser, imageUri: Uri, imageBitMap: Bitmap?)
        = this.userRepository.updateUser(authorizedUser.user.target, imageUri, imageBitMap)

}
