package com.eatfood.app.suggestfood.features.session.model

import com.eatfood.app.suggestfood.entity.AuthorizedUser
import com.eatfood.app.suggestfood.entity.User

/**
 * Basic interface for the session repository in the Clean pattern.
 *
 * Created by Usuario on 04/09/2017.
 */
interface SessionRepository {

    /**
     * Method to check if the current session of the user is valid.
     *
     * @return true - if the current session is not be expired
     * @see AuthorizedUser.isValid
     */
    val isValidSession: Boolean

    /**
     * Method to obtain the current user session.
     *
     * @return the session
     * @see AuthorizedUser
     */
    var session: AuthorizedUser?

    /**
     * Method to update the user information into the [session] object.
     */
    fun updateUserInfo(user: User)

    /**
     * Method to invalidate the current user session.
     */
    fun invalidateSession()
}
