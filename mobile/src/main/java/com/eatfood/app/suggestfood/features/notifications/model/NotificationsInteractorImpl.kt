package com.eatfood.app.suggestfood.features.notifications.model

import com.eatfood.app.suggestfood.entity.AuthorizedUser

/**
 * Basic implementation of the [NotificationsInteractor] interface
 * for interactor in the Clean pattern.
 *
 * @property notificationsRepository the notifications repository
 * @constructor Instantiates a new Notificatios interactor.
 * @see NotificationsInteractor
 * @see NotificationsRepository
 *
 * Created by Usuario on 19/04/2018.
 */
class NotificationsInteractorImpl(private val notificationsRepository: NotificationsRepository) : NotificationsInteractor {

    override var authorizedUser: AuthorizedUser
        get() = TODO("not implemented") //To change initializer of created properties use File | Settings | File Templates.
        set(value) { notificationsRepository.authorizedUser = value }

}