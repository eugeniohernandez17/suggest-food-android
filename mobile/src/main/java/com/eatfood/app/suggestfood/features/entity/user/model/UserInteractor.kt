package com.eatfood.app.suggestfood.features.entity.user.model

import android.graphics.Bitmap
import android.net.Uri
import com.eatfood.app.suggestfood.entity.AuthorizedUser

/**
 * Basic interface for the interactor in the Clean pattern.
 *
 * Created by Usuario on 22/09/2017.
 */
interface UserInteractor {

    /**
     * Method for getting the user information using the data of the authorized user.
     *
     * @param authorizedUser the authorized user
     * @see AuthorizedUser
     */
    fun getUser(authorizedUser: AuthorizedUser)

    /**
     * Method to update user information.
     *
     * @param authorizedUser the authorized user
     * @see AuthorizedUser
     */
    fun updateUser(authorizedUser: AuthorizedUser)

    /**
     * Method to update the user's profile image.
     *
     * @param authorizedUser the authorized user
     * @param imageUri the image's url in the file system of the phone
     * @param imageBitMap the image
     * @see AuthorizedUser
     * @see Uri
     * @see Bitmap
     */
    fun updateUser(authorizedUser: AuthorizedUser, imageUri: Uri, imageBitMap: Bitmap?)

}
