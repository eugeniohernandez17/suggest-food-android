package com.eatfood.app.suggestfood.features.notifications.model

import android.util.Log
import com.eatfood.app.suggestfood.entity.AuthorizedUser
import com.onesignal.OneSignal

/**
 * Basic implementation of the [NotificationsRepository] for the repository in the Clean pattern.
 *
 * @see NotificationsRepository
 *
 * Created by Usuario on 19/04/2018.
 */
class NotificationsRepositoryImpl : NotificationsRepository {

    companion object {
        private val TAG: String = NotificationsRepository::class.java.name
    }

    override var authorizedUser: AuthorizedUser
        get() = TODO("not implemented") //To change initializer of created properties use File | Settings | File Templates.
        set(value) {
            Log.d(TAG, "OneSignal.setEmail { email: ${value.email} - token: ${value.accessToken} }")
            OneSignal.setEmail(value.email, value.accessToken, object : OneSignal.EmailUpdateHandler {
                override fun onSuccess() {
                    Log.i(TAG, "User's email is successfully registered in one signal.")
                }

                override fun onFailure(error: OneSignal.EmailUpdateError) {
                    Log.e(TAG, "Error registering the user's email in one signal.")
                    Log.e(TAG, "Error Type: ${error.type.name}")
                    Log.e(TAG, "Error Message: ${error.message}")
                }
            })
        }
}