package com.eatfood.app.suggestfood.domain.presenter

import com.eatfood.app.suggestfood.domain.event.MessageEvent
import com.eatfood.app.suggestfood.domain.view.type.progress.ProgressView
import com.eatfood.app.suggestfood.lib.eventbus.EventBus

/**
 * Support of the presenter for the display progress of a task in the UI.
 *
 * @param view     the view
 * @param eventBus the event bus object
 * @constructor Instantiates a new Progress presenter.
 * @see ProgressView
 * @see GeneralPresenterImpl
 * @see EventBus
 *
 * Created by Usuario on 22/09/2017.
 */
abstract class ProgressPresenter<T : ProgressView, E : Any>(view: T?, eventBus: EventBus)
    : GeneralPresenterImpl<T, E>(view, eventBus) {

    protected open fun doAction(requestCode: Int, callback: () -> Unit) = view?.let {
        if (it.isNetworkConnectivityAvailable) {
            it.showProgress()
            callback()
        } else {
            it.onNetworkConnectivityDisabled(requestCode, this)
        }
    }

    protected open fun onError(event: MessageEvent<E>, requestCode: Int) = view?.let {
        it.hideProgress()

        if (event.isSimpleMessage)
            it.displayError(event.message ?: "")
        else
            it.displayError(event.error, requestCode)
    }
}
