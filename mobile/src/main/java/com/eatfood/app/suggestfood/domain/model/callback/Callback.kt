package com.eatfood.app.suggestfood.domain.model.callback

import okhttp3.Headers
import okhttp3.ResponseBody
import java.io.Serializable

/**
 * Basic interface for the return of calls to the request.
 * Indicates the three possible events for a normal request to the services api.
 *
 * @param <S> the type parameter object for the [onSuccessful] method
 * @param <F> the type parameter object for the [onFailure] method and the [onError] method
 * @see Serializable
 *
 * Created by Usuario on 01/09/2017.
 */
interface Callback<S : Serializable, F : Serializable> {

    /**
     * On successful event.
     *
     * @param entity  the entity
     * @param headers the headers
     * @see Headers
     */
    fun onSuccessful(entity: S, headers: Headers)

    /**
     * On failure event.
     *
     * @param entity       the entity
     * @param httpCode     the http response code
     * @param responseBody the response body
     * @see ResponseBody
     */
    fun onFailure(entity: F, httpCode: Int, responseBody: ResponseBody)

    /**
     * On error event.
     *
     * @param entity    the entity
     * @param throwable the throwable error
     * @see Throwable
     */
    fun onError(entity: F, throwable: Throwable)

}
