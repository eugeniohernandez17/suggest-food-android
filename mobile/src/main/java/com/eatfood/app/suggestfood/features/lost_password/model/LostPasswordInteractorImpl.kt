package com.eatfood.app.suggestfood.features.lost_password.model

import com.eatfood.app.suggestfood.entity.User

/**
 * Basic implementation of the [LostPasswordInteractor] interface
 * for interactor in the Clean pattern.
 *
 * @property lostPasswordRepository the lost password repository
 * @constructor Instantiates a new Lost password interactor.
 * @see LostPasswordInteractor
 * @see LostPasswordRepository
 *
 * Created by Usuario on 18/09/2017.
 */
class LostPasswordInteractorImpl(private val lostPasswordRepository: LostPasswordRepository)
    : LostPasswordInteractor {

    override fun doRecoverPassword(email: String)
        = this.lostPasswordRepository.sendInstruccionsForRecoveryPassword(User(email = email))
}
