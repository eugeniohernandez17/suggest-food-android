package com.eatfood.app.suggestfood.domain.view

import android.app.Activity
import android.content.Context
import android.content.Intent
import android.net.ConnectivityManager
import android.net.NetworkInfo
import android.support.annotation.IdRes
import android.support.annotation.MenuRes
import android.support.annotation.PluralsRes
import android.support.v4.app.DialogFragment
import android.support.v4.app.Fragment
import android.support.v7.app.ActionBar
import android.support.v7.app.AppCompatActivity
import android.view.Menu
import android.view.View
import android.view.Window
import android.view.inputmethod.InputMethodManager
import java.io.Serializable

/**
 * Basic class for the app's Activity objects.
 *
 * @see Activity
 * @see AppCompatActivity
 *
 * Created by Eugenio on 03/05/2016.
 */
abstract class AbsctractActivity : AppCompatActivity() {

    /*Own Methods*/

    /**
     * Method to check if the internet connection is enabled.
     *
     * @return true - if the internet connection is enabled
     * @see NetworkInfo.isConnectedOrConnecting
     */
    protected open val isNetworkConnectivityAvailable: Boolean
        get() {
            val cm = getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
            val netInfo = cm.activeNetworkInfo
            return netInfo != null && netInfo.isConnectedOrConnecting
        }

    /**
     * Method to hide the phone keypad.
     *
     * @see View
     * @see InputMethodManager
     */
    protected open fun hideKeyboard() {
        val view = this.currentFocus
        if (view != null) {
            val imm = getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
            imm.hideSoftInputFromWindow(view.windowToken, 0)
        }
    }

    /**
     * Method for creating the menu using an XML file.
     *
     * @param menu      the menu object
     * @param idMenuRes the id of the XML file.
     * @return true - if the menu is created successfully
     * @see Menu
     */
    protected fun createOptionsMenu(menu: Menu, @MenuRes idMenuRes: Int): Boolean {
        // Inflate the menu; this adds items to the action bar if it is present.
        menuInflater.inflate(idMenuRes, menu)
        return true
    }

    /**
     * Set up the [android.app.ActionBar], if the API is available.
     *
     * @see ActionBar
     */
    protected open fun setupActionBar() {
        val actionBar = supportActionBar
        actionBar?.setDisplayHomeAsUpEnabled(true)
    }

    /**
     * Method to hide the action bar.
     *
     * @see AppCompatActivity.getSupportActionBar
     */
    protected fun hideActionBar() {
        window.requestFeature(Window.FEATURE_ACTION_BAR)
        supportActionBar?.hide()
    }

    /**
     * Method to show a fragment in the context.
     *
     * @param containerViewId Identifier of the container whose fragment(s) are to be replaced.
     * @param fragment        The new fragment to place in the container.
     * @see Fragment
     */
    protected fun showFragment(@IdRes containerViewId: Int, fragment: Fragment)
        = supportFragmentManager.beginTransaction().replace(containerViewId, fragment).commit()

    /**
     * Method to show a dialog fragment.
     *
     * @param dialogFragment The new dialog fragment to display
     * @see DialogFragment
     */
    protected fun showFragment(dialogFragment: DialogFragment)
        = dialogFragment.show(supportFragmentManager, "dialog")

    /**
     * Method to update the extra object parameter.
     *
     * @param name  the extra's name
     * @param extra the extra value
     * @see Intent
     * @see Serializable
     */
    protected fun setExtra(name: String, extra: Serializable) = intent?.putExtra(name, extra)

    /**
     * Method to change the enabled state of the inputs
     *
     * @see View.setEnabled
     */
    protected fun setInputs(enabled: Boolean, vararg inputs: View)
        = inputs.forEach { it.isEnabled = enabled }

    protected fun getQuantityString(@PluralsRes idRes: Int, number: Int): String?
        = resources.getQuantityString(idRes, number, number)

    /**
     * Redirect to another activity.
     *
     * @param intent     the intent to redirect
     * @param withFinish true - if the current activity will be finished after the redirect
     */
    protected fun redirect(intent: Intent, withFinish: Boolean) {
        startActivity(intent)
        if (withFinish)
            this.finish()
    }
}
