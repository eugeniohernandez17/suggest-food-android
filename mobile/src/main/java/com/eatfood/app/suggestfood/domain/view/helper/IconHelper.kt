package com.eatfood.app.suggestfood.domain.view.helper

import android.content.Context
import android.graphics.drawable.Drawable
import android.os.Build
import android.support.annotation.DrawableRes
import android.support.graphics.drawable.VectorDrawableCompat
import android.support.v7.content.res.AppCompatResources
import android.widget.TextView


/**
 * Support for drawables icons.
 * Contain the support for the API 21 and below.
 *
 * @see Drawable
 *
 * Created by Usuario on 02/02/2018.
 */
class IconHelper(private val context: Context) {

    /**
     * The enum that indicates the icon position in the field.
     *
     * @see [setIcon]
     */
    enum class DrawablePosition {
        TOP, LEFT, RIGHt, BOTTOM
    }

    /**
     * Method for obtaining the drawable resource using their id.
     *
     * @param resId the resource id
     * @return the resource drawable
     */
    fun getDrawable (@DrawableRes resId: Int): Drawable?
        = if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            AppCompatResources.getDrawable(context, resId)
        } else {
            getVectorDrawable(resId)
        }

    /**
     * Method for obtaining the vector drawable resource using their id.
     *
     * @param resId the resource id
     * @return the vector drawable resource
     * @see VectorDrawableCompat
     */
    fun getVectorDrawable(@DrawableRes resId: Int): VectorDrawableCompat?
        = VectorDrawableCompat.create(context.resources, resId, context.theme)

    /**
     * Method to update the field icon in the position indicated.
     *
     * @param position the icon position
     * @param resId the resource id
     * @param view the field
     * @see DrawablePosition
     * @see TextView
     */
    fun setIcon(position: DrawablePosition, @DrawableRes resId: Int, view: TextView) {
        val drawable = getDrawable(resId)
        when (position) {
            IconHelper.DrawablePosition.LEFT -> view.setCompoundDrawablesRelativeWithIntrinsicBounds(drawable, null, null, null)
            IconHelper.DrawablePosition.TOP -> view.setCompoundDrawablesRelativeWithIntrinsicBounds(null, drawable, null, null)
            IconHelper.DrawablePosition.RIGHt -> view.setCompoundDrawablesRelativeWithIntrinsicBounds(null, null, drawable, null)
            IconHelper.DrawablePosition.BOTTOM -> view.setCompoundDrawablesRelativeWithIntrinsicBounds(null, null, null, drawable)
        }
    }

}
