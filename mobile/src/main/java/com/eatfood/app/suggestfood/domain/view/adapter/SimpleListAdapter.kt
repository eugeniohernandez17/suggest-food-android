package com.eatfood.app.suggestfood.domain.view.adapter

import android.content.Context
import android.widget.ArrayAdapter

/**
 * Basic adapter for a string list.
 *
 * @see ArrayAdapter
 *
 * Created by Usuario on 05/09/2017.
 */
class SimpleListAdapter(context: Context, objects: List<String>)
    : ArrayAdapter<String>(context, android.R.layout.simple_dropdown_item_1line, objects)
