package com.eatfood.app.suggestfood.features.feedback.model.callback

import android.content.Context
import com.eatfood.app.suggestfood.domain.event.MessageEvent
import com.eatfood.app.suggestfood.domain.model.callback.GeneralCallback
import com.eatfood.app.suggestfood.features.feedbacks.FeedbacksEventType
import com.eatfood.app.suggestfood.lib.eventbus.EventBus
import com.eatfood.app.suggestfood.receiver.sync.SyncReceiver
import com.eatfood.app.suggestfood.service.external.feedback.FeedbackReturn
import com.google.gson.Gson
import okhttp3.Headers
import okhttp3.ResponseBody

/**
 * Callback for the different request events in the feedback save process.
 *
 * @param eventBus   the event bus pattern
 * @param gson       the gson object
 * @property context the context
 * @constructor Instantiates a new Feedback callback.
 * @see GeneralCallback
 * @see FeedbackReturn
 * @see EventBus
 * @see Gson
 * @see Context
 *
 * Created by Usuario on 22/03/2018.
 */
class FeedbackCallback(eventBus: EventBus, gson: Gson, private val context: Context)
    : GeneralCallback<FeedbackReturn.Successful, FeedbackReturn.Failure>(eventBus, gson, FeedbackReturn.Failure::class.java) {

    override fun onSuccessful(entity: FeedbackReturn.Successful, headers: Headers) {
        val messageEvent = MessageEvent(FeedbacksEventType.ON_SAVE_FEEDBACKS_SUCCESS, entity.feedback)
        if (entity.remote) {
            eventBus.post(messageEvent)
        } else {
            SyncReceiver.sendBroadcast(context, messageEvent)
        }
    }

    override fun onFailure(entity: FeedbackReturn.Failure, httpCode: Int, responseBody: ResponseBody){
        val messageEvent = buildOnFailureMessage(entity, httpCode, responseBody)
        if (entity.remote) {
            eventBus.post(messageEvent)
        } else {
            SyncReceiver.sendBroadcast(context, messageEvent)
        }
    }

    override fun onError(entity: FeedbackReturn.Failure, throwable: Throwable) {
        val messageEvent = MessageEvent(FeedbacksEventType.ON_SAVE_FEEDBACK_ERROR, entity, throwable)
        if (entity.remote) {
            eventBus.post(messageEvent)
        } else {
            SyncReceiver.sendBroadcast(context, messageEvent)
        }
    }

    override fun buildOnFailureMessage(): MessageEvent<*>
            = MessageEvent(FeedbacksEventType.ON_SAVE_FEEDBACK_ERROR, true)

}