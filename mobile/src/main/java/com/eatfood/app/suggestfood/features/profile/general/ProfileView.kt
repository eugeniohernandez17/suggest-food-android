package com.eatfood.app.suggestfood.features.profile.general

import com.eatfood.app.suggestfood.entity.User

/**
 * Basic interface for the view in the MVP pattern.
 *
 * Created by Usuario on 18/10/2017.
 */
interface ProfileView {

    /**
     * Method to update the user object parameter for loading the user information.
     *
     * @see [loadProfile]
     * @see User
     */
    var userParam: User?

    /**
     * Method for loading the user information.
     *
     * @param user the user
     */
    fun loadProfile(user: User)

    companion object {

        /**
         * The constant EXTRA_USER for identifying the user object between the view's parameters.
         */
        const val EXTRA_USER = "EXTRA_USER"
    }

}
