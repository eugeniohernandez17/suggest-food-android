package com.eatfood.app.suggestfood.service.internal.sync

import android.accounts.Account
import android.annotation.SuppressLint
import android.content.ContentResolver
import android.content.Context
import android.content.SyncRequest
import android.os.Build
import android.os.Bundle
import com.eatfood.app.suggestfood.R
import com.eatfood.app.suggestfood.service.internal.sync.adapter.SyncAdapter
import org.jetbrains.anko.bundleOf

/**
 * The object that creates to the sync request.
 * It needs a sync type for executing.
 *
 * @see SyncType
 *
 * Created by Usuario on 03/04/2018.
 */
@SuppressLint("ObsoleteSdkInt")
object Sync {

    /**
     * The settings flags
     *
     * @see Bundle
     */
    private val settingsBundle: Bundle
        get() {
            val settingsBundle = Bundle()
            settingsBundle.putBoolean(ContentResolver.SYNC_EXTRAS_MANUAL, true)
            settingsBundle.putBoolean(ContentResolver.SYNC_EXTRAS_EXPEDITED, true)
            return settingsBundle
        }

    /**
     * Method for creating the sync request.
     *
     * @param context the context
     * @param account the user's account
     * @param syncType the sync type to execute
     * @see Context
     * @see Account
     * @see SyncType
     */
    fun request(context: Context, account: Account, syncType: SyncType) {
        val bundle = bundleOf(SyncAdapter.EXTRA_ACTION to syncType.name)
        request(context, account, bundle)
    }


    /**
     * Method to add the synchronization process periodically to the user's account.
     *
     * @param context the context
     * @param account the user's account
     * @param syncFrequency the frequency of sync in minutes
     */
    fun add(context: Context, account: Account, syncFrequency: Long) {
        val authority = context.getString(R.string.app_authority)
        val syncInterval = syncFrequency / 60
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
            // We can enable inexact timers in our periodic sync (better for batter life)
            val flexTime = (syncInterval * 0.1).toLong()
            val request = SyncRequest.Builder()
                    .syncPeriodic(syncInterval, flexTime)
                    .setSyncAdapter(account, authority)
                    .setExtras(Bundle()).build()
            ContentResolver.requestSync(request)
        } else {
            ContentResolver.addPeriodicSync(account, authority, Bundle(), syncInterval)
        }
    }

    /**
     * Method to remove the synchronization process periodically to the user's account.
     *
     * @param context the context
     * @param account the user's account
     */
    fun remove(context: Context, account: Account) {
        val authority = context.getString(R.string.app_authority)
        ContentResolver.removePeriodicSync(account, authority, Bundle())
    }

    /*Own Methods*/

    private fun request(context: Context, account: Account, bundle: Bundle) {
        val settingsBundle = this.settingsBundle
        settingsBundle.putAll(bundle)
        ContentResolver.requestSync(account, context.getString(R.string.app_authority), settingsBundle)
    }

}
