package com.eatfood.app.suggestfood.features.dashboard.presenter

import com.eatfood.app.suggestfood.domain.event.MessageEvent
import com.eatfood.app.suggestfood.domain.presenter.ProgressPresenter
import com.eatfood.app.suggestfood.entity.User
import com.eatfood.app.suggestfood.features.accounts.model.AccountsInteractor
import com.eatfood.app.suggestfood.features.dashboard.DashboardEventType
import com.eatfood.app.suggestfood.features.dashboard.view.DashboardView
import com.eatfood.app.suggestfood.features.entity.user.model.UserInteractor
import com.eatfood.app.suggestfood.features.session.model.SessionInteractor
import com.eatfood.app.suggestfood.features.settings.model.SettingsInteractor
import com.eatfood.app.suggestfood.lib.eventbus.EventBus
import org.greenrobot.eventbus.Subscribe

/**
 * Basic implementation of the [DashboardPresenter] interface for the presenter in the MVP pattern.
 *
 * @param dashboardView         the dashboard view
 * @param eventBus              the event bus pattern
 * @property sessionInteractor  the session interactor
 * @property userInteractor     the user interactor
 * @property settingsInteractor the settings interactor
 * @property accountsInteractor the accounts interactor
 * @constructor Instantiates a new Dashboard presenter.
 * @see DashboardPresenter
 * @see DashboardView
 * @see EventBus
 * @see SessionInteractor
 * @see UserInteractor
 * @see SettingsInteractor
 * @see AccountsInteractor
 *
 * Created by Usuario on 07/01/2017.
 */
class DashboardPresenterImpl(
        dashboardView: DashboardView,
        eventBus: EventBus,
        private val sessionInteractor: SessionInteractor,
        private val userInteractor: UserInteractor,
        private val settingsInteractor: SettingsInteractor,
        private val accountsInteractor: AccountsInteractor
) :
        ProgressPresenter<DashboardView, DashboardEventType>(dashboardView, eventBus),
        DashboardPresenter
{

    override fun onCreate() {
        super.onCreate()
        checkCurrentSession()
    }

    /*INTERFACES*/
    //1. DashboardPresenter

    override fun checkCurrentSession() = view?.let {
        if (!sessionInteractor.isValidSession) {
            it.navigateToLoginScreen()
        } else {
            doAction(DashboardPresenter.GET_USER_INFO_REQUEST_CODE) {
                userInteractor.getUser(sessionInteractor.session!!)
            }
        }
    }

    override fun logout() {
        sessionInteractor.invalidateSession()
        settingsInteractor.clearSettings()
        view?.navigateToLoginScreen()
    }

    override fun onViewProfile() {
        sessionInteractor.session?.let { view?.navigateToProfileScreen(it.user.target) }
    }

    override fun onViewSettings() = view?.navigateToSettingsScreen()

    override fun onViewFeedbacks() = view?.navigateToFeedbacksScreen()

    override fun onProfileShowingFinish(user: User) {
        view?.loadUser(user)
    }

    @Subscribe
    override fun onMessageEvent(event: MessageEvent<DashboardEventType>) = when (event.type) {
        DashboardEventType.ON_GET_USER_INFO_SUCCESS -> {
            val user: User = event.getData()
            if (user.notificationsUserId != null) {
                loadUserInfo(event.getData())
            } else {
                logout()
            }
        }
        DashboardEventType.ON_GET_USER_INFO_ERROR -> {
            onError(event, DashboardPresenter.GET_USER_INFO_REQUEST_CODE)
        }
    }

    override fun onResolution(requestCode: Int) = when (requestCode) {
        DashboardPresenter.GET_USER_INFO_REQUEST_CODE -> checkCurrentSession()
        else -> {}
    }

    /*Own Methods*/

    private fun loadUserInfo(user: User) = view?.let {
        sessionInteractor.updateUserInfo(user)
        val account = accountsInteractor.find(user.email)
        it.hideProgress()
        it.loadUser(user)
        account?.let {
            val syncFrequency = settingsInteractor.syncFrequency
            if (syncFrequency > 0) {
                view!!.addPeriodicSync(it, syncFrequency)
            } else {
                view!!.removePeriodicSync(it)
            }
        }
    }
}
