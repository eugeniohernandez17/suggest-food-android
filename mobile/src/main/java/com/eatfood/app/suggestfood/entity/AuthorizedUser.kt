package com.eatfood.app.suggestfood.entity

import com.google.gson.annotations.Expose
import io.objectbox.annotation.Entity
import io.objectbox.annotation.Id
import io.objectbox.relation.ToOne
import java.io.Serializable
import java.util.*

/**
 * Business entity that represents a user correctly authenticated with their credentials.
 *
 * @constructor Instantiates a new Authorized user.
 * @see Serializable
 * @see Credentials
 *
 * Created by Usuario on 01/09/2017.
 */

@Entity
data class AuthorizedUser(
        @Id
        @Expose(serialize = false, deserialize = false)
        var id: Long = 0,
        var accessToken: String? = null,
        var client: String? = null,
        var uid: String? = null,
        var expiry: Long? = null,
        var createdAt: Date = Date()
) : Serializable, Credentials {

    companion object {
        private const val serialVersionUID = 3835725563334924497L
    }

    lateinit var user: ToOne<User>

    /*INTERFACES*/
    //2. Credentials

    override var email: String
        get() { return user.target.email }
        set(email) { user.target.email = email }

    override var password: String
        get() { return user.target.password }
        set(password) { user.target.password = password }

    /*Own Methods*/

    /**
     * Method that verifies if the session of the user has not yet expired.
     *
     * @return true - if the session has not expired
     */
    val isValid: Boolean
        get() = (expiry != null) and (Date().time - createdAt.time < expiry!!)

}
