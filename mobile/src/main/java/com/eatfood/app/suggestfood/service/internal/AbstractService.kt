package com.eatfood.app.suggestfood.service.internal

import android.app.Service

/**
 * Basic class for the app's service objects.
 *
 * @see Service
 *
 * Created by Usuario on 04/04/2018.
 */
abstract class AbstractService : Service()