package com.eatfood.app.suggestfood.domain.resolution.di

import android.content.Context
import com.eatfood.app.suggestfood.domain.resolution.http.HttpResolution
import com.eatfood.app.suggestfood.domain.resolution.http.HttpResolutionImpl
import com.eatfood.app.suggestfood.domain.resolution.networkconnectivity.NetworkConnectivityResolution
import com.eatfood.app.suggestfood.domain.resolution.networkconnectivity.NetworkConnectivityResolutionImpl
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

/**
 * Injection dependency of the Resolution module.
 *
 * @constructor Instantiates a new Resolution module.
 * @property context the context
 * @see Module
 * @see Context
 *
 * Created by Usuario on 08/09/2017.
 */
@Module
class ResolutionModule(private val context: Context) {

    /**
     * Provide network connectivity resolution.
     *
     * @return the network connectivity resolution
     * @see NetworkConnectivityResolution
     */
    @Provides
    @Singleton
    fun provideNetworkConnectivityResolution(): NetworkConnectivityResolution
        = NetworkConnectivityResolutionImpl(context)

    /**
     * Provide http resolution.
     *
     * @return the http resolution
     * @see HttpResolution
     */
    @Provides
    @Singleton
    fun provideHttpResolution(): HttpResolution = HttpResolutionImpl(context)

}
