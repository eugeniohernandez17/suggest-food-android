package com.eatfood.app.suggestfood.domain.view.adapter

import android.support.annotation.LayoutRes
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.eatfood.app.suggestfood.domain.view.adapter.EntityRecyclerViewAdapter.OnListInteractionListener
import java.io.Serializable

/**
 * [RecyclerView.Adapter] that can display a [E] list and makes a call to the
 * specified [EntityRecyclerViewAdapter.OnListInteractionListener].
 *
 * @param E  the entity type parameter
 * @param VH the view holder type parameter
 * @param L  the listener type parameter
 * @property mValues    the items
 * @property mListener the listener
 * @constructor Instantiates a new Entity recycler view adapter.
 * @see RecyclerView.Adapter
 * @see RecyclerView.ViewHolder
 * @see OnListInteractionListener
 *
 * Created by Usuario on 08/10/2017.
 */
abstract class EntityRecyclerViewAdapter<E : Serializable,
                                         VH : RecyclerView.ViewHolder,
                                         L : EntityRecyclerViewAdapter.OnListInteractionListener<E>>
protected constructor(
        private val mValues: MutableList<E>,
        protected val mListener: L
) : RecyclerView.Adapter<VH>() {

    /**
     * The identifier of the layout to be placed in the view for each item.
     */
    @get:LayoutRes
    protected abstract val layout: Int

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): VH {
        val view = LayoutInflater.from(parent.context).inflate(layout, parent, false)
        return getViewHolder(view)
    }

    override fun onBindViewHolder(holder: VH, position: Int)
        = onBindViewHolder(mValues[position], holder, position)

    override fun getItemCount(): Int = mValues.size

    /*ABSCTRACT METHODS*/

    /**
     * Instantiates a new view holder.
     *
     * @param view the view
     * @return the view holder
     * @see VH
     * @see RecyclerView.ViewHolder
     * @see View
     */
    protected abstract fun getViewHolder(view: View): VH

    /**
     * Called by RecyclerView to display the data at the specified position.
     *
     * @param entity   the entity
     * @param holder   the view holder
     * @param position the position in the [mValues]
     * @see E
     * @see VH
     * @see RecyclerView.ViewHolder
     */
    protected abstract fun onBindViewHolder(entity: E, holder: VH, position: Int)

    /*Own Methods*/

    /**
     * Method to load the list of items.
     *
     * @param items the items
     * @see List
     * @see E
     */
    fun addAll(items: List<E>?) = items?.let {
        mValues.addAll(items)
        notifyDataSetChanged()
    }

    /**
     * Method to delete the list of items.
     */
    fun clear() {
        mValues.clear()
        notifyDataSetChanged()
    }

    /**
     * Basic interaction interface with views added to the pattern.
     *
     * @param E the entity type parameter
     * @see E
     * @see Serializable
     */
    interface OnListInteractionListener<E : Serializable> {

        /**
         * Event generated when selecting an item from the list.
         *
         * @param item the item
         * @see E
         */
        fun onSelect(item: E)

    }
}
