package com.eatfood.app.suggestfood.features.settings.view.fragments

import android.preference.Preference
import com.eatfood.app.suggestfood.domain.view.listeners.SyncListener

/**
 * Basic interface for the view in the MVP pattern.
 * It includes the methods for data management through the sync process.
 *
 * @see SyncListener
 *
 * Created by Usuario on 17/04/2018.
 */
interface PreferenceView : SyncListener {

    companion object {

        /**
         * The constants that identifying the preference that has changed their value.
         * They are necessary for use in the business logic of the presenter and
         * make the actions required.
         *
         * @see SettingsPreferenceFragment
         * @see Preference.OnPreferenceChangeListener
         */
        const val SYNC_FREQUENCY: String = "sync_frequency"

    }

}
