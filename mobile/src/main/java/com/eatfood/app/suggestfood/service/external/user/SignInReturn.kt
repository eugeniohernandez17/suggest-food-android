package com.eatfood.app.suggestfood.service.external.user

import com.eatfood.app.suggestfood.entity.User
import com.eatfood.app.suggestfood.service.external.helper.ErrorHelper
import com.google.gson.annotations.SerializedName
import java.io.Serializable

/**
 * Created by Usuario on 04/09/2017.
 */

class SignInReturn {

    class Successful(
            @field:SerializedName(value = "authorized_user", alternate = ["data"])
            val user: User
    ) : Serializable

    class Failure(val user: User) : Serializable {

        var errors: List<String>? = null

        override fun toString(): String = ErrorHelper.getError(errors)
    }
}
