package com.eatfood.app.suggestfood.features.lost_password.model

/**
 * Basic interface for the interactor in the Clean pattern of the lost password feature.
 *
 * Created by Usuario on 14/09/2017.
 */
interface LostPasswordInteractor {

    /**
     * Method to recover the password of the user.
     *
     * @param email the user email
     */
    fun doRecoverPassword(email: String)

}
