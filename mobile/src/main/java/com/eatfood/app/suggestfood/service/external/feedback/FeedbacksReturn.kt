package com.eatfood.app.suggestfood.service.external.feedback

import com.eatfood.app.suggestfood.entity.Feedback
import com.google.gson.annotations.SerializedName

import java.io.Serializable

/**
 * Created by Usuario on 13/10/2017.
 */

class FeedbacksReturn(
        var page: Int,
        @field:SerializedName("type")
        var feedbackType: Feedback.FeedbackType?
) : Serializable {

    constructor(page: Int, feedbackType: Feedback.FeedbackType, feedbacks: List<Feedback>)
            : this(page, feedbackType) {
        this.feedbacks = feedbacks
    }

    var feedbacks: List<Feedback>? = null

    val isFirstPage: Boolean
        get() = page == 1

    val nextPage: Int
        get() = page + 1

}
