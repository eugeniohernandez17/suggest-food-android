package com.eatfood.app.suggestfood.features.profile.edit.model.callback

import com.eatfood.app.suggestfood.domain.event.MessageEvent
import com.eatfood.app.suggestfood.domain.model.callback.GeneralCallback
import com.eatfood.app.suggestfood.features.profile.edit.ProfileEventType
import com.eatfood.app.suggestfood.lib.eventbus.EventBus
import com.eatfood.app.suggestfood.service.external.user.UserReturn
import com.google.gson.Gson

import okhttp3.Headers

/**
 * Callback for the different request events in the profile updating process.
 *
 * @param eventBus   the event bus pattern
 * @param gson       the gson object
 * @constructor Instantiates a new User callback.
 * @see GeneralCallback
 * @see UserReturn.Successful
 * @see UserReturn.Failure
 * @see EventBus
 * @see Gson
 *
 * Created by Usuario on 28/09/2017.
 */
class UserCallback<U : UserReturn.Successful>(eventBus: EventBus, gson: Gson)
    : GeneralCallback<U, UserReturn.Failure>(eventBus, gson, UserReturn.Failure::class.java) {

    override fun buildOnFailureMessage(): MessageEvent<*>
        = MessageEvent(ProfileEventType.ON_UPDATE_PROFILE_ERROR)

    /*INTERFACES*/
    //1. GeneralCallback

    override fun onSuccessful(entity: U, headers: Headers)
        = eventBus.post(MessageEvent(ProfileEventType.ON_UPDATE_PROFILE_SUCCESS, entity.message ?: "", entity))

    override fun onError(entity: UserReturn.Failure, throwable: Throwable)
        = eventBus.post(MessageEvent(ProfileEventType.ON_UPDATE_PROFILE_ERROR, throwable))
}
