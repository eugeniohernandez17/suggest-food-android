package com.eatfood.app.suggestfood.domain.event

import com.eatfood.app.suggestfood.lib.eventbus.EventBus
import java.io.Serializable

/**
 * Base class for maintaining the communication between the objects
 * in the EventBus pattern. The parameter [T] indicates the type event of the message.
 *
 * @param T the type event class
 * @property type the type event
 * @constructor Instantiates a new Message event.
 * @see EventBus
 * @see Serializable
 *
 * Created by Usuario on 12/01/2017.
 */
class MessageEvent<T : Any>(val type: T) : Serializable {

    /**
     * Instantiates a new Message event.
     *
     * @param type    the type event
     * @param message the message
     */
    constructor(type: T, message: String) : this(type) {
        this.message = message
    }

    /**
     * Instantiates a new Message event.
     *
     * @param type the type event
     * @param data the extra data
     */
    constructor(type: T, data: Any) : this(type) {
        this.data = data
    }

    /**
     * Instantiates a new Message event.
     *
     * @param type    the type event
     * @param message the message
     * @param data    the extra data
     */
    constructor(type: T, message: String, data: Any) : this(type, message) {
        this.data = data
    }

    /**
     * Instantiates a new Message event.
     *
     * @param type  the type event
     * @param error the error
     * @see Throwable
     */
    constructor(type: T, error: Throwable) : this(type) {
        this.error = error
    }

    /**
     * Instantiates a new Message event.
     *
     * @param type  the type event
     * @param data    the extra data
     * @param error the error
     * @see Throwable
     */
    constructor(type: T, data: Any, error: Throwable) : this(type, data){
        this.error = error
    }

    var message: String? = null

    private var data: Any? = null

    var error: Throwable? = null

    override fun hashCode(): Int {
        val code = type.hashCode()
        return when {
            message != null -> code + message!!.hashCode()
            error != null -> code + error!!.hashCode()
            else -> code
        }
    }

    override fun equals(other: Any?): Boolean = when(other){
        is MessageEvent<*> -> isSameEvent(other) and if (isSimpleMessage) isSameMessage(other) else true
        else -> super.equals(other)
    }

    /*Own Methods*/

    /**
     * Method to verify the error caused is a simple message or an error object.
     *
     * @return true - if the message is a string object
     *         false - if the message is a error object
     * @see Throwable
     */
    val isSimpleMessage: Boolean
        get() = this.message != null

    private fun isSameEvent(other: MessageEvent<*>): Boolean = this.type == other.type

    private fun isSameMessage(other: MessageEvent<*>): Boolean = this.message == other.message

    /**
     * Get extra data.
     *
     * @param D the type of the extra data
     * @return the extra data
     */
    @Suppress("UNCHECKED_CAST")
    fun <D> getData(): D = data as D

    /**
     * Sets extra data.
     *
     * @param data the data
     */
    fun setData(data: Any) {
        this.data = data
    }
}
