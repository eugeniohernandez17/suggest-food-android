package com.eatfood.app.suggestfood.features.feedback.model

import com.eatfood.app.suggestfood.entity.Feedback
import com.eatfood.app.suggestfood.entity.Feedback_
import com.eatfood.app.suggestfood.features.feedback.model.callback.FeedbackCallback
import com.eatfood.app.suggestfood.service.external.feedback.FeedbackReturn
import com.eatfood.app.suggestfood.service.external.feedback.FeedbacksService
import io.objectbox.Box
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

/**
 * Basic implementation of the [FeedbackRepository] for the repository in the Clean pattern.
 *
 * @property feedbackBox the feedback box
 * @property feedbacksService the feedbacks service
 * @property feedbackCallback the feedbacks callback
 * @constructor Instantiates a new Feedback repository.
 * @see Box
 * @see FeedbackRepository
 * @see FeedbacksService
 * @see FeedbackCallback
 *
 * Created by Usuario on 22/03/2018.
 */
class FeedbackRepositoryImpl(
        private val feedbackBox: Box<Feedback>,
        private val feedbacksService: FeedbacksService,
        private val feedbackCallback: FeedbackCallback
) : FeedbackRepository {

    override val totalPendingFeedback = feedbackBox.count().toInt()

    override val lastPendingFeedback: Feedback?
        get() {
            val feedback = feedbackBox.query().orderDesc(Feedback_.id).build().findFirst()
            feedback?.let { removePendingFeedback(feedback) }
            return feedback
        }

    override fun addPendingFeedback(feedback: Feedback) {
        feedbackBox.put(feedback)
    }

    override fun removePendingFeedback(feedback: Feedback) = feedbackBox.remove(feedback)

    override fun addFeedback(feedback: Feedback, remote: Boolean) {
        val user = feedback.user.target
        val params = mapOf("feedback" to feedback )
        val request = feedbacksService.registerFeedback(user.id, params = params)

        if (remote){
            request.enqueue(object : Callback<Feedback> {
                override fun onResponse(call: Call<Feedback>, response: Response<Feedback>)
                        = onAddFeedbackResponse(feedback, response, remote)

                override fun onFailure(call: Call<Feedback>, throwable: Throwable)
                        = onAddFeedbackFailure(feedback, remote, throwable)
            })
        } else {
            try {
                onAddFeedbackResponse(feedback, request.execute(), remote)
            } catch (t: Throwable){
                onAddFeedbackFailure(feedback, remote, t)
            }
        }
    }

    /*Own Methods*/

    private fun onAddFeedbackResponse(feedback: Feedback, response: Response<Feedback>, remote: Boolean){
        if (response.isSuccessful) {
            val feedbackResponse = response.body()!!
            feedbackResponse.feedbackAssociate.target = feedback
            feedbackCallback.onSuccessful(FeedbackReturn.Successful(feedbackResponse, remote), response.headers())
        } else {
            feedbackCallback.onFailure(FeedbackReturn.Failure(feedback, remote), response.code(), response.errorBody()!!)
        }
    }

    private fun onAddFeedbackFailure(feedback: Feedback, remote: Boolean, throwable: Throwable){
        feedbackCallback.onError(FeedbackReturn.Failure(feedback, remote), throwable)
    }

}