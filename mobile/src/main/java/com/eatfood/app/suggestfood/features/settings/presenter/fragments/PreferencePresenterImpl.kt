package com.eatfood.app.suggestfood.features.settings.presenter.fragments

import android.accounts.Account
import android.util.Log
import com.eatfood.app.suggestfood.features.accounts.model.AccountsInteractor
import com.eatfood.app.suggestfood.features.session.model.SessionInteractor
import com.eatfood.app.suggestfood.features.settings.model.SettingsInteractor
import com.eatfood.app.suggestfood.features.settings.view.fragments.PreferenceView


/**
 * Basic implementation of the [PreferencePresenter] interface for the presenter in the MVP pattern.
 *
 * @property view               the view
 * @property settingsInteractor the settings interactor
 * @property sessionInteractor  the session interactor
 * @property accountsInteractor the accounts interactor
 * @constructor Instantiates a new Settings presenter.
 * @see PreferencePresenter
 * @see PreferenceView
 * @see SettingsInteractor
 * @see SessionInteractor
 * @see AccountsInteractor
 *
 * Created by Usuario on 17/04/2018.
 */
class PreferencePresenterImpl(
        val view: PreferenceView?,
        val settingsInteractor: SettingsInteractor,
        val sessionInteractor: SessionInteractor,
        val accountsInteractor: AccountsInteractor
) : PreferencePresenter {

    companion object {
        private val TAG = PreferencePresenter::class.java.name
    }

    override fun onPreferenceChange(key: String, value: Any) = when(key){
        PreferenceView.SYNC_FREQUENCY -> updateSyncProcess(value)
        else -> {}
    }

    /*Own Methods*/

    private fun updateSyncProcess(value: Any) = view?.let {
        val newSyncFrequency = value.toString().toLong()
        val syncFrequency = settingsInteractor.syncFrequency
        if (syncFrequency != newSyncFrequency) {
            val session = sessionInteractor.session
            val account = session?.let { accountsInteractor.find(it.email) }
            account?.let { updatePeriodicSync(it, syncFrequency, newSyncFrequency) }
        }
    }

    private fun updatePeriodicSync(account: Account, syncFrequency: Long, newSyncFrequency: Long) = view?.let {
        if (newSyncFrequency > 0) {
            view.addPeriodicSync(account, newSyncFrequency)
            Log.i(TAG, "The frequency range for data synchronization has changed from $syncFrequency minutes to $newSyncFrequency minutes")
        } else {
            view.removePeriodicSync(account)
            Log.i(TAG, "The data synchronization has been removed")
        }
    }
}