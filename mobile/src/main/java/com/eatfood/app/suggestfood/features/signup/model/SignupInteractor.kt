package com.eatfood.app.suggestfood.features.signup.model

import com.eatfood.app.suggestfood.entity.AuthorizedUser

/**
 * Basic interface for the interactor in the Clean pattern of the sign up feature.
 *
 * Created by Usuario on 06/09/2017.
 */

interface SignupInteractor {

    /**
     * Method that performs the sign up process.
     *
     * @param email    the user email
     * @param password the user password
     * @see AuthorizedUser
     */
    fun doSignUp(email: String, password: String)

}
