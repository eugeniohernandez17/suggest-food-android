package com.eatfood.app.suggestfood.domain.view.helper

import android.annotation.SuppressLint
import android.app.Activity
import android.support.design.widget.Snackbar
import android.view.Gravity
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import android.widget.Toast
import com.eatfood.app.suggestfood.R

/**
 * Class for the visualization of the messages in the interfaces.
 *
 * @see Snackbar
 *
 * Created by Usuario on 18/09/2017.
 */
object MessageHelper {

    /**
     * Method to show the general messages.
     *
     * @param view    the context view
     * @param message the message text
     */
    fun display(view: View, message: String) {
        val snackbar = Snackbar.make(view, message, Snackbar.LENGTH_LONG)
        val snackbarView = snackbar.view
        val textView = snackbarView.findViewById<TextView>(android.support.design.R.id.snackbar_text)
        textView.maxLines = 5
        snackbar.show()
    }

    /**
     * Method to show the general messages in the top of screen and below of the action bar.
     *
     * @param activity the activity
     * @param message  the message text
     */
    @SuppressLint("PrivateResource")
    fun display(activity: Activity, message: String){
        val viewGroup = activity.findViewById<ViewGroup>(R.id.custom_toast_container)
        val layout = activity.layoutInflater.inflate(R.layout.custom_toast, viewGroup)
        layout.findViewById<TextView>(R.id.text).text = message

        val y = activity.resources.getDimensionPixelSize(R.dimen.abc_action_bar_default_height_material)
        val toast = Toast(activity.applicationContext)
        toast.setGravity(Gravity.TOP or Gravity.START or Gravity.FILL_HORIZONTAL, 0, y)
        toast.duration = Toast.LENGTH_LONG
        toast.view = layout
        toast.show()
    }

}
