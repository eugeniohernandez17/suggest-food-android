package com.eatfood.app.suggestfood.features.signup.view

import com.eatfood.app.suggestfood.domain.view.fields.ConfirmationPasswordValidationView
import com.eatfood.app.suggestfood.domain.view.fields.EmailValidationView
import com.eatfood.app.suggestfood.domain.view.fields.PasswordValidationView
import com.eatfood.app.suggestfood.domain.view.type.fields.FieldsView
import com.eatfood.app.suggestfood.entity.User

/**
 * Basic interface for the view in the MVP pattern.
 * It includes the methods to validate the fields of email, password and confirmation password.
 *
 * @see FieldsView
 * @see EmailValidationView
 * @see PasswordValidationView
 *
 * Created by Usuario on 06/09/2017.
 */
interface SignupView : FieldsView, EmailValidationView, PasswordValidationView, ConfirmationPasswordValidationView {

    /**
     * Method to navigate to the login screen with the user registered.
     *
     * @param user the user information
     * @see User
     */
    fun navigateToLoginScreen(user: User)

}
