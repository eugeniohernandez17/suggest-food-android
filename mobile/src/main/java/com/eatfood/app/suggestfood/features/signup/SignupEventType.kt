package com.eatfood.app.suggestfood.features.signup

import com.eatfood.app.suggestfood.lib.eventbus.EventBus

/**
 * General events used in the EventBus pattern to identify api responses in the sign up process.
 *
 * @see EventBus
 *
 * Created by Usuario on 06/09/2017.
 */
enum class SignupEventType {

    /**
     * Event when the user was successfully registered.
     */
    ON_SIGN_UP_SUCCESS,

    /**
     * Event generated when there is an error in the sign up process.
     */
    ON_SIGN_UP_ERROR
}
