package com.eatfood.app.suggestfood.features.capture_image.view

import android.content.Context
import android.graphics.Bitmap
import android.net.Uri
import android.provider.MediaStore
import android.support.v7.app.AppCompatActivity
import android.util.Log
import com.eatfood.app.suggestfood.R
import com.robertlevonyan.components.picker.ItemModel
import com.robertlevonyan.components.picker.PickerDialog
import java.io.FileNotFoundException
import java.io.IOException

/**
 * Basic implementation of [CaptureImageView] interface
 * using the [PickerDialog] object as a view component.
 *
 * @param activity the activity where the [CaptureImageView] will be displayed
 * @constructor Instantiates a new Capture Image view
 * @see CaptureImageView
 * @see PickerDialog
 * @see AppCompatActivity
 *
 * Created by Usuario on 24/04/2018.
 */
class CaptureImage(activity: AppCompatActivity) : CaptureImageView {

    companion object {
        private val TAG = CaptureImageView::class.java.name
    }

    private var context: Context = activity

    private var builder: PickerDialog.Builder = PickerDialog.Builder(activity)

    private lateinit var pickerDialog: PickerDialog

    override fun onPermissionsResult(requestCode: Int, permissions: Array<out String>, grantResults: IntArray)
        = pickerDialog.onPermissionsResult(requestCode, permissions, grantResults)

    override fun show(listener: CaptureImageView.OnCaptureResultListener) {
        val items = arrayListOf(
                ItemModel(type = ItemModel.ITEM_GALLERY, itemLabel = context.getString(R.string.capture_image_select_from_gallery)),
                ItemModel(type = ItemModel.ITEM_CAMERA, itemLabel = context.getString(R.string.capture_image_take_a_photo))
        )
        pickerDialog = builder.setTitle(R.string.capture_image_title)
                .setTitleTextSize(context.resources.getDimension(R.dimen.sheets_text_size))
                .setListType(PickerDialog.TYPE_LIST)
                .setItems(items)
                .create()
        pickerDialog.setPickerCloseListener { type, uri ->
            when (type) {
                ItemModel.ITEM_CAMERA, ItemModel.ITEM_GALLERY -> {
                    listener.onResult(uri, getBitmapForUri(uri))
                }
            }
        }
        pickerDialog.show()
    }

    /*Own Methods*/

    private fun getBitmapForUri(uri: Uri): Bitmap? {
        return try {
            MediaStore.Images.Media.getBitmap(context.contentResolver, uri)
        } catch (e: FileNotFoundException){
            Log.e(TAG, "File Not Found, caused by: ${e.message}", e)
            null
        } catch (e: IOException) {
            Log.e(TAG, e.message, e)
            null
        }
    }
}