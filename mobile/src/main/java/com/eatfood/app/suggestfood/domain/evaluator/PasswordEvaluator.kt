package com.eatfood.app.suggestfood.domain.evaluator

import android.content.Context
import android.support.annotation.StringRes

import com.eatfood.app.suggestfood.R
import com.eatfood.app.suggestfood.domain.visitor.Evaluate

/**
 * Password evaluator based on the [MinMaxLengthEvaluator] class.
 * It contains the methods to know in detail if it is a valid password.
 *
 * @param context  the context
 * @param evaluate the evaluate object
 * @constructor Instantiates a new Password evaluator.
 * @see MinMaxLengthEvaluator
 * @see Evaluate
 * @see Context
 *
 * Created by Usuario on 30/08/2017.
 */
internal class PasswordEvaluator(context: Context, evaluate: Evaluate)
    : MinMaxLengthEvaluator(context, evaluate, 8, 12) {

    @get:StringRes
    override val errorMinLength = R.string.general_error_invalid_password_min

    @get:StringRes
    override val errorMaxLength = R.string.general_error_invalid_password_max
}
