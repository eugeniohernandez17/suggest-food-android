package com.eatfood.app.suggestfood.features.settings.presenter.fragments

/**
 * Basic interface for the presenter in the MVP pattern of the preference updating feature.
 *
 * Created by Usuario on 17/04/2018.
 */
interface PreferencePresenter {

    /**
     * Method that will be called when the value of the preference changes.
     *
     * @param key the name of the preference
     * @param value the new value
     */
    fun onPreferenceChange(key: String, value: Any): Unit?

}
