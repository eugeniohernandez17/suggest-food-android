package com.eatfood.app.suggestfood.features.profile.edit.view

import com.eatfood.app.suggestfood.domain.view.type.fields.FieldsView
import com.eatfood.app.suggestfood.entity.User

/**
 * Basic interface for the view in the MVP pattern.
 * It includes the methods to validate the phone field.
 *
 * @see FieldsView
 * @see com.eatfood.app.suggestfood.features.profile.general.ProfileView
 *
 * Created by Usuario on 25/09/2017.
 */
interface ProfileView : FieldsView, com.eatfood.app.suggestfood.features.profile.general.ProfileView {

    /**
     * Method to get the name entered.
     *
     * @return the name
     */
    val name: String

    /**
     * Method to get the phone entered.
     *
     * @return the phone
     */
    val phone: String

    /**
     * Method to show some error in the name field.
     *
     * @param error the error
     */
    fun showErrorInName(error: String?)

    /**
     * Method to show some error in the phone field.
     *
     * @param error the error
     */
    fun showErrorInPhone(error: String?)

    /**
     * Method to navigate to the profile screen.
     *
     * @param user the user information
     */
    fun navigateToProfileScreen(user: User)

    /**
     * Method to display the options list to capture the profile image.
     */
    fun displayOptionsToCaptureImage()
}
