package com.eatfood.app.suggestfood.service.internal.sync.adapter

import android.accounts.Account
import android.content.AbstractThreadedSyncAdapter
import android.content.ContentProviderClient
import android.content.Context
import android.content.SyncResult
import android.os.Bundle
import android.util.Log
import com.eatfood.app.suggestfood.content_provider.di.DatabaseModule
import com.eatfood.app.suggestfood.domain.di.DaggerDomainComponent
import com.eatfood.app.suggestfood.domain.di.DomainModule
import com.eatfood.app.suggestfood.domain.resolution.di.ResolutionModule
import com.eatfood.app.suggestfood.features.authorization.di.DaggerAuthorizationComponent
import com.eatfood.app.suggestfood.features.di.DaggerFeaturesComponent
import com.eatfood.app.suggestfood.features.feedback.di.FeedbackModule
import com.eatfood.app.suggestfood.lib.di.LibsModule
import com.eatfood.app.suggestfood.service.internal.sync.SyncType
import com.eatfood.app.suggestfood.service.internal.sync.di.DaggerSyncComponent
import com.eatfood.app.suggestfood.service.internal.sync.di.SyncModule
import com.eatfood.app.suggestfood.service.internal.sync.presenter.SyncPresenter
import me.eugeniomarletti.extras.bundle.BundleExtra
import me.eugeniomarletti.extras.bundle.base.String
import javax.inject.Inject

/**
 * Handle the transfer of data between a server and an app,
 * using the Android sync adapter framework.
 *
 * @param context the context
 * @constructor Instantiates a new Sync Adapter.
 * @see AbstractThreadedSyncAdapter
 * @see Context
 *
 * Created by Usuario on 03/04/2018.
 */
class SyncAdapter(context: Context) : AbstractThreadedSyncAdapter(context, true) {

    companion object {
        /**
         * The constant to identify the sync type to execute.
         */
        const val EXTRA_ACTION = "EXTRA_ACTION"
    }

    object BundleOptions {
        var Bundle.action by BundleExtra.String(name = EXTRA_ACTION, defaultValue = SyncType.ALL.name)
    }

    init {
        val databaseModule = DatabaseModule(context)
        val authorization = DaggerAuthorizationComponent.builder()
                .databaseModule(databaseModule)
                .build().provideAuthorization()
        val domainComponent = DaggerDomainComponent.builder()
                .libsModule(LibsModule(context, authorization))
                .resolutionModule(ResolutionModule(context))
                .domainModule(DomainModule(context))
                .build()
        val featuresComponent = DaggerFeaturesComponent.builder()
                .domainComponent(domainComponent)
                .databaseModule(databaseModule)
                .build()
        DaggerSyncComponent.builder()
                .featuresComponent(featuresComponent)
                .databaseModule(databaseModule)
                .feedbackModule(FeedbackModule(context))
                .syncModule(SyncModule())
                .build().inject(this)
    }

    @Inject
    lateinit var presenter: SyncPresenter

    override fun onPerformSync(account: Account?, extras: Bundle?, authority: String?,
                               provider: ContentProviderClient?, syncResult: SyncResult?) {
        extras?.let {
            with(BundleOptions){
                Log.d("debug", "Sync Type: ${extras.action}")
                presenter.sync(SyncType.valueOf(extras.action))
            }
        }
    }

}