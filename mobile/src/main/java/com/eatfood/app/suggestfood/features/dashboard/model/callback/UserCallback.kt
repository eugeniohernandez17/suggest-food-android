package com.eatfood.app.suggestfood.features.dashboard.model.callback

import com.eatfood.app.suggestfood.domain.event.MessageEvent
import com.eatfood.app.suggestfood.domain.model.callback.GeneralCallback
import com.eatfood.app.suggestfood.entity.User
import com.eatfood.app.suggestfood.features.dashboard.DashboardEventType
import com.eatfood.app.suggestfood.lib.eventbus.EventBus
import com.eatfood.app.suggestfood.service.external.user.UserReturn
import com.google.gson.Gson

import okhttp3.Headers
import okhttp3.ResponseBody

/**
 * Callback for the different request events in the user information management process.
 *
 * @param eventBus   the event bus pattern
 * @param gson       the gson object
 * @constructor Instantiates a new User callback.
 * @see GeneralCallback
 * @see User
 * @see UserReturn.Failure
 * @see EventBus
 * @see Gson
 *
 * Created by Usuario on 24/09/2017.
 */
class UserCallback(eventBus: EventBus, gson: Gson)
    : GeneralCallback<User, UserReturn.Failure>(eventBus, gson, UserReturn.Failure::class.java) {

    override fun buildOnFailureMessage(): MessageEvent<*>
            = MessageEvent(DashboardEventType.ON_GET_USER_INFO_ERROR)

    /*INTERFACES*/
    //1. GeneralCallback

    override fun onSuccessful(entity: User, headers: Headers)
        = eventBus.post(MessageEvent(DashboardEventType.ON_GET_USER_INFO_SUCCESS, entity))

    override fun onFailure(entity: UserReturn.Failure, httpCode: Int, responseBody: ResponseBody) {
        val event = buildOnFailureMessage()
        when (httpCode) {
            404 -> {
                onFailure(event, entity, httpCode, responseBody)
                eventBus.post(event)
            }
            else -> super.onFailure(entity, httpCode, responseBody)
        }
    }

    override fun onError(entity: UserReturn.Failure, throwable: Throwable)
        = eventBus.post(MessageEvent(DashboardEventType.ON_GET_USER_INFO_ERROR, throwable))

}
