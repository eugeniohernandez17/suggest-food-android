package com.eatfood.app.suggestfood.features.accounts.model

import android.accounts.Account
import com.eatfood.app.suggestfood.entity.AuthorizedUser

/**
 * Basic interface for the interactor in the Clean pattern.
 *
 * Created by Usuario on 04/04/2018.
 */
interface AccountsInteractor {

    /**
     * Method of getting the account associated with the [email].
     *
     * @param email the user's email
     * @return the account associated - if exists
     * @see Account
     */
    fun find(email: String): Account?

    /**
     * Method to create the account of the [authorizedUser].
     *
     * @param authorizedUser the user information
     * @return the account associated - if the account could be created
     * @see Account
     */
    fun create(authorizedUser: AuthorizedUser?): Account?

}