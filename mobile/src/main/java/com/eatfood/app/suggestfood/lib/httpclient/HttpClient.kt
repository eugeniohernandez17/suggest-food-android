package com.eatfood.app.suggestfood.lib.httpclient

import okhttp3.MediaType
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor

/**
 * Created by Usuario on 31/08/2017.
 */
object HttpClient {

    private const val TYPE = "application/json"

    /**
     * The value of Content-Type in the headers
     */
    val mediaType = MediaType.parse(TYPE)

    /**
     * The http client construction method.
     *
     * @param authorization the authorization method for the requests
     * @return the http client
     * @see Authorization
     */
    fun build(authorization: Authorization<*>?): OkHttpClient {
        val logging = HttpLoggingInterceptor()
        logging.level = HttpLoggingInterceptor.Level.BODY
        return OkHttpClient.Builder()
                .addInterceptor { chain ->
                    val ongoing = chain.request().newBuilder()
                    ongoing.addHeader("Content-Type", TYPE)

                    authorization?.setAuthorizationHeader(ongoing)

                    chain.proceed(ongoing.build())
                }
                .addInterceptor(logging).build()
    }
}
