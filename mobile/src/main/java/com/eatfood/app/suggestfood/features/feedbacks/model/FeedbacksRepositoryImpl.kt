package com.eatfood.app.suggestfood.features.feedbacks.model

import com.eatfood.app.suggestfood.entity.Feedback
import com.eatfood.app.suggestfood.entity.User
import com.eatfood.app.suggestfood.features.feedbacks.model.callback.FeedbacksCallback
import com.eatfood.app.suggestfood.service.external.feedback.FeedbacksReturn
import com.eatfood.app.suggestfood.service.external.feedback.FeedbacksService

import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

/**
 * Basic implementation of the [FeedbacksRepository] for the repository in the Clean pattern.
 *
 * @property feedbacksService  the feedbacks service
 * @property feedbacksCallback the feedbacks callback
 * @constructor Instantiates a new Feedbacks repository.
 * @see FeedbacksRepository
 * @see FeedbacksService
 * @see FeedbacksCallback
 *
 * Created by Usuario on 10/10/2017.
 */
class FeedbacksRepositoryImpl(
        private val feedbacksService: FeedbacksService,
        private val feedbacksCallback: FeedbacksCallback
) : FeedbacksRepository {

    override fun loadFeedbacks(feedbackType: Feedback.FeedbackType, page: Int, user: User) {
        val feedbacksReturn = FeedbacksReturn(page, feedbackType)
        feedbacksService.getFeedbacks(user.id, feedbackType, page).enqueue(object : Callback<FeedbacksReturn> {
            override fun onResponse(call: Call<FeedbacksReturn>,
                                    response: Response<FeedbacksReturn>) =
                    if (response.isSuccessful) {
                        feedbacksCallback.onSuccessful(response.body()!!, response.headers())
                    } else {
                        feedbacksCallback.onFailure(feedbacksReturn,
                                response.code(), response.errorBody()!!)
                    }

            override fun onFailure(call: Call<FeedbacksReturn>, throwable: Throwable) {
                feedbacksCallback.onError(feedbacksReturn, throwable)
            }
        })
    }

}
