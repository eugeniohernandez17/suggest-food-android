package com.eatfood.app.suggestfood.domain.resolution.networkconnectivity

import android.content.Context
import android.content.DialogInterface

import com.eatfood.app.suggestfood.R
import com.eatfood.app.suggestfood.domain.presenter.GeneralPresenter
import com.eatfood.app.suggestfood.domain.resolution.GeneralResolution
import com.eatfood.app.suggestfood.domain.view.helper.IconHelper

/**
 * Basic implementation of the [NetworkConnectivityResolution] interface.
 * It presents an [DialogInterface] with the actions that the user can perform.
 *
 * @param context the context
 * @property iconHelper the icon helper
 * @constructor Instantiates a new Network connectivity resolution.
 * @see NetworkConnectivityResolution
 * @see GeneralResolution
 * @see DialogInterface
 * @see Context
 * @see IconHelper
 *
 * Created by Usuario on 23/01/2017.
 */
class NetworkConnectivityResolutionImpl(context: Context)
    : GeneralResolution(context), NetworkConnectivityResolution {

    private var iconHelper: IconHelper = IconHelper(context)

    override fun onConnectivityUnavailable(requestCode: Int, presenter: GeneralPresenter<*>) {
        builder.setIcon(iconHelper.getDrawable(R.drawable.ic_signal_wifi_off_black_24dp))
        builder.setTitle(context.getString(R.string.network_error_no_connectivity))
        builder.setPositiveButton(R.string.retry, onRetry(requestCode, presenter))
        builder.setNegativeButton(R.string.cancel, null)
        builder.show()
    }

    /*Own Methods*/

    private fun onRetry(requestCode: Int, presenter: GeneralPresenter<*>)
            : DialogInterface.OnClickListener = DialogInterface.OnClickListener {
        _, _ -> presenter.onResolution(requestCode)
    }

}
