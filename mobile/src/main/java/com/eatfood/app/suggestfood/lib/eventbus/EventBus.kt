package com.eatfood.app.suggestfood.lib.eventbus

/**
 * Basic interface for the implementation of the EventBus pattern.
 *
 * Created by Usuario on 11/06/2016.
 */
interface EventBus {

    /**
     * Method for registering an observer or event listener object.
     *
     * @param subscriber the observer or listener
     */
    fun register(subscriber: Any)

    /**
     * Method for unregisterring an observer or event listener object.
     *
     * @param subscriber the observer or listener
     */
    fun unregister(subscriber: Any)

    /**
     * Method to publish an event.
     *
     * @param event the event
     */
    fun post(event: Any)

}
