package com.eatfood.app.suggestfood.lib.retrofit

/**
 * Basic interface for the creation of external services.
 *
 * Created by Usuario on 13/01/2017.
 */
interface Retrofit {

    /**
     * Method to create the services that communicate with the api.
     *
     * @param T            the service type
     * @param classService the class of the service type
     * @return the service object
     * @see Class
     */
    fun <T> createService(classService: Class<T>): T

}
