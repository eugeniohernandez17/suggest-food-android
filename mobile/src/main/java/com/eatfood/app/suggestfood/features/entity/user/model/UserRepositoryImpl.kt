package com.eatfood.app.suggestfood.features.entity.user.model

import android.graphics.Bitmap
import android.net.Uri
import com.eatfood.app.suggestfood.domain.model.callback.GeneralCallback
import com.eatfood.app.suggestfood.entity.User
import com.eatfood.app.suggestfood.service.external.user.UserReturn
import com.eatfood.app.suggestfood.service.external.user.UserService
import com.google.firebase.storage.FirebaseStorage
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.io.Serializable

/**
 * Basic implementation of the [UserRepository] for the repository in the Clean pattern.
 *
 * @property firebaseStorage the firebase storage instance
 * @property userService     the user service
 * @property userCallback    the user callback
 * @constructor Instantiates a new User repository.
 * @see UserRepository
 * @see FirebaseStorage
 * @see UserService
 * @see GeneralCallback
 *
 * Created by Usuario on 24/09/2017.
 */
class UserRepositoryImpl(
        private val firebaseStorage: FirebaseStorage,
        private val userService: UserService,
        private val userCallback: GeneralCallback<Serializable, UserReturn.Failure>
) : UserRepository {

    override fun getUser(user: User) {
        userService.getUser(user.id).enqueue(object : Callback<User> {
            override fun onResponse(call: Call<User>, response: Response<User>) =
                    if (response.isSuccessful) {
                        userCallback.onSuccessful(response.body()!!, response.headers())
                    } else {
                        userCallback.onFailure(UserReturn.Failure(user),
                                response.code(), response.errorBody()!!)
                    }

            override fun onFailure(call: Call<User>, throwable: Throwable) {
                userCallback.onError(UserReturn.Failure(user), throwable)
            }
        })
    }

    override fun updateUser(user: User) {
        userService.updateUser(user.id, user).enqueue(object : Callback<UserReturn.Successful> {
            override fun onResponse(call: Call<UserReturn.Successful>,
                                    response: Response<UserReturn.Successful>) =
                    if (response.isSuccessful) {
                        userCallback.onSuccessful(response.body()!!, response.headers())
                    } else {
                        userCallback.onFailure(UserReturn.Failure(user),
                                response.code(), response.errorBody()!!)
                    }

            override fun onFailure(call: Call<UserReturn.Successful>, throwable: Throwable) {
                userCallback.onError(UserReturn.Failure(user), throwable)
            }
        })
    }

    override fun updateUser(user: User, imageUri: Uri, imageBitMap: Bitmap?) {
        val storageRef = firebaseStorage.reference
        val imageProfileRef = storageRef.child("users/${user.id}/profile/picture.jpg")
        val uploadTask = imageProfileRef.putFile(imageUri)
        uploadTask.addOnSuccessListener {
            user.image = it.downloadUrl.toString()
            updateUser(user)
        }
        uploadTask.addOnFailureListener {
            userCallback.onError(UserReturn.Failure(user), it)
        }
    }

}
