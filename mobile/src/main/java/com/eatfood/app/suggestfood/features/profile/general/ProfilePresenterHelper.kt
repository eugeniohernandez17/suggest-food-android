package com.eatfood.app.suggestfood.features.profile.general

import com.eatfood.app.suggestfood.entity.User

/**
 * Class of help for the presenter in the profile feature.
 *
 * @property view the view
 * @constructor Instantiates a new Profile presenter helper.
 * @see ProfileView
 *
 * Created by Usuario on 18/10/2017.
 */
class ProfilePresenterHelper(private val view: ProfileView?) {

    /**
     * Method for getting the user information.
     *
     * @return the user information
     */
    val userParam: User?
        get() = view?.userParam

    /**
     * Method for loading the user information.
     *
     * @see [loadProfile]
     */
    fun loadProfile() = userParam?.let { loadProfile(it) }

    /**
     * Method for loading the user information.
     *
     * @param user the user information
     * @see ProfileView.loadProfile
     */
    fun loadProfile(user: User) = view?.loadProfile(user)

}
