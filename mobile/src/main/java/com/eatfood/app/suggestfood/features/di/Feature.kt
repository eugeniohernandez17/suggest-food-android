package com.eatfood.app.suggestfood.features.di

import javax.inject.Scope

/**
 * Identifies a type that the injector is an app's feature and
 * only need instantiate while the screen exists.
 *
 * @see Scope
 *
 * Created by Usuario on 21/04/2018.
 */
@Scope
@kotlin.annotation.Retention
annotation class Feature
