package com.eatfood.app.suggestfood.features.feedbacks.di

import com.eatfood.app.suggestfood.features.di.Feature
import com.eatfood.app.suggestfood.features.di.FeaturesComponent
import com.eatfood.app.suggestfood.features.feedbacks.view.FeedbacksActivity
import dagger.Component

/**
 * Injection component of the feedbacks feature.
 *
 * @see FeedbacksModule
 * @see FeaturesComponent
 * @see FeedbacksActivity
 *
 * Created by Usuario on 08/10/2017.
 */
@Feature
@Component(modules = [(FeedbacksModule::class)], dependencies = [(FeaturesComponent::class)])
internal interface FeedbacksComponent {

    /**
     * Method for injecting dependencies.
     *
     * @param feedbacksActivity the activity
     * @see FeedbacksActivity
     */
    fun inject(feedbacksActivity: FeedbacksActivity)

}
