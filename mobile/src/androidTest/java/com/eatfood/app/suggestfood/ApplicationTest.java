package com.eatfood.app.suggestfood;

import static android.support.test.espresso.Espresso.onView;
import static android.support.test.espresso.action.ViewActions.click;
import static android.support.test.espresso.action.ViewActions.closeSoftKeyboard;
import static android.support.test.espresso.action.ViewActions.replaceText;
import static android.support.test.espresso.assertion.ViewAssertions.matches;
import static android.support.test.espresso.matcher.ViewMatchers.isDisplayed;
import static android.support.test.espresso.matcher.ViewMatchers.withId;
import static android.support.test.espresso.matcher.ViewMatchers.withText;

/**
 * <a href="http://d.android.com/tools/testing/testing_android.html">Testing Fundamentals</a>
 */
public abstract class ApplicationTest {

    /**GENERAL FUNCTIONS*/

    protected void replaceTextOnView(int id, String text){
        onView(withId(id)).perform(click(), replaceText(text), closeSoftKeyboard());
    }

    protected void clickOnButton(int id){
        onView(withId(id)).perform(click());
    }

    protected void checkTextOnView(String error){
        onView(withText(error)).check(matches(isDisplayed()));
    }
}