package com.eatfood.app.suggestfood.features.login;

import android.support.test.filters.LargeTest;
import android.support.test.rule.ActivityTestRule;
import android.support.test.runner.AndroidJUnit4;

import com.eatfood.app.suggestfood.R;
import com.eatfood.app.suggestfood.features.FeaturesTest;
import com.eatfood.app.suggestfood.features.login.view.LoginActivity;

import org.junit.Test;
import org.junit.runner.RunWith;

/**
 * Created by Usuario on 29/04/2017.
 */

@LargeTest
@RunWith(AndroidJUnit4.class)
public class LoginTest extends FeaturesTest<LoginActivity> {

    protected ActivityTestRule<LoginActivity> createRule(){
        return new ActivityTestRule<>(LoginActivity.class,
                false /* Initial touch mode */, true /*  launch activity */);
    }

    @Test
    public void verifyEmptyEmail(){
        replaceTextOnView(R.id.txtEmail, "");

        clickOnButton(R.id.btnSignIn);

        String error = activity.getString(R.string.general_error_field_required);

        checkTextOnView(error);
    }

    @Test
    public void verifyEmptyPassword(){
        replaceTextOnView(R.id.txtPassword, "");

        clickOnButton(R.id.btnSignIn);

        String error = activity.getString(R.string.general_error_field_required);

        checkTextOnView(error);
    }
}
