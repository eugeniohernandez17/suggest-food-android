package com.eatfood.app.suggestfood.features;

import android.app.Activity;
import android.content.Intent;
import android.support.test.rule.ActivityTestRule;

import com.eatfood.app.suggestfood.ApplicationTest;

import org.junit.Before;
import org.junit.Rule;

/**
 * Created by Usuario on 07/09/2017.
 */

public abstract class FeaturesTest<A extends Activity> extends ApplicationTest {

    protected A activity;

    @Rule
    public final ActivityTestRule<A> rule = createRule();

    @Before
    public void setUp() throws Exception {
        activity = rule.getActivity();
        rule.launchActivity(new Intent());
    }

    /*ABSTRACT METHODS*/

    protected abstract ActivityTestRule<A> createRule();

}
