package com.eatfood.app.suggestfood.features.signup;

import android.support.test.filters.LargeTest;
import android.support.test.rule.ActivityTestRule;
import android.support.test.runner.AndroidJUnit4;

import com.eatfood.app.suggestfood.R;
import com.eatfood.app.suggestfood.features.FeaturesTest;
import com.eatfood.app.suggestfood.features.signup.view.SignupActivity;

import org.junit.Test;
import org.junit.runner.RunWith;

/**
 * Created by Usuario on 07/09/2017.
 */

@LargeTest
@RunWith(AndroidJUnit4.class)
public class Signup extends FeaturesTest<SignupActivity> {

    protected ActivityTestRule<SignupActivity> createRule(){
        return new ActivityTestRule<>(SignupActivity.class,
                false /* Initial touch mode */, true /*  launch activity */);
    }

    @Test
    public void verifyNotMatchPasswords(){
        replaceTextOnView(R.id.txtEmail, "foo@example.com");
        replaceTextOnView(R.id.txtPassword, "123456789");
        replaceTextOnView(R.id.txtPasswordConfirmation, "123");

        clickOnButton(R.id.btnSignUp);

        String error = activity.getString(R.string.general_error_not_match_password);
        checkTextOnView(error);
    }
}
