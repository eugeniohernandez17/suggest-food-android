package com.eatfood.app.suggestfood.features.dashboard;

import com.eatfood.app.suggestfood.features.FeaturesTest;
import com.eatfood.app.suggestfood.features.accounts.model.AccountsInteractor;
import com.eatfood.app.suggestfood.features.dashboard.presenter.DashboardPresenter;
import com.eatfood.app.suggestfood.features.dashboard.presenter.DashboardPresenterImpl;
import com.eatfood.app.suggestfood.features.dashboard.view.DashboardView;
import com.eatfood.app.suggestfood.features.entity.user.model.UserInteractor;
import com.eatfood.app.suggestfood.features.session.model.SessionInteractor;
import com.eatfood.app.suggestfood.features.settings.model.SettingsInteractor;

import org.junit.Before;
import org.mockito.Mock;

/**
 * Created by Usuario on 04/09/2017.
 */

abstract class DashboardTest extends FeaturesTest {

    @Mock
    DashboardView dashboardView;

    @Mock
    SessionInteractor sessionInteractor;

    @Mock
    SettingsInteractor settingsInteractor;

    @Mock
    AccountsInteractor accountsInteractor;

    @Mock
    UserInteractor userInteractor;

    DashboardPresenter dashboardPresenter;

    @Override
    @Before
    public void setUp() throws Exception {
        super.setUp();

        dashboardPresenter = new DashboardPresenterImpl(dashboardView, eventBus,
                sessionInteractor, userInteractor, settingsInteractor, accountsInteractor);
    }

}
