package com.eatfood.app.suggestfood.features.signup;

import com.eatfood.app.suggestfood.domain.event.MessageEvent;
import com.eatfood.app.suggestfood.entity.User;
import com.eatfood.app.suggestfood.features.signup.presenter.SignupPresenter;

import org.junit.Test;

import static org.mockito.Mockito.verify;

/**
 * Created by Usuario on 06/09/2017.
 */

@SuppressWarnings("ConstantConditions")
public class EventsValidation extends SignupTest {

    @Test
    public void signupFailed(){
        MessageEvent<SignupEventType> signupEvent
                = new MessageEvent<>(SignupEventType.ON_SIGN_UP_ERROR, "General Error");

        onMessageEventFailed(signupEvent);

        verify(signupView).displayError(signupEvent.getMessage());
    }

    @Test
    public void signupFailedWithException(){
        MessageEvent<SignupEventType> signupEvent
                = new MessageEvent<>(SignupEventType.ON_SIGN_UP_ERROR, new Throwable());

        onMessageEventFailed(signupEvent);

        verify(signupView).displayError(signupEvent.getError(), SignupPresenter.SIGNUP_REQUEST_RESOLVED);
    }

    @Test
    public void validSignup(){
        User user = new User(0, "foo@example.com", "123456789");
        MessageEvent<SignupEventType> signupEvent
                = new MessageEvent<>(SignupEventType.ON_SIGN_UP_SUCCESS, user);

        signupPresenter.onMessageEvent(signupEvent);

        verify(signupView).hideProgress();
        verify(signupView).navigateToLoginScreen(user);
    }

    private void onMessageEventFailed(MessageEvent<SignupEventType> signupEvent){
        signupPresenter.onMessageEvent(signupEvent);

        verify(signupView).enableInputs();
        verify(signupView).hideProgress();
        verify(signupView).setPassword(null);
        verify(signupView).setConfirmationPassword(null);
    }
}
