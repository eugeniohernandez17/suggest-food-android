package com.eatfood.app.suggestfood.features.signup;

import com.eatfood.app.suggestfood.features.FieldsViewTest;
import com.eatfood.app.suggestfood.features.signup.model.SignupInteractor;
import com.eatfood.app.suggestfood.features.signup.presenter.SignupPresenter;
import com.eatfood.app.suggestfood.features.signup.presenter.SignupPresenterImpl;
import com.eatfood.app.suggestfood.features.signup.view.SignupView;

import org.junit.Before;
import org.mockito.Mock;

/**
 * Created by Usuario on 05/09/2017.
 */

abstract class SignupTest extends FieldsViewTest {

    @Mock
    SignupView signupView;

    @Mock
    SignupInteractor signupInteractor;

    SignupPresenter signupPresenter;

    @Override
    @Before
    public void setUp() throws Exception {
        super.setUp();
        signupPresenter = new SignupPresenterImpl(signupView, eventBus,
                emailEvaluator, passwordEvaluator, passwordsMatchEvaluator, signupInteractor);
    }
}
