package com.eatfood.app.suggestfood.features.login;

import com.eatfood.app.suggestfood.entity.User;

import org.junit.Test;

import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

/**
 * Created by Usuario on 18/09/2017.
 */

public class AutoLogin extends LoginTest {

    @Test
    public void loginBeforeSignup(){
        User user = new User(0,"user@example.com", "123456789");
        when(loginView.isAutoLogin()).thenReturn(true);
        when(loginView.getUserParam()).thenReturn(user);
        when(loginView.isNetworkConnectivityAvailable()).thenReturn(true);

        loginPresenter.onCreate();

        verify(loginInteractor).doSignIn(user.getEmail(), user.getPassword());
    }
}
