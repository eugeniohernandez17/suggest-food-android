package com.eatfood.app.suggestfood.features.settings;

import android.accounts.Account;

import com.eatfood.app.suggestfood.entity.AuthorizedUser;
import com.eatfood.app.suggestfood.entity.User;
import com.eatfood.app.suggestfood.features.settings.view.fragments.PreferenceView;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;

import io.objectbox.relation.ToOne;

import static org.mockito.Mockito.never;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

/**
 * Created by Usuario on 17/04/2018.
 */

public class DataSyncPreferenceTest extends SettingsTest {

    private final AuthorizedUser authorizedUser = new AuthorizedUser();
    private final User user = new User(0, "foo@example.com");

    @Mock
    private ToOne<User> toOneUser;

    @Mock
    private Account account;

    @Override
    @Before
    public void setUp() throws Exception {
        super.setUp();
        authorizedUser.setUser(toOneUser);
        when(toOneUser.getTarget()).thenReturn(user);
        when(sessionInteractor.getSession()).thenReturn(authorizedUser);
        when(accountsInteractor.find(authorizedUser.getEmail())).thenReturn(account);
    }

    @Test
    public void ifTheSyncPeriodNotChangeThenTheSynchronizationProcessShouldNotBeReConfigured(){
        when(settingsInteractor.getSyncFrequency()).thenReturn(180L);

        preferencePresenter.onPreferenceChange(PreferenceView.SYNC_FREQUENCY, "180");

        verify(preferenceView, never()).addPeriodicSync(account, 180);
    }

    @Test
    public void ifTheSyncPeriodChangeThenTheSynchronizationProcessShouldBeReConfigured(){
        preferencePresenter.onPreferenceChange(PreferenceView.SYNC_FREQUENCY, "4");

        verify(preferenceView).addPeriodicSync(account, 4);
    }

    @Test
    public void ifTheSyncPeriodChangeToNeverThenTheSynchronizationProcessShouldBeRemoved(){
        preferencePresenter.onPreferenceChange(PreferenceView.SYNC_FREQUENCY, "-1");

        verify(preferenceView).removePeriodicSync(account);
    }

}
