package com.eatfood.app.suggestfood.features.login;

import com.eatfood.app.suggestfood.domain.error.HttpException;
import com.eatfood.app.suggestfood.domain.event.MessageEvent;
import com.eatfood.app.suggestfood.entity.AuthorizedUser;
import com.eatfood.app.suggestfood.entity.User;
import com.eatfood.app.suggestfood.features.FeaturesTest;
import com.eatfood.app.suggestfood.features.login.model.callback.LoginCallback;
import com.eatfood.app.suggestfood.lib.gson.GsonBuilder;
import com.eatfood.app.suggestfood.lib.httpclient.Authorization;
import com.eatfood.app.suggestfood.lib.httpclient.HttpClient;
import com.eatfood.app.suggestfood.service.external.user.SignInReturn;

import org.junit.Before;
import org.junit.Test;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.Mock;

import kotlin.Unit;
import kotlin.jvm.functions.Function1;
import okhttp3.Headers;
import okhttp3.ResponseBody;
import retrofit2.Response;

import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.verify;

/**
 * Created by Usuario on 31/08/2017.
 */

@SuppressWarnings("ConstantConditions")
public class ServiceValidation extends FeaturesTest {

    private final User invalidUser = new User(0,"user@email.com", "123456789");

    @Mock
    private Authorization<AuthorizedUser> authorization;

    @Captor
    private ArgumentCaptor<Function1<AuthorizedUser, Unit>> callbackCaptor;

    private LoginCallback loginCallback;

    @Override
    @Before
    public void setUp() throws Exception {
        super.setUp();

        loginCallback = new LoginCallback(eventBus, GsonBuilder.INSTANCE.build(), authorization);
    }

    @Test
    public void serviceUnavailable(){
        Response<User> response = Response.error(503,
                ResponseBody.create(HttpClient.INSTANCE.getMediaType(), ""));

        loginCallback.onFailure(new SignInReturn.Failure(invalidUser), response.code(), response.errorBody());

        HttpException httpException = new HttpException(response.code());

        verify(eventBus).post(new MessageEvent<>(LoginEventType.ON_SIGN_IN_ERROR, httpException));
    }

    @Test
    public void incorrectCredentials() {
        String message = "Invalid login credentials. Please try again.";

        Response<User> response = Response.error(401,
                ResponseBody.create(HttpClient.INSTANCE.getMediaType(), "{\"errors\":[\""+message+"\"]}"));

        loginCallback.onFailure(new SignInReturn.Failure(invalidUser), response.code(),
                response.errorBody());

        verify(eventBus).post(new MessageEvent<>(LoginEventType.ON_SIGN_IN_ERROR, message));
    }

    @Test
    public void faliureService(){
        User user = new User();
        NullPointerException exception = new NullPointerException();

        loginCallback.onError(new SignInReturn.Failure(user), exception);

        verify(eventBus).post(new MessageEvent<>(LoginEventType.ON_SIGN_IN_ERROR, exception));
    }

    @Test
    public void validSignIn(){
        User user = new User(0, "foo@example.com", "123456789");
        Headers headers = Headers.of();

        loginCallback.onSuccessful(new SignInReturn.Successful(user), headers);

        verify(authorization).getAuthorizationHeader(eq(headers), callbackCaptor.capture());
        verify(eventBus).post(new MessageEvent<>(LoginEventType.ON_SIGN_IN_SUCCESS));
    }
}
