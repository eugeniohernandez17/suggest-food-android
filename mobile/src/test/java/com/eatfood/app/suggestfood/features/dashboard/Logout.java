package com.eatfood.app.suggestfood.features.dashboard;

import org.junit.Test;

import static org.mockito.Mockito.verify;

/**
 * Created by Usuario on 04/09/2017.
 */

public class Logout extends DashboardTest {

    @Test
    public void theLogoutActionShouldRedirectToLoginScreenAndClearingSettings(){
        dashboardPresenter.logout();

        verify(sessionInteractor).invalidateSession();
        verify(settingsInteractor).clearSettings();
        verify(dashboardView).navigateToLoginScreen();
    }
}
