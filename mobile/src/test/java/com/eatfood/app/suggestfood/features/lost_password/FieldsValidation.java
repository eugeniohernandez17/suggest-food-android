package com.eatfood.app.suggestfood.features.lost_password;

import com.eatfood.app.suggestfood.domain.Callback;

import org.junit.Test;

import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

/**
 * Created by Usuario on 14/09/2017.
 */

public class FieldsValidation extends LostPasswordTest {

    @Test
    public void invalidEmail(){
        lostPassword("foo@.com");

        verify(lostPasswordView).showErrorInEmail(emailEvaluator.getError());
    }

    @Test
    public void validLostPassword(){
        final String email = "foo@example.com";

        lostPassword(email);

        evaluateActionWithValidatedFieldsCorrectly(lostPasswordView, new Callback() {
            @Override
            public void execute() {
                verify(lostPasswordView).showErrorInEmail(null);
                verify(lostPasswordInteractor).doRecoverPassword(email);
            }
        });
    }

    private void lostPassword(String email){
        when(lostPasswordView.getEmail()).thenReturn(email);

        evaluateFields(lostPasswordView, new Callback() {
            @Override
            public void execute() {
                lostPasswordPresenter.validLostPassword();
            }
        });
    }

}
