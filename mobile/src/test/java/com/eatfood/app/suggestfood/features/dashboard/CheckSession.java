package com.eatfood.app.suggestfood.features.dashboard;

import com.eatfood.app.suggestfood.entity.AuthorizedUser;

import org.junit.Test;

import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

/**
 * Created by Usuario on 04/09/2017.
 */

public class CheckSession extends DashboardTest {

    @Test
    public void invalidSessionShouldBeRedirectedToLogin(){
        when(sessionInteractor.isValidSession()).thenReturn(false);

        dashboardPresenter.checkCurrentSession();

        verify(dashboardView).navigateToLoginScreen();
    }

    @Test
    public void validSessionShouldLoadTheCurrentUserInfo(){
        AuthorizedUser user = new AuthorizedUser();
        when(sessionInteractor.isValidSession()).thenReturn(true);
        when(sessionInteractor.getSession()).thenReturn(user);
        when(dashboardView.isNetworkConnectivityAvailable()).thenReturn(true);

        dashboardPresenter.checkCurrentSession();

        verify(userInteractor).getUser(user);
    }

    @Test
    public void validSessionShouldLoadTheCurrentUserInfoUsingTheServices(){
        AuthorizedUser authorizedUser = new AuthorizedUser();
        when(sessionInteractor.isValidSession()).thenReturn(true);
        when(dashboardView.isNetworkConnectivityAvailable()).thenReturn(true);
        when(sessionInteractor.getSession()).thenReturn(authorizedUser);

        dashboardPresenter.checkCurrentSession();

        verify(userInteractor).getUser(authorizedUser);
    }
}
