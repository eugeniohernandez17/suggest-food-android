package com.eatfood.app.suggestfood.features.dashboard;

import com.eatfood.app.suggestfood.entity.AuthorizedUser;
import com.eatfood.app.suggestfood.entity.User;

import org.junit.Test;

import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

/**
 * Created by Usuario on 22/09/2017.
 */

public class UserInformation extends DashboardTest {

    @Test
    public void userInfoShouldBeConsultedWhenLoadingTheView(){
        final AuthorizedUser authorizedUser = new AuthorizedUser();

        when(sessionInteractor.isValidSession()).thenReturn(true);
        when(dashboardView.isNetworkConnectivityAvailable()).thenReturn(true);
        when(sessionInteractor.getSession()).thenReturn(authorizedUser);

        dashboardPresenter.onCreate();

        verify(userInteractor).getUser(authorizedUser);
    }

    @Test
    public void profileShouldBeUploadedAfterBeingEdited(){
        User user = new User(0, "user@example.com");
        user.setName("User");
        user.setPhone("+12025550186");

        dashboardPresenter.onProfileShowingFinish(user);

        verify(dashboardView).loadUser(user);
    }
}
