package com.eatfood.app.suggestfood.features.accounts;

import android.accounts.Account;
import android.os.Bundle;

import com.eatfood.app.suggestfood.entity.AuthorizedUser;
import com.eatfood.app.suggestfood.entity.User;

import org.concordion.integration.junit4.ConcordionRunner;
import org.jetbrains.annotations.Nullable;
import org.junit.runner.RunWith;
import org.mockito.Mock;

import java.lang.reflect.Field;

import io.objectbox.relation.ToOne;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.ArgumentMatchers.nullable;
import static org.mockito.Mockito.when;

/**
 * Created by Usuario on 03/04/2018.
 */

@RunWith(ConcordionRunner.class)
public class ManageAccount extends AccountsTest {

    private final AuthorizedUser authorizedUser = new AuthorizedUser();

    @Mock
    private ToOne<User> toOneUser;

    public boolean createValidAccount(){
        User user = new User(0, "user@gmail.com", "123456789");
        user.setName("user");
        authorizedUser.setUser(toOneUser);
        when(toOneUser.getTarget()).thenReturn(user);
        return createAccount(authorizedUser, true);
    }

    public boolean createInvalidAccount(){
        User user = new User(0, "", "");
        authorizedUser.setUser(toOneUser);
        when(toOneUser.getTarget()).thenReturn(user);
        return createAccount(authorizedUser, false);
    }

    public boolean createInvalidAccountWithNullParameter(){
        return createAccount(null, false);
    }

    private boolean createAccount(@Nullable AuthorizedUser authorizedUser, boolean isValid){
        when(accountManager.addAccountExplicitly(any(Account.class), anyString(), nullable(Bundle.class))).thenReturn(isValid);
        Account account = accountsRepository.create(authorizedUser);
        return account != null;
    }

    public boolean findAccount(String email){
        Account[] accounts = validAccounts();
        when(accountManager.getAccounts()).thenReturn(accounts);
        Account account = accountsRepository.find(email);
        return account != null;
    }

    private Account[] validAccounts(){
        return new Account[]{account("test@test.com"), account("user@gmail.com")};
    }

    private Account account(String email){
        Account account = new Account(email, "");
        try {
            Field nameField = account.getClass().getDeclaredField("name");
            nameField.setAccessible(true);
            nameField.set(account, email);
        } catch (NoSuchFieldException | IllegalAccessException e){
            e.printStackTrace();
        }
        return account;
    }
}
