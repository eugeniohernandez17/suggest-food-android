package com.eatfood.app.suggestfood.features.profile.edit;

import com.eatfood.app.suggestfood.domain.error.HttpException;
import com.eatfood.app.suggestfood.domain.event.MessageEvent;
import com.eatfood.app.suggestfood.entity.User;
import com.eatfood.app.suggestfood.features.FeaturesTest;
import com.eatfood.app.suggestfood.features.profile.edit.model.callback.UserCallback;
import com.eatfood.app.suggestfood.lib.gson.GsonBuilder;
import com.eatfood.app.suggestfood.lib.httpclient.HttpClient;
import com.eatfood.app.suggestfood.service.external.user.UserReturn;

import org.junit.Before;
import org.junit.Test;

import java.net.SocketTimeoutException;

import okhttp3.Headers;
import okhttp3.ResponseBody;
import retrofit2.Response;

import static org.mockito.Mockito.verify;

/**
 * Created by Usuario on 28/09/2017.
 */

@SuppressWarnings("ConstantConditions")
public class ServiceValidation extends FeaturesTest {

    private final User user = new User(0,"foo@example.com");

    private UserCallback userCallback;

    @Override
    @Before
    public void setUp() throws Exception {
        super.setUp();

        userCallback = new UserCallback(eventBus, GsonBuilder.INSTANCE.build());
    }

    @Test
    @SuppressWarnings("unchecked")
    public void serviceUnavailable(){
        Response<UserReturn.Successful> response = Response.error(503,
                ResponseBody.create(HttpClient.INSTANCE.getMediaType(), ""));

        userCallback.onFailure(new UserReturn.Failure(user),
                response.code(), response.errorBody());

        HttpException httpException = new HttpException(response.code());

        verify(eventBus).post(new MessageEvent<>(ProfileEventType.ON_UPDATE_PROFILE_ERROR, httpException));
    }

    @Test
    public void faliureService(){
        SocketTimeoutException exception = new SocketTimeoutException();

        userCallback.onError(new UserReturn.Failure(user), exception);

        verify(eventBus).post(new MessageEvent<>(ProfileEventType.ON_UPDATE_PROFILE_ERROR, exception));
    }

    @Test
    @SuppressWarnings("unchecked")
    public void validUpateProfile(){
        UserReturn.Successful successful = new UserReturn.Successful(user, "Profile saved");

        userCallback.onSuccessful(successful, Headers.of());

        verify(eventBus).post(new MessageEvent<>(
                ProfileEventType.ON_UPDATE_PROFILE_SUCCESS, successful.getMessage(), successful));
    }
}
