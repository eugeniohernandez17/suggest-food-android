package com.eatfood.app.suggestfood.features.profile.edit;

import android.graphics.Bitmap;
import android.net.Uri;

import com.eatfood.app.suggestfood.domain.event.MessageEvent;
import com.eatfood.app.suggestfood.entity.AuthorizedUser;
import com.eatfood.app.suggestfood.entity.User;
import com.eatfood.app.suggestfood.features.profile.edit.presenter.ProfilePresenter;
import com.eatfood.app.suggestfood.service.external.user.UserReturn;

import org.junit.Test;
import org.mockito.Mock;

import io.objectbox.relation.ToOne;

import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

/**
 * Created by Usuario on 28/09/2017.
 */

@SuppressWarnings("ConstantConditions")
public class EventsValidation extends ProfileTest {

    private final AuthorizedUser authorizedUser = new AuthorizedUser();
    private final User user = new User();

    @Mock
    private ToOne<User> toOneUser;

    @Mock
    private Uri imageUri;

    @Mock
    private Bitmap imageBitMap;

    @Test
    public void editImagePhoto(){
        authorizedUser.setUser(toOneUser);
        when(sessionInteractor.getSession()).thenReturn(authorizedUser);
        when(toOneUser.getTarget()).thenReturn(user);

        profilePresenter.onEditProfileImage();

        verify(profileView).displayOptionsToCaptureImage();

        profilePresenter.onEditProfileImage(imageUri, imageBitMap);

        verify(profileView).showProgress();
        verify(userInteractor).updateUser(authorizedUser, imageUri, imageBitMap);
    }

    @Test
    public void updateProfileWithExeption(){
        MessageEvent<ProfileEventType> event
                = new MessageEvent<>(ProfileEventType.ON_UPDATE_PROFILE_ERROR, new Throwable());

        onMessageEvent(event);

        verify(profileView)
                .displayError(event.getError(), ProfilePresenter.UPDATE_PROFILE_REQUEST_RESOLVED);
    }

    @Test
    public void updateProfileSuccess(){
        User user = new User();
        UserReturn.Successful successful = new UserReturn.Successful(user, "Profile saved");
        MessageEvent<ProfileEventType> event
                = new MessageEvent<>(ProfileEventType.ON_UPDATE_PROFILE_SUCCESS, successful.getMessage(), successful);

        onMessageEvent(event);

        verify(profileView).setUserParam(user);
        verify(profileView).displayMessage(event.getMessage());
    }

    private void onMessageEvent(MessageEvent<ProfileEventType> event){
        profilePresenter.onMessageEvent(event);

        verify(profileView).enableInputs();
        verify(profileView).hideProgress();
    }

}
