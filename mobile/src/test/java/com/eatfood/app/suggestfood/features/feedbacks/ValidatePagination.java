package com.eatfood.app.suggestfood.features.feedbacks;

import com.eatfood.app.suggestfood.entity.AuthorizedUser;
import com.eatfood.app.suggestfood.entity.Feedback;
import com.eatfood.app.suggestfood.entity.User;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;

import io.objectbox.relation.ToOne;

import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

/**
 * Created by Usuario on 10/10/2017.
 */

public class ValidatePagination extends FeedbacksTest {

    private final AuthorizedUser authorizedUser = new AuthorizedUser();
    private final User user = new User();

    @Mock
    private ToOne<User> toOneUser;

    @Override
    @Before
    public void setUp() throws Exception {
        super.setUp();
        authorizedUser.setUser(toOneUser);
        when(sessionInteractor.getSession()).thenReturn(authorizedUser);
        when(toOneUser.getTarget()).thenReturn(user);
        when(feedbacksView.isNetworkConnectivityAvailable()).thenReturn(true);
    }

    @Test
    public void loadFirstPageOfComplaints(){
        feedbacksPresenter.onViewComplaintsScreen();
        feedbacksPresenter.onRefresh();

        verify(feedbacksInteractor).loadFeedbacks(Feedback.FeedbackType.COMPLAINT, 1, user);
    }

    @Test
    public void loadFirstPageOfSuggestionsScreen(){
        feedbacksPresenter.onViewSuggestionsScreen();
        feedbacksPresenter.onRefresh();

        verify(feedbacksInteractor).loadFeedbacks(Feedback.FeedbackType.SUGGESTION, 1, user);
    }

    @Test
    public void loadFirstPageOfFeedbacksScreen(){
        feedbacksPresenter.onViewFeedbacksScreen();
        feedbacksPresenter.onRefresh();

        verify(feedbacksInteractor).loadFeedbacks(Feedback.FeedbackType.FEEDBACK, 1, user);
    }

    @After
    public void tearDown() throws Exception {
        verify(feedbacksView).hideNoFeedbacksError();
    }
}
