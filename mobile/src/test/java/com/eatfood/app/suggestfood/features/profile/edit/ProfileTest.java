package com.eatfood.app.suggestfood.features.profile.edit;

import com.eatfood.app.suggestfood.features.FeaturesTest;
import com.eatfood.app.suggestfood.features.entity.user.model.UserInteractor;
import com.eatfood.app.suggestfood.features.profile.edit.presenter.ProfilePresenter;
import com.eatfood.app.suggestfood.features.profile.edit.presenter.ProfilePresenterImpl;
import com.eatfood.app.suggestfood.features.profile.edit.view.ProfileView;
import com.eatfood.app.suggestfood.features.profile.general.ProfilePresenterHelper;
import com.eatfood.app.suggestfood.features.session.model.SessionInteractor;

import org.junit.Before;
import org.mockito.Mock;

/**
 * Created by Usuario on 25/09/2017.
 */

abstract class ProfileTest extends FeaturesTest {

    @Mock
    ProfileView profileView;

    @Mock
    SessionInteractor sessionInteractor;

    @Mock
    UserInteractor userInteractor;

    ProfilePresenter profilePresenter;

    @Override
    @Before
    public void setUp() throws Exception {
        super.setUp();

        ProfilePresenterHelper profileHelper = new ProfilePresenterHelper(profileView);
        profilePresenter = new ProfilePresenterImpl(profileView, eventBus, profileHelper,
                textEvaluator, phoneEvaluator, sessionInteractor, userInteractor);
    }
}
