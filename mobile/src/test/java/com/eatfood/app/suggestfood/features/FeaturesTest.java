package com.eatfood.app.suggestfood.features;

import com.eatfood.app.suggestfood.UnitTest;
import com.eatfood.app.suggestfood.domain.evaluator.EmailEvaluator;
import com.eatfood.app.suggestfood.domain.evaluator.PasswordEvaluator;
import com.eatfood.app.suggestfood.domain.evaluator.PasswordsMatchEvaluator;
import com.eatfood.app.suggestfood.domain.evaluator.PhoneEvaluator;
import com.eatfood.app.suggestfood.domain.evaluator.TextEvaluator;
import com.eatfood.app.suggestfood.lib.eventbus.EventBus;

import org.junit.Before;
import org.mockito.Mock;

/**
 * Created by Usuario on 04/09/2017.
 */

public abstract class FeaturesTest extends UnitTest {

    @Mock
    protected EventBus eventBus;

    protected TextEvaluator textEvaluator;
    protected EmailEvaluator emailEvaluator;
    protected PasswordEvaluator passwordEvaluator;
    protected PasswordsMatchEvaluator passwordsMatchEvaluator;
    protected PhoneEvaluator phoneEvaluator;

    @Override
    @Before
    public void setUp() throws Exception {
        super.setUp();
        textEvaluator = new TextEvaluator(context, fieldEvaluate);
        emailEvaluator = new EmailEvaluator(context, fieldEvaluate);
        passwordEvaluator = new PasswordEvaluator(context, fieldEvaluate);
        passwordsMatchEvaluator = new PasswordsMatchEvaluator(context, fieldEvaluate);
        phoneEvaluator = new PhoneEvaluator(context, fieldEvaluate);
    }
}
