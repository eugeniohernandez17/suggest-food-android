package com.eatfood.app.suggestfood.features.accounts;

import android.accounts.AccountManager;

import com.eatfood.app.suggestfood.UnitTest;
import com.eatfood.app.suggestfood.features.accounts.model.AccountsRepository;
import com.eatfood.app.suggestfood.features.accounts.model.AccountsRepositoryImpl;

import org.junit.Before;
import org.mockito.Mock;

/**
 * Created by Usuario on 03/04/2018.
 */

abstract class AccountsTest extends UnitTest {

    @Mock
    AccountManager accountManager;

    AccountsRepository accountsRepository;

    @Override
    @Before
    public void setUp() throws Exception {
        super.setUp();

        accountsRepository = new AccountsRepositoryImpl(accountManager);
    }
}
