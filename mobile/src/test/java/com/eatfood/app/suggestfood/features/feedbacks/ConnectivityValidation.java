package com.eatfood.app.suggestfood.features.feedbacks;

import com.eatfood.app.suggestfood.entity.Feedback;
import com.eatfood.app.suggestfood.features.feedbacks.presenter.FeedbacksPresenter;

import org.junit.Before;
import org.junit.Test;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

/**
 * Created by Usuario on 10/10/2017.
 */

public class ConnectivityValidation extends FeedbacksTest {

    @Override
    @Before
    public void setUp() throws Exception {
        super.setUp();
        when(feedbacksView.isNetworkConnectivityAvailable()).thenReturn(false);
        feedbacksPresenter.onViewComplaintsScreen();
    }

    @Test
    public void networkConnectivityDisabledOnInitLoad(){
        feedbacksPresenter.initLoadFeedbacks();

        verify(feedbacksView).displayNoFeedbacksError(any(Feedback.FeedbackType.class));
        verify(feedbacksView).onNetworkConnectivityDisabled(
                FeedbacksPresenter.LOAD_FEEDBACKS_REQUEST_RESOLVED,
                feedbacksPresenter);
    }

    @Test
    public void networkConnectivityDisabledOnRefresh(){
        feedbacksPresenter.onRefresh();

        verify(feedbacksView).hideProgress();
        verify(feedbacksView).displayNoFeedbacksError(any(Feedback.FeedbackType.class));
        verify(feedbacksView).onNetworkConnectivityDisabled(
                FeedbacksPresenter.LOAD_FEEDBACKS_REQUEST_RESOLVED,
                feedbacksPresenter);
    }

    @Test
    public void resolutionOfNetworkConnectivityDisabled(){
        feedbacksPresenter.onResolution(FeedbacksPresenter.LOAD_FEEDBACKS_REQUEST_RESOLVED);

        verify(feedbacksView).initLoadFeedbacks();
    }

}
