package com.eatfood.app.suggestfood.features.dashboard;

import com.eatfood.app.suggestfood.domain.error.HttpException;
import com.eatfood.app.suggestfood.domain.event.MessageEvent;
import com.eatfood.app.suggestfood.entity.User;
import com.eatfood.app.suggestfood.features.FeaturesTest;
import com.eatfood.app.suggestfood.features.dashboard.model.callback.UserCallback;
import com.eatfood.app.suggestfood.lib.gson.GsonBuilder;
import com.eatfood.app.suggestfood.lib.httpclient.HttpClient;
import com.eatfood.app.suggestfood.service.external.user.UserReturn;

import org.junit.Before;
import org.junit.Test;

import java.net.SocketTimeoutException;

import okhttp3.Headers;
import okhttp3.ResponseBody;
import retrofit2.Response;

import static org.mockito.Mockito.verify;

/**
 * Created by Usuario on 24/09/2017.
 */

@SuppressWarnings("ConstantConditions")
public class ServiceValidation extends FeaturesTest {

    private final User invalidUser = new User(0, "not.found@example.com");

    private UserCallback userCallback;

    @Override
    @Before
    public void setUp() throws Exception {
        super.setUp();

        userCallback = new UserCallback(eventBus, GsonBuilder.INSTANCE.build());
    }

    @Test
    public void serviceUnavailable(){
        Response<User> response = Response.error(503,
                ResponseBody.create(HttpClient.INSTANCE.getMediaType(), ""));

        userCallback.onFailure(new UserReturn.Failure(invalidUser),
                response.code(), response.errorBody());

        HttpException httpException = new HttpException(response.code());

        verify(eventBus).post(new MessageEvent<>(DashboardEventType.ON_GET_USER_INFO_ERROR, httpException));
    }

    @Test
    public void userNotFound(){
        String message = "User not found";
        Response<User> response = Response.error(404, ResponseBody.create(HttpClient.INSTANCE.getMediaType(),
                "{\"errors\":[\""+message+"\"]}"));

        userCallback.onFailure(new UserReturn.Failure(invalidUser),
                response.code(), response.errorBody());

        verify(eventBus).post(new MessageEvent<>(DashboardEventType.ON_GET_USER_INFO_ERROR, message));
    }

    @Test
    public void faliureService(){
        SocketTimeoutException exception = new SocketTimeoutException();

        userCallback.onError(new UserReturn.Failure(invalidUser), exception);

        verify(eventBus).post(new MessageEvent<>(DashboardEventType.ON_GET_USER_INFO_ERROR, exception));
    }

    @Test
    @SuppressWarnings("unchecked")
    public void getUserInfoSuccess(){
        User user = new User(0, "foo@example.com");
        user.setName("User Foo");
        user.setPhone("+12025550186");

        userCallback.onSuccessful(user, Headers.of());

        MessageEvent<DashboardEventType> event
                = new MessageEvent<>(DashboardEventType.ON_GET_USER_INFO_SUCCESS, user);
        verify(eventBus).post(event);
    }
}
