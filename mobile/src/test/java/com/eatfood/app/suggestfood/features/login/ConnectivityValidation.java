package com.eatfood.app.suggestfood.features.login;

import com.eatfood.app.suggestfood.features.login.presenter.LoginPresenter;

import org.junit.Test;

import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

/**
 * Created by Usuario on 05/09/2017.
 */

public class ConnectivityValidation extends LoginTest {

    @Test
    public void networkConnectivityDisabled(){
        when(loginView.getEmail()).thenReturn("foo@example.com");
        when(loginView.getPassword()).thenReturn("123456789");
        when(loginView.isNetworkConnectivityAvailable()).thenReturn(false);

        loginPresenter.validateLogin();

        verify(loginView).onNetworkConnectivityDisabled(LoginPresenter.LOGIN_REQUEST_RESOLVED, loginPresenter);
    }
}
