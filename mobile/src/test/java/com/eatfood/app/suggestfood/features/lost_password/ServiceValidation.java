package com.eatfood.app.suggestfood.features.lost_password;

import com.eatfood.app.suggestfood.domain.error.HttpException;
import com.eatfood.app.suggestfood.domain.event.MessageEvent;
import com.eatfood.app.suggestfood.entity.User;
import com.eatfood.app.suggestfood.features.FeaturesTest;
import com.eatfood.app.suggestfood.features.lost_password.model.callback.LostPasswordCallback;
import com.eatfood.app.suggestfood.lib.gson.GsonBuilder;
import com.eatfood.app.suggestfood.lib.httpclient.HttpClient;
import com.eatfood.app.suggestfood.service.external.user.LostPasswordReturn;
import com.eatfood.app.suggestfood.service.external.user.UserReturn;

import org.junit.Before;
import org.junit.Test;

import java.net.SocketTimeoutException;

import okhttp3.Headers;
import okhttp3.ResponseBody;
import retrofit2.Response;

import static org.mockito.Mockito.verify;

/**
 * Created by Usuario on 17/09/2017.
 */

@SuppressWarnings("ConstantConditions")
public class ServiceValidation extends FeaturesTest {

    private final User invalidUser = new User(0,"not.found@example.com");

    private LostPasswordCallback lostPasswordCallback;

    @Override
    @Before
    public void setUp() throws Exception {
        super.setUp();

        lostPasswordCallback = new LostPasswordCallback(eventBus, GsonBuilder.INSTANCE.build());
    }

    @Test
    public void serviceUnavailable(){
        Response<User> response = Response.error(503,
                ResponseBody.create(HttpClient.INSTANCE.getMediaType(), ""));

        lostPasswordCallback.onFailure(new UserReturn.Failure(invalidUser),
                response.code(), response.errorBody());

        HttpException httpException = new HttpException(response.code());

        verify(eventBus).post(new MessageEvent<>(LostPasswordEventType.ON_LOST_PASSWORD_ERROR, httpException));
    }

    @Test
    public void emailNotFound(){
        String message = "Unable to find user with email '"+invalidUser.getEmail()+"'.";
        Response<User> response = Response.error(404, ResponseBody.create(HttpClient.INSTANCE.getMediaType(),
                "{\"errors\":[\""+message+"\"]}"));

        lostPasswordCallback.onFailure(new UserReturn.Failure(invalidUser),
                response.code(), response.errorBody());

        verify(eventBus).post(new MessageEvent<>(LostPasswordEventType.ON_LOST_PASSWORD_ERROR, message));
    }

    @Test
    public void faliureService(){
        SocketTimeoutException exception = new SocketTimeoutException();

        lostPasswordCallback.onError(new UserReturn.Failure(invalidUser), exception);

        verify(eventBus).post(new MessageEvent<>(LostPasswordEventType.ON_LOST_PASSWORD_ERROR, exception));
    }

    @Test
    public void validLostPassword(){
        String email = "foo@example.com";
        String message = "An email has been sent to '"+email+
                "' containing instructions for resetting your password.";
        User user = new User(0, email);

        lostPasswordCallback.onSuccessful(new LostPasswordReturn.Successful(user, message), Headers.of());

        verify(eventBus).post(new MessageEvent<>(LostPasswordEventType.ON_LOST_PASSWORD_SUCCESS, message));
    }
}
