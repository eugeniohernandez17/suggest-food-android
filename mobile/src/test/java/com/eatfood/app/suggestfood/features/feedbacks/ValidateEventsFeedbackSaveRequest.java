package com.eatfood.app.suggestfood.features.feedbacks;

import com.eatfood.app.suggestfood.domain.event.MessageEvent;
import com.eatfood.app.suggestfood.entity.Feedback;
import com.eatfood.app.suggestfood.features.feedbacks.presenter.FeedbacksPresenter;
import com.eatfood.app.suggestfood.service.external.feedback.FeedbackReturn;

import org.junit.Before;
import org.junit.Test;
import org.mockito.ArgumentMatchers;
import org.mockito.Mock;

import io.objectbox.relation.ToOne;

import static org.mockito.Mockito.never;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

/**
 * Created by Usuario on 22/03/2018.
 */

public class ValidateEventsFeedbackSaveRequest extends FeedbacksTest {

    private final Feedback feedback = new Feedback();

    private final Feedback feedbackAssociate = new Feedback();

    @Mock
    private ToOne<Feedback> toOneFeedback;

    private FeedbackReturn.Failure response;

    @Override
    @Before
    public void setUp() throws Exception {
        super.setUp();
        feedback.setType(Feedback.FeedbackType.COMPLAINT);
        feedback.setFeedbackAssociate(toOneFeedback);
        when(toOneFeedback.getTarget()).thenReturn(feedbackAssociate);
        response = new FeedbackReturn.Failure(feedback, false);
        feedbacksPresenter.onCreate();
    }

    @Test
    public void saveFeedbackFailedWithExepcion(){
        MessageEvent<FeedbacksEventType> event
                = new MessageEvent<>(FeedbacksEventType.ON_SAVE_FEEDBACK_ERROR, response, new Throwable());

        feedbacksPresenter.onMessageEvent(event);

        verify(feedbacksView).displayError(event.getError(), FeedbacksPresenter.ADD_FEEDBACK_REQUEST_RESOLVED);
    }

    @Test
    public void saveFeedbackFailedWithExepcionButIfTheNavigationChangesTheEventShouldBeIgnored(){
        MessageEvent<FeedbacksEventType> event
                = new MessageEvent<>(FeedbacksEventType.ON_SAVE_FEEDBACK_ERROR, response, new Throwable());

        feedbacksPresenter.onViewFeedbacksScreen();
        feedbacksPresenter.onMessageEvent(event);

        verify(feedbacksView, never()).displayError(event.getError(), FeedbacksPresenter.ADD_FEEDBACK_REQUEST_RESOLVED);
    }

    @Test
    public void saveFeedbackSuccessfullyButIfTheNavigationChangesTheEventShouldBeIgnored(){
        MessageEvent<FeedbacksEventType> event
                = new MessageEvent<>(FeedbacksEventType.ON_SAVE_FEEDBACKS_SUCCESS, feedback);

        feedbacksPresenter.onViewFeedbacksScreen();
        feedbacksPresenter.onMessageEvent(event);

        verify(feedbackInteractor).removePendingFeedback(feedbackAssociate);
        verify(feedbacksView, never()).loadFeedbacks(ArgumentMatchers.<Feedback>anyList());
    }

    @Test
    public void saveFeedbackSuccessfully(){
        MessageEvent<FeedbacksEventType> event
                = new MessageEvent<>(FeedbacksEventType.ON_SAVE_FEEDBACKS_SUCCESS, feedback);

        feedbacksPresenter.onMessageEvent(event);

        verify(feedbackInteractor).removePendingFeedback(feedbackAssociate);
        verify(feedbacksView).loadFeedbacks(ArgumentMatchers.<Feedback>anyList());
        verify(feedbacksView).hideNoFeedbacksError();
    }
}
