package com.eatfood.app.suggestfood.features.signup;

import com.eatfood.app.suggestfood.features.signup.presenter.SignupPresenter;

import org.junit.Test;

import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

/**
 * Created by Usuario on 05/09/2017.
 */

public class ConnectivityValidation extends SignupTest {

    @Test
    public void networkConnectivityDisabled(){
        when(signupView.getEmail()).thenReturn("foo@example.com");
        when(signupView.getPassword()).thenReturn("123456789");
        when(signupView.getConfirmationPassword()).thenReturn("123456789");
        when(signupView.isNetworkConnectivityAvailable()).thenReturn(false);

        signupPresenter.validateSignUp();

        verify(signupView).onNetworkConnectivityDisabled(SignupPresenter.SIGNUP_REQUEST_RESOLVED, signupPresenter);
    }
}
