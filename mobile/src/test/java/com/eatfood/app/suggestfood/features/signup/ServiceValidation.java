package com.eatfood.app.suggestfood.features.signup;

import com.eatfood.app.suggestfood.domain.error.HttpException;
import com.eatfood.app.suggestfood.domain.event.MessageEvent;
import com.eatfood.app.suggestfood.entity.User;
import com.eatfood.app.suggestfood.features.FeaturesTest;
import com.eatfood.app.suggestfood.features.signup.model.callback.SignupCallback;
import com.eatfood.app.suggestfood.lib.gson.GsonBuilder;
import com.eatfood.app.suggestfood.lib.httpclient.HttpClient;
import com.eatfood.app.suggestfood.service.external.user.SignUpReturn;

import org.junit.Before;
import org.junit.Test;

import java.net.SocketTimeoutException;

import okhttp3.Headers;
import okhttp3.ResponseBody;
import retrofit2.Response;

import static org.mockito.Mockito.verify;

/**
 * Created by Usuario on 06/09/2017.
 */

@SuppressWarnings("ConstantConditions")
public class ServiceValidation extends FeaturesTest {

    private final User user = new User(0, "user@example.com", "123456789");

    private SignupCallback signupCallback;

    @Override
    @Before
    public void setUp() throws Exception {
        super.setUp();

        signupCallback = new SignupCallback(eventBus, GsonBuilder.INSTANCE.build());
    }

    @Test
    public void serviceUnavailable(){
        Response<User> response = Response.error(503,
                ResponseBody.create(HttpClient.INSTANCE.getMediaType(), ""));

        signupCallback.onFailure(new SignUpReturn.Failure(user), response.code(), response.errorBody());

        HttpException httpException = new HttpException(response.code());

        verify(eventBus).post(new MessageEvent<>(SignupEventType.ON_SIGN_UP_ERROR, httpException));
    }

    @Test
    public void emailInUse(){
        User user = new User(0, "foo@example.com", "123456789");
        String message = "Email already in use";

        Response<User> response = Response.error(422, ResponseBody.create(HttpClient.INSTANCE.getMediaType(),
                        "{\"errors\":{\"full_messages\":[\""+message+"\"]}}"));

        signupCallback.onFailure(new SignUpReturn.Failure(user), response.code(), response.errorBody());

        verify(eventBus).post(new MessageEvent<>(SignupEventType.ON_SIGN_UP_ERROR, message));
    }

    @Test
    public void faliureService(){
        SocketTimeoutException exception = new SocketTimeoutException();

        signupCallback.onError(new SignUpReturn.Failure(user), exception);

        verify(eventBus).post(new MessageEvent<>(SignupEventType.ON_SIGN_UP_ERROR, exception));
    }

    @Test
    public void signupSuccess(){
        signupCallback.onSuccessful(new SignUpReturn.Successful(user), Headers.of());

        verify(eventBus).post(new MessageEvent<>(SignupEventType.ON_SIGN_UP_SUCCESS, user));
    }
}
