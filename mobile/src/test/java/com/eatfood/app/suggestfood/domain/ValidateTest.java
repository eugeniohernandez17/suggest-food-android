package com.eatfood.app.suggestfood.domain;

import com.eatfood.app.suggestfood.R;
import com.eatfood.app.suggestfood.UnitTest;
import com.eatfood.app.suggestfood.domain.visitor.Evaluator;

import org.junit.Before;

import static org.mockito.Mockito.when;

/**
 * Created by Usuario on 30/08/2017.
 */

abstract class ValidateTest extends UnitTest {

    Evaluator evaluator;

    @Before
    public void setUp() throws Exception {
        super.setUp();
        when(context.getString(R.string.general_error_field_required))
                .thenReturn("This field is required");
    }

    public String getError(){
        return evaluator.getError();
    }

    @SuppressWarnings("unchecked")
    public boolean valid(String text){
        return evaluator.validate(text);
    }
}
