package com.eatfood.app.suggestfood.features.feedbacks;

import com.eatfood.app.suggestfood.domain.event.MessageEvent;
import com.eatfood.app.suggestfood.entity.AuthorizedUser;
import com.eatfood.app.suggestfood.entity.Feedback;
import com.eatfood.app.suggestfood.entity.User;
import com.eatfood.app.suggestfood.features.feedbacks.presenter.FeedbacksPresenter;
import com.eatfood.app.suggestfood.service.external.feedback.FeedbacksReturn;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;

import java.util.ArrayList;

import io.objectbox.relation.ToOne;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.atLeastOnce;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

/**
 * Created by Usuario on 10/10/2017.
 */

@SuppressWarnings("ConstantConditions")
public class ValidateEventsFeedbacksListRequest extends FeedbacksTest {

    private final AuthorizedUser authorizedUser = new AuthorizedUser();
    private final User user = new User();

    @Mock
    private ToOne<User> toOneUser;

    private FeedbacksReturn feedbacksReturn;
    private ArrayList<Feedback> feedbacks;

    @Override
    @Before
    public void setUp() throws Exception {
        super.setUp();
        feedbacks = new ArrayList<>();
        feedbacksReturn = new FeedbacksReturn(1, Feedback.FeedbackType.FEEDBACK, feedbacks);
        authorizedUser.setUser(toOneUser);
        when(feedbacksView.isNetworkConnectivityAvailable()).thenReturn(true);
        when(sessionInteractor.getSession()).thenReturn(authorizedUser);
        when(toOneUser.getTarget()).thenReturn(user);
    }

    @Test
    public void emptyFeedbackListAndFirstPage(){
        MessageEvent<FeedbacksEventType> event
                = new MessageEvent<>(FeedbacksEventType.ON_LOAD_FEEDBACKS_SUCCESS, feedbacksReturn);

        feedbacksPresenter.onViewFeedbacksScreen();
        feedbacksPresenter.onMessageEvent(event);

        assertTrue(feedbacksPresenter.hasLoadedAllItems());
        verify(feedbacksView, never()).loadFeedbacks(feedbacksReturn.getFeedbacks());
        verify(feedbacksView).displayNoFeedbacksError(Feedback.FeedbackType.FEEDBACK);
    }

    @Test
    public void emptyFeedbackListAndNotFirstPage(){
        feedbacksReturn.setPage(2);
        MessageEvent<FeedbacksEventType> event
                = new MessageEvent<>(FeedbacksEventType.ON_LOAD_FEEDBACKS_SUCCESS, feedbacksReturn);

        feedbacksPresenter.onViewFeedbacksScreen();
        feedbacksPresenter.onMessageEvent(event);

        assertTrue(feedbacksPresenter.hasLoadedAllItems());
        verify(feedbacksView).loadFeedbacks(feedbacksReturn.getFeedbacks());
        verify(feedbacksView, never()).displayNoFeedbacksError(Feedback.FeedbackType.FEEDBACK);
    }

    @Test
    public void notEmptyFeedbackList(){
        feedbacks.add(new Feedback(1, "Name", "user@foo.com","topic", "Description", Feedback.FeedbackType.FEEDBACK));
        MessageEvent<FeedbacksEventType> event
                = new MessageEvent<>(FeedbacksEventType.ON_LOAD_FEEDBACKS_SUCCESS, feedbacksReturn);

        feedbacksPresenter.onViewFeedbacksScreen();
        feedbacksPresenter.onMessageEvent(event);

        assertFalse(feedbacksPresenter.hasLoadedAllItems());
        verify(feedbacksView).loadFeedbacks(feedbacksReturn.getFeedbacks());
    }

    @Test
    public void loadFeedbacksFailedWithExepcion(){
        MessageEvent<FeedbacksEventType> event
                = new MessageEvent<>(FeedbacksEventType.ON_LOAD_FEEDBACKS_ERROR, new Throwable());

        feedbacksPresenter.onMessageEvent(event);

        assertFalse(feedbacksPresenter.hasLoadedAllItems());
        verify(feedbacksView).displayError(event.getError(), FeedbacksPresenter.LOAD_FEEDBACKS_REQUEST_RESOLVED);
    }

    @Test
    public void ifTheNavigationChangesTheEventShouldBeIgnored(){
        feedbacksPresenter.onViewComplaintsScreen();
        feedbacksPresenter.onRefresh();

        feedbacksPresenter.onViewSuggestionsScreen();

        feedbacks.add(new Feedback( 1, "Name", "user@foo.com","topic", "Description", Feedback.FeedbackType.COMPLAINT));
        MessageEvent<FeedbacksEventType> event
                = new MessageEvent<>(FeedbacksEventType.ON_LOAD_FEEDBACKS_SUCCESS, feedbacksReturn);

        feedbacksPresenter.onMessageEvent(event);

        verify(feedbacksView, never()).loadFeedbacks(feedbacksReturn.getFeedbacks());

        feedbacks.clear();

        feedbacksPresenter.onMessageEvent(event);

        verify(feedbacksView, never()).displayNoFeedbacksError(any(Feedback.FeedbackType.class));
    }

    @After
    public void tearDown() throws Exception {
        assertFalse(feedbacksPresenter.isLoading());
        verify(feedbacksView, atLeastOnce()).hideProgress();
    }
}
