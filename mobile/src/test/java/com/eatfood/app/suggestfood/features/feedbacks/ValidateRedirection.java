package com.eatfood.app.suggestfood.features.feedbacks;

import android.accounts.Account;

import com.eatfood.app.suggestfood.R;
import com.eatfood.app.suggestfood.entity.AuthorizedUser;
import com.eatfood.app.suggestfood.entity.Feedback;
import com.eatfood.app.suggestfood.entity.User;
import com.eatfood.app.suggestfood.features.feedbacks.presenter.FeedbacksPresenter;

import org.junit.Test;
import org.mockito.Mock;

import io.objectbox.relation.ToOne;

import static org.junit.Assert.assertEquals;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

/**
 * Created by Usuario on 10/10/2017.
 */

@SuppressWarnings("unchecked")
public class ValidateRedirection extends FeedbacksTest {

    private final AuthorizedUser authorizedUser = new AuthorizedUser();
    private final Feedback feedback = new Feedback();

    @Mock
    private ToOne<User> toOneUser;

    @Test
    public void theListOfComplaintsShouldBeDisplayedWhileTheViewIsLoaded(){
        feedbacksPresenter.onCreate();

        verify(feedbacksView).displayComplaintsScreen();
    }

    @Test
    public void ifTheUserHasThePendingFeedbackShouldBeDisplayedAMessageWhileTheViewIsLoaded(){
        int totalPendingFeedback = 2;
        String email = "user@gmail.com";
        User user = new User(0, email);
        Account account = new Account(email, "");
        authorizedUser.setUser(toOneUser);
        when(toOneUser.getTarget()).thenReturn(user);
        when(sessionInteractor.getSession()).thenReturn(authorizedUser);
        when(accountsInteractor.find(anyString())).thenReturn(account);
        when(feedbackInteractor.getTotalPendingFeedback()).thenReturn(totalPendingFeedback);

        feedbacksPresenter.onCreate();

        verify(feedbacksView).displaySyncPendingFeedbackMessage(totalPendingFeedback);
        verify(feedbacksView).syncPendingFeedbacks(account);
    }

    @Test
    public void displayTheListOfSuggestions(){
        feedbacksPresenter.onViewSuggestionsScreen();

        verify(feedbacksView).displaySuggestionsScreen();
    }

    @Test
    public void displayTheListOfFeedbacks(){
        feedbacksPresenter.onViewFeedbacksScreen();

        verify(feedbacksView).displayFeedbacksScreen();
    }

    @Test
    public void displayNewFeedbackSreen(){
        feedbacksPresenter.onViewNewFeedbackScreen();

        verify(feedbacksView).displayNewFeedbackScreen(any(Feedback.FeedbackType.class));
    }

    @Test
    public void notAddFeedbackIfIsLoadingFeedbacks(){
        feedbacksPresenter.onRefresh();

        assertEquals(true, feedbacksPresenter.isLoading());

        when(sessionInteractor.getSession()).thenReturn(authorizedUser);
        when(feedbackInteractor.getTotalPendingFeedback()).thenReturn(1);

        feedbacksPresenter.onAddFeedback(feedback);

        verify(feedbackInteractor, never()).addFeedback(feedback, true);
        verify(feedbackInteractor).addPendingFeedback(feedback);
        verify(feedbacksView).displayPendingFeedbackMessage(1);
    }

    @Test
    public void addFeedbackWithNetworkConnectivityDisabled() {
        when(feedbacksView.isNetworkConnectivityAvailable()).thenReturn(false);
        feedbacksPresenter.onCreate();

        assertEquals(false, feedbacksPresenter.isLoading());

        when(sessionInteractor.getSession()).thenReturn(authorizedUser);

        feedbacksPresenter.onAddFeedback(feedback);

        verify(feedbackInteractor, never()).addFeedback(feedback, true);
        verify(feedbackInteractor).addPendingFeedback(feedback);
        verify(feedbacksView).onNetworkConnectivityDisabled(FeedbacksPresenter.ADD_FEEDBACK_REQUEST_RESOLVED,
                feedbacksPresenter);
        verify(feedbacksView, never()).displayNoFeedbacksError(any(Feedback.FeedbackType.class));
    }

    @Test
    public void addFeedbackWithNetworkConnectivityAvailable() {
        when(feedbacksView.isNetworkConnectivityAvailable()).thenReturn(true);
        feedbacksPresenter.onCreate();

        assertEquals(false, feedbacksPresenter.isLoading());

        when(sessionInteractor.getSession()).thenReturn(authorizedUser);

        feedbacksPresenter.onAddFeedback(feedback);

        verify(feedbackInteractor).addFeedback(feedback, true);
        verify(feedbacksView).displayMessage(R.string.activity_feedback_saving_feedback);
    }

}
