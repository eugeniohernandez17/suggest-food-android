package com.eatfood.app.suggestfood.features.profile.show;

import com.eatfood.app.suggestfood.entity.User;

import org.junit.Test;

import static org.mockito.Mockito.never;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

/**
 * Created by Usuario on 29/09/2017.
 */

@SuppressWarnings("ConstantConditions")
public class LoadProfileWhileTheViewIsCreated extends ProfileTest {

    private final User user = new User(0,"user@example.com", "","User", null, "+12025550186");

    @Test
    public void profileShouldBeLoadedWhileTheViewIsCreated(){
        when(profileView.getUserParam()).thenReturn(user);

        profilePresenter.onCreate();

        verify(profileView).loadProfile(user);
    }

    @Test
    public void profileShouldBeUploadedAfterBeingEdited(){
        profilePresenter.onProfileEditingFinish(user);

        verify(profileView).setUserParam(user);
        verify(profileView).loadProfile(user);
    }

    @Test
    public void profileShouldNotBeLoadedWhenIsNull(){
        when(profileView.getUserParam()).thenReturn(null);

        profilePresenter.onCreate();

        verify(profileView, never()).loadProfile(null);
    }
}
