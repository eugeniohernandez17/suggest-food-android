package com.eatfood.app.suggestfood.domain;

import com.eatfood.app.suggestfood.R;
import com.eatfood.app.suggestfood.domain.evaluator.PasswordsMatchEvaluator;

import org.concordion.integration.junit4.ConcordionRunner;
import org.junit.Before;
import org.junit.runner.RunWith;

import static org.mockito.Mockito.when;

@RunWith(ConcordionRunner.class)
public class ValidateConfirmationPassword extends ValidateTest {

    @Override
    @Before
    public void setUp() throws Exception {
        super.setUp();
        this.evaluator = new PasswordsMatchEvaluator(context, fieldEvaluate);
        when(context.getString(R.string.general_error_not_match_password))
                .thenReturn("The passwords not match");
    }

    @SuppressWarnings("unchecked")
    public boolean match(String password, String confirmationPassword){
        return this.evaluator.validate(new String[]{password, confirmationPassword});
    }
}
