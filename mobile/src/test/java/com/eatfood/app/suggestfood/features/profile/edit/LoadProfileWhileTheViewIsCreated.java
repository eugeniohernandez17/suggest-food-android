package com.eatfood.app.suggestfood.features.profile.edit;

import com.eatfood.app.suggestfood.entity.User;

import org.junit.Test;

import static org.mockito.Mockito.never;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

/**
 * Created by Usuario on 29/09/2017.
 */

@SuppressWarnings("ConstantConditions")
public class LoadProfileWhileTheViewIsCreated extends ProfileTest {

    @Test
    public void profileShouldBeLoaded(){
        User user = new User(0, "user@example.com");
        user.setName("User");
        user.setPhone("+12025550186");
        when(profileView.getUserParam()).thenReturn(user);

        profilePresenter.onCreate();

        verify(profileView).loadProfile(user);
    }

    @Test
    public void profileShouldNotBeLoadedWhenIsNull(){
        when(profileView.getUserParam()).thenReturn(null);

        profilePresenter.onCreate();

        verify(profileView, never()).loadProfile(null);
    }
}
