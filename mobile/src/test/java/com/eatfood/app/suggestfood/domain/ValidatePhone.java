package com.eatfood.app.suggestfood.domain;

import com.eatfood.app.suggestfood.R;
import com.eatfood.app.suggestfood.domain.evaluator.PhoneEvaluator;

import org.concordion.integration.junit4.ConcordionRunner;
import org.junit.Before;
import org.junit.runner.RunWith;

import static org.mockito.Mockito.when;

@RunWith(ConcordionRunner.class)
public class ValidatePhone extends ValidateTest {

    @Override
    @Before
    public void setUp() throws Exception {
        super.setUp();
        this.evaluator = new PhoneEvaluator(context, fieldEvaluate);
        when(context.getString(R.string.general_error_invalid_phone))
                .thenReturn("This phone number is invalid");
        when(context.getString(R.string.general_error_invalid_phone_min, 6))
                .thenReturn("This phone is too short.  Minimum length is 6");
        when(context.getString(R.string.general_error_invalid_phone_max, 13))
                .thenReturn("This phone is too long. Maximum length is 13");
    }
}
