package com.eatfood.app.suggestfood.features.session;

import com.eatfood.app.suggestfood.UnitTest;
import com.eatfood.app.suggestfood.entity.AuthorizedUser;
import com.eatfood.app.suggestfood.entity.User;
import com.eatfood.app.suggestfood.features.session.model.SessionRepository;
import com.eatfood.app.suggestfood.features.session.model.SessionRepositoryImpl;

import org.concordion.integration.junit4.ConcordionRunner;
import org.junit.Before;
import org.junit.runner.RunWith;
import org.mockito.Answers;
import org.mockito.Mock;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

import io.objectbox.Box;

import static org.mockito.Mockito.when;

/**
 * Created by Usuario on 01/09/2017.
 */

@RunWith(ConcordionRunner.class)
public class ValidateSession extends UnitTest {

    private static final DateFormat DATE_FORMAT = new SimpleDateFormat("yyyy-MM-dd", Locale.US);

    @Mock(answer = Answers.RETURNS_DEEP_STUBS)
    private Box<User> userBox;

    @Mock(answer = Answers.RETURNS_DEEP_STUBS)
    private Box<AuthorizedUser> authorizedUserBox;

    private SessionRepository sessionRepository;

    @Override
    @Before
    public void setUp() throws Exception {
        super.setUp();

        sessionRepository = new SessionRepositoryImpl(userBox, authorizedUserBox);
    }

    public boolean valid(String date) {
        long expiry = 1505503612; // 2 Weeks
        Date createdAt = new Date();

        try {
            createdAt = DATE_FORMAT.parse(date);
        } catch (ParseException e){
            e.printStackTrace();
        }

        AuthorizedUser authorizedUser = new AuthorizedUser();
        authorizedUser.setExpiry(expiry);
        authorizedUser.setCreatedAt(createdAt);

        when(authorizedUserBox.query().build().findFirst()).thenReturn(authorizedUser);

        return sessionRepository.isValidSession();
    }
}
