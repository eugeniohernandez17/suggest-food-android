package com.eatfood.app.suggestfood.features.lost_password;

import com.eatfood.app.suggestfood.features.lost_password.presenter.LostPasswordPresenter;

import org.junit.Test;

import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

/**
 * Created by Usuario on 14/09/2017.
 */

public class ConnectivityValidation extends LostPasswordTest {

    @Test
    public void networkConnectivityDisabled(){
        when(lostPasswordView.getEmail()).thenReturn("foo@example.com");
        when(lostPasswordView.isNetworkConnectivityAvailable()).thenReturn(false);

        lostPasswordPresenter.validLostPassword();

        verify(lostPasswordView)
                .onNetworkConnectivityDisabled(LostPasswordPresenter.LOST_PASSWORD_REQUEST_RESOLVED,
                        lostPasswordPresenter);
    }
}
