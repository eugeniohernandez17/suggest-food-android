package com.eatfood.app.suggestfood.domain;

import com.eatfood.app.suggestfood.R;
import com.eatfood.app.suggestfood.domain.evaluator.EmailEvaluator;

import org.concordion.integration.junit4.ConcordionRunner;
import org.junit.Before;
import org.junit.runner.RunWith;

import static org.mockito.Mockito.when;

@RunWith(ConcordionRunner.class)
public class ValidateEmail extends ValidateTest {

    @Override
    @Before
    public void setUp() throws Exception {
        super.setUp();
        this.evaluator = new EmailEvaluator(context, fieldEvaluate);
        when(context.getString(R.string.general_error_invalid_email))
                .thenReturn("This email address is invalid");
    }
}
