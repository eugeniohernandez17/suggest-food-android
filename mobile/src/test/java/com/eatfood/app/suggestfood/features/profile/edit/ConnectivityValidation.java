package com.eatfood.app.suggestfood.features.profile.edit;

import com.eatfood.app.suggestfood.entity.AuthorizedUser;
import com.eatfood.app.suggestfood.features.profile.edit.presenter.ProfilePresenter;

import org.junit.Test;

import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

/**
 * Created by Usuario on 28/09/2017.
 */

public class ConnectivityValidation extends ProfileTest {

    @Test
    public void networkConnectivityDisabled(){
        when(profileView.getName()).thenReturn("User foo");
        when(profileView.getPhone()).thenReturn("+12025550186");
        when(profileView.isNetworkConnectivityAvailable()).thenReturn(false);
        when(sessionInteractor.getSession()).thenReturn(new AuthorizedUser());

        profilePresenter.updateProfile();

        verify(profileView)
                .onNetworkConnectivityDisabled(ProfilePresenter.UPDATE_PROFILE_REQUEST_RESOLVED,
                        profilePresenter);
    }
}
