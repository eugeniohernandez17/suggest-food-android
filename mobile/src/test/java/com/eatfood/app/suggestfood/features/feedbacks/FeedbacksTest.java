package com.eatfood.app.suggestfood.features.feedbacks;

import com.eatfood.app.suggestfood.features.FeaturesTest;
import com.eatfood.app.suggestfood.features.accounts.model.AccountsInteractor;
import com.eatfood.app.suggestfood.features.feedback.model.FeedbackInteractor;
import com.eatfood.app.suggestfood.features.feedbacks.model.FeedbacksInteractor;
import com.eatfood.app.suggestfood.features.feedbacks.presenter.FeedbacksPresenter;
import com.eatfood.app.suggestfood.features.feedbacks.presenter.FeedbacksPresenterImpl;
import com.eatfood.app.suggestfood.features.feedbacks.view.FeedbacksView;
import com.eatfood.app.suggestfood.features.session.model.SessionInteractor;

import org.junit.Before;
import org.mockito.Mock;

/**
 * Created by Usuario on 08/10/2017.
 */

abstract class FeedbacksTest extends FeaturesTest {

    @Mock
    FeedbacksView feedbacksView;

    @Mock
    SessionInteractor sessionInteractor;

    @Mock
    FeedbacksInteractor feedbacksInteractor;

    @Mock
    FeedbackInteractor feedbackInteractor;

    @Mock
    AccountsInteractor accountsInteractor;

    FeedbacksPresenter feedbacksPresenter;

    @Override
    @Before
    public void setUp() throws Exception {
        super.setUp();

        feedbacksPresenter = new FeedbacksPresenterImpl(feedbacksView, eventBus,
                feedbacksInteractor, feedbackInteractor, sessionInteractor, accountsInteractor);
    }
}
