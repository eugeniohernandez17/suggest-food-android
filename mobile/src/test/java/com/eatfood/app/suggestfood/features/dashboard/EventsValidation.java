package com.eatfood.app.suggestfood.features.dashboard;

import android.accounts.Account;

import com.eatfood.app.suggestfood.domain.event.MessageEvent;
import com.eatfood.app.suggestfood.entity.User;
import com.eatfood.app.suggestfood.features.dashboard.presenter.DashboardPresenter;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;

import static org.mockito.Mockito.never;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

/**
 * Created by Usuario on 23/09/2017.
 */

@SuppressWarnings("ConstantConditions")
public class EventsValidation extends DashboardTest {

    @Mock
    private Account account;

    private User user;

    @Override
    @Before
    public void setUp() throws Exception {
        super.setUp();
        user = new User(0, "foo@example.com", "123456789");
        user.setName("User Foo");
        user.setImage("http://user.com/image.png");
        user.setNotificationsUserId("123456789");
    }

    @Test
    public void userNotFound(){
        MessageEvent<DashboardEventType> event
                = new MessageEvent<>(DashboardEventType.ON_GET_USER_INFO_ERROR, "User not found");

        dashboardPresenter.onMessageEvent(event);

        verify(dashboardView).displayError(event.getMessage());
    }

    @Test
    public void getUserInfoFailedWithExeption(){
        MessageEvent<DashboardEventType> event
                = new MessageEvent<>(DashboardEventType.ON_GET_USER_INFO_ERROR, new Throwable());

        dashboardPresenter.onMessageEvent(event);

        verify(dashboardView).displayError(event.getError(), DashboardPresenter.GET_USER_INFO_REQUEST_CODE);
    }

    @Test
    public void userInfoWithoutNotificationUserIdShouldBeLoginAgain(){
        user.setNotificationsUserId(null);
        MessageEvent<DashboardEventType> event
                = new MessageEvent<>(DashboardEventType.ON_GET_USER_INFO_SUCCESS, user);

        dashboardPresenter.onMessageEvent(event);

        verify(dashboardView).navigateToLoginScreen();
    }

    @Test
    public void userInfoShouldBeLoadedWhenPresentingTheViewAndAddTheSynchronizationProcessPeriodically(){
        Long syncFrequency = 180L;

        checkLoadUser(syncFrequency);

        verify(dashboardView).addPeriodicSync(account, syncFrequency);
    }

    @Test
    public void userInfoShouldBeLoadedWhenPresentingTheViewAndRemoveTheSynchronizationProcessPeriodically() {
        Long syncFrequency = -1L;

        checkLoadUser(syncFrequency);

        verify(dashboardView, never()).addPeriodicSync(account, syncFrequency);
        verify(dashboardView).removePeriodicSync(account);
    }

    /*Own Methods*/

    private void checkLoadUser(Long syncFrequency){
        when(accountsInteractor.find(user.getEmail())).thenReturn(account);
        when(settingsInteractor.getSyncFrequency()).thenReturn(syncFrequency);

        MessageEvent<DashboardEventType> event
                = new MessageEvent<>(DashboardEventType.ON_GET_USER_INFO_SUCCESS, user);

        dashboardPresenter.onMessageEvent(event);

        verify(dashboardView).loadUser(user);
    }

}
