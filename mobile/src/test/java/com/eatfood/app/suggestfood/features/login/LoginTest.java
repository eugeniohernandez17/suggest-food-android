package com.eatfood.app.suggestfood.features.login;

import com.eatfood.app.suggestfood.features.FieldsViewTest;
import com.eatfood.app.suggestfood.features.accounts.model.AccountsInteractor;
import com.eatfood.app.suggestfood.features.login.model.LoginInteractor;
import com.eatfood.app.suggestfood.features.login.presenter.LoginPresenter;
import com.eatfood.app.suggestfood.features.login.presenter.LoginPresenterImpl;
import com.eatfood.app.suggestfood.features.login.view.LoginView;
import com.eatfood.app.suggestfood.features.notifications.model.NotificationsInteractor;
import com.eatfood.app.suggestfood.features.session.model.SessionInteractor;

import org.junit.Before;
import org.mockito.Mock;

/**
 * Created by Usuario on 05/09/2017.
 */

abstract class LoginTest extends FieldsViewTest {

    @Mock
    LoginView loginView;

    @Mock
    LoginInteractor loginInteractor;

    @Mock
    AccountsInteractor accountsInteractor;

    @Mock
    SessionInteractor sessionInteractor;

    @Mock
    NotificationsInteractor notificationsInteractor;

    LoginPresenter loginPresenter;

    @Override
    @Before
    public void setUp() throws Exception {
        super.setUp();
        loginPresenter = new LoginPresenterImpl(loginView, eventBus, emailEvaluator, passwordEvaluator,
                loginInteractor, sessionInteractor, accountsInteractor, notificationsInteractor);


    }
}
