package com.eatfood.app.suggestfood.service.sync;

import com.eatfood.app.suggestfood.entity.AuthorizedUser;
import com.eatfood.app.suggestfood.entity.Feedback;
import com.eatfood.app.suggestfood.service.internal.sync.SyncType;

import org.junit.Before;
import org.junit.Test;

import static org.mockito.Mockito.atLeastOnce;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

/**
 * Created by Usuario on 04/04/2018.
 */

public class FeedbacksSyncTest extends SyncServiceTest {

    @Override
    @Before
    public void setUp() throws Exception {
        super.setUp();
        syncType = SyncType.FEEDBACKS;
        when(sessionInteractor.getSession()).thenReturn(new AuthorizedUser());
    }

    @Test
    public void syncWithPendingFeedbacksEmptyList(){
        when(feedbackInteractor.getTotalPendingFeedback()).thenReturn(0);

        syncPresenter.sync(syncType);

        verify(feedbackInteractor, never()).getLastPendingFeedback();
    }

    @Test
    public void syncWithPendingFeedbacks(){
        Feedback feedback = new Feedback();
        when(feedbackInteractor.getTotalPendingFeedback()).thenReturn(1);
        when(feedbackInteractor.getLastPendingFeedback()).thenReturn(feedback);

        syncPresenter.sync(syncType);

        verify(feedbackInteractor, atLeastOnce()).addFeedback(feedback, false);
        verify(feedbackInteractor, never()).removePendingFeedback(feedback);
    }
}
