package com.eatfood.app.suggestfood.features.login;

import com.eatfood.app.suggestfood.domain.event.MessageEvent;
import com.eatfood.app.suggestfood.entity.AuthorizedUser;
import com.eatfood.app.suggestfood.features.login.presenter.LoginPresenter;

import org.junit.Test;

import static org.mockito.Mockito.verify;

/**
 * Created by Usuario on 31/08/2017.
 */

public class EventsValidation extends LoginTest {

    @Test
    public void loginFailed(){
        MessageEvent<LoginEventType> loginEvent
                = new MessageEvent<>(LoginEventType.ON_SIGN_IN_ERROR, "General Error");

        onMessageEventFailed(loginEvent);

        assert loginEvent.getMessage() != null;
        verify(loginView).displayError(loginEvent.getMessage());
    }

    @Test
    public void loginFailedWithExepcion(){
        MessageEvent<LoginEventType> loginEvent
                = new MessageEvent<>(LoginEventType.ON_SIGN_IN_ERROR, new Throwable());

        onMessageEventFailed(loginEvent);

        verify(loginView).displayError(loginEvent.getError(), LoginPresenter.LOGIN_REQUEST_RESOLVED);
    }

    @Test
    public void loginSuccess(){
        AuthorizedUser authorizedUser = new AuthorizedUser();
        MessageEvent<LoginEventType> loginEvent
                = new MessageEvent<>(LoginEventType.ON_SIGN_IN_SUCCESS, authorizedUser);

        loginPresenter.onMessageEvent(loginEvent);

        verify(sessionInteractor).setSession(authorizedUser);
        verify(accountsInteractor).create(authorizedUser);
        verify(notificationsInteractor).setAuthorizedUser(authorizedUser);
        verify(loginView).hideProgress();
        verify(loginView).navigateToMainScreen();
    }

    private void onMessageEventFailed(MessageEvent<LoginEventType> loginEvent){
        loginPresenter.onMessageEvent(loginEvent);

        verify(loginView).enableInputs();
        verify(loginView).hideProgress();
        verify(loginView).setPassword(null);
    }
}
