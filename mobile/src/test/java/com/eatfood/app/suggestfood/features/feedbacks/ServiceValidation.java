package com.eatfood.app.suggestfood.features.feedbacks;

import com.eatfood.app.suggestfood.domain.error.HttpException;
import com.eatfood.app.suggestfood.domain.event.MessageEvent;
import com.eatfood.app.suggestfood.entity.Feedback;
import com.eatfood.app.suggestfood.features.FeaturesTest;
import com.eatfood.app.suggestfood.features.feedback.model.callback.FeedbackCallback;
import com.eatfood.app.suggestfood.features.feedbacks.model.callback.FeedbacksCallback;
import com.eatfood.app.suggestfood.lib.gson.GsonBuilder;
import com.eatfood.app.suggestfood.lib.httpclient.HttpClient;
import com.eatfood.app.suggestfood.service.external.feedback.FeedbackReturn;
import com.eatfood.app.suggestfood.service.external.feedback.FeedbacksReturn;

import org.junit.Before;
import org.junit.Test;

import java.net.SocketTimeoutException;
import java.util.ArrayList;

import okhttp3.Headers;
import okhttp3.ResponseBody;
import retrofit2.Response;

import static org.mockito.Mockito.verify;

/**
 * Created by Usuario on 13/10/2017.
 */

@SuppressWarnings("ConstantConditions")
public class ServiceValidation extends FeaturesTest {

    private final FeedbacksReturn feedbacksReturn = new FeedbacksReturn(1, Feedback.FeedbackType.FEEDBACK);
    private final Feedback feedback = new Feedback();

    private FeedbacksCallback feedbacksCallback;

    private FeedbackCallback feedbackCallback;

    @Override
    @Before
    public void setUp() throws Exception {
        super.setUp();

        feedbacksCallback = new FeedbacksCallback(eventBus, GsonBuilder.INSTANCE.build());
        feedbackCallback = new FeedbackCallback(eventBus, GsonBuilder.INSTANCE.build(), context);
    }

    @Test
    public void serviceUnavailableForFeedbackListRequest(){
        Response<FeedbacksReturn> response = Response.error(503,
                ResponseBody.create(HttpClient.INSTANCE.getMediaType(), ""));

        feedbacksCallback.onFailure(feedbacksReturn, response.code(), response.errorBody());

        HttpException httpException = new HttpException(response.code());
        verify(eventBus).post(new MessageEvent<>(FeedbacksEventType.ON_LOAD_FEEDBACKS_ERROR, httpException));
    }

    @Test
    public void serviceUnavailableForAddNewFeedbackRequest(){
        Response<Feedback> response = Response.error(503,
                ResponseBody.create(HttpClient.INSTANCE.getMediaType(), ""));

        FeedbackReturn.Failure error = new FeedbackReturn.Failure(feedback, true);

        feedbackCallback.onFailure(error, response.code(), response.errorBody());

        HttpException httpException = new HttpException(response.code());
        verify(eventBus).post(new MessageEvent<>(FeedbacksEventType.ON_SAVE_FEEDBACK_ERROR, httpException));
    }

    @Test
    public void faliureServiceForFeedbackListRequest(){
        SocketTimeoutException exception = new SocketTimeoutException();

        feedbacksCallback.onError(feedbacksReturn, exception);

        verify(eventBus).post(new MessageEvent<>(FeedbacksEventType.ON_LOAD_FEEDBACKS_ERROR, exception));
    }

    @Test
    public void faliureServiceForAddNewFeedbackRequest(){
        SocketTimeoutException exception = new SocketTimeoutException();

        FeedbackReturn.Failure error = new FeedbackReturn.Failure(feedback, true);

        feedbackCallback.onError(error, exception);

        verify(eventBus).post(new MessageEvent<>(FeedbacksEventType.ON_SAVE_FEEDBACK_ERROR, exception));
    }

    @Test
    public void getFeedbackListSuccess(){
        feedbacksReturn.setFeedbacks(new ArrayList<Feedback>());

        feedbacksCallback.onSuccessful(feedbacksReturn, Headers.of());

        verify(eventBus).post(new MessageEvent<>(FeedbacksEventType.ON_LOAD_FEEDBACKS_SUCCESS, feedbacksReturn));
    }

    @Test
    public void addNewFeedbackSuccess(){
        FeedbackReturn.Successful feedbacksReturn = new FeedbackReturn.Successful(feedback, true);

        feedbackCallback.onSuccessful(feedbacksReturn, Headers.of());

        verify(eventBus).post(new MessageEvent<>(FeedbacksEventType.ON_SAVE_FEEDBACKS_SUCCESS, feedback));
    }
}
