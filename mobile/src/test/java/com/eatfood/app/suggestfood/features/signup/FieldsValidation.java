package com.eatfood.app.suggestfood.features.signup;

import com.eatfood.app.suggestfood.domain.Callback;
import com.eatfood.app.suggestfood.entity.User;

import org.junit.Before;
import org.junit.Test;

import static org.mockito.Mockito.never;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

/**
 * Created by Usuario on 06/09/2017.
 */

public class FieldsValidation extends SignupTest {

    @Override
    @Before
    public void setUp() throws Exception {
        super.setUp();
        when(signupView.isNetworkConnectivityAvailable()).thenReturn(true);
    }

    @Test
    public void emptyFields(){
        User user = new User(0, "", "");
        signup(user, "");

        verify(signupView).showErrorInEmail(emailEvaluator.getError());
        verify(signupView).showErrorInPassword(passwordEvaluator.getError());
        verify(signupInteractor, never()).doSignUp(user.getEmail(), user.getPassword());
    }

    @Test
    public void invalidEmail(){
        User user = new User(0, "foo@.com", "123456789");
        signup(user, "123456789");

        verify(signupView).showErrorInEmail(emailEvaluator.getError());
        verify(signupInteractor, never()).doSignUp(user.getEmail(), user.getPassword());
    }

    @Test
    public void invalidPassword(){
        User user = new User(0, "foo@example.com", "123");
        signup(user, "123");

        verify(signupView).showErrorInPassword(passwordEvaluator.getError());
        verify(signupInteractor, never()).doSignUp(user.getEmail(), user.getPassword());
    }

    @Test
    public void invalidEmailAndPassword(){
        User user = new User(0, "foo@.com", "123");
        signup(user, "123");

        verify(signupView).showErrorInEmail(emailEvaluator.getError());
        verify(signupView).showErrorInPassword(passwordEvaluator.getError());
        verify(signupInteractor, never()).doSignUp(user.getEmail(), user.getPassword());
    }

    @Test
    public void passwordsNotMatch(){
        User user = new User(0, "foo@example.com", "123456789");
        signup(user, "123");

        verify(signupView).showErrorInConfirmationPassword(passwordsMatchEvaluator.getError());
        verify(signupInteractor, never()).doSignUp(user.getEmail(), user.getPassword());
    }

    @Test
    public void validSignup(){
        final User user = new User(0, "foo@example.com", "123456789");

        signup(user, user.getPassword());

        evaluateActionWithValidatedFieldsCorrectly(signupView, new Callback() {
            @Override
            public void execute() {
                verify(signupView).showErrorInEmail(null);
                verify(signupView).showErrorInPassword(null);
                verify(signupView).showErrorInConfirmationPassword(null);
                verify(signupInteractor).doSignUp(user.getEmail(), user.getPassword());
            }
        });
    }

    private void signup(User user, String confirmationPassword) {
        when(signupView.getEmail()).thenReturn(user.getEmail());
        when(signupView.getPassword()).thenReturn(user.getPassword());
        when(signupView.getConfirmationPassword()).thenReturn(confirmationPassword);

        evaluateFields(signupView, new Callback() {
            @Override
            public void execute() {
                signupPresenter.validateSignUp();
            }
        });
    }

}
