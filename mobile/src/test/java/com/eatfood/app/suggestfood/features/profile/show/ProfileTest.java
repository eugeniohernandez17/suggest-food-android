package com.eatfood.app.suggestfood.features.profile.show;

import com.eatfood.app.suggestfood.features.FeaturesTest;
import com.eatfood.app.suggestfood.features.profile.general.ProfilePresenterHelper;
import com.eatfood.app.suggestfood.features.profile.show.presenter.ProfilePresenter;
import com.eatfood.app.suggestfood.features.profile.show.presenter.ProfilePresenterImpl;
import com.eatfood.app.suggestfood.features.profile.show.view.ProfileView;

import org.junit.Before;
import org.mockito.Mock;

/**
 * Created by Usuario on 25/09/2017.
 */

abstract class ProfileTest extends FeaturesTest {

    @Mock
    ProfileView profileView;

    ProfilePresenter profilePresenter;

    @Override
    @Before
    public void setUp() throws Exception {
        super.setUp();

        ProfilePresenterHelper profileHelper = new ProfilePresenterHelper(profileView);
        profilePresenter = new ProfilePresenterImpl(profileView, eventBus, profileHelper);
    }
}
