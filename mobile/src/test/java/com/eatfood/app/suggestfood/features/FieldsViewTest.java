package com.eatfood.app.suggestfood.features;

import com.eatfood.app.suggestfood.domain.Callback;
import com.eatfood.app.suggestfood.domain.view.type.fields.FieldsView;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyInt;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

/**
 * Created by Usuario on 06/09/2017.
 */

public class FieldsViewTest extends FeaturesTest {

    protected void evaluateActionWithValidatedFieldsCorrectly(FieldsView fieldsView,
                                                              Callback callback){
        verify(fieldsView).disableInputs();
        verify(fieldsView).showProgress();

        callback.execute();
    }

    protected void evaluateFields(FieldsView fieldsView, Callback callback){
        when(fieldsView.isNetworkConnectivityAvailable()).thenReturn(true);
        when(context.getString(anyInt())).thenReturn("");
        when(context.getString(anyInt(), any())).thenReturn("");

        callback.execute();

        verify(fieldsView).hideKeyboard();
    }
}
