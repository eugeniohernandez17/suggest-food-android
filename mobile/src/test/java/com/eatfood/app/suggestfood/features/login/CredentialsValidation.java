package com.eatfood.app.suggestfood.features.login;

import com.eatfood.app.suggestfood.domain.Callback;
import com.eatfood.app.suggestfood.entity.User;

import org.junit.Before;
import org.junit.Test;

import static org.mockito.Mockito.never;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

public class CredentialsValidation extends LoginTest {

    @Override
    @Before
    public void setUp() throws Exception {
        super.setUp();
        when(loginView.isNetworkConnectivityAvailable()).thenReturn(true);
    }

    @Test
    public void invalidEmail(){
        User user = new User(0, "foo@.com", "123456789");
        login(user);

        verify(loginView).showErrorInEmail(emailEvaluator.getError());
        verify(loginInteractor, never()).doSignIn(user.getEmail(), user.getPassword());
    }

    @Test
    public void invalidPassword(){
        User user = new User(0, "foo@example.com", "123");
        login(user);

        verify(loginView).showErrorInPassword(passwordEvaluator.getError());
        verify(loginInteractor, never()).doSignIn(user.getEmail(), user.getPassword());
    }

    @Test
    public void invalidEmailAndPassword(){
        User user = new User(0, "foo@.com", "123");
        login(user);

        verify(loginView).showErrorInEmail(emailEvaluator.getError());
        verify(loginView).showErrorInPassword(passwordEvaluator.getError());
        verify(loginInteractor, never()).doSignIn(user.getEmail(), user.getPassword());
    }

    @Test
    public void validLogin(){
        final User user = new User(0, "foo@example.com", "123456789");

        login(user);

        evaluateActionWithValidatedFieldsCorrectly(loginView, new Callback() {
            @Override
            public void execute() {
                verify(loginView).showErrorInEmail(null);
                verify(loginView).showErrorInPassword(null);
                verify(loginInteractor).doSignIn(user.getEmail(), user.getPassword());
            }
        });
    }

    private void login(User user) {
        when(loginView.getEmail()).thenReturn(user.getEmail());
        when(loginView.getPassword()).thenReturn(user.getPassword());

        evaluateFields(loginView, new Callback() {
            @Override
            public void execute() {
                loginPresenter.validateLogin();
            }
        });
    }
}
