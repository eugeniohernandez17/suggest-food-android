package com.eatfood.app.suggestfood.domain;

/**
 * A class that allows to perform hidden operations of its implementation
 * by means of the method {@link #execute()}.
 *
 * Created by Usuario on 06/09/2017.
 */
public interface Callback {

    /**
     * Executes the associated action.
     */
    void execute();

}
