package com.eatfood.app.suggestfood.features.profile.edit;

import com.eatfood.app.suggestfood.entity.AuthorizedUser;
import com.eatfood.app.suggestfood.entity.User;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;

import io.objectbox.relation.ToOne;

import static org.junit.Assert.assertEquals;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyInt;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

/**
 * Created by Usuario on 25/09/2017.
 */

public class FieldsValidation extends ProfileTest {

    private final AuthorizedUser authorizedUser = new AuthorizedUser();

    @Mock
    private ToOne<User> toOneUser;

    private User user;

    @Override
    @Before
    public void setUp() throws Exception {
        super.setUp();

        user = new User();
        authorizedUser.setUser(toOneUser);
        when(toOneUser.getTarget()).thenReturn(user);
        when(profileView.isNetworkConnectivityAvailable()).thenReturn(true);
    }

    @Test
    public void emptyFields(){
        updateProfile("", "");

        verify(profileView).showErrorInName(textEvaluator.getError());
        verify(profileView).showErrorInPhone(textEvaluator.getError());
        verify(userInteractor, never()).updateUser(authorizedUser);
    }

    @Test
    public void emptyName(){
        updateProfile("", "+12025550186");

        verify(profileView).showErrorInName(textEvaluator.getError());
        verify(userInteractor, never()).updateUser(authorizedUser);
    }

    @Test
    public void invalidPhone(){
        updateProfile("User foo", "+02025550186");

        verify(profileView).showErrorInName(null);
        verify(profileView).showErrorInPhone(phoneEvaluator.getError());
        verify(userInteractor, never()).updateUser(authorizedUser);
    }

    @Test
    public void validUpdateProfile(){
        final String name = "User foo";
        final String phone = "+12025550186";

        updateProfile(name, phone);

        assertEquals(name, user.getName());
        assertEquals(phone, user.getPhone());
        verify(profileView).showErrorInPhone(null);
        verify(userInteractor).updateUser(authorizedUser);
    }

    private void updateProfile(String name, String phone){
        when(profileView.getName()).thenReturn(name);
        when(profileView.getPhone()).thenReturn(phone);
        when(sessionInteractor.getSession()).thenReturn(authorizedUser);
        when(context.getString(anyInt())).thenReturn("");
        when(context.getString(anyInt(), any())).thenReturn("");

        profilePresenter.updateProfile();
    }
}
