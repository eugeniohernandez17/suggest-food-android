package com.eatfood.app.suggestfood.features.lost_password;

import com.eatfood.app.suggestfood.domain.event.MessageEvent;
import com.eatfood.app.suggestfood.features.lost_password.presenter.LostPasswordPresenter;

import org.junit.Test;

import static org.mockito.Mockito.verify;

/**
 * Created by Usuario on 14/09/2017.
 */

@SuppressWarnings("ConstantConditions")
public class EventsValidation extends LostPasswordTest {

    @Test
    public void emailNotFound(){
        MessageEvent<LostPasswordEventType> lostPasswordEvent
                = new MessageEvent<>(LostPasswordEventType.ON_LOST_PASSWORD_ERROR,
                "Unable to find user with email 'not.found@example.com'.");

        onMessageEvent(lostPasswordEvent);

        verify(lostPasswordView).displayError(lostPasswordEvent.getMessage());
    }

    @Test
    public void lostPasswordFailedWithExeption(){
        MessageEvent<LostPasswordEventType> lostPasswordEvent
                = new MessageEvent<>(LostPasswordEventType.ON_LOST_PASSWORD_ERROR, new Throwable());

        onMessageEvent(lostPasswordEvent);

        verify(lostPasswordView)
                .displayError(lostPasswordEvent.getError(),
                        LostPasswordPresenter.LOST_PASSWORD_REQUEST_RESOLVED);
    }

    @Test
    public void lostPasswordSuccess(){
        MessageEvent<LostPasswordEventType> lostPasswordEvent
                = new MessageEvent<>(LostPasswordEventType.ON_LOST_PASSWORD_SUCCESS,
                "An email has been sent to 'foo@example.com' containing instructions for resetting your password.");

        onMessageEvent(lostPasswordEvent);

        verify(lostPasswordView).displayMessage(lostPasswordEvent.getMessage());
    }

    private void onMessageEvent(MessageEvent<LostPasswordEventType> lostPasswordEvent){
        lostPasswordPresenter.onMessageEvent(lostPasswordEvent);

        verify(lostPasswordView).enableInputs();
        verify(lostPasswordView).hideProgress();
    }

}
