package com.eatfood.app.suggestfood.service.sync;

import com.eatfood.app.suggestfood.UnitTest;
import com.eatfood.app.suggestfood.features.feedback.model.FeedbackInteractor;
import com.eatfood.app.suggestfood.features.session.model.SessionInteractor;
import com.eatfood.app.suggestfood.service.internal.sync.SyncType;
import com.eatfood.app.suggestfood.service.internal.sync.presenter.SyncPresenter;
import com.eatfood.app.suggestfood.service.internal.sync.presenter.SyncPresenterImpl;

import org.junit.Before;
import org.mockito.Mock;

/**
 * Created by Usuario on 04/04/2018.
 */

abstract class SyncServiceTest extends UnitTest {

    @Mock
    SessionInteractor sessionInteractor;

    @Mock
    FeedbackInteractor feedbackInteractor;

    SyncType syncType;

    SyncPresenter syncPresenter;

    @Override
    @Before
    public void setUp() throws Exception {
        super.setUp();

        syncPresenter = new SyncPresenterImpl(sessionInteractor, feedbackInteractor);
    }
}
