package com.eatfood.app.suggestfood.features.dashboard;

import com.eatfood.app.suggestfood.entity.AuthorizedUser;
import com.eatfood.app.suggestfood.features.dashboard.presenter.DashboardPresenter;

import org.junit.Test;

import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

/**
 * Created by Usuario on 23/09/2017.
 */

public class ConnectivityValidation extends DashboardTest {

    @Test
    public void networkConnectivityDisabled(){
        when(sessionInteractor.isValidSession()).thenReturn(true);
        when(sessionInteractor.getSession()).thenReturn(new AuthorizedUser());
        when(dashboardView.isNetworkConnectivityAvailable()).thenReturn(false);

        dashboardPresenter.onCreate();

        verify(dashboardView).onNetworkConnectivityDisabled(
                        DashboardPresenter.GET_USER_INFO_REQUEST_CODE,
                        dashboardPresenter);
    }
}
