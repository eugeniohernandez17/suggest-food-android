package com.eatfood.app.suggestfood.features.profile.show;

import com.eatfood.app.suggestfood.entity.User;

import org.junit.Test;

import static org.mockito.Mockito.never;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

/**
 * Created by Usuario on 18/10/2017.
 */

@SuppressWarnings("ConstantConditions")
public class EditProfileTest extends ProfileTest {

    @Test
    public void editProfile(){
        User user = new User(0, "user@example.com");
        user.setName("User");
        user.setPhone("+12025550186");
        when(profileView.getUserParam()).thenReturn(user);

        profilePresenter.onViewEditProfile();

        verify(profileView).navigateToEditProfileScreen(user);
    }

    @Test
    public void profileShouldNotBeEditedWhenIsNull(){
        when(profileView.getUserParam()).thenReturn(null);

        profilePresenter.onViewEditProfile();

        verify(profileView, never()).navigateToEditProfileScreen(null);
    }

}
