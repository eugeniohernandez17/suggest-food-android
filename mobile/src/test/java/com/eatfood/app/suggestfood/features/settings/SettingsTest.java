package com.eatfood.app.suggestfood.features.settings;

import com.eatfood.app.suggestfood.UnitTest;
import com.eatfood.app.suggestfood.features.accounts.model.AccountsInteractor;
import com.eatfood.app.suggestfood.features.session.model.SessionInteractor;
import com.eatfood.app.suggestfood.features.settings.model.SettingsInteractor;
import com.eatfood.app.suggestfood.features.settings.presenter.fragments.PreferencePresenter;
import com.eatfood.app.suggestfood.features.settings.presenter.fragments.PreferencePresenterImpl;
import com.eatfood.app.suggestfood.features.settings.view.fragments.PreferenceView;

import org.junit.Before;
import org.mockito.Mock;

/**
 * Created by Usuario on 17/04/2018.
 */

abstract class SettingsTest extends UnitTest {

    @Mock
    PreferenceView preferenceView;

    @Mock
    SessionInteractor sessionInteractor;

    @Mock
    SettingsInteractor settingsInteractor;

    @Mock
    AccountsInteractor accountsInteractor;

    PreferencePresenter preferencePresenter;

    @Override
    @Before
    public void setUp() throws Exception {
        super.setUp();

        preferencePresenter = new PreferencePresenterImpl(preferenceView, settingsInteractor,
                sessionInteractor, accountsInteractor);
    }
}
