package com.eatfood.app.suggestfood.domain;

import android.annotation.SuppressLint;

import com.eatfood.app.suggestfood.R;
import com.eatfood.app.suggestfood.domain.evaluator.PasswordEvaluator;

import org.concordion.integration.junit4.ConcordionRunner;
import org.junit.Before;
import org.junit.runner.RunWith;

import static org.mockito.Mockito.when;

@RunWith(ConcordionRunner.class)
public class ValidatePassword extends ValidateTest {

    @Override
    @Before
    @SuppressLint("StringFormatInvalid")
    public void setUp() throws Exception {
        super.setUp();
        this.evaluator = new PasswordEvaluator(context, fieldEvaluate);
        when(context.getString(R.string.general_error_invalid_password_max, 12))
                .thenReturn("This password is too long. Maximum length is 12");
        when(context.getString(R.string.general_error_invalid_password_min, 8))
                .thenReturn("This password is too short. Minimum length is 8");
    }
}
