package com.eatfood.app.suggestfood.features.login;

import org.junit.Test;

import static org.mockito.Mockito.verify;

/**
 * Created by Usuario on 05/09/2017.
 */

public class LoadContacts extends LoginTest {

    @Test
    public void contactsOfUserMustBeLoadedWhileTheInterfaceIsBeingCreated(){
        loginPresenter.onCreate();

        verify(loginView).loadContacts();
    }
}
