package com.eatfood.app.suggestfood;

import android.content.Context;

import com.eatfood.app.suggestfood.domain.evaluate.FieldEvaluate;
import com.eatfood.app.suggestfood.lib.eventbus.EventBus;
import com.google.i18n.phonenumbers.PhoneNumberUtil;

import org.junit.Before;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.util.Locale;

/**
 * To work on unit tests, switch the Test Artifact in the Build Variants view.
 */
public abstract class UnitTest {

    @Mock
    protected Context context;

    @Mock
    protected EventBus eventBus;

    protected FieldEvaluate fieldEvaluate;

    @Before
    public void setUp() throws Exception {
        MockitoAnnotations.initMocks(this);

        fieldEvaluate = new FieldEvaluate(PhoneNumberUtil.getInstance(), Locale.US);
    }
}