package com.eatfood.app.suggestfood.features.lost_password;

import com.eatfood.app.suggestfood.features.FieldsViewTest;
import com.eatfood.app.suggestfood.features.lost_password.model.LostPasswordInteractor;
import com.eatfood.app.suggestfood.features.lost_password.presenter.LostPasswordPresenter;
import com.eatfood.app.suggestfood.features.lost_password.presenter.LostPasswordPresenterImpl;
import com.eatfood.app.suggestfood.features.lost_password.view.LostPasswordView;

import org.junit.Before;
import org.mockito.Mock;

/**
 * Created by Usuario on 14/09/2017.
 */

abstract class LostPasswordTest extends FieldsViewTest {

    @Mock
    LostPasswordView lostPasswordView;

    @Mock
    LostPasswordInteractor lostPasswordInteractor;

    LostPasswordPresenter lostPasswordPresenter;

    @Override
    @Before
    public void setUp() throws Exception {
        super.setUp();

        lostPasswordPresenter = new LostPasswordPresenterImpl(lostPasswordView, eventBus,
                emailEvaluator, lostPasswordInteractor);
    }
}
